Rails.application.routes.draw do

    root    'pub/root#index'

    match   'api/oai/request'   => 'pub/api/oai#index', :format => 'xml', :via => [:get,:post]

    get     'api/info'              => 'pub/api/info#index',        :format => 'json'
    get     'api/info/oaipmh'                => 'pub/api/info#oaipmh',       :format => 'json'
    get     'api/info/oaipmh/year'           => 'pub/api/info#oaipmh_year',  :format => 'json'
    get     'api/info/oaipmh/month/(:month)' => 'pub/api/info#oaipmh_month', :format => 'json'

    get     'api/search/document'   => 'pub/api/search#document',   :format => 'xml'

    get     'api/list/:list_def_code/info'  => 'pub/api/info#list', :format => 'json'

    get     'api/statistics/all/month/:month/(:token)'          => 'pub/api/statistics#all_month',      :format => 'json'
    get     'api/statistics/document/:id/month/:month/(:token)' => 'pub/api/statistics#document_month', :format => 'json'
    get     'api/statistics/person/:id/month/:month/(:token)'   => 'pub/api/statistics#person_month',   :format => 'json'

    get     'api/statistics/all/monthly/(:token)'           => 'pub/api/statistics#all_monthly',      :format => 'json'
    get     'api/statistics/document/:id/monthly/(:token)'  => 'pub/api/statistics#document_monthly', :format => 'json'
    get     'api/statistics/person/:id/monthly/(:token)'    => 'pub/api/statistics#person_monthly',   :format => 'json'

    get     'api/statistics/all/(:token)'           => 'pub/api/statistics#all',      :format => 'json'
    get     'api/statistics/document/:id/(:token)'  => 'pub/api/statistics#document', :format => 'json'
    get     'api/statistics/person/:id/(:token)'    => 'pub/api/statistics#person',   :format => 'json'

    get     'api/count/:id'  => 'pub/api/misc_counts#show', :format => 'json'


    post     'api/post_data'    => 'pub/api/syslink#post_data'
    get      'api/get_data'     => 'pub/api/syslink#get_data'
    get      'api/get_co_data'  => 'pub/api/syslink#get_co_data'
    get      'api/id_search'    => 'pub/api/syslink#id_search'

    scope '(:locale)', constraints: { locale: /(en|ja)/ } do
    scope '(:logined)', constraints: { logined: :user } do

        devise_for :staffs, :skip => [:registrations]

        [{ name: nil,       prefix: 'pub' },
         { name: 'lim',     prefix: 'lim' },
         { name: 'pri',     prefix: 'pri' }].each do |layer|
        scope layer[:name], module: layer[:prefix], controller: 'root', as: layer[:name] || 'pub' do

            get     '/'                 => :index
            get     'p/:page'           => :index
            get     ':id'               => :index,  as: :item,  constraints: { id: /\w*\d+/ }
            get     'p/:page/:id'       => :index,              constraints: { id: /\d+/ }

            get     ':id/thumnail'      => :thumnail,   as: :thumnail, constraints: { id: /\d+/ }
            get     ':id/attach/:name'  => :attach,     as: :attach,   constraints: { id: /\d+/, :name => /[^\.\/][^\/]+/ }
            get     'metadata/:id'      => :index,      as: :metadata, constraints: { id: /\w*\d+/ }

            get     ':id/file'            => 'files#show_first',       as: :first_file,            constraints: { id: /\d+/ }
            get     ':id/file/thumnail'   => 'files#thumnail_first',   as: :first_file_thumnail,   constraints: { id: /\d+/ }

            get     'file/:document_id/:field_def_id/:id'            => 'files#show',       as: :file,           constraints: { document_id: /\d+/, field_def_id: /\d+/, id: /\d+/ }
            get     'file/:document_id/:field_def_id/:id/thumnail'   => 'files#thumnail',   as: :file_thumnail,  constraints: { document_id: /\d+/, field_def_id: /\d+/, id: /\d+/ }

            get     'contact_us'        => 'contact_us#index'
            post    'send_contact_us'   => 'contact_us#send_mail'
            get     'result_contact_us' => 'contact_us#result'

            scope 'page/:static_page_def_code', controller: :static_pages, as: :static_page do
                get     '/(:param1)/(:param2)/(:param3)/(:param4)/(:param5)'     => :index
            end

            scope 'search/', controller: :searches, as: :search do
                post    '/'                     => :search
                get     '/search'               => :search
                get     '/'                     => :index
                get     'p/:page'               => :index
                get     'item/:id'              => :index
                get     'p/:page/item/:id'      => :index
                get     'dl/:download_id'       => :index, :format => 'xml'
            end

            scope 'list/:list_def_code/', controller: :lists, as: :list do
                get     '/'                         => :index
                get     's/:search_query'           => :index, as: :search
                get     'p/:page'                   => :index
                get     'item/:id'                  => :index
                get     'p/:page/item/:id'          => :index
                get     'dl/:download_id'           => :index, :format => 'xml'

                scope ':level1', as: :level1 do
                    get     '/'                     => :level1
                    get     's/:search_query'       => :index, as: :search
                    get     'p/:page'               => :level1
                    get     'item/:id'              => :level1
                    get     'p/:page/item/:id'      => :level1
                    get     'dl/:download_id'       => :level1, :format => 'xml'
                end

                scope ':level1/:level2', as: :level2 do
                    get     '/'                     => :level2
                    get     's/:search_query'       => :index, as: :search
                    get     'p/:page'               => :level2
                    get     'item/:id'              => :level2
                    get     'p/:page/item/:id'      => :level2
                    get     'dl/:download_id'       => :level2, :format => 'xml'
                end

                scope ':level1/:level2/:level3', as: :level3 do
                    get     '/'                     => :level3
                    get     's/:search_query'       => :index, as: :search
                    get     'p/:page'               => :level3
                    get     'item/:id'              => :level3
                    get     'p/:page/item/:id'      => :level3
                    get     'dl/:download_id'       => :level3, :format => 'xml'
                end

                scope ':level1/:level2/:level3/:level4', as: :level4 do
                    get     '/'                     => :level4
                    get     's/:search_query'       => :index, as: :search
                    get     'p/:page'               => :level4
                    get     'item/:id'              => :level4
                    get     'p/:page/item/:id'      => :level4
                    get     'dl/:download_id'       => :level4, :format => 'xml'
                end

                scope ':level1/:level2/:level3/:level4/:level5', as: :level5 do
                    get     '/'                     => :level5
                    get     's/:search_query'       => :index, as: :search
                    get     'p/:page'               => :level5
                    get     'item/:id'              => :level5
                    get     'p/:page/item/:id'      => :level5
                    get     'dl/:download_id'       => :level5, :format => 'xml'
                end
            end

            scope 'journal/:ej_journal_code', controller: :journals, as: :journal do
                get     '/'           => :journal
                get     'all_index'   => :all_index,   as: :all_index
                get     ':page_code'  => :static_page, as: :static_page

                scope ':volume/:issue', as: :voliss do
                    get     '/'                     => :voliss
                    get     'p/:page'               => :voliss
                    get     'dl/:download_id'       => :voliss, :format => 'xml'
                    get     'article/:id'           => :article, as: :article
                    get     'p/:page/article/:id'   => :article
                end
            end

        end
        end


        scope 'staff', as: :staff do
            get     '/'                     => 'pri_documents#index'
            post    '/'                     => 'pri_documents#search'
            get     'page/:page'            => 'pri_documents#index'
            get     'page/:page/item/:id'   => 'pri_documents#index'
            get     'dl/:download_id'       => 'pri_documents#index', :format => 'xml'

            get     'edit_password'         => 'passwords#edit'
            put     'update_password'       => 'passwords#update'

            resources :staffs, except: [:show]

            resources :custom_selects do
                resources :custom_select_values, except: [:index, :show] do
                    post    :search,    on: :collection
                    put     :up,        on: :member
                    put     :down,      on: :member
                end
            end

            get     'coverpage_settings'            => 'coverpage_settings#index'
            get     'coverpage_settings/edit'       => 'coverpage_settings#edit'
            patch   'coverpage_settings/update'     => 'coverpage_settings#update'

            scope 'view_settings', module: :view_settings, as: :view_setting do
                scope 'system', controller: :system, as: :system do
                    get     '/'             => :index
                    get     :edit
                    patch   :update
                    get     :param_form
                    get     :edit_top_page
                    patch   :update_top_page
                    put     :clear_cache
                    get     :edit_detail_view
                    patch   :update_detail_view
                end
                scope 'list', controller: :list, as: :list do
                    get     '/'        => :index
                    get     ':id'      => :show,   as: :show
                    get     ':id/edit' => :edit,   as: :edit
                    patch   ':id'      => :update, as: :update

                    scope ':list_def_id', controller: :list_field, as: :list_field do
                        get     ':id'      => :show,   as: :show
                        get     ':id/edit' => :edit,   as: :edit
                        patch   ':id'      => :update, as: :update
                    end
                end
                scope 'search', controller: :search, as: :search do
                    get     '/'             => :index
                    get     :edit
                    patch   :update
                end
                scope 'ej', controller: :ej, as: :ej do
                    get     '/'        => :index
                    get     ':id'      => :show,   as: :show
                    get     ':id/edit' => :edit,   as: :edit
                    patch   ':id'      => :update, as: :update

                end

                scope 'ej_journal/:id/', controller: :ej_journal, as: :ej_journal do
                    get     '/'      => :show,   as: :show
                    get     'edit'   => :edit,   as: :edit
                    patch   '/'      => :update, as: :update

                    post    'create_attach_file'  => :create_attach_file
                    delete  'destroy_attach_file' => :destroy_attach_file

                    get     'all_journal_index'   => :all_journal_index
                    post    'all_journal_index'   => :gen_all_journal_index
                    delete  'all_journal_index'   => :delete_all_journal_index
                    put     'all_journal_index'   => :update_all_journal_index
                end
            end

            scope 'ej_editors', module: :ej_editors, as: :ej_editors do
                scope 'ej_journal', controller: :ej_journal, as: :ej_journal do
                    get     '/'      => :show,   as: :show
                    get     'edit'   => :edit,   as: :edit
                    patch   '/'      => :update, as: :update

                    post    'create_attach_file'  => :create_attach_file
                    delete  'destroy_attach_file' => :destroy_attach_file
                end
                resources :ej_static_page_defs, except: [:index] do
                    put     :up,            on: :member
                    put     :down,          on: :member
                end
            end


            resources :system_settings, except: [:show, :new, :create, :destroy] do
                put     :maintenance_mode_on,   on: :collection
                put     :maintenance_mode_off,  on: :collection

                put     :pub_reindex,       on: :collection
                put     :pri_reindex,       on: :collection
                put     :clear_cache,       on: :collection
            end

            resources :permalink_fields, except: [:show] do
                put     :up,        on: :member
                put     :down,      on: :member

                get     :edit_default_setting,         on: :collection
                patch   :update_default_setting,       on: :collection
            end

            resources :oaipmh_settings,     except: [:show, :new, :create, :destroy] do
                get     :edit_self_doi,     on: :member
                patch   :self_doi,          on: :member, action: :update_self_doi
            end
            resources :oaipmh_formats,      except: [:show, :index]
            resources :oaipmh_filter_defs,  except: [:index, :show] do
                get     :param_form,    on: :collection
            end

            resources :field_defs, except: [:show] do
                put     :field_order,       on: :collection
                get     :param_form,        on: :collection

                put     :up,        on: :member
                put     :down,      on: :member
            end

            resources :sortable_fields, except: [:index, :show] do
                put     :up,        on: :member
                put     :down,      on: :member
            end

            resources :input_groups do
                put     :up,        on: :member
                put     :down,      on: :member

                resources :input_group_fields, except: [:index, :show] do
                    put     :up,        on: :member
                    put     :down,      on: :member
                end
            end

            resources :download_formats, except: [:show]

            resources :search_defs do
                get     :edit_search_setting,       on: :collection
                patch   :update_search_setting,     on: :collection

                put     :up,        on: :member
                put     :down,      on: :member

                resources :search_field_defs, except: [:index, :show]
            end
            resources :search_filter_defs, except: [:index, :show] do
                get     :param_form,    on: :collection
            end
            resources :search_result_fields, except: [:index] do
                put     :up,            on: :member
                put     :down,          on: :member
            end

            resources :list_defs do
                resources :list_field_defs, except: [:index] do
                    put     :up,            on: :member
                    put     :down,          on: :member
                end
                resources :list_filter_defs, except: [:index, :show] do
                    get     :param_form,    on: :collection
                end
                resources :list_result_fields, except: [:index] do
                    put     :up,            on: :member
                    put     :down,          on: :member
                end
            end

            resources :ej_defs do
                resources :ej_journal_defs, except: [:index] do
                    resources :ej_static_page_defs, except: [:index] do
                        put     :up,            on: :member
                        put     :down,          on: :member
                    end
                end

                resources :ej_filter_defs, except: [:index, :show] do
                    get     :param_form,    on: :collection
                end
                resources :ej_result_fields, except: [:index] do
                    put     :up,            on: :member
                    put     :down,          on: :member
                end
                resources :ej_detail_fields, except: [:index] do
                    put     :up,            on: :member
                    put     :down,          on: :member
                end
            end

            resources :left_menu_defs, except: [:show] do
                get     :param_form,    on: :collection
                put     :up,            on: :member
                put     :down,          on: :member
                put     :left,          on: :member
                put     :right,         on: :member
            end

            resources :static_page_defs
            resources :contact_us_settings, except: [:show]
            resources :html_parts_defs,     except: [:show]

            resources :authers_mails, only: [:index, :show] do
                post    :send_mail,            on: :collection
            end

            scope 'static_files' do
                scope 'public', controller: :static_public_files, as: :static_public_files do
                    get     '/'             => :index
                    post    '/'             => :create
                    delete  '/'             => :destroy
                    post    '/directory'    => :create_directory
                    delete  '/directory'    => :destroy_directory
                    put     '/directory'    => :rename_directory

                    get     'd/:dir/'           => :index
                    post    'd/:dir/'           => :create
                    delete  'd/:dir/'           => :destroy
                    post    'd/:dir/directory'  => :create_directory
                    delete  'd/:dir/directory'  => :destroy_directory
                    put     'd/:dir/directory'  => :rename_directory
                end

                scope 'private', controller: :static_private_files, as: :static_private_files do
                    get     '/'             => :index
                    post    '/'             => :create
                    delete  '/'             => :destroy
                    post    '/directory'    => :create_directory
                    delete  '/directory'    => :destroy_directory
                    put     '/directory'    => :rename_directory

                    get     'f/:file'       => :download, as: :donwload_current

                    get     'd/:dir/'           => :index
                    post    'd/:dir/'           => :create
                    delete  'd/:dir/'           => :destroy
                    post    'd/:dir/directory'  => :create_directory
                    delete  'd/:dir/directory'  => :destroy_directory
                    put     'd/:dir/directory'  => :rename_directory

                    get     'f/:dir/:file'      => :download, as: :donwload
                end
            end

            scope 'batch', controller: :batch, as: :batch do
                get     '/'             => :index
                post    '/'             => :search
                post    'new_value'     => :new_value
                post    'confirm'       => :confirm
                post    'start_batch'   => :start_batch
                put     'pri_reindex'   => :pri_reindex
            end

            scope 'field_value_update', controller: :field_value_update, as: :field_value_update do
                get     '/'             => :index
                post    '/'             => :search
                post    'new_value'     => :new_value
                post    'confirm'       => :confirm
                post    'start_batch'   => :start_batch
                put     'pri_reindex'   => :pri_reindex
            end

            scope 'self_doi', controller: :self_doi, as: :self_doi do
                get     '/'             => :index
                post    '/check'        => :check
            end

            scope 'publish', controller: :publish, as: :publish do
                get     '/'             => :index
                get     'p/:page'       => :index
                put     'publish'       => :publish

                get     'update_all_data' => :update_all_data
                put     'reset_publish'   => :reset_publish

                post    'search'            => :search
                get     'searched'          => :searched
                get     'searched/p/:page'  => :searched
            end

            scope 'csv', controller: :csv, as: :csv do
                get     '/'                 => :index
                post    'update'            => :update
                get     'result'            => :result
                post    'test'              => :test

                post    'update_tsv'        => :update_tsv
                post    'test_tsv'          => :test_tsv

                post    'update_xml'        => :update_xml
                post    'test_xml'          => :test_xml


                get     'generate_updated'  => :generate_updated
                get     'generate_created'  => :generate_created
                get     'generate_all'      => :generate_all

                get     'generate_updated_tsv'  => :generate_updated_tsv
                get     'generate_created_tsv'  => :generate_created_tsv
                get     'generate_all_tsv'      => :generate_all_tsv

                get     'download_updated'  => :download_updated
                get     'download_created'  => :download_created
                get     'download_all'      => :download_all

                get     'download_updated_tsv'  => :download_updated_tsv
                get     'download_created_tsv'  => :download_created_tsv
                get     'download_all_tsv'      => :download_all_tsv

            end

            resources :pri_documents, except: [:index], path: 'documents' do
                put     :change_thumnail,   on: :member
                put     :destroy_thumnail,  on: :member
                put     :create_coverpage,  on: :member
                put     :destroy_coverpage, on: :member

                put     :undelete, on: :member

                post    :create_attach_file,   on: :member, path: :attach_file
                delete  :destroy_attach_file,  on: :member, path: :attach_file
            end

            resources :document_templates

            resources :people_settings, except: [:index, :show, :new, :create, :destroy]
            resources :people do
                post    :search,        on: :collection
                get     :download,      on: :collection
                post    :test,          on: :collection
                post    :upload,        on: :collection
            end

            resources :access_count_settings, except: [:show, :new, :create, :destroy]
            scope 'access_counts', controller: :access_counts, as: :access_counts do
                get     '/'                 => :index
                get     ':month/'           => :month
                get     ':month/download'   => :month_download
                get     'person/:month/'            => :person_month
                get     'person/:month/download'    => :person_month_download
            end

        end
        end
    end
end
