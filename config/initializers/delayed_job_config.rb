Delayed::Worker.sleep_delay = 10
Delayed::Worker.max_attempts = 2
Delayed::Worker.logger = Logger.new(File.join(Rails.root, 'log', 'delayed_job_worker.log'), 'daily')
Delayed::Worker.max_run_time = 12.hours

