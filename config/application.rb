require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Earmas

    SORT_MAX = "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"

    class Application < Rails::Application
        # Settings in config/environments/* take precedence over those specified here.
        # Application configuration should go into files in config/initializers
        # -- all .rb files in that directory are automatically loaded.

        # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
        # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
        # config.time_zone = 'Central Time (US & Canada)'

        # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
        # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]

        I18n.enforce_available_locales = true
        config.i18n.default_locale = :ja

        config.autoload_paths += %W(#{config.root}/lib/extras)
        config.eager_load_paths += %W(#{config.root}/vendor/custom)
        config.field_type_models = %W(SimpleFile SimplePerson SimpleSelect SimpleText SimpleTextarea Nav RepositoryPerson HtmlTextarea MarkdownTextarea SimpleDate UrlGenerate CodeText RepositoryTitle DoubleLangText GrantId SystemValue)
        config.theme_models = %W(Theme1 Theme2)
    end

    def self.int32_to_s(int_value)
        sprintf('%032d', int_value)
    end
end
