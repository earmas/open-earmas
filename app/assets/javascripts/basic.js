$(function(){

    $('.load-content').each(function() {
        var url = $(this).attr('data-url');
        $(this).load(url);
    });

    $('form.static-post-form').each(function() {
        var csrf_token = $('meta[name=csrf-token]').attr('content');
        $(this).find('[name=authenticity_token]').val(csrf_token);
    });
});

