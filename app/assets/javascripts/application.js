// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require bootstrap-sprockets
//= require_tree .

/*
 * form data
 */

function to_64_int(string){
    return ("00000000000000000000" + string).slice(-20);
}

function replace_name_id(elem, id){
    elem.attr('name', elem.attr('name').replace('[0]', '[new_' + id + ']'));
    elem.attr('id', elem.attr('id').replace('_0_', '_new_' + id + '_'));
};
function replace_label_for_id(elem, id){
    if(elem.attr('for') == undefined){ return; }
    elem.attr('for', elem.attr('for').replace('_0_', '_new_' + id + '_'));
};

function replace_form_field_name_id(elem, id){
    elem.find('label').each(function(){
        replace_label_for_id($(this), id);
    });
    elem.find('input').each(function(){
        replace_name_id($(this), id);
    });
    elem.find('textarea').each(function(){
        replace_name_id($(this), id);
    });
    elem.find('select').each(function(){
        replace_name_id($(this), id);
    });
};
function reset_form_data(elem){

    elem.find('input[type=checkbox]:checked').each(function(){
        $(this).val('');
    });
    elem.find('input[type=radio][checked]').each(function(){
        $(this).prop('checked', true);
    });
    elem.find('input[type!=radio][type!=hidden][type!=checkbox]').each(function(){
        $(this).val('');
    });
    elem.find('textarea').each(function(){
        $(this).val('');
        var org_row = $(this).attr('data-default_row');
        text_area_size($(this), org_row);
    });
    elem.find('select').each(function(){
        $('option[selected]', $(this)).each(function(){
            $(this).prop('selected', true);
        });
        $('option:not([selected])', $(this)).each(function(){
            $(this).prop('selected', false);
        });

    });
}



$(function(){

    $(document).on('click', '.selectable-container:not(.selected) .show-btn', function(event){
        $(this).closest('.selectable-container').addClass('selected');
    });
    $(document).on('click', '.selectable-container.selected .close-btn', function(){
        $(this).closest('.selectable-container').removeClass('selected');
    });

    $(document).on('click', '.selectable-container .cancel-btn', function(){
        $(this).closest('.selectable-container').removeClass('selected');
        reset_form_data($(this).closest('form'));
    });

    $(document).on('click', '.fields_container .field_add_button', function(event){
        var fields_container = $(this).closest('.fields_container');
        add_field(fields_container);
    });

    $(document).on('click', '.fields_container .fields .field_del_button', function(event){
        var field = $(this).closest('.field');
        var fields = field.closest('.fields');

        delete_field(fields, field);
    });

    $(document).on('click', '.fields_container .fields .field_clear_button', function(event){
        var field = $(this).closest('.field');
        var fields = field.closest('.fields');
        var fields_container = field.closest('.fields_container');

        if (fields_container.hasClass('has_file_field')){
            delete_field(fields, field);
            add_field(fields_container);
        }
        else{
            reset_form_data(field);
        }
    });

    function add_field(fields_container){
        var fields = fields_container.find('.fields').first();
        var tmpl = fields_container.find('.tmpl .field').first();
        var new_field = tmpl.clone();

        var next_field_index = fields_container.find('.tmpl').first().attr("next-field-index");

        replace_form_field_name_id(new_field, next_field_index);

        fields_container.find('.tmpl').first().attr("next-field-index", ++next_field_index);

        fields.append(new_field);
    }

    function delete_field(fields, field){
        if(field.attr('data-id') == 0){
            field.remove();
        }
        else{
            field.addClass('deleted');
            field.find('.deleted_flg').val("true");
            fields.prepend(field);
        }
    }
});

/*
 * loading
 */

var loading_timer = null;
function show_loading(){
    if (loading_timer) {
        clearTimeout(loading_timer);
    }

    loading_timer = setTimeout(function() {
        loading_timer = null;
        $('#loading').addClass('show');
    }, 500);
}
function hide_loading(){
    if (loading_timer) {
        clearTimeout(loading_timer);
    }
    $('#loading').removeClass('show');
}

/*
 * error
 */

$(function(){
    $(document).on('click', '.closable-box .close-button', function(event){
        var closable_box = $(this).closest('.closable-box');
        closable_box.removeClass('show');
    });
});


var clear_closable_box_timer = null;

function clear_closable_box(){
    if (clear_closable_box_timer) {
        clearTimeout(clear_closable_box_timer);
    }

    clear_closable_box_timer = setTimeout(function() {
        clear_closable_box_timer = null;
        $('.closable-box').removeClass('show');
    }, 1500);
}
clear_closable_box();

function show_error(message){
    $('.closable-box.std-error .message').empty();
    $('.closable-box.std-error .message').append(message);
    $('.closable-box.std-error').addClass('show');
    clear_closable_box();
}
function show_ajax_error(){
    $('.closable-box.ajax-error').addClass('show');
    clear_closable_box();
}

/*
 * post
 */

function loading_post(url, method, post_data, beforeSend_func, success_func, error_func, complate_func){
    var csrf_token = $('meta[name=csrf-token]').attr('content');
    var csrf_param = $('meta[name=csrf-param]').attr('content');

    post_data['_method'] = method;
    post_data[csrf_param] = csrf_token;

    $.ajax({
        type : 'POST',
        url : url,
        headers : { "cache-control": "no-cache" },
        dataType : 'json',
        data : post_data,
        beforeSend : function(){
            show_loading();
            if(beforeSend_func != undefined) beforeSend_func();
        },
        success : success_func,
        error : function(xhr){
            show_ajax_error();
            if(error_func != undefined) error_func();
        },
        complete : function(xhr){
            hide_loading();
            if(complate_func != undefined) complate_func();
        }
    });
}


/*
 * get
 */

function loading_get(url, post_data, beforeSend_func, success_func, error_func, complate_func){
    var csrf_token = $('meta[name=csrf-token]').attr('content');
    var csrf_param = $('meta[name=csrf-param]').attr('content');

    $.ajax({
        url : url,
        headers : { "cache-control": "no-cache" },
        dataType : 'json',
        data : post_data,
        beforeSend : function(){
            show_loading();
            if(beforeSend_func != undefined) beforeSend_func();
        },
        success : success_func,
        error : function(xhr){
            show_ajax_error();
            if(error_func != undefined) error_func();
        },
        complete : function(xhr){
            hide_loading();
            if(complate_func != undefined) complate_func();
        }
    });
}

function delete_data(elem, success_func){
    var url = elem.attr('data-url');

    var csrf_token = $('meta[name=csrf-token]').attr('content');
    var csrf_param = $('meta[name=csrf-param]').attr('content');

    var post_data = { _method : 'delete' };
    post_data[csrf_param] = csrf_token;

    $.ajax({
        type : 'POST',
        url : url,
        headers : { "cache-control": "no-cache" },
        dataType : 'json',
        data : post_data,
        beforeSend : function(){
            show_loading();
        },
        success : success_func,
        error : function(xhr){
            show_ajax_error();
        },
        complete : function(xhr){
            hide_loading();
        }
    });
}


$(function(){

    input_group_tag_init();
    touch_overflow();

    $(document).on('click', '#input-group-tab a', function(event){
        var input_group_id = $(this).attr('data-input-group-id');
        var target = $(this).attr('href');

        if (target != '#main') return;

        var form = $(target).find('form').first();

        if(input_group_id == 'all'){
            $('.field_defs > .field_def').show();
            $('.input_group_id').val(0);

            order_input_group($('.field_defs > .field_def').get(), 'all');
            set_place_holder_input_group($('.field_defs > .field_def'), 'all');
            set_field_caption_input_group($('.field_defs > .field_def'), 'all');
            set_must_input_group($('.field_defs > .field_def'), 'all');
        }
        else{
            $('.field_defs > .field_def.input-group-' + input_group_id + '').show();
            $('.field_defs > .field_def:not(.input-group-' + input_group_id + ')').hide();
            $('.input_group_id').val(input_group_id);

            order_input_group($('.field_defs > .field_def').get(), input_group_id);
            set_place_holder_input_group($('.field_defs > .field_def'), input_group_id);
            set_field_caption_input_group($('.field_defs > .field_def'), input_group_id);
            set_must_input_group($('.field_defs > .field_def'), input_group_id);
        }
    });

    $(document).on('click', '.field .search-person', function(event){
        var url = $(this).attr('url');
        var field = $(this).closest('.field');

        var id = field.find('.id').val();
        var family_name_ja = field.find('.family-name-ja').first().val();
        var given_name_ja = field.find('.given-name-ja').first().val();
        var family_name_en = field.find('.family-name-en').first().val();
        var given_name_en = field.find('.given-name-en').first().val();
        var family_name_tra = field.find('.family-name-tra').first().val();
        var given_name_tra = field.find('.given-name-tra').first().val();

        var post_data = {};
        post_data['id'] = id;
        post_data['family_name_ja'] = family_name_ja;
        post_data['given_name_ja'] = given_name_ja;
        post_data['family_name_en'] = family_name_en;
        post_data['given_name_en'] = given_name_en;
        post_data['family_name_tra'] = family_name_tra;
        post_data['given_name_tra'] = given_name_tra;


        var success_func = function(xhr){
            if(xhr.result == 'success'){
                field.find('.search-result').empty();
                field.find('.search-result').append(xhr.html);
            }
            else{
                show_error(xhr.message);
            }
        };


        loading_post(url, 'post', post_data, undefined, success_func, undefined, undefined)
    });

    $(document).on('click', '.field .pick-search-person-result', function(event){
        var field = $(this).closest('.field');
        var person = $(this).closest('.person');

        var result_id = person.find('.search-result-id').first().text();
        var result_family_name_ja = person.find('.search-result-family-name-ja').first().text();
        var result_given_name_ja = person.find('.search-result-given-name-ja').first().text();
        var result_family_name_en = person.find('.search-result-family-name-en').first().text();
        var result_given_name_en = person.find('.search-result-given-name-en').first().text();
        var result_family_name_tra = person.find('.search-result-family-name-tra').first().text();
        var result_given_name_tra = person.find('.search-result-given-name-tra').first().text();

        var result_affiliation_ja = person.find('.search-result-affiliation-ja').first().text();
        var result_affiliation_en = person.find('.search-result-affiliation-en').first().text();


        field.find('.id').first().val(result_id);
        field.find('.family-name-ja').first().val(result_family_name_ja);
        field.find('.given-name-ja').first().val(result_given_name_ja);
        field.find('.family-name-en').first().val(result_family_name_en);
        field.find('.given-name-en').first().val(result_given_name_en);
        field.find('.family-name-tra').first().val(result_family_name_tra);
        field.find('.given-name-tra').first().val(result_given_name_tra);
        field.find('.affiliation-ja').first().val(result_affiliation_ja);
        field.find('.affiliation-en').first().val(result_affiliation_en);

        var field = $(this).closest('.field');
        field.find('.search-result').empty();
    });

    $(document).on('click', '.field .search-select-value', function(event){
        var url = $(this).attr('url');
        var field = $(this).closest('.field');

        var value = field.find('.value').val();
        var caption_ja = field.find('.caption-ja').first().val();
        var caption_en = field.find('.caption-en').first().val();

        var post_data = {};
        post_data['value'] = value;
        post_data['caption_ja'] = caption_ja;
        post_data['caption_en'] = caption_en;


        var success_func = function(xhr){
            if(xhr.result == 'success'){
                field.find('.search-result').empty();
                field.find('.search-result').append(xhr.html);
            }
            else{
                show_error(xhr.message);
            }
        };


        loading_post(url, 'post', post_data, undefined, success_func, undefined, undefined)
    });

    $(document).on('click', '.field .pick-search-select-value-result', function(event){
        var field = $(this).closest('.field');
        var person = $(this).closest('.select-value');

        var result_value = person.find('.search-result-value').first().text();
        var result_caption_ja = person.find('.search-result-caption-ja').first().text();
        var result_caption_en = person.find('.search-result-caption-en').first().text();

        field.find('.value').first().val(result_value);
        field.find('.caption-ja').first().val(result_caption_ja);
        field.find('.caption-en').first().val(result_caption_en);

        var field = $(this).closest('.field');
        field.find('.search-result').empty();
    });

    $(document).on('click', '.field .close-search-result', function(event){
        var field = $(this).closest('.field');
        field.find('.search-result').empty();
    });


    /*
     * toggle container
     */

    $(document).on('click', '.toggle-container .toggle-show-button', function(event){
        event.preventDefault();
        var toggle_container = $(this).closest('.toggle-container');
        toggle_container.addClass('show');
    })
    $(document).on('click', '.toggle-container .toggle-hide-button', function(event){
        event.preventDefault();
        var toggle_container = $(this).closest('.toggle-container');
        toggle_container.removeClass('show');
    })

    /*
     * upload document attach file
     */


    $(document).on('click', '.append-document-attach-file-button', function(event){
        var attach_file_container = $(this).closest('.document-attach-files');
        var attach_files = attach_file_container.find('.attach-files').first();

        var form = $(this).closest('form');

        form.submit();
        $('#file-upload-frame').off('load');
        $("#file-upload-frame").on('load', function(){
            var body = $('body', $(this).contents());
            var result = $('.result', body);

            if(result.text() == 'success'){
                var new_attach_files = $('.attach-files', body);
                attach_files.replaceWith(new_attach_files);
            }
            else{
                var message = $('.message', body);
                show_error(message.text());
            }
        });
        return false;
    });


    $(document).on('click', '.delete-document-attach-file-button', function(event){
        event.preventDefault();
        var button = $(this);
        var attach_file = button.closest('.attach-file');

        delete_data(button, function(xhr){
            if(xhr.result == 'success'){
                attach_file.remove();
            }
            else{
                show_error(xhr.message);
            }
        });
    });
});

function input_group_tag_init(){
    var input_group_id = $('#input-group-tab .active a').attr('data-input-group-id');

    var target = $('#input-group-tab .active a').attr('href');

    if (target != '#main') return;

    var form = $(target).find('form').first();

    if(input_group_id == 'all'){
        $('.field_defs > .field_def').show();
        $('.input_group_id').val(0);
        order_input_group($('.field_defs > .field_def').get(), 'all');
        set_place_holder_input_group($('.field_defs > .field_def'), 'all');
        set_field_caption_input_group($('.field_defs > .field_def'), 'all');
        set_must_input_group($('.field_defs > .field_def'), 'all');
    }
    else{
        $('.field_defs > .field_def.input-group-' + input_group_id + '').show();
        $('.field_defs > .field_def:not(.input-group-' + input_group_id + ')').hide();
        $('.input_group_id').val(input_group_id);
        order_input_group($('.field_defs > .field_def').get(), input_group_id);
        set_place_holder_input_group($('.field_defs > .field_def'), input_group_id);
        set_field_caption_input_group($('.field_defs > .field_def'), input_group_id);
        set_must_input_group($('.field_defs > .field_def'), input_group_id);
    }
}

function order_input_group(field_defs, input_group_id){
    field_defs.sort(function(a, b){
        var attr_name = 'data-input-group-' + input_group_id + '-sort-index';
        var attr_all_name = 'data-field-def-id';

        if(to_64_int($(a).attr(attr_name)) > to_64_int($(b).attr(attr_name))) return 1;
        if(to_64_int($(a).attr(attr_name)) < to_64_int($(b).attr(attr_name))) return -1;

        if(to_64_int($(a).attr(attr_all_name)) > to_64_int($(b).attr(attr_all_name))) return -1;
        if(to_64_int($(a).attr(attr_all_name)) < to_64_int($(b).attr(attr_all_name))) return 1;

        return 0;
    });


    for (var i = 0; i < field_defs.length; i++) {
        $('.field_defs').append(field_defs[i]);
    }
}

function set_place_holder_input_group(field_defs, input_group_id){
    field_defs.each(function(){
        var placeholder = $(this).attr('data-input-group-' + input_group_id + '-placeholder');

        placeholders = placeholder.split(',');

        $(this).find('.fields > .field, .tmpl > .field').each(function(){
            $(this).find("input:text, textarea, option:first-child").each(function(index){
                this_placeholder = placeholders[index]
                if(this_placeholder == undefined){
                    this_placeholder = ""
                }
                if($(this)[0].tagName == 'OPTION'){
                    if(this_placeholder == ''){
                        $(this).html('選択してください');
                    }
                    else{
                        $(this).html(this_placeholder);
                    }
                }
                else{
                    $(this).attr('placeholder', this_placeholder);
                }
            });
        });
    });
}
function set_field_caption_input_group(field_defs, input_group_id){
    field_defs.each(function(){
        var field_caption = $(this).attr('data-input-group-' + input_group_id + '-field-caption');

        $(this).find('.fields > .field, .tmpl > .field').each(function(){
            $(this).find("> label").each(function(index){
                $(this).html(field_caption);
            });
        });
    });
}
function set_must_input_group(field_defs, input_group_id){
    field_defs.each(function(){
        var must = $(this).attr('data-input-group-' + input_group_id + '-must');

        $(this).find('.fields > .field, .tmpl > .field').each(function(){
            $(this).find("> label").each(function(index){
                if(must == 'true'){
                    $(this).addClass('must-field');
                }
                else{
                    $(this).removeClass('must-field');
                }
            });
        });
    });
}

function isTouch(){
  return (document.ontouchstart !== undefined);
}
function touch_overflow(){
    if(isTouch()){ return; }

    var target = $('.touch-overflow-panel');
    target.mousedown(function (event) {
        if(isTouch()){ return; }
        if($(event.target).hasClass('no-touch-overflow-panel')){ return; }
        if(event.target.tagName == 'SELECT'){ return; }

        $(this)
            .data('down', true)
            .data('x', event.clientX)
            .data('y', event.clientY)
            .data('scrollLeft', this.scrollLeft);
            return false;
    });

    $(document).mousemove(function (event) {
        if(isTouch()){ return; }

        if (target.data('down') == true) {
            target.scrollLeft(target.data('scrollLeft') + target.data('x') - event.clientX);
            return false;
        }
    }).mouseup(function (event) {
        target.data('down', false);
    });
}
