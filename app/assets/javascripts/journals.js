$(function(){

    $('#all-journal-index .voliss ul').sortable({
        cursor: "move",
        distance: 15,
        tolerance: "pointer",
        opacity: 0.5,
    });


    $(document).on('click', '.update-all-journal-index', function(event){
        var url = $(this).attr('data-url');
        var all_journal_index = $('#all-journal-index');

        var result_data = {}
        all_journal_index.find('.voliss').each(function(){
            var voliss = $(this);
            var voliss_index = voliss.attr('data-voliss-index');
            result_data[voliss_index] = []

            voliss.find('.article').each(function(){
                var article = $(this);
                var article_value = {}
                if(article.attr('data-type') == 1){
                    article_value['type'] = 1;
                    article_value['id'] = article.attr('data-id');
                }
                else{
                    article_value['type'] = 2;

                    article_value['section'] = {'ja': article.find('.section .ja').first().html(), 'en': article.find('.section .en').first().html()};
                    article_value['start_page'] = {'ja': article.find('.start-page .ja').first().html(), 'en': article.find('.start-page .en').first().html()};
                    article_value['end_page'] = {'ja': article.find('.end-page .ja').first().html(), 'en': article.find('.end-page .en').first().html()};

                    article_value['fields'] = []
                    article.find('.field').each(function(){
                        var field = $(this);
                        article_value['fields'].push({'ja': field.find('.ja').first().html(), 'en': field.find('.en').first().html()});
                    });
                }
                result_data[voliss_index].push(article_value);
            });

        });

        $('.json-string-field-value').val(JSON.stringify(result_data));
    });

    $(document).on('click', '.article-form .add-article', function(event){
        var button = $(this);
        var form = button.closest('.article-form');
        var voliss = button.closest('.voliss');
        var articles = voliss.find('ul').first();


        var new_article = $('.tmpl-container .article').clone();

        form.find('.misc-field').each(function(){
            var misc_field = $(this);

            add_field(misc_field, "misc", new_article, "field");
        });

        add_field(form, "section", new_article, "section");
        add_field(form, "start-page", new_article, "start-page");
        add_field(form, "end-page", new_article, "end-page");

        if(form.data('article') == undefined){
            articles.prepend(new_article);
        }
        else{
            form.data('article').replaceWith(new_article);
        }

        voliss.removeClass('selected');

    });

    $(document).on('click', '.voliss .show-btn', function(event){
        var button = $(this);
        var voliss = button.closest('.voliss');
        var form = voliss.find('.article-form').first();
        form.data('article', undefined);
        reset_form_data(form);
    });

    $(document).on('click', '.edit-article-btn', function(event){
        var button = $(this);
        var article = button.closest('.article');
        var voliss = button.closest('.voliss');
        var form = voliss.find('.article-form').first();

        article.find('.field').each(function(index){
            var field = $(this);

            set_form_field(field, form.find('.misc-field').slice(index), "misc");
        });

        set_form_field(article.find('.section').first(), form, "section");
        set_form_field(article.find('.start-page').first(), form, "start-page");
        set_form_field(article.find('.end-page').first(), form, "end-page");

        voliss.addClass('selected');
        form.data('article', article);
    });

    $(document).on('click', '.delete-article-btn', function(event){
        var button = $(this);
        var article = button.closest('.article');
        article.remove();
    });

    $(document).on('click', 'form#browse .browse-btn', function(event){
        window.location.replace($("form#browse #url").val());
    });

});

function set_form_field(elem, form, new_elem_name){
    if(form == undefined){ return; }
    form.find('.' + new_elem_name + '-field-ja').val(elem.find('.ja').first().html());
    form.find('.' + new_elem_name + '-field-en').val(elem.find('.en').first().html());
}

function add_field(elem, field_name, parent_obj, new_elem_name){
    var new_field = $('<div class="' + new_elem_name + '"></div>');
    var new_field_ja = $('<span class="ja"></span>');
    new_field_ja.html(elem.find('.' + field_name + '-field-ja').first().val());
    var new_field_en = $('<span class="en"></span>');
    new_field_en.html(elem.find('.' + field_name + '-field-en').first().val());

    new_field.append(new_field_ja);
    new_field.append(' / ');
    new_field.append(new_field_en);
    parent_obj.append(new_field);
}


