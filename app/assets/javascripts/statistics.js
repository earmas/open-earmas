$(function(){

    function set_result_misc_count(container, label, document_id, counts){
        var item = container.find('.view-result-item[document-id=' + document_id + ']');

        var misc_count_container = $('<div class="misc_counts"></div>');
        item.append(misc_count_container);

        $.each(label, function(index, value) {
            if(counts[index + '_url'] == ''){
                misc_count = $('<span class="' + index + '"></span>');
                misc_count_container.append(misc_count);
                misc_count.append($('<span class="prefix"></span>').append(value['pre']));
                misc_count.append($('<span class="count"></span>').append(counts[index]));
                misc_count.append($('<span class="suffix"></span>').append(value['suf']));
            }
            else{
                misc_count_link = $('<a href="' + counts[index + '_url'] + '"></a>');
                misc_count = $('<span class="' + index + '"></span>');
                misc_count_link.append(misc_count);
                misc_count_container.append(misc_count_link);
                misc_count.append($('<span class="prefix"></span>').append(value['pre']));
                misc_count.append($('<span class="count"></span>').append(counts[index]));
                misc_count.append($('<span class="suffix"></span>').append(value['suf']));
            }
        });
    }

    $('.load-result-misc-count').each(function() {
        var url = $(this).attr('data-url');
        var container = $('.view-result-items');

        $.getJSON(url, function(data){
            $.each(data.data, function(index, value) {
                set_result_misc_count(container, data.label, index, value)
            });
        });
    });

    function set_detail_misc_count(label, document_id, counts){
        var item = $('.load-detail-misc-count');

        var misc_count_container = $('<div class="misc_counts"></div>');
        item.append(misc_count_container);

        $.each(label, function(index, value) {
            if(counts[index + '_url'] == ''){
                misc_count = $('<span class="' + index + '"></span>');
                misc_count_container.append(misc_count);
                misc_count.append($('<span class="prefix"></span>').append(value['pre']));
                misc_count.append($('<span class="count"></span>').append(counts[index]));
                misc_count.append($('<span class="suffix"></span>').append(value['suf']));
            }
            else{
                misc_count_link = $('<a href="' + counts[index + '_url'] + '"></a>');
                misc_count = $('<span class="' + index + '"></span>');
                misc_count_link.append(misc_count);
                misc_count_container.append(misc_count_link);
                misc_count.append($('<span class="prefix"></span>').append(value['pre']));
                misc_count.append($('<span class="count"></span>').append(counts[index]));
                misc_count.append($('<span class="suffix"></span>').append(value['suf']));
            }
        });
    }

    $('.load-detail-misc-count').each(function() {
        var url = $(this).attr('data-url');

        $.getJSON(url, function(data){
            $.each(data.data, function(index, value) {
                set_detail_misc_count(data.label, index, value)
            });
        });
    });


    function set_rank(container, data, target){
        if(data.length > 0){
            new_ul = container.find('.load-statictics-rank').first().clone();
            new_ul.removeClass('hidden_container');
            li_tmpl = new_ul.find('li').first().clone();
            new_ul.find('li').remove();

            $.each(data, function(index, value) {
                new_li = li_tmpl.clone();
                var a = new_li.find('a');
                if(a.hasClass('ja')){
                    a.attr('href', value.url).html(value.title_ja);
                }
                else{
                    a.attr('href', value.url).html(value.title_en);
                }
                new_li.find('.count').html(value.count);
                new_ul.append(new_li);
            });

            container.find(target).html(new_ul);
        }
    }

    $('.load-statictics').each(function() {
        var url = $(this).attr('data-url');
        var container = $(this);

        $.getJSON(url, function(data){
            container.find('.total-count').html(data.total.count)
            container.find('.access-count').html(data.access.count)
            container.find('.download-count').html(data.download.count)

            if(data.docuument != undefined){
                if(data.docuument.total != undefined){
                    set_rank(container, data.docuument.total, '.total-rank');
                }
                if(data.docuument.access != undefined){
                    set_rank(container, data.docuument.access, '.access-rank');
                }
                if(data.docuument.download != undefined){
                    set_rank(container, data.docuument.download, '.download-rank');
                }
            }
        });
    });

    $('.load-info').each(function() {
        var url = $(this).attr('data-url');
        var container = $(this);

        $.getJSON(url, function(data){
            container.find('.total-count').html(data.total.count)
        });
    });

});

