#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module ViewStateHelper


    def view_state_login_name
        staff_signed_in? ? 'user_' : ''
    end

    def view_state_cache_name
        if ViewState.pub?
            view_state_login_name + 'pub'
        elsif ViewState.lim?
            view_state_login_name + 'lim'
        else
            view_state_login_name + 'pri'
        end
    end

    def static_file_path(file)
        "public/files/static/" + file
    end

    def static_file_url_path(file)
        root_path(locale: nil, logined: nil) + Rails.application.config.public_static_file_url_path + file
    end

    def public_dir_file_to_url_path(file_path)
        root_path(locale: nil, logined: nil) + file_path.sub(Rails.application.config.public_file_store_dir, Rails.application.config.public_file_url_path) 
    end

    def public_dir_file_to_url_path_url(file_path)
        root_url(locale: nil, logined: nil) + file_path.sub(Rails.application.config.public_file_store_dir, Rails.application.config.public_file_url_path) 
    end

    def view_state_document_thumnail_path_gen(document)
        if ViewState.pub?
            public_dir_file_to_url_path(document.thumnail_path)
        elsif ViewState.lim?
            public_dir_file_to_url_path(document.thumnail_path)
        else
            view_state_path_gen(:thumnail_path).call(document.id)
        end
    end

    def view_state_document_attach_path_gen(document)
        if ViewState.pub?
            public_dir_file_to_url_path(document.document_attach_file_dir)
        elsif ViewState.lim?
            public_dir_file_to_url_path(document.document_attach_file_dir)
        else
            pri_item_path(document.id) + '/attach'
        end
    end


    def view_state_path_gen(path_name, prefix = nil)
        prefix = prefix.to_s + '_' if prefix != nil

        if ViewState.pub?
            self.method(prefix.to_s + 'pub_' + path_name.to_s)
        elsif ViewState.lim?
            self.method(prefix.to_s + 'lim_' + path_name.to_s)
        else
            self.method(prefix.to_s + 'pri_' + path_name.to_s)
        end
    end

    def view_state_class_name_gen(class_name)
        if ViewState.pri?
            ('Pri' + class_name.to_s).constantize
        else
            ('Pub' + class_name.to_s).constantize
        end
    end

    def view_state_relation(class_name)
        if ViewState.pub?
            view_state_class_name_gen(class_name).where(limited: false)
        else
            view_state_class_name_gen(class_name)
        end
    end

    def view_state_word_relation(class_name)
        if ViewState.pub?
            view_state_class_name_gen(class_name).where(limited: false)
        elsif ViewState.lim?
            view_state_class_name_gen(class_name).where(limited: true)
        else
            view_state_class_name_gen(class_name)
        end
    end

    def view_state_list_word_relation(level)
        if ViewState.pub?
            ('PubListLevel' + level.to_s + 'Word').constantize.where(limited: false)
        elsif ViewState.lim?
            ('PubListLevel' + level.to_s + 'Word').constantize.where(limited: true)
        else
            ('PriListLevel' + level.to_s + 'Word').constantize
        end
    end

    def view_state_search(search)
        if ViewState.pub?
            search.with :limited, false
        end
    end

    def view_state_visible(data)
        case data.visible
        when PriDocument::PRIVATE_TYPE
            ViewState.pri?
        when PriDocument::LIMITED_TYPE
            ViewState.pri? || ViewState.lim?
        when PriDocument::PUBLIC_TYPE
            true
        end
    end

end

