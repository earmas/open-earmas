#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module SearchHelper

    def show_search_term
        @search_term_exists ? ' show' : nil
    end

    def saerch_breadcrumb
        is_detail_page = @document.present?

        content_tag :ol, class: 'breadcrumb' do
            if is_detail_page
                concat(content_tag(:li){
                    link_to t('view.search_result'), view_state_path_gen(:search_path).call(params.delete_if{|key,value| key == 'id'})
                })
                concat(content_tag :li, get_document_title_string(@document, '詳細').strip, class: 'active')
            else
                concat(content_tag :li, t('view.search'), class: 'active')
            end
        end
    end
end
