#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module LocaleHelper

    def locale_name
        if I18n.locale == :ja
            'ja'
        else
            'en'
        end
    end

    def show_locale_value(caption)
        return nil if caption.blank?
        return caption[:ja] if I18n.locale == :ja || caption[:en].blank?
        return caption[:en]
    end

    def show_locale_hash(caption)
        return nil if caption.blank?
        return caption['ja'] if I18n.locale == :ja || caption['en'].blank?
        return caption['en']
    end

    def show_caption(caption_holder)
        return nil if caption_holder.blank?
        return caption_holder.caption_ja if I18n.locale == :ja || caption_holder.caption_en.blank?
        return caption_holder.caption_en
    end

    def show_field_def_caption(document, field_def)
        input_group_field = document.get_input_group_field(field_def.id)

        show_caption_input_group_field_or_field_def(input_group_field, field_def)
    end

    def show_caption_input_group_field_or_field_def(input_group_field, field_def)
        if input_group_field.blank?
            show_caption(field_def)
        else
            input_group_field_caption = show_caption(input_group_field)
            if input_group_field_caption.blank?
                show_caption(field_def)
            else
                input_group_field_caption
            end
        end
    end

    def send_local_name(obj, name)
        obj.send(set_locale_suffix(name))
    end

    def set_locale_suffix(name)
        return name.to_s + "_ja" if I18n.locale == :ja
        return name.to_s + "_en"
    end

    def to_sort_number(string)
        return Earmas::SORT_MAX if string.blank?
        string.slice(0..30).codepoints.map{|a|format("%04x", a)}.join()
    end

    def to_full_sort_number(string)
        return Earmas::SORT_MAX if string.blank?
        to_sort_number(string).ljust(64, '0')
    end

end


