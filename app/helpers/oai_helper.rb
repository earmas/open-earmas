#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module OaiHelper

    def show_oai_identifier(document)
        return nil if @oaipmh_setting.identifier_prefix.blank?
        @oaipmh_setting.identifier_prefix + @system_setting.document_id_to_public_id(document.id)
    end

    def show_utc_date(date_time)
        return nil if date_time.blank?
        date_time.in_time_zone('UTC').strftime('%F')
    end

    def show_oai_date(date_time)
        return nil if date_time.blank?
        date_time.in_time_zone('UTC').strftime('%FT%TZ')
    end

    def show_utc_publish_date(document)
        return nil if document.try(:max_publish_at).blank?
        return document.max_publish_at.in_time_zone('UTC').strftime('%F')
    end

    def show_oai_publish_date(document)
        return nil if document.try(:max_publish_at).blank?
        return document.max_publish_at.in_time_zone('UTC').strftime('%FT%TZ')
    end

    def show_value_blank(value)
        value.blank? ? "" : value
    end
end
