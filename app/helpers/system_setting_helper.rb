#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module SystemSettingHelper

    def check_not_mentenance_mode
        return show_locked if @system_setting.mentenance_mode
    end
    def check_mentenance_mode
        return show_invalid if @system_setting.mentenance_mode == false
    end

    def set_nonmaintenance_data_lock
        return show_locked if SystemSetting.where(data_lock: false, mentenance_mode: false).update_all(data_lock: true) != 1
    end

    def set_maintenance_data_lock
        return show_locked if SystemSetting.where(data_lock: false, mentenance_mode: true).update_all(data_lock: true) != 1
    end

    def unset_data_lock
        SystemSetting.update_all(data_lock: false)
    end

    def set_maintenance_mode
        SystemSetting.where(data_lock: false, mentenance_mode: false).update_all(mentenance_mode: true) == 1
    end

    def unset_maintenance_mode
        SystemSetting.where(field_locked_at: nil, list_locked_at: nil, ej_locked_at: nil, search_locked_at: nil, oaipmh_locked_at: nil, pri_field_locked_at: nil, pri_list_locked_at: nil, pri_ej_locked_at: nil, pri_search_locked_at: nil, pri_oaipmh_locked_at: nil).update_all(mentenance_mode: false)
    end

    def check_any_lock
        @system_setting.field_locked_at.present? || @system_setting.pri_field_locked_at.present? || 
        @system_setting.list_locked_at.present? || @system_setting.pri_list_locked_at.present? || 
        @system_setting.ej_locked_at.present? || @system_setting.pri_ej_locked_at.present? || 
        @system_setting.search_locked_at.present? || @system_setting.pri_search_locked_at.present? || 
        @system_setting.oaipmh_locked_at.present? || @system_setting.pri_oaipmh_locked_at.present?
    end

    # pub

    def pub_locking?
        @system_setting.pub_locking?
    end

    def lock_sort
        if SystemSetting.where(mentenance_mode: true).update_all(field_locked_at: Time.zone.now) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end
    def unlock_sort
        if SystemSetting.where(mentenance_mode: true).update_all(field_locked_at: nil) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end
    def lock_list(list_def_id)
        if SystemSetting.where(mentenance_mode: true).update_all(['list_locked_at = ?, locked_list_ids = locked_list_ids || ?', Time.zone.now, list_def_id]) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end
    def lock_lists(list_def_ids)
        if SystemSetting.where(mentenance_mode: true).update_all(['list_locked_at = ?, locked_list_ids = locked_list_ids || ARRAY[?]', Time.zone.now, list_def_ids]) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end
    def unlock_list
        if SystemSetting.where(mentenance_mode: true).update_all(['list_locked_at = ?, locked_list_ids = \'{}\'', nil]) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end

    def lock_ej(ej_def_id)
        if SystemSetting.where(mentenance_mode: true).update_all(['ej_locked_at = ?, locked_ej_ids = locked_ej_ids || ?', Time.zone.now, ej_def_id]) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end
    def lock_ejs(ej_def_ids)
        if SystemSetting.where(mentenance_mode: true).update_all(['ej_locked_at = ?, locked_ej_ids = locked_ej_ids || ARRAY[?]', Time.zone.now, ej_def_ids]) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end
    def unlock_ej
        if SystemSetting.where(mentenance_mode: true).update_all(['ej_locked_at = ?, locked_ej_ids = \'{}\'', nil]) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end

    def lock_search
        if SystemSetting.where(mentenance_mode: true).update_all(search_locked_at: Time.zone.now) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end
    def unlock_search
        if SystemSetting.where(mentenance_mode: true).update_all(search_locked_at: nil) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end

    def lock_oaipmh
        return if @system_setting.enable_oaipmh == false
        if SystemSetting.where(mentenance_mode: true).update_all(oaipmh_locked_at: Time.zone.now) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end
    def unlock_oaipmh
        if SystemSetting.where(mentenance_mode: true).update_all(oaipmh_locked_at: nil) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end

    # pri lock

    def pri_locking?
        @system_setting.pri_locking?
    end

    def lock_pri_sort
        if SystemSetting.where(mentenance_mode: true).update_all(pri_field_locked_at: Time.zone.now) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end
    def unlock_pri_sort
        if SystemSetting.where(mentenance_mode: true).update_all(pri_field_locked_at: nil) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end

    def lock_pri_list(list_def_id)
        if SystemSetting.where(mentenance_mode: true).update_all(['pri_list_locked_at = ?, locked_pri_list_ids = locked_pri_list_ids || ?', Time.zone.now, list_def_id]) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end
    def lock_pri_lists(list_def_ids)
        if SystemSetting.where(mentenance_mode: true).update_all(['pri_list_locked_at = ?, locked_pri_list_ids = locked_pri_list_ids || ARRAY[?]', Time.zone.now, list_def_ids]) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end
    def unlock_pri_list
        if SystemSetting.where(mentenance_mode: true).update_all(['pri_list_locked_at = ?, locked_pri_list_ids = \'{}\'', nil]) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end

    def lock_pri_ej(ej_def_id)
        if SystemSetting.where(mentenance_mode: true).update_all(['pri_ej_locked_at = ?, locked_pri_ej_ids = locked_pri_ej_ids || ?', Time.zone.now, ej_def_id]) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end
    def lock_pri_ejs(ej_def_ids)
        if SystemSetting.where(mentenance_mode: true).update_all(['pri_ej_locked_at = ?, locked_pri_ej_ids = locked_pri_ej_ids || ARRAY[?]', Time.zone.now, ej_def_ids]) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end
    def unlock_pri_ej
        if SystemSetting.where(mentenance_mode: true).update_all(['pri_ej_locked_at = ?, locked_pri_ej_ids = \'{}\'', nil]) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end

    def lock_pri_search
        if SystemSetting.where(mentenance_mode: true).update_all(pri_search_locked_at: Time.zone.now) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end
    def unlock_pri_search
        if SystemSetting.where(mentenance_mode: true).update_all(pri_search_locked_at: nil) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end

    def lock_pri_oaipmh
        return if @system_setting.enable_oaipmh == false
        if SystemSetting.where(mentenance_mode: true).update_all(pri_oaipmh_locked_at: Time.zone.now) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end
    def unlock_pri_oaipmh
        if SystemSetting.where(mentenance_mode: true).update_all(pri_oaipmh_locked_at: nil) != 1
            raise DataCheckException, "メンテナンスモードではありません。"
        end
    end

end
