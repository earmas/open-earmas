#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module DocumentsHelper

    def get_document_url(document)
        view_state_path_gen(:item_url).call(id: @system_setting.document_id_to_public_id(document.id), locale: nil)
    end

    def get_public_document_url(document)
        get_public_document_id_url(document.id)
    end

    def get_public_document_id_url(document_id)
        if ViewState.pri?
            view_state_path_gen(:item_url).call(id: @system_setting.document_id_to_public_id(document_id), locale: nil)
        else
            view_state_path_gen(:item_url).call(id: @system_setting.document_id_to_public_id(document_id), locale: nil, logined: nil)
        end
    end

    def get_perm_document_url(document)
        get_perm_document_id_url(document.id)
    end

    def get_perm_document_id_url(document_id)
        pub_item_url(id: @system_setting.document_id_to_public_id(document_id), locale: nil, logined: nil)
    end

    def document_field_by_num(document, field_defs, num)
        field_def = field_defs[num]

        if field_def.present? && document.get_fields(field_def.id).present?
            field_def
        else
            nil
        end
    end
    def render_document_field_caption_by_num(document, field_defs, num, else_value)
        field_def = document_field_by_num(document, field_defs, num)

        if field_def.present?
            show_field_def_caption(document, field_def)
        else
            else_value
        end
    end
    def render_document_field_line_by_num(document, field_defs, num, else_value)
        field_def = document_field_by_num(document, field_defs, num)

        if field_def.present?
            render_view_state_line_field(field_def, document)
        else
            else_value
        end
    end

    def render_document_field_block_by_num(document, field_defs, num, else_value)
        field_def = document_field_by_num(document, field_defs, num)

        if field_def.present?
            render_view_state_block_field(field_def, document)
        else
            else_value
        end
    end

    def render_document_title(document, else_value)
        title_field_def = FieldDefLoader.title_field_def_by_input_group_field(document.input_group_id)

        if title_field_def.present? && document.get_fields(title_field_def.id).present?
            render_view_state_line_field(title_field_def, document)
        else
            else_value
        end
    end

    def set_page_title_by_document
        return if @document == nil
        title_string = get_document_title_string(@document, nil)
        return if title_string == nil
        @page_title = title_string + ' - ' + @page_title
    end

    def get_document_title_string(document, else_value)
        title_field_def = FieldDefLoader.title_field_def_by_input_group_field(document.input_group_id)

        if title_field_def.present? && document.get_fields(title_field_def.id).present?
            get_view_state_string(title_field_def, document)
        else
            else_value
        end
    end

    def get_document_title_strings(document, else_value)
        title_field_def = FieldDefLoader.title_field_def_by_input_group_field(document.input_group_id)

        if title_field_def.present? && document.get_fields(title_field_def.id).present?
            fields = document.get_fields(title_field_def.id)

            title_ja = fields.map do |field|
                field.get_list_caption_with_unit_ja
            end.join(' ')

            title_en = fields.map do |field|
                field.get_list_caption_with_unit_en
            end.join(' ')

            {ja: title_ja, en: title_en}
        else
            {ja: else_value, en: else_value}
        end
    end

    def render_document_title_for_csv_check(document, fields_by_field_def_id_map, else_value)
        title_field_def = FieldDefLoader.title_field_def_by_input_group_field(document.input_group_id)

        if title_field_def.present? && fields_by_field_def_id_map[title_field_def.id].present?
            render_view_state_check(title_field_def, document, fields_by_field_def_id_map, 'line')
        else
            else_value
        end
    end

    def show_delete_flg_in_form(field)
        if field.deleted == 'true'
            'deleted'
        else
            ''
        end
    end

private

    def get_view_state_string(field_def, document)
        fields = document.get_fields(field_def.id)

        fields.map do |field|
            field.send(set_locale_suffix('get_list_caption_with_unit'))
        end.join(' ')
    end

end
