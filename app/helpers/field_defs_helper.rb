#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FieldDefsHelper

    def show_field_def(field_def_id)
        show_caption FieldDefLoader.get_field_def(field_def_id)
    end

    def show_field_type_select_option
        options = []
        FieldDef.get_field_type_map.values.each do |field_type|
            options << [show_caption(field_type), field_type::TYPE_ID]
        end
        options
    end

    def show_input_field_def_class(field_def)
        return " has_file_field" if field_def.field_type_class.file_holder?
    end

    def render_field_def_param(field_def)
        render partial: "fields/" + field_def.get_pri_view_dir + "/field_param",
            locals: {field_def: field_def}
    end

    def render_pri_form_field(f, field_def, field)
        render partial: "fields/" + field_def.get_pri_view_dir + "/form",
            locals: { f: f, field_def: field_def, field: field }
    end

    def render_pri_form_tmpl_field_def(f, field_def, field)
        render partial: "fields/" + field_def.get_pri_view_dir + "/form_tmpl",
            locals: { f: f, field_def: field_def, field: field }
    end

    def render_search_field_def(field_def, field)
        render partial: "fields/" + field_def.get_pri_view_dir + "/search",
            locals: { field_def: field_def, field: field }
    end

    def render_value_count_field_def(field_def, count_values)
        render partial: "fields/" + field_def.get_pri_view_dir + "/value_count",
            locals: { field_def: field_def, count_values: count_values }
    end



    def render_view_state_line_field(field_def, document)
        render_view_state(field_def, document, 'line')
    end

    def render_view_state_block_field(field_def, document)
        render_view_state(field_def, document, 'block')
    end

    def render_ej_view_state_line_field(field_def, document)
        render_ej_view_state(field_def, document, 'line')
    end

    def render_ej_view_state_block_field(field_def, document)
        render_ej_view_state(field_def, document, 'block')
    end

    def render_view_state_xml_field(field_def, document, xml)
        fields = document.get_fields(field_def.id)
        return if fields.blank? && field_def.field_type_class.must_show_field?(field_def, document) == false

        xml << (render partial: "fields/" + field_def.get_type_class_by_view_state.get_view_dir + "/xml",
            locals: { document: document, field_def: field_def, fields: fields, xml: xml })
    end

    def render_view_state_check(field_def, document, fields_by_field_def_id_map, view_name)
        fields = fields_by_field_def_id_map[field_def.id]
        return if fields.blank?
        render partial: File.join("fields", field_def.get_type_class_by_view_state.get_view_dir, view_name),
            locals: { document: document, field_def: field_def, fields: fields }
    end

    def get_ej_render_field(ej_field_def, document)
        if I18n.locale == :ja
            field_def = ej_field_def[:ja]
            fields = document.get_fields(field_def.id) if field_def.present?

            if fields.blank? && ej_field_def[:en_instead_ja]
                field_def = ej_field_def[:en]
                return nil if field_def.blank?

                fields = document.get_fields(field_def.id)
            end
        else
            field_def = ej_field_def[:en]
            fields = document.get_fields(field_def.id) if field_def.present?

            if fields.blank? && ej_field_def[:ja_instead_en]
                field_def = ej_field_def[:ja]
                return nil if field_def.blank?
                fields = document.get_fields(field_def.id)
            end
        end

        return nil if field_def.blank?
        return nil if fields.blank? && field_def.field_type_class.must_show_field?(field_def, document) == false

        {field_def: field_def, fields: fields}
    end

private

    def render_view_state(field_def, document, view_name)
        fields = document.get_fields(field_def.id)
        return if fields.blank? && field_def.field_type_class.must_show_field?(field_def, document) == false
        render partial: File.join("fields", field_def.get_type_class_by_view_state.get_view_dir, view_name),
            locals: { document: document, field_def: field_def, fields: fields }
    end

    def render_ej_view_state(ej_field_def, document, view_name)
        render_field = get_ej_render_field(ej_field_def, document)
        return if render_field == nil

        render partial: File.join("fields", render_field[:field_def].get_type_class_by_view_state.get_view_dir, view_name),
            locals: { document: document, field_def: render_field[:field_def], fields: render_field[:fields] }
    end

end
