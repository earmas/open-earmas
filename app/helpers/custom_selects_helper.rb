#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module CustomSelectsHelper

    def get_custom_select_caption(datum)
        custom_select_value = datum.get_master_value
        if custom_select_value.blank?
            show_locale_value(ja: '不正な値', en: 'invalid value')
        else
            show_locale_value(ja: custom_select_value.caption_ja, en: custom_select_value.caption_en)
        end
    end

    def get_custom_select_for_xml(datum)
        custom_select_value = datum.get_master_value
        if custom_select_value.blank?
            {value: '', caption_ja: '', caption_en: ''}
        else
            {value: custom_select_value.value, caption_ja: custom_select_value.caption_ja, caption_en: custom_select_value.caption_en}
        end
    end

end
