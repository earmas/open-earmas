#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module ListFiltersHelper

    def show_filter_type_value(filter)
        case filter.filter_type
        when DocumentFilterDef::FILTER_TYPES[:field_value_filter][:id]
            show_field_def(filter.some_id)
        when DocumentFilterDef::FILTER_TYPES[:field_key_filter][:id]
            show_field_def(filter.some_id)
        when DocumentFilterDef::FILTER_TYPES[:input_group_filter][:id]
            show_input_group(filter.some_id)
        else
            return false
        end
    end

    def show_list_filter_type_select_option
        options = []
        DocumentFilterDef::FILTER_TYPES.each do |k, v|
            options << [show_locale_value(v[:caption]), v[:id]]
        end
        options
    end

    def render_list_filter_param(list_filter_def)
        render partial: "list_filter_defs/filter_param", locals: { list_filter_def: list_filter_def }
    end

    def show_search_filter_type_select_option
        options = []
        DocumentFilterDef::FILTER_TYPES.each do |k, v|
            options << [show_locale_value(v[:caption]), v[:id]]
        end
        options
    end

    def render_search_filter_param(search_filter_def)
        render partial: "search_filter_defs/filter_param", locals: { search_filter_def: search_filter_def }
    end

    def show_ej_filter_type_select_option
        options = []
        DocumentFilterDef::FILTER_TYPES.each do |k, v|
            options << [show_locale_value(v[:caption]), v[:id]]
        end
        options
    end

    def render_ej_filter_param(ej_filter_def)
        render partial: "ej_filter_defs/filter_param", locals: { ej_filter_def: ej_filter_def }
    end

    def show_oaipmh_filter_type_select_option
        options = []
        DocumentFilterDef::FILTER_TYPES.each do |k, v|
            options << [show_locale_value(v[:caption]), v[:id]]
        end
        options
    end

    def render_oaipmh_filter_param(oaipmh_filter_def)
        render partial: "oaipmh_filter_defs/filter_param", locals: { oaipmh_filter_def: oaipmh_filter_def }
    end
end
