#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module InputGroupsHelper

    def show_input_group(input_group_id)
        FieldDefLoader.get_input_group(input_group_id).try(:name)
    end

    def show_input_group_class(field_def)
        set_input_group_map
        return if @input_group_field_order_by_field_def_id_map[field_def.id] == nil

        classes = []
        @input_group_field_order_by_field_def_id_map[field_def.id].keys.each do |input_group_id|
            classes << ' input-group-' + input_group_id.to_s
        end

        return classes.join()
    end

    def show_input_group_order_attr(field_def)
        set_input_group_map

        order_attrs = []

        FieldDefLoader.input_groups.each do |input_group|
            input_group_field_index = @input_group_field_order_by_field_def_id_map[field_def.id] && @input_group_field_order_by_field_def_id_map[field_def.id][input_group.id]

            if input_group_field_index == nil
                order_attrs << ' data-input-group-' + input_group.id.to_s + '-sort-index=9999'
            else
                order_attrs << ' data-input-group-' + input_group.id.to_s + '-sort-index=' + input_group_field_index.to_s
            end
        end
        return order_attrs.join()
    end

    def show_input_group_field_caption(field_def)
        order_attrs = []

        FieldDefLoader.input_groups.each do |input_group|
            input_group_field = FieldDefLoader.input_group_field_by_field_def_id(input_group.id, field_def.id)

            if input_group_field == nil
                order_attrs << ' data-input-group-' + input_group.id.to_s + '-field-caption'
            else
                order_attrs << ' data-input-group-' + input_group.id.to_s + '-field-caption=' + show_caption_input_group_field_or_field_def(input_group_field, field_def)
            end
        end
        return order_attrs.join()
    end

    def show_input_group_placeholder(field_def)
        order_attrs = []

        FieldDefLoader.input_groups.each do |input_group|
            input_group_field = FieldDefLoader.input_group_field_by_field_def_id(input_group.id, field_def.id)

            if input_group_field == nil || input_group_field.description.blank?
                order_attrs << ' data-input-group-' + input_group.id.to_s + '-placeholder'
            else
                order_attrs << ' data-input-group-' + input_group.id.to_s + '-placeholder=' + input_group_field.description
            end
        end
        return order_attrs.join()
    end

    def show_input_group_must(field_def)
        order_attrs = []

        FieldDefLoader.input_groups.each do |input_group|
            input_group_field = FieldDefLoader.input_group_field_by_field_def_id(input_group.id, field_def.id)

            if input_group_field == nil || input_group_field.must == false
                order_attrs << ' data-input-group-' + input_group.id.to_s + '-must=false'
            else
                order_attrs << ' data-input-group-' + input_group.id.to_s + '-must=true'
            end
        end
        return order_attrs.join()
    end

private

    def set_input_group_map
        return if @input_group_field_order_by_field_def_id_map != nil

        @input_group_field_order_by_field_def_id_map = {}
        FieldDefLoader.input_groups.each do |input_group|
            input_group.fields.each_with_index do |field, index|
                @input_group_field_order_by_field_def_id_map[field.field_def_id] ||= {}
                @input_group_field_order_by_field_def_id_map[field.field_def_id][input_group.id] = index + 1
            end
        end
    end

end
