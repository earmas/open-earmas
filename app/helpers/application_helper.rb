#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module ApplicationHelper

    def show_system_title
        return 'Earmas' if @system_setting.blank?

        if I18n.locale == :ja
            @system_setting.sytem_name_ja
        else
            @system_setting.sytem_name_en
        end
    end

    def active?(value1, value2)
        case value2.class.to_s
        when Array.to_s
            'active' if value2.include?(value1)
        else
            'active' if value1 == value2
        end
    end

    def path_active?(path)
        request.original_fullpath == path ? "active" : ""
    end

    def path_start_with_active?(path)
        original_fullpath = request.original_fullpath.split('?')[0]
        (original_fullpath == path || original_fullpath.start_with?(path + '/')) ? "active" : ""
    end

    def fields_index_start
        @next_new_field_index = 0
    end

    def field_index(id)
        if id == nil
            @next_new_field_index += 1
            'new_' + @next_new_field_index.to_s
        else
            id.to_s
        end
    end

    def next_field_index
        @next_new_field_index += 1
        @next_new_field_index.to_s
    end

    def show_theme_select_option
        options = []
        ThemeHolder.get_theme_map.values.each do |theme|
            options << [show_caption(theme), theme::THEME_ID]
        end
        options
    end


    def show_list_param_view_type_option
        options = []
        ViewType::ListParam::VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_result_view_type_option
        options = []
        ViewType::Result::VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_detail_view_type_option
        options = []
        ViewType::Detail::VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_journal_view_type_option
        options = []
        ViewType::Journal::VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_article_view_type_option
        options = []
        ViewType::Article::VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_journal_all_index_view_type_option
        options = []
        ViewType::JournalAllIndex::VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_journal_static_view_type_option
        options = []
        ViewType::JournalStatic::VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end


    def show_list_param_sub_header_view_type_option
        options = []
        ViewType::ListParam::SUB_HEADER_VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_result_sub_header_view_type_option
        options = []
        ViewType::Result::SUB_HEADER_VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_detail_sub_header_view_type_option
        options = []
        ViewType::Detail::SUB_HEADER_VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_journal_sub_header_view_type_option
        options = []
        ViewType::Journal::SUB_HEADER_VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_article_sub_header_view_type_option
        options = []
        ViewType::Article::SUB_HEADER_VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_journal_static_sub_header_view_type_option
        options = []
        ViewType::JournalStatic::SUB_HEADER_VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_journal_all_index_sub_header_view_type_option
        options = []
        ViewType::JournalAllIndex::SUB_HEADER_VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end


    def show_list_param_sub_footer_view_type_option
        options = []
        ViewType::ListParam::SUB_FOOTER_VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_result_sub_footer_view_type_option
        options = []
        ViewType::Result::SUB_FOOTER_VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_detail_sub_footer_view_type_option
        options = []
        ViewType::Detail::SUB_FOOTER_VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_journal_sub_footer_view_type_option
        options = []
        ViewType::Journal::SUB_FOOTER_VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_article_sub_footer_view_type_option
        options = []
        ViewType::Article::SUB_FOOTER_VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_journal_all_index_sub_footer_view_type_option
        options = []
        ViewType::JournalAllIndex::SUB_FOOTER_VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end
    def show_journal_static_sub_footer_view_type_option
        options = []
        ViewType::JournalStatic::SUB_FOOTER_VIEW_MAP.values.each do |view_type|
            options << [show_locale_value(view_type[:caption]), view_type[:view_id]]
        end
        options
    end


    def show_list_param_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::ListParam::VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::ListParam::VIEW_MAP[view_type_id][:caption])
    end
    def show_result_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::Result::VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::Result::VIEW_MAP[view_type_id][:caption])
    end
    def show_detail_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::Detail::VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::Detail::VIEW_MAP[view_type_id][:caption])
    end
    def show_journal_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::Journal::VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::Journal::VIEW_MAP[view_type_id][:caption])
    end
    def show_article_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::Article::VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::Article::VIEW_MAP[view_type_id][:caption])
    end
    def show_journal_all_index_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::JournalAllIndex::VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::JournalAllIndex::VIEW_MAP[view_type_id][:caption])
    end
    def show_journal_static_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::JournalStatic::VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::JournalStatic::VIEW_MAP[view_type_id][:caption])
    end


    def show_list_param_sub_header_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::ListParam::SUB_HEADER_VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::ListParam::SUB_HEADER_VIEW_MAP[view_type_id][:caption])
    end
    def show_result_sub_header_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::Result::SUB_HEADER_VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::Result::SUB_HEADER_VIEW_MAP[view_type_id][:caption])
    end
    def show_detail_sub_header_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::Detail::SUB_HEADER_VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::Detail::SUB_HEADER_VIEW_MAP[view_type_id][:caption])
    end
    def show_journal_sub_header_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::Journal::SUB_HEADER_VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::Journal::SUB_HEADER_VIEW_MAP[view_type_id][:caption])
    end
    def show_article_sub_header_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::Article::SUB_HEADER_VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::Article::SUB_HEADER_VIEW_MAP[view_type_id][:caption])
    end
    def show_journal_all_index_sub_header_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::JournalAllIndex::SUB_HEADER_VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::JournalAllIndex::SUB_HEADER_VIEW_MAP[view_type_id][:caption])
    end
    def show_journal_static_sub_header_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::JournalStatic::SUB_HEADER_VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::JournalStatic::SUB_HEADER_VIEW_MAP[view_type_id][:caption])
    end


    def show_list_param_sub_footer_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::ListParam::SUB_FOOTER_VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::ListParam::SUB_FOOTER_VIEW_MAP[view_type_id][:caption])
    end
    def show_result_sub_footer_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::Result::SUB_FOOTER_VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::Result::SUB_FOOTER_VIEW_MAP[view_type_id][:caption])
    end
    def show_detail_sub_footer_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::Detail::SUB_FOOTER_VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::Detail::SUB_FOOTER_VIEW_MAP[view_type_id][:caption])
    end
    def show_journal_sub_footer_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::Journal::SUB_FOOTER_VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::Journal::SUB_FOOTER_VIEW_MAP[view_type_id][:caption])
    end
    def show_article_sub_footer_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::Article::SUB_FOOTER_VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::Article::SUB_FOOTER_VIEW_MAP[view_type_id][:caption])
    end
    def show_journal_all_index_sub_footer_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::JournalAllIndex::SUB_FOOTER_VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::JournalAllIndex::SUB_FOOTER_VIEW_MAP[view_type_id][:caption])
    end
    def show_journal_static_sub_footer_view_type(view_type_id)
        return 'なし' if view_type_id.blank? || ViewType::JournalStatic::SUB_FOOTER_VIEW_MAP[view_type_id].blank?
        show_locale_value(ViewType::JournalStatic::SUB_FOOTER_VIEW_MAP[view_type_id][:caption])
    end

    def render_list_param_by_view_type
        render partial: File.join('view_types', 'list_param', @page_obj.list_param_view_type_hash[:file_name])
    end
    def render_result_by_view_type
        render partial: File.join('view_types', 'result', @page_obj.result_view_type_hash[:file_name])
    end
    def render_detail_by_view_type
        render partial: File.join('view_types', 'detail', @page_obj.detail_view_type_hash[:file_name])
    end
    def render_journal_by_view_type
        render partial: File.join('view_types', 'journal', @page_obj.journal_view_type_hash[:file_name])
    end
    def render_article_by_view_type
        render partial: File.join('view_types', 'article', @page_obj.article_view_type_hash[:file_name])
    end
    def render_journal_all_index_by_view_type
        render partial: File.join('view_types', 'journal_all_index', @page_obj.journal_all_index_view_type_hash[:file_name])
    end
    def render_journal_static_by_view_type
        render partial: File.join('view_types', 'journal_static', @page_obj.journal_static_view_type_hash[:file_name])
    end


    def render_list_param_by_sub_header_view_type
        render partial: File.join('view_types', 'list_param_sub_header', @page_obj.list_param_sub_header_view_type_hash[:file_name])
    end
    def render_result_by_sub_header_view_type
        render partial: File.join('view_types', 'result_sub_header', @page_obj.result_sub_header_view_type_hash[:file_name])
    end
    def render_detail_by_sub_header_view_type
        render partial: File.join('view_types', 'detail_sub_header', @page_obj.detail_sub_header_view_type_hash[:file_name])
    end
    def render_journal_by_sub_header_view_type
        render partial: File.join('view_types', 'journal_sub_header', @page_obj.journal_sub_header_view_type_hash[:file_name])
    end
    def render_article_by_sub_header_view_type
        render partial: File.join('view_types', 'article_sub_header', @page_obj.article_sub_header_view_type_hash[:file_name])
    end
    def render_journal_all_index_by_sub_header_view_type
        render partial: File.join('view_types', 'journal_all_index_sub_header', @page_obj.journal_all_index_sub_header_view_type_hash[:file_name])
    end
    def render_journal_static_by_sub_header_view_type
        render partial: File.join('view_types', 'journal_static_sub_header', @page_obj.journal_static_sub_header_view_type_hash[:file_name])
    end


    def render_list_param_by_sub_footer_view_type
        render partial: File.join('view_types', 'list_param_sub_footer', @page_obj.list_param_sub_footer_view_type_hash[:file_name])
    end
    def render_result_by_sub_footer_view_type
        render partial: File.join('view_types', 'result_sub_footer', @page_obj.result_sub_footer_view_type_hash[:file_name])
    end
    def render_detail_by_sub_footer_view_type
        render partial: File.join('view_types', 'detail_sub_footer', @page_obj.detail_sub_footer_view_type_hash[:file_name])
    end
    def render_journal_by_sub_footer_view_type
        render partial: File.join('view_types', 'journal_sub_footer', @page_obj.journal_sub_footer_view_type_hash[:file_name])
    end
    def render_article_by_sub_footer_view_type
        render partial: File.join('view_types', 'article_sub_footer', @page_obj.article_sub_footer_view_type_hash[:file_name])
    end
    def render_journal_all_index_by_sub_footer_view_type
        render partial: File.join('view_types', 'journal_all_index_sub_footer', @page_obj.journal_all_index_sub_footer_view_type_hash[:file_name])
    end
    def render_journal_static_by_sub_footer_view_type
        render partial: File.join('view_types', 'journal_static_sub_footer', @page_obj.journal_static_sub_footer_view_type_hash[:file_name])
    end

    def show_select_option(options)
        result = []
        options.each do |k,v|
            result << [v, k.to_s]
        end
        result
    end

    def show_option_value(options, value)
        options[value] || ''
    end

    def show_bool_radio(f, name, options)
        render partial: 'shared/bool_radio', locals: {f: f, name: name, options: options}
    end
    def show_bool_value(options, value)
        options[(value ? 0 : 1)] || ''
    end

    def show_sort_field
        case @sort_field_code
        when 'id'
            'ID' + show_sort_order
        when 'updated_at'
            t('view.updated_at') + show_sort_order
        else
            sort_field  = FieldDefLoaderForInput.field_defs_by_code_by_input_group_id(nil, @sort_field_code)
            return 'ID' + show_sort_order if sort_field.blank?
            show_caption(sort_field.first) + show_sort_order
        end
    end

    def show_sort_order
        @sort_reverse ? ' (' + t('view.desc') + ')' : ' (' + t('view.asc') + ')'
    end

    def show_sortable_field_select_option
        options = []
        options << ['ID', -1]
        options << ['更新日', -2]
        SortableField.includes(:field_def).each do |t|
            options << [show_caption(t.field_def), t.id]
        end
        options
    end

    def show_sortable_field(sortable_field_id)
        return '指定なし' if sortable_field_id.blank?

        case sortable_field_id
        when -1
            'ID'
        when -2
            '更新日'
        else
            show_caption(@list_def.sortable_field.field_def)
        end
    end

    def show_document_publish_to(document)
        case document.publish_to
        when PriDocument::PRIVATE_TYPE
            '非公開'
        when PriDocument::LIMITED_TYPE
            '限定'
        when PriDocument::PUBLIC_TYPE
            '公開'
        end
    end

    def show_publish_to(publish_to)
        case publish_to
        when PriDocument::PRIVATE_TYPE
            '非公開'
        when PriDocument::LIMITED_TYPE
            '限定'
        when PriDocument::PUBLIC_TYPE
            '公開'
        end
    end

    def show_self_doi_type(self_doi_type)
        case self_doi_type
        when SelfDoiDocument::SELF_DOI_TYPE_UNUSED
            'なし'
        when SelfDoiDocument::SELF_DOI_TYPE_JALC_AUTO
            'Jalc自動採番'
        when SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU
            'Jalc直接入力'
        when SelfDoiDocument::SELF_DOI_TYPE_CROS_AUTO
            'CrossRef自動採番'
        when SelfDoiDocument::SELF_DOI_TYPE_JALC_DELETED
            '削除済'
        when SelfDoiDocument::SELF_DOI_TYPE_CROS_DELETED
            '削除済'
        end
    end

    def show_self_doi(document)
        case document.self_doi_type
        when SelfDoiDocument::SELF_DOI_TYPE_UNUSED
            '未設定'
        when SelfDoiDocument::SELF_DOI_TYPE_JALC_DELETED
            '削除済'
        when SelfDoiDocument::SELF_DOI_TYPE_CROS_DELETED
            '削除済'
        else
            link_to document.self_doi_url, document.self_doi_url
        end
    end

    def show_visible(data)
        case data.visible
        when PriDocument::PRIVATE_TYPE
            '非公開'
        when PriDocument::LIMITED_TYPE
            '限定'
        when PriDocument::PUBLIC_TYPE
            '公開'
        end
    end

    def textarea_format(text)
        return (Rinku.auto_link html_escape(text).gsub(/\r\n|\r|\n/, "<br />")).html_safe
    end

    def markdown_format(text)
        @markdown ||= Redcarpet::Markdown.new(Redcarpet::Render::HTML.new(hard_wrap: true), autolink: true, space_after_headers: true, underline: false)
        return @markdown.render(html_variable(text)).html_safe
    end

    def html_format(html)
        html_variable(html).html_safe
    end

    def publish_target_count_badge
        if PriDocument.publish_query.count > 0
            content_tag :span, PriDocument.publish_query.count.to_s, class: ['badge', 'pull-right']
        end
    end

    def self_doi_target_count_badge
        if PriDocument.where(self_doi_checked: false).count > 0
            content_tag :span, PriDocument.where(self_doi_checked: false).count.to_s, class: ['badge', 'pull-right']
        end
    end

    def decode_url(value)
        begin
            return URI.decode(value).scrub
        rescue => ex
            return value
        end
    end

    def show_staff_email(staff_id)
        staff = Staff.where(id: staff_id.to_i).first
        return '該当なし' if staff.blank?

        staff.email
    end

    def og_url
        if @document != nil
            get_public_document_url(@document)
        else
            root_url.chop + url_for(locale: nil, logined: nil)
        end
    end

    def cache_key_obj
        return @current_list_field_def if @current_list_field_def != nil
        @page_obj
    end

    def cache_key_obj_value
        if @current_list_field_def != nil && @level_params.present?
            return get_level_params_file_name(@level_params)
        else
            return nil
        end
    end

private

    def html_variable(html)
        return '' if html.blank?
        html = html.gsub('${root}', view_state_path_gen(:url).call)
        html = html.gsub('${root_path}', view_state_path_gen(:path).call)
        html = html.gsub('${base_path}', root_path(locale: nil, logined: nil))
        html = html.gsub('${base_url}', root_url(locale: nil, logined: nil))

        if @documents != nil
            document_ids = @documents.map{|document| @system_setting.document_id_to_public_id(document.id).to_s }
            html = html.gsub('${documents.ids}', document_ids.join(','))
        end

        if @document != nil
            html = html.gsub('${document.perm_uri}', get_perm_document_url(@document))
            html = html.gsub('${document.public_uri}', get_public_document_url(@document))
            html = html.gsub('${document.uri}', get_document_url(@document))
            html = html.gsub('${document.id}', @system_setting.document_id_to_public_id(@document.id).to_s)
            html = html.gsub('${document.attach}', view_state_document_attach_path_gen(@document))
            @document.get_all_fields.each do |field|
                html = html.gsub('${document.' + field.get_field_def.code + '}', field.to_csv_field)
                html = html.gsub('${document.' + field.get_field_def.code + '.or_zero}', field.to_csv_field)
            end
        end

        html = html.gsub(/\$\{document\.\S+\.or_zero\}/, '0')
        html = html.gsub(/\$\{document\.\S+\}/, '')

        if @level != nil
            html = html.gsub('${param1.caption}', show_caption(@level_params[0])) if @level > 0
            html = html.gsub('${param1.value}',   @level_params[0].value.to_s)    if @level > 0
            html = html.gsub('${param2.caption}', show_caption(@level_params[1])) if @level > 1
            html = html.gsub('${param2.value}',   @level_params[1].value.to_s)    if @level > 1
            html = html.gsub('${param3.caption}', show_caption(@level_params[2])) if @level > 2
            html = html.gsub('${param3.value}',   @level_params[2].value.to_s)    if @level > 2
            html = html.gsub('${param4.caption}', show_caption(@level_params[3])) if @level > 3
            html = html.gsub('${param4.value}',   @level_params[3].value.to_s)    if @level > 3
            html = html.gsub('${param5.caption}', show_caption(@level_params[4])) if @level > 4
            html = html.gsub('${param5.value}',   @level_params[4].value.to_s)    if @level > 4
        end

        html = html.gsub('${param1}', @param1) if @param1.present?
        html = html.gsub('${param2}', @param2) if @param2.present?
        html = html.gsub('${param3}', @param3) if @param3.present?
        html = html.gsub('${param4}', @param4) if @param4.present?
        html = html.gsub('${param5}', @param5) if @param5.present?

        html = html.gsub(/\$\{param\d\S*\}/, '')

        if @ej_journal_def.present?
            html = html.gsub('${journal.code}', @ej_journal_def.code)
            html = html.gsub('${journal.thumnail_path}', public_dir_file_to_url_path(@ej_journal_def.thumnail_path))
            html = html.gsub('${journal.attach}', public_dir_file_to_url_path(@ej_journal_def.journal_attach_file_dir))
            html = html.gsub('${journal.all_index_path}', view_state_path_gen(:journal_all_index_path).call(@ej_journal_def.code))

            html = html.gsub('${journal.first_publish_date}', @ej_date_first.to_s)
            html = html.gsub('${journal.last_publish_date}',  @ej_date_last.to_s)
            html = html.gsub('${journal.data_count}',         @ej_data_count.to_s)
            html = html.gsub('${journal.volume_count}',       @ej_volume_count.to_s)
            html = html.gsub('${journal.issue_count}',        @ej_issue_count.to_s)
        end

        if @ej_word.present?
            html = html.gsub('${journal.caption}',              @ej_word.send(set_locale_suffix(:journal_caption)).to_s)
            html = html.gsub('${journal.volume_caption}',       @ej_word.send(set_locale_suffix(:volume_caption)).to_s)
            html = html.gsub('${journal.issue_caption}',        @ej_word.send(set_locale_suffix(:issue_caption)).to_s)
            html = html.gsub('${journal.publish_date_caption}', @ej_word.send(set_locale_suffix(:publish_date_caption)).to_s)

            html = html.gsub('${journal.journal}',       @ej_word.send(:journal).to_s)
            html = html.gsub('${journal.volume}',       @ej_word.send(:volume).to_s)
            html = html.gsub('${journal.issue}',        @ej_word.send(:issue).to_s)
            html = html.gsub('${journal.publish_date}', @ej_word.send(:publish_date).to_s)
        end

        html
    end

end

