#--
# Open Earmas
#
# Copyright (C) 2014 Hiroshima University, ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module EjHelper

    def show_ej_active(ej_word = nil)
        return 'active' if @ej_word_first && ej_word == nil
        return 'active' if @ej_word_first == false && @ej_word == ej_word
    end

    def show_ej_selected(ej_word = nil)
        return 'selected="selected"' if @ej_word_first && ej_word == nil
        return 'selected="selected"' if @ej_word_first == false && @ej_word == ej_word
    end

    def gen_voliss_path(ej_word)
        param = create_journal_params(ej_word)

        view_state_path_gen(:journal_voliss_path).call(param[:journal], param[:volume], param[:issue])
    end

    def create_journal_params(ej_word)
        journal_params = {}
        journal_params[:journal] = @ej_journal_def.code

        if ej_word.volume.present?
            journal_params[:volume] = ej_word.volume
        else
            journal_params[:volume] = JournalPage::SKIP_VALUE
        end
        if ej_word.issue.present?
            journal_params[:issue] = ej_word.issue
        else
            journal_params[:issue] = JournalPage::SKIP_VALUE
        end

        journal_params
    end

    def gen_article_path_path(ej_word, document)
        param = create_journal_params(ej_word)
        path = view_state_path_gen(:journal_voliss_article_path).call(param[:journal], param[:volume], param[:issue], document.id)
    end

    def show_journal_is_newest
        if @ej_word_is_newest
            'newest-journal-view-block'
        end
    end

    def show_ej_field_def_caption(document, ej_field_def)
        return if get_ej_render_field(ej_field_def, document) == nil

        input_group_field = document.get_input_group_field(ej_field_def[:ja].id)

        show_caption_input_group_field_or_field_def(input_group_field, ej_field_def[:ja])
    end

    def document_ej_field_by_num(document, ej_field_defs, num)
        ej_field_def = ej_field_defs[num]

        if ej_field_def.present?
            ej_field_def
        else
            nil
        end
    end
    def document_ej_field_exists?(document, ej_field_defs, num)
        ej_field_def = document_ej_field_by_num(document, ej_field_defs, num)

        get_ej_render_field(ej_field_def, document) != nil
    end
    def render_document_ej_field_caption_by_num(document, ej_field_defs, num, else_value)
        ej_field_def = document_ej_field_by_num(document, ej_field_defs, num)

        if ej_field_def.present?
            show_ej_field_def_caption(document, ej_field_def)
        else
            else_value
        end
    end
    def render_document_ej_field_line_by_num(document, ej_field_defs, num, else_value)
        ej_field_def = document_ej_field_by_num(document, ej_field_defs, num)

        if ej_field_def.present?
            render_ej_view_state_line_field(ej_field_def, document)
        else
            else_value
        end
    end

    def render_document_ej_field_block_by_num(document, ej_field_defs, num, else_value)
        ej_field_def = document_ej_field_by_num(document, ej_field_defs, num)

        if ej_field_def.present?
            render_ej_view_state_block_field(ej_field_def, document)
        else
            else_value
        end
    end

end

