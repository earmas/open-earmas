#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module ListHelper

    def show_list_field_sort_type(list_field)
        case list_field.sort_type
        when ListFieldDef::SORT_TYPE_VALUE
            'ソート値'
        when ListFieldDef::SORT_TYPE_KEY
            'ソートキー'
        end
    end

    def show_list_field_type(list_field)
        case list_field.list_field_type
        when ListFieldDef::LIST_FIELD_TYPE_STD
            '通常一覧'
        when ListFieldDef::LIST_FIELD_TYPE_PREFIX
            '頭文字一覧'
        end
    end

    def show_show_others_link_type(list_def)
        case list_def.show_others_link_type
        when ListDef::SHOW_OTHERS_LINK_HIDDEN
            '表示しない'
        when ListDef::SHOW_OTHERS_LINK_SHOW
            '表示する'
        end
    end

    def create_level_params(list_def, params)

        level = 0
        5.times do |index|
            index_level = index + 1
            if params[index_level].present?
                level = index_level
            end
        end

        level_params = []
        return level_params if level == 0

        list_word_relation = view_state_list_word_relation(level).where(list_def_id: list_def.id)

        5.times do |index|
            index_level = index + 1
            if params[index_level].present?
                if params[index_level] == ListPage::SKIP_VALUE
                    list_word_relation.where!('value' + index_level.to_s => '')
                else
                    list_word_relation.where!('value' + index_level.to_s => params[index_level].to_s)
                end
            end
        end
        list_word = list_word_relation.first
        level.times do |index|
            index_level = index + 1

            if list_word.blank?
                level_params[index_level - 1] = ListWordSkip.new
            else
                level_params[index_level - 1] = list_word.get_level_list_word(index_level - 1)
            end
        end

        level_params
    end

    # get_document_contains_links

    def get_document_contains_links(document)
        list_data = view_state_relation(:ListDatum).joins(:list_def).where(document_id: document.id, list_defs: { show_others_link_type: ListDef::SHOW_OTHERS_LINK_SHOW })

        result = "".html_safe
        gen_list_paths_by_list_data(list_data, document).each do |link|
            result << link
        end
        result
    end

    def gen_list_paths_by_list_data(list_data, document)
        param_table = create_list_param_table(list_data)
        paths = []

        param_table.each do |row|
            list_def = row[0]

            level_params = create_level_params(list_def, row)
            paths << list_breadcrumb(list_def, level_params, document, true)
        end
        paths
    end

    def create_list_param_table(list_data)
        new_table = []
        list_data.each do |list_datum|
            next if view_state_visible(list_datum.list_def) == false
            max = list_datum.list_def.list_fields.size

            my_table = []
            my_table << [list_datum.list_def]
            new_table += create_list_param_table_on_level(my_table, list_datum, 1, max)
        end
        new_table
    end
    def create_list_param_table_on_level(table, list_datum, level, max)
        return table if level > 5 || level > max

        new_table = []
        if list_datum.send('level' + level.to_s).size == 0
            my_table = array_deep_clone(table)
            my_table.each do |row|
                row << ListPage::SKIP_VALUE
            end
            new_table += create_list_param_table_on_level(my_table, list_datum, level + 1, max)
        else
            list_datum.send('level' + level.to_s).each do |param|
                my_table = array_deep_clone(table)
                my_table.each do |row|
                    row << param
                end
                new_table += create_list_param_table_on_level(my_table, list_datum, level + 1, max)
            end
        end

        new_table
    end
    def array_deep_clone(array)
        new_array = []
        array.each do |item|
            item = array_deep_clone(item) if item.class.name == "Array"
            new_array << item
        end
        new_array
    end


    # list_breadcrumb

    def list_breadcrumb(list_def, level_params, document = nil, document_link = false)
        is_detail_page = document.present?

        content_tag :ol, class: 'breadcrumb' do
            if level_params[0].present? || is_detail_page
                concat(content_tag(:li){
                    link_to show_caption(list_def), view_state_path_gen(:list_path).call(list_def.code)
                })
            else
                concat(content_tag :li, show_caption(list_def), class: 'active')
            end

            if level_params[0].present?
                if level_params[1].present? || is_detail_page
                    concat(content_tag(:li){
                        link_to show_list_param(level_params, 0), gen_list_path(list_def, level_params[0...1])
                    })
                else
                    concat(content_tag :li, show_list_param(level_params, 0), class: 'active')
                end
            end
            if level_params[1].present?
                if level_params[2].present? || is_detail_page
                    concat(content_tag(:li){
                        link_to show_list_param(level_params, 1), gen_list_path(list_def, level_params[0...2])
                    })
                else
                    concat(content_tag :li, show_list_param(level_params, 1), class: 'active')
                end
            end
            if level_params[2].present?
                if level_params[3].present? || is_detail_page
                    concat(content_tag(:li){
                        link_to show_list_param(level_params, 2), gen_list_path(list_def, level_params[0...3])
                    })
                else
                    concat(content_tag :li, show_list_param(level_params, 2), class: 'active')
                end
            end
            if level_params[3].present?
                if level_params[4].present? || is_detail_page
                    concat(content_tag(:li){
                        link_to show_list_param(level_params, 3), gen_list_path(list_def, level_params[0...4])
                    })
                else
                    concat(content_tag :li, show_list_param(level_params, 3), class: 'active')
                end
            end
            if level_params[4].present?
                if is_detail_page
                    concat(content_tag(:li){
                        link_to show_list_param(level_params, 4), gen_list_path(list_def, level_params[0...5])
                    })
                else
                    concat(content_tag :li, show_list_param(level_params, 4), class: 'active')
                end
            end

            if is_detail_page
                if document_link
                    concat(content_tag(:li){
                        link_to get_document_title_string(document, '値なし').strip, gen_document_path(level_params[0...5], document, list_def)
                    })
                else
                    concat(content_tag :li, get_document_title_string(document, '値なし').strip, class: 'active')
                end
            end
        end
    end

    def gen_current_list_path(level_params)
        return url_for(params) if level_params == nil

        case level_params.size
        when 0
            view_state_path_gen(:list_path).call(@list_def.code)
        when 1
            view_state_path_gen(:list_level1_path).call(@list_def.code, level_params[0].value)
        when 2
            view_state_path_gen(:list_level2_path).call(@list_def.code, level_params[0].value, level_params[1].value)
        when 3
            view_state_path_gen(:list_level3_path).call(@list_def.code, level_params[0].value, level_params[1].value, level_params[2].value)
        when 4
            view_state_path_gen(:list_level4_path).call(@list_def.code, level_params[0].value, level_params[1].value, level_params[2].value, level_params[3].value)
        when 5
            view_state_path_gen(:list_level5_path).call(@list_def.code, level_params[0].value, level_params[1].value, level_params[2].value, level_params[3].value, level_params[4].value)
        end
    end

    def gen_current_list_url(level_params)
        return url_for(params) if level_params == nil

        case level_params.size
        when 0
            view_state_path_gen(:list_url).call(@list_def.code)
        when 1
            view_state_path_gen(:list_level1_url).call(@list_def.code, level_params[0].value)
        when 2
            view_state_path_gen(:list_level2_url).call(@list_def.code, level_params[0].value, level_params[1].value)
        when 3
            view_state_path_gen(:list_level3_url).call(@list_def.code, level_params[0].value, level_params[1].value, level_params[2].value)
        when 4
            view_state_path_gen(:list_level4_url).call(@list_def.code, level_params[0].value, level_params[1].value, level_params[2].value, level_params[3].value)
        when 5
            view_state_path_gen(:list_level5_url).call(@list_def.code, level_params[0].value, level_params[1].value, level_params[2].value, level_params[3].value, level_params[4].value)
        end
    end

    def gen_next_list_path(level_params, next_list_word)
        case level_params.size
        when 0
            view_state_path_gen(:list_level1_path).call(@list_def.code, next_list_word.value)
        when 1
            view_state_path_gen(:list_level2_path).call(@list_def.code, level_params[0].value, next_list_word.value)
        when 2
            view_state_path_gen(:list_level3_path).call(@list_def.code, level_params[0].value, level_params[1].value, next_list_word.value)
        when 3
            view_state_path_gen(:list_level4_path).call(@list_def.code, level_params[0].value, level_params[1].value, level_params[2].value, next_list_word.value)
        when 4
            view_state_path_gen(:list_level5_path).call(@list_def.code, level_params[0].value, level_params[1].value, level_params[2].value, level_params[3].value, next_list_word.value)
        else
            view_state_path_gen(:list_path).call(@list_def.code)
        end
    end

    def gen_document_path(level_params, document, list_def = nil)
        if list_def == nil
            list_def = @list_def
        end
        return url_for(params.merge(id: document.id)) if level_params == nil

        case level_params.size
        when 0
            path = view_state_path_gen(:list_path).call(list_def.code)
        when 1
            path = view_state_path_gen(:list_level1_path).call(list_def.code, level_params[0].value)
        when 2
            path = view_state_path_gen(:list_level2_path).call(list_def.code, level_params[0].value, level_params[1].value)
        when 3
            path = view_state_path_gen(:list_level3_path).call(list_def.code, level_params[0].value, level_params[1].value, level_params[2].value)
        when 4
            path = view_state_path_gen(:list_level4_path).call(list_def.code, level_params[0].value, level_params[1].value, level_params[2].value, level_params[3].value)
        when 5
            path = view_state_path_gen(:list_level5_path).call(list_def.code, level_params[0].value, level_params[1].value, level_params[2].value, level_params[3].value, level_params[4].value)
        else
            path = nil
        end

        if path == nil
            url_for(params.merge(id: document.id))
        else
            path.slice!(root_path) # サブディレクトリ運用対応
            path = '/' + path
            url_for(Rails.application.routes.recognize_path(path).merge(id: document.id))
        end
    end

    def show_level_params(level_params)
        values = []
        level_params.each_with_index do |level_param, index|
            next if level_param.value == ListPage::SKIP_VALUE
            next if @list_def.get_field(index + 1) != nil && @list_def.get_field(index + 1).prefix_list?
            values << show_caption(level_param)
        end
        values.join(' ')
    end

    def get_level_params_file_name(level_params)
        values = []
        level_params.each_with_index do |level_param, index|
            next if level_param.value == ListPage::SKIP_VALUE
            next if @list_def.get_field(index + 1) != nil && @list_def.get_field(index + 1).prefix_list?
            values << level_param.value.to_s
        end
        values.join('-')
    end

private

    def show_list_param(level_params, index)
        return "--" if level_params[index].value == ListPage::SKIP_VALUE
        show_caption(level_params[index])
    end

    def gen_list_path(list_def, level_params)
        case level_params.size
        when 1
             view_state_path_gen(:list_level1_path).call(list_def.code, level_params[0].value)
        when 2
            view_state_path_gen(:list_level2_path).call(list_def.code, level_params[0].value, level_params[1].value)
        when 3
            view_state_path_gen(:list_level3_path).call(list_def.code, level_params[0].value, level_params[1].value, level_params[2].value)
        when 4
            view_state_path_gen(:list_level4_path).call(list_def.code, level_params[0].value, level_params[1].value, level_params[2].value, level_params[3].value)
        when 5
            view_state_path_gen(:list_level5_path).call(list_def.code, level_params[0].value, level_params[1].value, level_params[2].value, level_params[3].value, level_params[4].value)
        else
            view_state_path_gen(:list_path).call(list_def.code)
        end
    end


end
