#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module OaiPmhSettingsHelper

    # 未公開
    # 公開しているタイプと同じ
    # 削除時はダメ
    def self_doi_type_selectable(document)
        @document.get_pub_document == nil || @document.get_pub_document.self_doi_active == false
    end

    # 削除時はダメ
    def self_doi_status_selectable(document)
        @document.get_pub_document != nil && (@document.get_pub_document.self_doi_enable || @document.get_pub_document.self_doi_type != @document.self_doi_type)
    end

    def get_self_doi_delete_type(document)
        if @document.get_pub_document.self_doi_jalc_active
            return SelfDoiDocument::SELF_DOI_TYPE_JALC_DELETED
        end
        if @document.get_pub_document.self_doi_crossref_active
            return SelfDoiDocument::SELF_DOI_TYPE_CROS_DELETED
        end
        nil
    end
end
