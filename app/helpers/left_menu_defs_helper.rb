#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module LeftMenuDefsHelper

    def render_left_menu_def_param(left_menu_def)
        render partial: 'left_menu_defs/param_form', locals: { left_menu_def: left_menu_def }
    end

    def render_top_page_param(system_setting)
        render partial: 'view_settings/system/param_form', locals: { system_setting: system_setting }
    end

    def show_menu_type_select_option
        options = []
        options << ["リスト", LeftMenuDef::TYPE_LIST_DEF]
        options << ["静的ページ", LeftMenuDef::TYPE_STATIC_PAGE_DEF]
        options << ["空白", LeftMenuDef::TYPE_BLANK]
        options << ["ラベル", LeftMenuDef::TYPE_LABEL]
        options << ["リンク", LeftMenuDef::TYPE_LINK]
        options
    end

    def show_top_page_type_select_option
        options = []
        options << ["リスト", LeftMenuDef::TYPE_LIST_DEF]
        options << ["静的ページ", LeftMenuDef::TYPE_STATIC_PAGE_DEF]
        options
    end

    def show_menu_type(menu_type)
        case menu_type
        when LeftMenuDef::TYPE_LIST_DEF
            "リスト"
        when LeftMenuDef::TYPE_STATIC_PAGE_DEF
            "静的ページ"
        when LeftMenuDef::TYPE_BLANK
            "空白"
        when LeftMenuDef::TYPE_LABEL
            "ラベル"
        when LeftMenuDef::TYPE_LINK
            "リンク"
        end
    end

    def show_menu_def_link(menu_def)
        case menu_def.menu_type
        when LeftMenuDef::TYPE_LIST_DEF
            list_def = LeftMenuDefLoader.get_list_def(menu_def.menu_id)
            list_def = ListDef.find_by(id: menu_def.menu_id) if list_def.blank? # for top page
            return content_tag :span, "Not Found" if  list_def.blank?
            link_to show_caption(list_def), view_state_path_gen(:list_path).call(list_def.code)
        when LeftMenuDef::TYPE_STATIC_PAGE_DEF
            static_page_def = LeftMenuDefLoader.get_static_page_def(menu_def.menu_id)
            static_page_def = StaticPageDef.find_by(id: menu_def.menu_id) if static_page_def.blank? # for top page
            return content_tag :span, "Not Found" if  static_page_def.blank?
            link_to show_caption(static_page_def), view_state_path_gen(:static_page_path).call(static_page_def.code)
        when LeftMenuDef::TYPE_BLANK
            content_tag :span, ""
        when LeftMenuDef::TYPE_LABEL
            content_tag :span, show_caption(menu_def)
        when LeftMenuDef::TYPE_LINK
            link_to show_caption(menu_def), menu_def.value
        end
    end

    def show_gen_menu
        get_menu_def

        @parent_menus.size == 0
        level = 0

        menus = LeftMenuDefLoader.get_menu_by_parent(nil)
        return if menus.size == 0

        content_tag :ul, class: 'nav nav-pills nav-stacked' do
            content1 = "".html_safe

            menus.each do |menu_def|
                next if menu_visible(menu_def) == false
                content1 << (content_tag :li, class: menu_active?(@current_menu, menu_def) do
                    show_menu_def_link(menu_def)
                end)

                if @parent_menus[level].present? && @parent_menus[level].id == menu_def.id
                    content1 << show_gen_menu_child(level + 1, menu_def)
                end
            end
            content1
        end
    end

private

    def menu_visible(menu_def)
        case menu_def.menu_type
        when LeftMenuDef::TYPE_LIST_DEF
            list_def = LeftMenuDefLoader.get_list_def(menu_def.menu_id)
            return false if list_def == nil
            return view_state_visible(list_def)
        when LeftMenuDef::TYPE_STATIC_PAGE_DEF
            static_page_def = LeftMenuDefLoader.get_static_page_def(menu_def.menu_id)
            return false if static_page_def == nil
            return view_state_visible(static_page_def)
        when LeftMenuDef::TYPE_BLANK
            return true
        when LeftMenuDef::TYPE_LABEL
            return true
        when LeftMenuDef::TYPE_LABEL
            return true
        end
    end

    def get_menu_def
        @parent_menus = []

        return if @page_obj.blank?

        case @page_obj.class.name
        when ListDef.name
            @current_menu = LeftMenuDefLoader.get_menu_by_type_id(LeftMenuDef::TYPE_LIST_DEF, @page_obj.id).first
        when ListFieldDef.name
            @current_menu = LeftMenuDefLoader.get_menu_by_type_id(LeftMenuDef::TYPE_LIST_DEF, @page_obj.list_def_id).first
        when StaticPageDef.name
            @current_menu = LeftMenuDefLoader.get_menu_by_type_id(LeftMenuDef::TYPE_STATIC_PAGE_DEF, @page_obj.id).first
        end

        return if @current_menu.blank?

        child_menu = @current_menu
        while child_menu.present?
            @parent_menus << child_menu
            child_menu = LeftMenuDefLoader.get_menu_by_id(child_menu.parent_menu)
        end

        @parent_menus.reverse!
    end

    def show_gen_menu_child(level, parent)
        menus = LeftMenuDefLoader.get_menu_by_parent(parent.id)
        return if menus.size == 0

        content_tag :li, class: 'child-holder' do
            content_tag :ul, class: 'nav nav-pills nav-stacked offset-menu' do
                content1 = "".html_safe

                menus.each do |menu_def|
                    content1 << (content_tag :li, class: menu_active?(@current_menu, menu_def) do
                        show_menu_def_link(menu_def)
                    end)

                    if @parent_menus[level].present? && @parent_menus[level].id == menu_def.id
                        content1 << show_gen_menu_child(level + 1, menu_def)
                    end
                end
                content1
            end
        end
    end

    def menu_active?(current_menu, menu_def)
        if current_menu.present? && menu_def.present? && current_menu.id == menu_def.id
            ['active', send_local_name(menu_def, :html_class)]
        else
            send_local_name(menu_def, :html_class)
        end
    end

end
