#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module PdfHelper

    require 'open3'

    PDF_TO_TEXT_STD_OPTIONS = '-raw -enc UTF-8 -nopgbrk'
    PDF_TO_TEXT_ENC_OPTIONS = '-raw -enc UTF-8 -nopgbrk -opw'

    def create_text_from_pdf(pdf_file_path, text_file_path)
        expand_pdf_file_path = File.expand_path(pdf_file_path)
        expand_text_file_path = File.expand_path(text_file_path)

        escaped_expand_pdf_file_path = Shellwords.escape(expand_pdf_file_path)
        escaped_expand_text_file_path = Shellwords.escape(expand_text_file_path)

        stdout_str, stderr_str, status = Open3.capture3("#{Rails.application.config.pdftotext_path} #{PDF_TO_TEXT_STD_OPTIONS} #{escaped_expand_pdf_file_path} #{escaped_expand_text_file_path}")

        if status.success? == false
            logger.error stdout_str
            logger.error stderr_str
        end

        File.exist?(expand_text_file_path)
    end

    CONVERT_STD_OPTIONS = '-flatten -format png -resize 200 -background white -colorspace rgb'
    CONVERT_ENC_OPTIONS = '-flatten -format png -resize 200 -background white -colorspace rgb -authenticate'

    def create_thumnail_from_pdf(pdf_file_path, thumnail_file_path, has_coverpage)
        expand_pdf_file_path = File.expand_path(pdf_file_path)
        expand_thumnail_file_path = File.expand_path(thumnail_file_path)

        escaped_expand_pdf_file_path = Shellwords.escape(expand_pdf_file_path)
        escaped_expand_thumnail_file_path = Shellwords.escape(expand_thumnail_file_path)

        if has_coverpage
            stdout_str, stderr_str, status = Open3.capture3("#{Rails.application.config.convert_path} #{CONVERT_STD_OPTIONS} #{escaped_expand_pdf_file_path}[1] #{escaped_expand_thumnail_file_path}")
        else
            stdout_str, stderr_str, status = Open3.capture3("#{Rails.application.config.convert_path} #{CONVERT_STD_OPTIONS} #{escaped_expand_pdf_file_path}[0] #{escaped_expand_thumnail_file_path}")
        end

        if status.success? == false
            logger.error stdout_str
            logger.error stderr_str
        end

        File.exist?(expand_thumnail_file_path)
    end

    def create_thumnail_from_image(image_file_path, thumnail_file_path)
        expand_image_file_path = File.expand_path(image_file_path)
        expand_thumnail_file_path = File.expand_path(thumnail_file_path)

        escaped_expand_image_file_path = Shellwords.escape(expand_image_file_path)
        escaped_expand_thumnail_file_path = Shellwords.escape(expand_thumnail_file_path)

        stdout_str, stderr_str, status = Open3.capture3("#{Rails.application.config.convert_path} #{CONVERT_STD_OPTIONS} #{escaped_expand_image_file_path} #{escaped_expand_thumnail_file_path}")

        if status.success? == false
            logger.error stdout_str
            logger.error stderr_str
        end

        File.exist?(expand_thumnail_file_path)
    end

    def create_cover_page(xml_string, cover_page_pdf_path)
        FileUtils.mkdir_p "#{Rails.root}/tmp/coverpage/"
        document_xml_path = Tempfile.new('test_tmp', "#{Rails.root}/tmp/coverpage/")
        File.open(document_xml_path, "wb") do |file|
            file.write(xml_string)
        end

        expand_document_xml_path = File.expand_path(document_xml_path)
        expand_conf_xml_path = File.expand_path('lib/coverpage/config.xml')
        expand_xsl_file_path = File.expand_path('lib/coverpage/coverpage.xsl')
        expand_cover_page_pdf_path = File.expand_path(cover_page_pdf_path)

        escaped_expand_document_xml_path = Shellwords.escape(expand_document_xml_path)
        escaped_expand_conf_xml_path = Shellwords.escape(expand_conf_xml_path)
        escaped_expand_xsl_file_path = Shellwords.escape(expand_xsl_file_path)
        escaped_expand_cover_page_pdf_path = Shellwords.escape(expand_cover_page_pdf_path)

        stdout_str, stderr_str, status = Open3.capture3("#{Rails.application.config.fop_path} -c #{escaped_expand_conf_xml_path} -xsl #{escaped_expand_xsl_file_path} -xml #{escaped_expand_document_xml_path} -pdf #{escaped_expand_cover_page_pdf_path}")

        if status.success? == false
            logger.error stdout_str
            logger.error stderr_str
        end

        File.delete(document_xml_path)
        File.exist?(expand_cover_page_pdf_path)
    end

    def marge_pdf(pdf1_path, pdf2_path, merged_pdf_path)
        expand_pdf1_path = File.expand_path(pdf1_path)
        expand_pdf2_path = File.expand_path(pdf2_path)
        expand_merged_pdf_path = File.expand_path(merged_pdf_path)

        escaped_expand_pdf1_path = Shellwords.escape(expand_pdf1_path)
        escaped_expand_pdf2_path = Shellwords.escape(expand_pdf2_path)
        escaped_expand_merged_pdf_path = Shellwords.escape(expand_merged_pdf_path)

        stdout_str, stderr_str, status = Open3.capture3("#{Rails.application.config.pdftk_path} #{escaped_expand_pdf1_path} #{escaped_expand_pdf2_path} cat output #{escaped_expand_merged_pdf_path}")

        if status.success? == false
            logger.error stdout_str
            logger.error stderr_str
        end

        File.exist?(expand_merged_pdf_path)
    end

    def delete_cover_page(covered_pdf_path, original_pdf_path)
        escaped_covered_pdf_path = Shellwords.escape(covered_pdf_path)
        escaped_original_pdf_path = Shellwords.escape(original_pdf_path)

        stdout_str, stderr_str, status = Open3.capture3("#{Rails.application.config.pdftk_path} #{escaped_covered_pdf_path} cat 2-end output #{escaped_original_pdf_path}")

        if status.success? == false
            logger.error stdout_str
            logger.error stderr_str
        end

        File.exist?(original_pdf_path)
    end


end
