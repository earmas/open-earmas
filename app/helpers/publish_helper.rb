#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module PublishHelper

    LIMIT = 100

private

    def parse_publish_param(params)
        @search_input_group_id = params[:input_group_id]

        @search_updater_staff_id = params[:updater_staff_id]
        @search_creator_staff_id = params[:creator_staff_id]

        @search_created_at_start = params[:created_at_start]
        @search_created_at_end = params[:created_at_end]

        @search_updated_at_start = params[:updated_at_start]
        @search_updated_at_end = params[:updated_at_end]

        @except_ids = params[:except_ids]
    end

    def find_by_publish_document_query
        relation = PriDocument.publish_query

        relation.where!(input_group_id: @search_input_group_id) if @search_input_group_id.present?
        relation.where!(creator_staff_id: @search_creator_staff_id) if @search_creator_staff_id.present?
        relation.where!(updater_staff_id: @search_updater_staff_id) if @search_updater_staff_id.present?
        relation.where!('created_at >= ?', @search_created_at_start) if @search_created_at_start.present?
        relation.where!('created_at <= ?', @search_created_at_end) if @search_created_at_end.present?
        relation.where!('updated_at >= ?', @search_updated_at_start) if @search_updated_at_start.present?
        relation.where!('updated_at <= ?', @search_updated_at_end) if @search_updated_at_end.present?
        relation.where!.not(id: @except_ids.split(',')) if @except_ids.present?
        relation
    end

end
