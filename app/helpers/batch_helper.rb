#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module BatchHelper

    LIMIT = 100

private

    def parse_search_param(params)
        @search_input_group_id = params[:search_document][:input_group_id]
        @search_publish_to = params[:search_document][:publish_to]
        @search_self_doi_type = params[:search_document][:self_doi_type]

        @search_updater_staff_id = params[:search_document][:updater_staff_id]
        @search_creator_staff_id = params[:search_document][:creator_staff_id]

        @search_created_at_start = params[:search_document][:created_at_start]
        @search_created_at_end = params[:search_document][:created_at_end]

        @search_updated_at_start = params[:search_document][:updated_at_start]
        @search_updated_at_end = params[:search_document][:updated_at_end]

        @search_published_at_start = params[:search_document][:published_at_start]
        @search_published_at_end = params[:search_document][:published_at_end]

        @search_field_map = {}
        params[:search_field].keys.each do |field_def_id|
            field_param = params[:search_field][field_def_id]

            field_def = FieldDefLoaderForInput.get_field_def(field_def_id)
            search_field = field_def.new_pri_field(field_param)
            search_field.field_def_id = field_def.id
            @search_field_map[field_def.id] = search_field
        end
    end

    def parse_new_value_param(params)
        @new_input_group_id = params[:new_document][:input_group_id]
        @new_publish_to = params[:new_document][:publish_to]
        @new_self_doi_type = params[:new_document][:self_doi_type]

        @new_updater_staff_id = params[:new_document][:updater_staff_id]
        @new_creator_staff_id = params[:new_document][:creator_staff_id]

        @new_field_map = {}
        params[:new_field].keys.each do |field_def_id|
            field_param = params[:new_field][field_def_id]

            field_def = FieldDefLoaderForInput.get_field_def(field_def_id)
            new_field = field_def.new_pri_field(field_param)
            new_field.field_def_id = field_def.id
            @new_field_map[field_def.id] = new_field
        end
    end

    def get_exist_values(relation)
        @exist_values_field_def_map = {}

        if @new_input_group_id.present?
            @exist_values_field_def_map[:input_group_id] = PriDocument.unscoped.where(deleted: false).where(id: relation)
                .where.not(input_group_id: @new_input_group_id).group(:input_group_id).order('count_input_group_id desc').count(:input_group_id)
        end

        if @new_publish_to.present?
            @exist_values_field_def_map[:publish_to] = PriDocument.unscoped.where(deleted: false).where(id: relation)
                .where.not(publish_to: @new_publish_to).group(:publish_to).order('count_publish_to desc').count(:publish_to)
        end

        if @new_self_doi_type.present?
            @exist_values_field_def_map[:self_doi_type] = PriDocument.unscoped.where(deleted: false).where(id: relation)
                .where.not(self_doi_type: @new_self_doi_type).group(:self_doi_type).order('count_self_doi_type desc').count(:self_doi_type)
        end

        if @new_creator_staff_id.present?
            @exist_values_field_def_map[:creator_staff_id] = PriDocument.unscoped.where(deleted: false).where(id: relation)
                .where.not(creator_staff_id: @new_creator_staff_id).group(:creator_staff_id).order('count_creator_staff_id desc').count(:creator_staff_id)
        end

        if @new_updater_staff_id.present?
            @exist_values_field_def_map[:updater_staff_id] = PriDocument.unscoped.where(deleted: false).where(id: relation)
                .where.not(updater_staff_id: @new_updater_staff_id).group(:updater_staff_id).order('count_updater_staff_id desc').count(:updater_staff_id)
        end

        @new_field_map.values.each do |field|
            next if field.blank_value?
            exist_value_fields = field.get_exist_values(relation)
            @exist_values_field_def_map[field.field_def_id] = exist_value_fields if exist_value_fields.present?
        end
    end

    def get_query_document_ids(limit)
        result_document_ids = nil
        @search_field_map.values.each do |field|
            field_document_ids = field.get_search_document_ids(limit)
            if result_document_ids == nil
                result_document_ids = field_document_ids if field_document_ids != nil
            else
                result_document_ids &= field_document_ids if field_document_ids != nil
            end
        end

        relation = PriDocument.unscoped.where(deleted: false)
        relation.where!(input_group_id: @search_input_group_id) if @search_input_group_id.present?
        relation.where!(publish_to: @search_publish_to) if @search_publish_to.present?
        relation.where!(self_doi_type: @search_self_doi_type) if @search_self_doi_type.present?
        relation.where!(creator_staff_id: @search_creator_staff_id) if @search_creator_staff_id.present?
        relation.where!(updater_staff_id: @search_updater_staff_id) if @search_updater_staff_id.present?
        relation.where!('created_at >= ?', @search_created_at_start) if @search_created_at_start.present?
        relation.where!('created_at <= ?', @search_created_at_end) if @search_created_at_end.present?
        relation.where!('updated_at >= ?', @search_updated_at_start) if @search_updated_at_start.present?
        relation.where!('updated_at <= ?', @search_updated_at_end) if @search_updated_at_end.present?
        relation.where!('publish_at >= ?', @search_published_at_start) if @search_published_at_start.present?
        relation.where!('publish_at <= ?', @search_published_at_end) if @search_published_at_end.present?
        field_document_ids = relation.pluck(:id)

        if result_document_ids == nil
            result_document_ids = field_document_ids if field_document_ids.present?
        else
            result_document_ids &= field_document_ids if field_document_ids.present?
        end

        return [] if result_document_ids == nil

        result_document_ids
    end

    def find_by_document_query(result_document_ids)
        PriDocument.unscoped.where(deleted: false, id: result_document_ids)
    end


    # field value update 用

    def parse_field_value_update_search_param(params)
        @search_field_map = {}
        @active_search_field_map = []

        params[:search_field].keys.each do |field_def_id|
            field_param = params[:search_field][field_def_id]

            field_def = FieldDefLoaderForInput.get_field_def(field_def_id)
            search_field = field_def.new_pri_field(field_param)
            search_field.field_def_id = field_def.id
            @search_field_map[field_def.id] = search_field
            next if search_field.blank_value?
            @active_search_field_map << search_field
        end
    end

    def parse_field_value_update_new_value_param(params)
        @new_field = nil
        params[:new_field].keys.each do |field_def_id|
            field_param = params[:new_field][field_def_id]

            field_def = FieldDefLoaderForInput.get_field_def(field_def_id)
            new_field = field_def.new_pri_field(field_param)
            new_field.field_def_id = field_def.id
            @new_field = new_field
        end
    end

    def get_field_value_update_exist_values
        @exist_values_field_def_map = nil

        exist_value_fields = @active_search_field_map.first.get_all_exist_values
        @exist_values_field_def_map = exist_value_fields if exist_value_fields.present?
    end

end
