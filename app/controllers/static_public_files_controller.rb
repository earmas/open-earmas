#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class StaticPublicFilesController < BaseAdminController
    include StaticFile

    before_action :set_current_directory
    before_action :set_directory

    def destroy_directory
        return show_invalid if @current_dir == Rails.application.config.public_static_file_dir

        delete_dir(@current_dir)

        redirect_to({ action: :index, dir: @upper_sub_dir_param, only_path: false }, notice: 'ディレクトリを削除しました。')
    end

    def rename_directory
        return show_invalid if @current_dir == Rails.application.config.public_static_file_dir

        if paths = params[:name].match(/^[a-zA-Z0-9_]+$/)

            @sub_dirs[-1] = paths[0]
            new_current_dir = File.join(Rails.application.config.public_static_file_dir, File.join(@sub_dirs))

            File::rename(@current_dir, new_current_dir)

            redirect_to({ action: :index, dir: @sub_dirs.join(','), only_path: false }, notice: 'ディレクトリの名前を変更しました。')
        else
            redirect_to({ action: :index, dir: @sub_dir_param, only_path: false }, alert: '不正な文字列です。')
        end
    end


private

    def set_current_directory
        @current_dir = Rails.application.config.public_static_file_dir
        FileUtils.mkdir_p(@current_dir)

        @static_file_url = root_path(locale: nil, logined: nil) + Rails.application.config.public_static_file_url_path
        @current_dir_name = 'ルート'
    end

end
