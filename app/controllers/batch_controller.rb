#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class BatchController < BaseStaffController
    include BatchHelper

    before_action :check_not_mentenance_mode

    before_action :set_nonmaintenance_data_lock,    only: [:start_batch, :pri_reindex]

    def index
        @document = PriDocument.new

        @search_field_map = Hash[ FieldDefLoaderForInput.field_defs.delete_if{|a| a.field_type_class.no_batch? }.map{|a| [a.id, a.empty_pri_field] } ]
        @result = []
        @count = 0
    end

    def search
        parse_search_param(params)

        query_document_ids = get_query_document_ids(nil)
        relation = find_by_document_query(query_document_ids.take(BatchHelper::LIMIT))

        @document = PriDocument.new
        @result = relation.limit(BatchHelper::LIMIT).uniq
        @count = query_document_ids.count

        render 'index'
    end

    def new_value
        parse_search_param(params)

        @document = PriDocument.new
    end

    def confirm
        parse_search_param(params)
        parse_new_value_param(params)

        query_document_ids = get_query_document_ids(nil)
        relation = find_by_document_query(query_document_ids)

        get_exist_values(relation)

        @document = PriDocument.new
    end

    def start_batch
        begin
            if Rails.application.config.use_delayed_job
                Delayed::KickDelayedTask.new(current_staff).delay({:run_at => Rails.application.config.delay_time.from_now}).update_values(params)
            else
                Delayed::DelayTask.new.update_values(current_staff, params)
            end

            redirect_to staff_batch_url
        rescue Exception => ex
            unset_data_lock
            logger.error ex.message
            redirect_to staff_batch_url, alert: '更新に失敗しました'
        end
    end

    def pri_reindex
        begin
            if Rails.application.config.use_delayed_job
                Delayed::KickDelayedTask.new(current_staff).delay({:run_at => Rails.application.config.delay_time.from_now}).pri_reindex_all
            else
                Delayed::DelayTask.new.pri_reindex_all(current_staff)
            end

            redirect_to staff_url
        rescue Exception => ex
            unset_data_lock
            logger.error ex.message
            redirect_to staff_batch_url, alert: '未公開側再構築に失敗しました'
        end
    end


end
