#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class StaticPageDefsController < BaseAdminController

    before_action :check_static_page_def_exists, except: [:index, :new, :create]

    def index
        @static_page_defs = StaticPageDef.all
    end

    def new
        @static_page_def = StaticPageDef.new
        @static_page_def.set_default_child
    end

    def create
        @static_page_def = StaticPageDef.new(request_params)

        if @static_page_def.save
            redirect_to staff_static_page_def_url(@static_page_def), notice: '静的ページを作成しました。'
        else
            render action: 'new'
        end
    end

    def show
    end

    def edit
        @static_page_def.set_default_child
    end

    def update
        @static_page_def.assign_attributes(request_params)

        if @static_page_def.save
            redirect_to staff_static_page_def_url(@static_page_def), notice: '静的ページを更新しました。'
        else
            render action: 'edit'
        end
    end

    def destroy
        redirect_block(staff_static_page_def_url(@static_page_def), 'static_page_def', '静的ページの削除に失敗しました。') do
            check_use_data
            @static_page_def.destroy
            redirect_to staff_static_page_defs_url, notice: '静的ページを削除しました。'
        end
    end

private

    def check_use_data
        if LeftMenuDef.where(menu_type: LeftMenuDef::TYPE_STATIC_PAGE_DEF, menu_id: @static_page_def.id).count != 0
            raise DataCheckException, "左メニューで使用中です"
        end

        if SystemSetting.where(top_page_type: LeftMenuDef::TYPE_STATIC_PAGE_DEF, top_page_id: @static_page_def.id).count != 0
            raise DataCheckException, "トップページで使用中です"
        end
    end

    def check_static_page_def_exists
        @static_page_def = StaticPageDef.find(params[:id])
    end

    def request_params
        params.require(:static_page_def).permit(
            :code,
            :visible,
            :caption_ja,
            :caption_en,
            { html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            :body_main_html_string_ja,
            :body_main_html_string_en)
    end
end
