#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class PeopleSettingsController < BaseAdminController
    include FieldLock, BatchHelper

    before_action :check_mentenance_mode

    def edit
    end

    def update
        return if set_maintenance_data_lock == false

        @people_setting.assign_attributes(request_params)
        @lock_field_update = lock_field_update?
        touch_use_data

        begin
            if @people_setting.save
                if @lock_field_update
                    if Rails.application.config.use_delayed_job
                        Delayed::KickDelayedTask.new(current_staff).delay({:run_at => Rails.application.config.delay_time.from_now}).update_people_setting
                    else
                        Delayed::DelayTask.new.update_people_setting(current_staff)
                    end
                else
                    unset_data_lock
                end

#                Rails.cache.clear
                redirect_to staff_people_url, notice: '人物設定を更新しました。'
            else
                unset_data_lock
                render action: 'edit'
            end
        rescue Exception => ex
            unset_data_lock
            logger.error ex.message
            redirect_to staff_people_url, notice: '人物設定を更新しました。'
        end
    end

private

    def lock_field_update?
        @people_setting.cipher_type_changed? || @people_setting.cipher_iv_changed? || @people_setting.cipher_key_changed?
    end

    def touch_use_data
        return if lock_field_update? == false

        field_def_ids = FieldDef.where(field_type_id: FieldTypes::RepositoryPerson::TYPE_ID).pluck(:id)

        set_lock_pri_field(field_def_ids)
    end

    def request_params
        params.require(:people_setting).permit(:url, :cipher_type, :cipher_iv, :cipher_key,
            :key1_name_ja, :key2_name_ja, :key3_name_ja, :key4_name_ja, :key5_name_ja,:key6_name_ja, :key7_name_ja, :key8_name_ja, :key9_name_ja, :key10_name_ja,
            :key1_name_en, :key2_name_en, :key3_name_en, :key4_name_en, :key5_name_en,:key6_name_en, :key7_name_en, :key8_name_en, :key9_name_en, :key10_name_en,
            :key1_param_ja, :key2_param_ja, :key3_param_ja, :key4_param_ja, :key5_param_ja, :key6_param_ja, :key7_param_ja, :key8_param_ja, :key9_param_ja, :key10_param_ja,
            :key1_param_en, :key2_param_en, :key3_param_en, :key4_param_en, :key5_param_en, :key6_param_en, :key7_param_en, :key8_param_en, :key9_param_en, :key10_param_en
        )
    end

end
