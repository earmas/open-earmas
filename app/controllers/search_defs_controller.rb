#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class SearchDefsController < BaseAdminController
    include OrderHolder

    maintenance_data_lock_methods :create, :update, :destroy, :up, :down

    before_action :check_mentenance_mode
    before_action :check_search_def_exists,     except: [:index, :new, :create, :edit_search_setting, :update_search_setting]

    def index
        @search_defs = SearchDef.all
        @search_result_fields = SearchResultField.all
        @search_filter_defs = SearchFilterDef.all
    end

    def new
        @search_def = SearchDef.new
    end

    def create
        @search_def = SearchDef.new(request_params)
        @search_def.sort_index = SearchDef.maximum(:sort_index)
        @search_def.sort_index = @search_def.sort_index && @search_def.sort_index + 1 || 1

        if SearchDef.all.size >= 20
            @search_def.errors.add(:base, "検索項目は20個までです")
            return render action: 'new'
        end

        create_action_block('new', 'search_def', '検索項目の保存に失敗しました。') do
            if @search_def.save
                lock_search
                lock_pri_search
                if SearchDef.all.size > 20
                    raise DataCheckException, '検索項目は20個までです'
                end

                redirect_to staff_search_def_url(@search_def), notice: '検索項目を作成しました。'
            else
                render action: 'new'
            end
        end
    end

    def show
        @search_field_defs = @search_def.fields
    end

    def edit
    end

    def update
        @search_def.assign_attributes(request_params)
        if @search_def.save
#            Rails.cache.clear
            redirect_to staff_search_def_url(@search_def), notice: '検索項目を更新しました。'
        else
            render action: 'edit'
        end
    end

    def destroy
        redirect_block(staff_search_defs_url, 'search_def', '検索項目の削除に失敗しました。') do
            @search_def.destroy!
            lock_search
            lock_pri_search
            redirect_to staff_search_defs_url, notice: '検索項目を削除しました。'
        end
    end

    def edit_search_setting
        @search_setting.set_default_child
    end

    def update_search_setting
        @search_setting.assign_attributes(request_search_setting_params)

        if @search_setting.save
            redirect_to staff_search_defs_url, notice: '検索ページの情報を更新しました。'
        else
            render action: 'edit_search_setting'
        end
    end

    def up
        move_up(SearchDef.all.to_a, @search_def)

        redirect_to staff_search_defs_url, notice: '検索ページの情報を更新しました。'
    end

    def down
        move_down(SearchDef.all.to_a, @search_def)

        redirect_to staff_search_defs_url, notice: '検索ページの情報を更新しました。'
    end

private

    def check_search_def_exists
        @search_def = SearchDef.find(params[:id])
    end

    def request_params
        params.require(:search_def).permit(:code, :caption_ja, :caption_en, :description)
    end

    def request_search_setting_params
        params.require(:search_setting).permit(
            :result_view_type_id,
            { result_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            :detail_view_type_id,
            :detail_sub_header_view_type_id,
            :detail_sub_footer_view_type_id,
            { detail_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] })
    end
end
