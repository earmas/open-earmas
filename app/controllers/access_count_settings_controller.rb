#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class AccessCountSettingsController < BaseAdminController

    before_action :check_mentenance_mode

    def edit
    end

    def update
        @access_count_setting.assign_attributes(request_params)

        if @access_count_setting.save
            redirect_to staff_access_count_settings_url, notice: 'アクセス統計設定を更新しました。'
        else
            render action: 'edit'
        end
    end

private

    def request_params
        params.require(:access_count_setting).permit(:my_url, :filter_bot, :bot_names, :bot_address, :api_token)
    end

end
