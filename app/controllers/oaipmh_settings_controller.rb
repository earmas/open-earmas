#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class OaipmhSettingsController < BaseAdminController

    maintenance_data_lock_methods :update

    before_action :check_mentenance_mode
    before_action :check_oaipmh_enable

    def index
        @oaipmh_filter_defs = OaipmhFilterDef.all
    end

    def edit
    end

    def update
        @oaipmh_setting.assign_attributes(request_params)

        update_action_block('edit', 'oaipmh_setting', 'OAI-PMH設定の更新に失敗しました。') do
            need_lock = @oaipmh_setting.set_spec_custom_select_id_changed? || @oaipmh_setting.set_spec_default_value_changed?

            if @oaipmh_setting.save
                if need_lock
                    lock_pri_oaipmh
                end
                redirect_to staff_oaipmh_settings_url, notice: 'OAI-PMH設定を更新しました。'
            else
                render action: 'edit'
            end
        end
    end

    def edit_self_doi
    end


    def update_self_doi
        @oaipmh_setting.assign_attributes(request_self_doi_params)

        update_action_block('edit_self_doi', 'oaipmh_setting', 'Self Doi 設定の更新に失敗しました。') do
            need_lock = @oaipmh_setting.set_spec_custom_select_id_changed? || @oaipmh_setting.set_spec_default_value_changed?

            if @oaipmh_setting.save
                if need_lock
                    lock_pri_oaipmh
                end
                redirect_to staff_oaipmh_settings_url, notice: 'OAI-PMH設定を更新しました。'
            else
                render action: 'edit_self_doi'
            end
        end
    end

private

    def request_params
        params.require(:oaipmh_setting).permit(:identifier_prefix, :repository_name, :base_url, :admin_email, :set_spec_custom_select_id, :set_spec_default_value)
    end

    def request_self_doi_params
        params.require(:oaipmh_setting).permit(:enable_self_jalc_doi, :enable_self_crossref_doi, :self_doi_jalc_prefix, :self_doi_crossref_prefix,
        :title_field_def_id, :creator_field_def_id, :publisher_field_def_id, :issn_field_def_id, :isbn_field_def_id, :jtitle_field_def_id, :start_page_field_def_id, :nii_type_field_def_id)
    end
end
