#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class FieldDefsController < BaseAdminController
    include OrderHolder, FieldLock

    maintenance_data_lock_methods :create, :update, :destroy, :up, :down

    before_action :check_mentenance_mode
    before_action :check_field_def, except: [:index, :new, :create, :param_form]

    respond_to :js, only:[:param_form]

    def index
        @field_defs = FieldDef.all
        @sortable_fields = SortableField.all
        @system_setting.set_default_child
    end

    def new
        @field_def = FieldDef.new
        @field_def.field_type_id = 1
    end

    def create
        @field_def = FieldDef.new(request_params)
        @field_def.sort_index = FieldDef.maximum(:sort_index)
        @field_def.sort_index = @field_def.sort_index && @field_def.sort_index + 1 || 1

        if @field_def.save
#            Rails.cache.clear
            redirect_to staff_field_defs_url, notice: 'データ項目を作成しました。'
        else
            render action: "new"
        end
    end

    def edit
    end

    def update
        update_action_block('edit', 'field_def', 'データ項目の更新に失敗しました。') do
            @field_def.clear_optional_param
            @field_def.assign_attributes(update_request_params)
            check_field_update

            if @field_def.save
#                Rails.cache.clear
                redirect_to staff_field_defs_url, notice: 'データ項目を更新しました。'
            else
                render action: "edit"
            end
        end
    end

    def destroy
        redirect_block(staff_field_defs_url, 'field_def', 'データ項目の削除に失敗しました。') do
            check_use_data
            @field_def.destroy!
            delete_field_data
            lock_oaipmh
#            Rails.cache.clear
            redirect_to staff_field_defs_url, notice: 'データ項目を削除しました。'
        end
    end

    def param_form
        @field_def = FieldDef.new
        @field_def.field_type_id = params[:field_def][:field_type_id]
    end

    def up
        move_up(FieldDef.all.to_a, @field_def)
#        Rails.cache.clear

        redirect_to staff_field_defs_url, notice: 'データ項目を移動しました。'
    end

    def down
        move_down(FieldDef.all.to_a, @field_def)
#        Rails.cache.clear

        redirect_to staff_field_defs_url, notice: 'データ項目を移動しました。'
    end

private

    def lock_oaipmh_update?
        @field_def.code_changed? ||
        @field_def.to_pub_changed?
    end

    def lock_sort_update?
        ((@field_def.sort_conv_pattern_was.blank? && @field_def.sort_conv_pattern.blank?) == false && @field_def.sort_conv_pattern_changed?) ||
        ((@field_def.sort_conv_format_was.blank? && @field_def.sort_conv_format.blank?) == false && @field_def.sort_conv_format_changed?)
    end

    def lock_field_update?
        ((@field_def.unit_prefix_ja_was.blank? && @field_def.unit_prefix_ja.blank?) == false && @field_def.unit_prefix_ja_changed?) ||
        ((@field_def.unit_suffix_ja_was.blank? && @field_def.unit_suffix_ja.blank?) == false && @field_def.unit_suffix_ja_changed?) ||
        ((@field_def.unit_prefix_en_was.blank? && @field_def.unit_prefix_en.blank?) == false && @field_def.unit_prefix_en_changed?) ||
        ((@field_def.unit_suffix_en_was.blank? && @field_def.unit_suffix_en.blank?) == false && @field_def.unit_suffix_en_changed?)
    end

    def check_field_update
        if lock_sort_update?
            lock_sort
            lock_pri_sort
        end

        lock_oaipmh if lock_oaipmh_update?

        set_lock_pri_field([@field_def.id], false) if lock_field_update?
        set_lock_pri_field([@field_def.id]) if @field_def.field_type_class.lock_field_update?(@field_def)
    end

    def check_use_data
        if OaipmhFilterDef.where(parent_type: DocumentFilterDef::OAIPMH_TYPE, filter_type: [DocumentFilterDef::FILTER_TYPES[:field_value_filter][:id], DocumentFilterDef::FILTER_TYPES[:field_key_filter][:id]], some_id: @field_def.id).count != 0
            raise DataCheckException, "OAI-PMHフィルタで使用中です" 
        end

        if SortableField.where(field_def_id: @field_def.id).count != 0
            raise DataCheckException, "並び順で使用中です" 
        end

        if InputGroupField.where(field_def_id: @field_def.id).count != 0
            raise DataCheckException, "データグループで使用中です" 
        end


        if ListFieldDef.where(field_def_id: @field_def.id).count != 0
            raise DataCheckException, "リスト項目で使用中です" 
        end
        if ListFilterDef.where(parent_type: DocumentFilterDef::LIST_TYPE, filter_type: [DocumentFilterDef::FILTER_TYPES[:field_value_filter][:id], DocumentFilterDef::FILTER_TYPES[:field_key_filter][:id]], some_id: @field_def.id).count != 0
            raise DataCheckException, "リストのフィルタで使用中です" 
        end
        if ListResultField.where(field_def_id: @field_def.id).count != 0
            raise DataCheckException, "リストの結果項目で使用中です" 
        end


        if EjDef.where(journal_field_def_id: @field_def.id).count != 0
            raise DataCheckException, "雑誌の雑誌項目で使用中です" 
        end
        if EjDef.where(volume_field_def_id: @field_def.id).count != 0
            raise DataCheckException, "雑誌の巻項目で使用中です" 
        end
        if EjDef.where(issue_field_def_id: @field_def.id).count != 0
            raise DataCheckException, "雑誌の号項目で使用中です" 
        end
        if EjDef.where(section_field_def_id: @field_def.id).count != 0
            raise DataCheckException, "雑誌の論文種類項目で使用中です" 
        end
        if EjDef.where(publish_date_field_def_id: @field_def.id).count != 0
            raise DataCheckException, "雑誌の発行日項目で使用中です" 
        end
        if EjDef.where(sort_field_def_id: @field_def.id).count != 0
            raise DataCheckException, "雑誌の並び順項目で使用中です" 
        end
        if EjDef.where(start_page_field_def_id: @field_def.id).count != 0
            raise DataCheckException, "雑誌の開始ページ項目で使用中です" 
        end
        if EjDef.where(end_page_field_def_id: @field_def.id).count != 0
            raise DataCheckException, "雑誌の終了ページ項目で使用中です" 
        end
        if EjFilterDef.where(parent_type: DocumentFilterDef::EJ_TYPE, filter_type: [DocumentFilterDef::FILTER_TYPES[:field_value_filter][:id], DocumentFilterDef::FILTER_TYPES[:field_key_filter][:id]], some_id: @field_def.id).count != 0
            raise DataCheckException, "雑誌のフィルタで使用中です" 
        end
        if EjResultField.where(ja_field_def_id: @field_def.id).count != 0
            raise DataCheckException, "雑誌の結果項目で使用中です" 
        end
        if EjResultField.where(en_field_def_id: @field_def.id).count != 0
            raise DataCheckException, "雑誌の結果項目で使用中です" 
        end
        if EjDetailField.where(ja_field_def_id: @field_def.id).count != 0
            raise DataCheckException, "雑誌の詳細項目で使用中です" 
        end
        if EjDetailField.where(en_field_def_id: @field_def.id).count != 0
            raise DataCheckException, "雑誌の詳細項目で使用中です" 
        end


        if SearchFieldDef.where(field_def_id: @field_def.id).count != 0
            raise DataCheckException, "検索項目で使用中です" 
        end
        if SearchFilterDef.where(parent_type: DocumentFilterDef::SEARCH_TYPE, filter_type: [DocumentFilterDef::FILTER_TYPES[:field_value_filter][:id], DocumentFilterDef::FILTER_TYPES[:field_key_filter][:id]], some_id: @field_def.id).count != 0
            raise DataCheckException, "検索のフィルタで使用中です" 
        end
        if SearchResultField.where(field_def_id: @field_def.id).count != 0
            raise DataCheckException, "検索の結果項目で使用中です" 
        end

    end

    def check_field_def
        @field_def = FieldDef.find(params[:id])
    end

    def delete_field_data
        @field_def.get_pri_type_class.data_class.where(field_def_id: @field_def.id).delete_all
        @field_def.get_pub_type_class.data_class.where(field_def_id: @field_def.id).delete_all
    end

    def request_params
        params.require(:field_def).permit(
            :code, :caption_ja, :caption_en, :description,
            :csv_column, :must, :multi, :result, :detail, :to_pub, :multi_field_delim,
            :unit_prefix_ja, :unit_suffix_ja, :unit_prefix_en, :unit_suffix_en,
            :field_type_id,
            :default_value, :in_field_delim,
            :field_param_id1, :field_param_id2, :field_param_id3,
            :field_param_text1, :field_param_text2, :field_param_text3,
            :sort_conv_pattern, :sort_conv_format, :check_format)
    end

    def update_request_params
        params.require(:field_def).permit(
            :code, :caption_ja, :caption_en, :description,
            :csv_column, :must, :multi, :result, :detail, :to_pub, :multi_field_delim,
            :unit_prefix_ja, :unit_suffix_ja, :unit_prefix_en, :unit_suffix_en,
            :default_value, :in_field_delim,
            :field_param_id1, :field_param_id2, :field_param_id3,
            :field_param_text1, :field_param_text2, :field_param_text3,
            :sort_conv_pattern, :sort_conv_format, :check_format)
    end

end
