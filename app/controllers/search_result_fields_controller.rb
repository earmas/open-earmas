#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class SearchResultFieldsController < BaseAdminController
    include OrderHolder

    maintenance_data_lock_methods :create, :update, :destroy, :up, :down

    before_action :check_mentenance_mode
    before_action :check_search_result_field, except: [:index, :new, :create]

    def new
        @search_result_field = SearchResultField.new
    end

    def edit
    end

    def create
        @search_result_field = SearchResultField.new(request_params)
        @search_result_field.sort_index = SearchResultField.maximum(:sort_index)
        @search_result_field.sort_index = @search_result_field.sort_index && @search_result_field.sort_index + 1 || 1

        if @search_result_field.save
#            Rails.cache.clear
            redirect_to staff_search_defs_url, notice: '結果画面での表示項目を作成しました。'
        else
            render action: 'new'
        end
    end

    def update
        @search_result_field.assign_attributes(request_params)

        if @search_result_field.save
#            Rails.cache.clear
            redirect_to staff_search_defs_url, notice: '結果画面での表示項目を更新しました。'
        else
            render action: 'edit'
        end
    end

    def destroy
        @search_result_field.destroy
#        Rails.cache.clear
        redirect_to staff_search_defs_url, notice: '結果画面での表示項目を削除しました。'
    end

    def up
        move_up(SearchResultField.all.to_a, @search_result_field)
#        Rails.cache.clear

        redirect_to staff_search_defs_url, notice: '結果画面での表示項目を移動しました。'
    end

    def down
        move_down(SearchResultField.all.to_a, @search_result_field)
#        Rails.cache.clear

        redirect_to staff_search_defs_url, notice: '結果画面での表示項目を移動しました。'
    end

private

    def check_search_result_field
        @search_result_field = SearchResultField.find(params[:id])
    end

    def request_params
        params.require(:search_result_field).permit(:field_def_id)
    end
end
