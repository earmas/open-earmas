#--
# Open Earmas
#
# Copyright (C) 2014 Hiroshima University, ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class EjEditors::EjStaticPageDefsController < EjEditors::BaseController
    include OrderHolder

    before_action :check_ej_static_page_def_exists, except: [:new, :create]

    def new
        @ej_static_page_def = EjStaticPageDef.new
        @ej_static_page_def.set_default_child
    end

    def create
        @ej_static_page_def = EjStaticPageDef.new(request_params)
        @ej_static_page_def.ej_journal_def_id = @editable_ej_journal_def.id
        @ej_static_page_def.sort_index = EjStaticPageDef.where(ej_journal_def_id: @editable_ej_journal_def.id).maximum(:sort_index)
        @ej_static_page_def.sort_index = @ej_static_page_def.sort_index && @ej_static_page_def.sort_index + 1 || 1

        if @ej_static_page_def.save
            redirect_to staff_ej_editors_ej_journal_show_url, notice: '静的ページを作成しました。'
        else
            render action: 'new'
        end
    end

    def show
    end

    def edit
        @ej_static_page_def.set_default_child
    end

    def update
        @ej_static_page_def.assign_attributes(request_params)

        if @ej_static_page_def.save
            redirect_to staff_ej_editors_ej_journal_show_url, notice: '静的ページを更新しました。'
        else
            render action: 'edit'
        end
    end

    def destroy
        redirect_block(staff_ej_editors_ej_journal_show_url, 'ej_static_page_def', '静的ページの削除に失敗しました。') do
            @ej_static_page_def.destroy
            redirect_to staff_ej_editors_ej_journal_show_url, notice: '静的ページを削除しました。'
        end
    end

    def up
        move_up(@editable_ej_journal_def.static_page_defs.to_a, @ej_static_page_def)

        redirect_to staff_ej_editors_ej_journal_show_url, notice: '静的ページを移動しました。'
    end

    def down
        move_down(@editable_ej_journal_def.static_page_defs.to_a, @ej_static_page_def)

        redirect_to staff_ej_editors_ej_journal_show_url, notice: '静的ページを移動しました。'
    end

private

    def check_ej_static_page_def_exists
        @ej_static_page_def = @editable_ej_journal_def.static_page_defs.find(params[:id])
    end

    def request_params
        params.require(:ej_static_page_def).permit(
            :code,
            :visible,
            :caption_ja,
            :caption_en,
            :journal_static_view_type_id,
            :journal_static_sub_header_view_type_id,
            :journal_static_sub_footer_view_type_id,
            { journal_static_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            :body_main_html_string_ja,
            :body_main_html_string_en)
    end
end
