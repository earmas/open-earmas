#--
# Open Earmas
#
# Copyright (C) 2014 Hiroshima University, ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class EjEditors::EjJournalController < EjEditors::BaseController
    include OrderHolder, ViewStateHelper, LocaleHelper

    before_action :set_journal_attach_files,    only: [:show, :create_attach_file, :destroy_attach_file]

    def show
    end

    def edit
        @editable_ej_journal_def.set_default_child
    end

    def update
        @editable_ej_journal_def.assign_attributes(request_params)

        begin
            if @editable_ej_journal_def.save
                redirect_to staff_ej_editors_ej_journal_show_url, notice: '各雑誌を更新しました。'
            else
                render action: 'edit'
            end
        rescue => ex
            @editable_ej_journal_def.errors.add(:base, ex.message)
            render action: 'edit'
        end
    end

    def create_attach_file
        @upload_file = params[:upload_file]

        begin
            save_upload_attach_file

            render partial: 'upload_success'
        rescue => ex
            logger.error ex.message
            logger.error ex.backtrace.join("\n") if ex.backtrace.present?

            render partial: 'upload_fail', locals: { message: 'ファイルの追加に失敗しました。' }
        end
    end

    def destroy_attach_file
        destory_file_name = params[:name]

        begin
            Dir::glob(File.join(@current_dir, '*')).each do |static_file|
                if destory_file_name == File.basename(static_file)
                    File::delete(static_file) if File.exists?(static_file)
                    break;
                end
            end

            render json: {
                result: 'success',
            }
        rescue => ex
            logger.error ex.message
            logger.error ex.backtrace.join("\n") if ex.backtrace.present?

            render json: {
                result: 'fail',
                message: 'ファイルの削除に失敗しました。',
            }
        end
    end

private

    def set_journal_attach_files
        @current_dir = @editable_ej_journal_def.journal_attach_file_dir
    end

    def request_params
        params.require(:ej_journal_def).permit(
            :css_string, :upload_thumnail_file, :journals_html_ja, :journals_html_en,
            :journal_view_type_id,
            :journal_sub_header_view_type_id,
            :journal_sub_footer_view_type_id,
            { journal_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            :article_view_type_id,
            :article_sub_header_view_type_id,
            :article_sub_footer_view_type_id,
            { article_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            :journal_all_index_view_type_id,
            :journal_all_index_sub_header_view_type_id,
            :journal_all_index_sub_footer_view_type_id,
            { journal_all_index_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            { journal_top_html_set_def_attributes:  [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            { voliss_list_html_set_def_attributes:  [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            { article_list_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            { static_list_html_set_def_attributes:  [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            :css_string)

    end

    def save_upload_attach_file
        return if @upload_file.blank?

        local_file_path = File.join(@current_dir, @upload_file.original_filename)

        File.delete(local_file_path) if File.exists?(local_file_path)

        if @upload_file.size > 10.megabyte
            raise DataCheckException, 'ファイルのサイズは最大で10MBです。'
        end

        f = File.open(local_file_path, "wb")
        f.write(@upload_file.read)
        f.close
    end

end
