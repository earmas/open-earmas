#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class EjEditors::BaseController < ApplicationController

    before_action :authenticate_staff!
    before_action :set_view_state_pri
    before_action :set_editable_ej_journal_def

    layout :staff_layout

    def staff_layout
        if current_staff.ej_editor?
            'ej_editor'
        else
            'staff'
        end
    end

    def set_view_state_pri
        ViewState.to_pri
    end

    def check_ej_editor
        return show_invalid if current_staff.ej_editor? == false
    end

    def set_editable_ej_journal_def
        @editable_ej_journal_def = current_staff.editable_ej_journal_def
        return show_invalid if @editable_ej_journal_def == nil
        @ej_def = @editable_ej_journal_def.ej_def
    end

end
