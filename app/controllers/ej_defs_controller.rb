#--
# Open Earmas
#
# Copyright (C) 2014 Hiroshima University, ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class EjDefsController < BaseAdminController

    maintenance_data_lock_methods :create, :update, :destroy

    before_action :check_mentenance_mode
    before_action :check_ej_def_exists, except: [:index, :new, :create]

    def index
    end

    def new
        @ej_def = EjDef.new
        @ej_def.set_default_value
    end

    def create
        @ej_def = EjDef.new(request_params)
        create_action_block('new', 'ej_def', '雑誌の保存に失敗しました。') do
            if @ej_def.save
                lock_ej(@ej_def.id)
                lock_pri_ej(@ej_def.id)
                redirect_to staff_ej_def_url(@ej_def), notice: '雑誌を保存しました。'
            else
                render action: 'new'
            end
        end
    end

    def show
        @ej_journal_defs = @ej_def.journal_defs
        @ej_filter_defs = @ej_def.filter_fields
        @ej_result_fields = @ej_def.result_fields
        @ej_detail_fields = @ej_def.detail_fields
    end

    def edit
        @ej_def.set_default_value
    end

    def update
        @ej_def.assign_attributes(request_params)

        update_action_block('edit', 'ej_def', '雑誌の更新に失敗しました。') do
            if lock_ej_update?
                lock_ej(@ej_def.id)
                lock_pri_ej(@ej_def.id)
            end

            if @ej_def.save
                redirect_to staff_ej_def_url(@ej_def), notice: '雑誌を更新しました。'
            else
                render action: 'edit'
            end
        end
    end

    def destroy
        @ej_def.destroy
        redirect_to staff_ej_defs_url, notice: '雑誌を削除しました。'
    end

private

    def check_ej_def_exists
        @ej_def = EjDef.find(params[:id])
    end

    def lock_ej_update?
        @ej_def.journal_field_def_id_changed? || @ej_def.volume_field_def_id_changed? || @ej_def.issue_field_def_id_changed? || @ej_def.section_field_def_id_changed? || @ej_def.publish_date_field_def_id_changed? || @ej_def.sort_field_def_id_changed?
    end

    def request_params
        params.require(:ej_def).permit(
            :journal_field_def_id,
            :volume_field_def_id,
            :issue_field_def_id,
            :section_field_def_id,
            :publish_date_field_def_id,
            :sort_field_def_id,
            :start_page_field_def_id,
            :end_page_field_def_id,
            :css_string,
            :rss_xml_string)
    end
end
