#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ViewSettings::ListFieldController < BaseAdminController

    before_action :check_list_def_exists
    before_action :check_list_field_def_exists

    def show
    end

    def edit
        @list_field_def.set_default_child
    end

    def update
        @list_field_def.assign_attributes(request_params)

        if @list_field_def.save
            redirect_to staff_view_setting_list_list_field_show_path(@list_def, @list_field_def), notice: 'リスト階層を更新しました。'
        else
            render action: 'edit'
        end
    end

private

    def check_list_def_exists
        @list_def = ListDef.find(params[:list_def_id])
    end

    def check_list_field_def_exists
        @list_field_def = @list_def.list_fields.find(params[:id])
    end

    def request_params
        params.require(:list_field_def).permit(
            :list_param_view_type_id,
            :list_param_sub_header_view_type_id,
            :list_param_sub_footer_view_type_id,
            { list_param_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] })
    end

end
