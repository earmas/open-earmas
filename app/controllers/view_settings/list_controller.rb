#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ViewSettings::ListController < BaseAdminController

    before_action :check_list_def_exists, except: [:index]

    def show
        @list_field_defs = @list_def.list_fields
    end

    def edit
        @list_def.set_default_child
        @list_def.set_default_value
    end

    def update
        @list_def.assign_attributes(request_params)

        if @list_def.save
            redirect_to staff_view_setting_list_show_url(@list_def), notice: 'リストを更新しました。'
        else
            render action: 'edit'
        end
    end

private

    def request_params
        params.require(:list_def).permit(
            :caption_ja,
            :caption_en,
            :result_view_type_id,
            :result_sub_header_view_type_id,
            :result_sub_footer_view_type_id,
            { result_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            :detail_view_type_id,
            :detail_sub_header_view_type_id,
            :detail_sub_footer_view_type_id,
            { detail_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] }
        )
    end

    def check_list_def_exists
        @list_def = ListDef.includes(:result_html_set_def, :detail_html_set_def).find(params[:id])
    end

end
