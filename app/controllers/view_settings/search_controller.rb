#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ViewSettings::SearchController < BaseAdminController

    def index
        @search_defs = SearchDef.all
        @search_result_fields = SearchResultField.all
    end

    def edit
        @search_setting.set_default_child
    end

    def update
        @search_setting.assign_attributes(request_search_setting_params)

        if @search_setting.save
            redirect_to staff_view_setting_search_url, notice: '検索ページの情報を更新しました。'
        else
            render action: 'edit'
        end
    end


private

    def request_search_setting_params
        params.require(:search_setting).permit(
            :result_view_type_id,
            { result_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            :detail_view_type_id,
            :detail_sub_header_view_type_id,
            :detail_sub_footer_view_type_id,
            { detail_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] }
        )
    end

end
