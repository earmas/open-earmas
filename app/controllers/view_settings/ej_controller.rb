#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ViewSettings::EjController < BaseAdminController

    before_action :check_ej_def_exists, except: [:index]

    def show
        @ej_journal_defs = @ej_def.journal_defs
        @ej_result_fields = @ej_def.result_fields
        @ej_detail_fields = @ej_def.detail_fields
    end

    def edit
        @ej_def.set_default_value
    end

    def update
        @ej_def.assign_attributes(request_params)

        update_action_block('edit', 'ej_def', '雑誌の更新に失敗しました。') do
            if @ej_def.save
                redirect_to staff_view_setting_ej_show_url(@ej_def), notice: '雑誌を更新しました。'
            else
                render action: 'edit'
            end
        end
    end

private

    def check_ej_def_exists
        @ej_def = EjDef.find(params[:id])
    end

    def request_params
        params.require(:ej_def).permit(:css_string)
    end

end
