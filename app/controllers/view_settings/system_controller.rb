#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ViewSettings::SystemController < BaseAdminController

    def index
    end

    def edit
        @system_setting.set_default_value
    end

    def update
        @system_setting.assign_attributes(system_setting_params)

        update_action_block('edit', 'system_setting', 'システム設定の更新に失敗しました。') do
            if @system_setting.save
                redirect_to staff_view_setting_system_url, notice: 'システム設定を更新しました。'
            else
                render action: 'edit'
            end
        end
    end

    def edit_top_page
        @system_setting.top_page_type ||= 1
    end

    def param_form
        @system_setting.top_page_type = params[:system_setting][:top_page_type]
    end

    def update_top_page
        @system_setting.clear_optional_param
        @system_setting.assign_attributes(request_top_page_params)

        update_action_block('edit_top_page', 'system_setting', 'トップページの情報の更新に失敗しました。') do
            if @system_setting.save
                redirect_to staff_view_setting_system_url, notice: 'トップページの情報を更新しました。'
            else
                render action: 'edit_top_page'
            end
        end
    end

    def edit_detail_view
        @system_setting.set_default_child
    end

    def update_detail_view
        if @system_setting.update(update_detail_view_setting_params)
            redirect_to staff_view_setting_system_url, notice: '詳細表示を更新しました。'
        else
            render action: 'edit_detail_view'
        end
    end

private

    def system_setting_params
        params.require(:system_setting).permit(:sytem_name_ja, :sytem_name_en, :theme_id, :css_string)
    end

    def request_top_page_params
        params.require(:system_setting).permit(:top_page_type, :top_page_id)
    end

    def update_detail_view_setting_params
        params.require(:system_setting).permit(:detail_view_type_id, :detail_sub_header_view_type_id, :detail_sub_footer_view_type_id,
            { detail_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] }
        )
    end

end
