#--
# Open Earmas
#
# Copyright (C) 2014 Hiroshima University, ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ViewSettings::EjJournalController < BaseAdminController
    include OrderHolder, ViewStateHelper, LocaleHelper

    before_action :check_ej_journal_def_exists, except: [:index]
    before_action :set_journal_attach_files,    only: [:show, :create_attach_file, :destroy_attach_file]

    def show
    end

    def edit
        @ej_journal_def.set_default_child
    end

    def update
        @ej_journal_def.assign_attributes(request_params)

        begin
            if @ej_journal_def.save
                redirect_to staff_view_setting_ej_journal_show_url(@ej_journal_def), notice: '各雑誌を更新しました。'
            else
                render action: 'edit'
            end
        rescue => ex
            @ej_journal_def.errors.add(:base, ex.message)
            render action: 'edit'
        end
    end

    def all_journal_index
        all_journal_index = @ej_journal_def.all_journal_index
        if all_journal_index.blank?
            return show_invalid
        end

        @json = ActiveSupport::JSON.decode(all_journal_index)

        @ej_word_map = {}
        view_state_word_relation(:EjWord).where(ej_def_id: @ej_def.id, journal: @ej_journal_def.value).each do |ej_word|
            @ej_word_map[ej_word.id] = ej_word
        end

        @document_map = {}
        view_state_relation(:EjDatum).where(ej_def_id: @ej_def.id, journal: @ej_journal_def.value).includes(:document).each do |datum|
            @document_map[@ej_def.toc_index(datum.volume, datum.issue)] ||= {}
            @document_map[@ej_def.toc_index(datum.volume, datum.issue)][datum.document_id] = { datum: datum, used: false }
        end
    end

    def gen_all_journal_index
        @ej_words = view_state_word_relation(:EjWord).where(ej_def_id: @ej_def.id, journal: @ej_journal_def.value).order(set_locale_suffix('publish_date_sort_value') => :desc, volume: :desc, issue: :desc)

        json_data = {}

        @ej_words.each do |ej_word|

            articles = []
            json_data[ej_word.toc_index] = articles

            ej_word.get_data_relation.order(set_locale_suffix('sort_value') => :asc).includes(:document).each do |ej_datum|
                document = ej_datum.document
                article = {}
                article['type'] = 1
                article['id'] = document.id
                articles << article
            end
        end

        @ej_journal_def.all_journal_index = ActiveSupport::JSON.encode(json_data)
        @ej_journal_def.save
        redirect_to staff_view_setting_ej_journal_show_url(@ej_journal_def), notice: '総目次を生成しました。'
    end

    def update_all_journal_index
        @ej_journal_def.all_journal_index = ActiveSupport::JSON.encode(ActiveSupport::JSON.decode(params[:json_string]))
        @ej_journal_def.save
        redirect_to staff_view_setting_ej_journal_show_url(@ej_journal_def), notice: '総目次を更新しました。'
    end

    def delete_all_journal_index
        @ej_journal_def.all_journal_index = nil
        @ej_journal_def.save
        redirect_to staff_view_setting_ej_journal_show_url(@ej_journal_def), notice: '総目次を破棄しました。'
    end

    def create_attach_file
        @upload_file = params[:upload_file]

        begin
            save_upload_attach_file

            render partial: 'upload_success'
        rescue => ex
            logger.error ex.message
            logger.error ex.backtrace.join("\n") if ex.backtrace.present?

            render partial: 'upload_fail', locals: { message: 'ファイルの追加に失敗しました。' }
        end
    end

    def destroy_attach_file
        destory_file_name = params[:name]

        begin
            Dir::glob(File.join(@current_dir, '*')).each do |static_file|
                if destory_file_name == File.basename(static_file)
                    File::delete(static_file) if File.exists?(static_file)
                    break;
                end
            end

            render json: {
                result: 'success',
            }
        rescue => ex
            logger.error ex.message
            logger.error ex.backtrace.join("\n") if ex.backtrace.present?

            render json: {
                result: 'fail',
                message: 'ファイルの削除に失敗しました。',
            }
        end
    end

private

    def check_ej_journal_def_exists
        @ej_journal_def =EjJournalDef.find(params[:id])
        @ej_def = @ej_journal_def.ej_def
    end

    def set_journal_attach_files
        @current_dir = @ej_journal_def.journal_attach_file_dir
    end

    def request_params
        params.require(:ej_journal_def).permit(
            :css_string, :upload_thumnail_file, :journals_html_ja, :journals_html_en,
            :journal_view_type_id,
            :journal_sub_header_view_type_id,
            :journal_sub_footer_view_type_id,
            { journal_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            :article_view_type_id,
            :article_sub_header_view_type_id,
            :article_sub_footer_view_type_id,
            { article_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            :journal_all_index_view_type_id,
            :journal_all_index_sub_header_view_type_id,
            :journal_all_index_sub_footer_view_type_id,
            { journal_all_index_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            { journal_top_html_set_def_attributes:  [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            { voliss_list_html_set_def_attributes:  [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            { article_list_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            { static_list_html_set_def_attributes:  [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] }
        )

    end

    def save_upload_attach_file
        return if @upload_file.blank?

        local_file_path = File.join(@current_dir, @upload_file.original_filename)

        File.delete(local_file_path) if File.exists?(local_file_path)

        if @upload_file.size > 10.megabyte
            raise DataCheckException, 'ファイルのサイズは最大で10MBです。'
        end

        f = File.open(local_file_path, "wb")
        f.write(@upload_file.read)
        f.close
    end

end
