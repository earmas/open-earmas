#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class LeftMenuDefsController < BaseAdminController
    include OrderHolder

    before_action :check_left_menu_def_exists, except: [:index, :new, :create, :param_form]

    def index
        left_menu_defs = LeftMenuDef.all
        @left_menu_def_map_by_parent = {}

        left_menu_defs.each do |left_menu_def|
            @left_menu_def_map_by_parent[left_menu_def.parent_menu] = [] if @left_menu_def_map_by_parent[left_menu_def.parent_menu].blank?
            @left_menu_def_map_by_parent[left_menu_def.parent_menu] << left_menu_def
        end
    end

    def new
        @left_menu_def = LeftMenuDef.new
        @left_menu_def.menu_type = LeftMenuDef::TYPE_LIST_DEF
    end

    def create
        @left_menu_def = LeftMenuDef.new(request_params)
        @left_menu_def.sort_index = LeftMenuDef.where(parent_menu: nil).maximum(:sort_index)
        @left_menu_def.sort_index = @left_menu_def.sort_index && @left_menu_def.sort_index + 1 || 1

        if @left_menu_def.save
            redirect_to staff_left_menu_defs_url, notice: '左メニューを作成しました。'
        else
            render action: 'new'
        end
    end

    def edit
    end

    def update
        @left_menu_def.clear_optional_param
        @left_menu_def.assign_attributes(request_params)

        if @left_menu_def.save
            redirect_to staff_left_menu_defs_url, notice: '左メニューを更新しました。'
        else
            render action: 'edit'
        end
    end

    def destroy
        @left_menu_def.destroy
        redirect_to staff_left_menu_defs_url, notice: '左メニューを削除しました。'
    end

    def param_form
        @left_menu_def = LeftMenuDef.new
        @left_menu_def.menu_type = params[:left_menu_def][:menu_type]
    end

    def up
        same_level_menus = LeftMenuDef.where(parent_menu: @left_menu_def.parent_menu).to_a
        move_up(same_level_menus, @left_menu_def)

        redirect_to staff_left_menu_defs_url, notice: '左メニューを移動しました。'
    end

    def down
        same_level_menus = LeftMenuDef.where(parent_menu: @left_menu_def.parent_menu).to_a
        move_down(same_level_menus, @left_menu_def)

        redirect_to staff_left_menu_defs_url, notice: '左メニューを移動しました。'
    end

    def left
        parent_menu = LeftMenuDef.where(id: @left_menu_def.parent_menu).first

        if parent_menu.blank?
            redirect_to staff_left_menu_defs_url, notice: 'その操作は出来ません'
            return
        end

        parent_level_menus = LeftMenuDef.where(parent_menu: parent_menu.parent_menu).to_a

        current_index = 0
        parent_level_menus.each_with_index do |same_level_menu, index|
            current_index = index if same_level_menu.id == parent_menu.id
        end

        parent_level_menus.insert(current_index + 1, @left_menu_def)
        @left_menu_def.parent_menu = parent_menu.parent_menu

        new_index = 1
        parent_level_menus.each do |same_level_menu|
            same_level_menu.sort_index = new_index
            same_level_menu.save!
            new_index += 1
        end

        redirect_to staff_left_menu_defs_url, notice: '左メニューを移動しました。'
    end

    def right
        same_level_menus = LeftMenuDef.where(parent_menu: @left_menu_def.parent_menu).to_a

        prev_menu = nil
        same_level_menus.each_with_index do |same_level_menu|
            break if same_level_menu.id == @left_menu_def.id
            prev_menu = same_level_menu
        end

        if prev_menu.blank? || prev_menu.has_child == false
            redirect_to staff_left_menu_defs_url, notice: 'その操作は出来ません'
            return
        end

        prev_menu_childs = LeftMenuDef.where(parent_menu: prev_menu.id).to_a

        prev_menu_childs.insert(prev_menu_childs.size, @left_menu_def)
        @left_menu_def.parent_menu = prev_menu.id

        new_index = 1
        prev_menu_childs.each do |same_level_menu|
            same_level_menu.sort_index = new_index
            same_level_menu.save!
            new_index += 1
        end

        redirect_to staff_left_menu_defs_url, notice: '左メニューを移動しました。'
    end

private

    def check_left_menu_def_exists
        @left_menu_def = LeftMenuDef.find(params[:id])
    end

    def request_params
        params.require(:left_menu_def).permit(:parent_menu, :menu_type, :menu_id, :caption_ja, :caption_en, :value, :html_class_ja, :html_class_en)
    end

end
