#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class StaffsController < BaseAdminController

    before_action :check_staff_exists, except: [:index, :new, :create]

    def index
        @staffs = Staff.all
    end

    def new
        @staff = Staff.new
        @staff.role = Staff::ROLE_STAFF
    end

    def create
        @staff = Staff.new(request_params)

        if @staff.save
            redirect_to staff_staffs_url, notice: 'ユーザを作成しました。'
        else
            render action: 'new'
        end
    end

    def edit
    end

    def update
        params[:staff].delete(:password) if params[:staff][:password].blank?
        params[:staff].delete(:password_confirmation) if params[:staff][:password].blank? and params[:staff][:password_confirmation].blank?

        update_action_block('edit', 'staff', 'ユーザの更新に失敗しました。') do
            if @staff.update(request_params)
                if Staff.where(role: Staff::ROLE_ADMIN).size < 1
                    raise DataCheckException, "管理者は最低でも一人は必要です"
                end

                redirect_to staff_staffs_url, notice: 'ユーザを更新しました。'
            else
                render action: 'edit'
            end
        end
    end

    def destroy
        redirect_block(staff_staffs_url, 'staff', 'ユーザの削除に失敗しました。') do
            @staff.destroy!
            if Staff.where(role: Staff::ROLE_ADMIN).size < 1
                raise DataCheckException, "管理者は最低でも一人は必要です"
            end

            redirect_to staff_staffs_url, notice: 'ユーザを削除しました。'
        end
    end


private

    def check_staff_exists
        @staff = Staff.find(params[:id])
    end

    def request_params
        params.require(:staff).permit(
            :email,
            :role,
            :ej_journal_def_id,
            :password,
            :password_confirmation)
    end

end
