#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class PermalinkFieldsController < BaseAdminController
    include OrderHolder

    maintenance_data_lock_methods :create, :update, :destroy, :update_default_setting, :up, :down

    before_action :check_mentenance_mode
    before_action :check_permalink_field, except: [:index, :new, :create, :edit_default_setting, :update_default_setting]

    def new
        @permalink_field = PermalinkField.new
    end

    def edit
    end

    def create
        @permalink_field = PermalinkField.new(request_params)
        @permalink_field.sort_index = PermalinkField.maximum(:sort_index)
        @permalink_field.sort_index = @permalink_field.sort_index && @permalink_field.sort_index + 1 || 1

        if @permalink_field.save
            redirect_to staff_permalink_fields_url, notice: 'Parmlink 項目を作成しました。'
        else
            render action: 'new'
        end
    end

    def update
        @permalink_field.assign_attributes(request_params)

        if @permalink_field.save
            redirect_to staff_permalink_fields_url, notice: 'Parmlink 項目を更新しました。'
        else
            render action: 'edit'
        end
    end

    def destroy
        @permalink_field.destroy
        redirect_to staff_permalink_fields_url
    end

    def up
        move_up(PermalinkField.all.to_a, @permalink_field)

        redirect_to staff_permalink_fields_url, notice: 'Parmlink 項目を移動しました。'
    end

    def down
        move_down(PermalinkField.all.to_a, @permalink_field)

        redirect_to staff_permalink_fields_url, notice: 'Parmlink 項目を移動しました。'
    end

    def edit_default_setting
    end

    def update_default_setting
        if @system_setting.update(update_default_setting_params)
            redirect_to staff_permalink_fields_url
        else
            render action: 'edit_default_setting'
        end
    end

private

    def check_permalink_field
        @permalink_field = PermalinkField.find(params[:id])
    end

    def request_params
        params.require(:permalink_field).permit(:caption_ja, :caption_en, :field_def_id)
    end

    def update_default_setting_params
        params.require(:system_setting).permit(:permalink_caption_ja, :permalink_caption_en)
    end

end
