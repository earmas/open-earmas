#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ListFieldDefsController < BaseAdminController
    include OrderHolder

    maintenance_data_lock_methods :create, :update, :destroy, :up, :down

    before_action :check_mentenance_mode
    before_action :check_list_def_exists
    before_action :check_list_field_def_exists, except: [:index, :new, :create]

    def new
        @list_field_def = ListFieldDef.new
        @list_field_def.set_default_child
    end

    def create
        @list_field_def = ListFieldDef.new(request_params)
        @list_field_def.list_def_id = @list_def.id
        @list_field_def.sort_index = ListFieldDef.where(list_def_id: @list_def.id).maximum(:sort_index)
        @list_field_def.sort_index = @list_field_def.sort_index && @list_field_def.sort_index + 1 || 1

        if ListFieldDef.where(list_def_id: @list_def.id).size >= 5
            @list_field_def.errors.add(:base, "階層は5個までです")
            return render action: 'new'
        end

        create_action_block('new', 'list_field_def', 'リスト階層の作成に失敗しました。') do
            if @list_field_def.save
                lock_list(@list_def.id)
                lock_pri_list(@list_def.id)

                if ListFieldDef.where(list_def_id: @list_def.id).size > 5
                    raise DataCheckException, '階層は5個までです'
                end

                redirect_to staff_list_def_list_field_def_url(@list_def, @list_field_def), notice: 'リスト階層を作成しました。'
            else
                render action: 'new'
            end
        end
    end

    def show
    end

    def edit
        @list_field_def.set_default_child
    end

    def update
        @list_field_def.assign_attributes(request_params)
        update_action_block('edit', 'list_field_def', 'リスト階層の更新に失敗しました。') do
            if lock_list_update?
                lock_list(@list_def.id)
                lock_pri_list(@list_def.id)
            end

            if @list_field_def.save
                redirect_to staff_list_def_list_field_def_url(@list_def, @list_field_def), notice: 'リスト階層を更新しました。'
            else
                render action: 'edit'
            end
        end
    end

    def destroy
        redirect_block(staff_list_def_url(@list_def), 'list_field_def', 'リスト階層の削除に失敗しました。') do
            @list_field_def.destroy!
            lock_list(@list_def.id)
            lock_pri_list(@list_def.id)
            redirect_to staff_list_def_url(@list_def), notice: 'リスト階層を削除しました。'
        end
    end

    def up
        ActiveRecord::Base.transaction do
            move_up_no_transaction(@list_def.list_fields.to_a, @list_field_def)
            lock_list(@list_def.id)
            lock_pri_list(@list_def.id)
        end

        redirect_to staff_list_def_url(@list_def), notice: 'リスト階層を移動しました。'
    end

    def down
        ActiveRecord::Base.transaction do
            move_down_no_transaction(@list_def.list_fields.to_a, @list_field_def)
            lock_list(@list_def.id)
            lock_pri_list(@list_def.id)
        end

        redirect_to staff_list_def_url(@list_def), notice: 'リスト階層を移動しました。'
    end

private

    def lock_list_update?
        @list_field_def.field_def_id_changed? || @list_field_def.sort_type_changed?
    end

    def check_list_def_exists
        @list_def = ListDef.find(params[:list_def_id])
    end

    def check_list_field_def_exists
        @list_field_def = @list_def.list_fields.find(params[:id])
    end

    def request_params
        params.require(:list_field_def).permit(
            :field_def_id, :list_field_type, :sort_type, :sort_reverse,
            :list_param_view_type_id,
            :list_param_sub_header_view_type_id,
            :list_param_sub_footer_view_type_id,
            { list_param_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] })
    end
end
