#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class DocumentTemplatesController < BaseStaffController

    before_action :check_document_template_exists, except: [:index, :new, :create]

    before_action :set_new_document_child,      only: [:new, :create]
    before_action :set_exists_document_child,   only: [:destroy, :edit, :update]


    def index
    end

    def new
    end

    def create
        @document_template.assign_attributes(request_params)
        @document_template.staff_id = current_staff.id

        set_param_to_list_child

        create_action_block('new', 'document_template', 'データテンプレートの作成に失敗しました。') do
            if @document_template.save
                @update_fields.each do |template_field|
                    template_field.document_template_id = @document_template.id
                    template_field.save!
                end

                redirect_to staff_document_templates_url
            else
                render action: 'new'
            end
        end
    end

    def update
        @document_template.assign_attributes(request_params)

        set_param_to_list_child

        update_action_block('edit', 'document_template', 'データテンプレートの更新に失敗しました。') do
            if @document_template.save

                @update_fields.each do |template_field|
                    template_field.save!
                end

                redirect_to staff_document_templates_url
            else
                render action: 'edit'
            end
        end
    end

    def destroy
        if @document_template.destroy
            redirect_to staff_document_templates_url
        else
            @document_template.errors.add(:base, 'データテンプレートの削除に失敗しました。')
            render action: 'edit'
        end
    end

private

    def check_document_template_exists
        @document_template = DocumentTemplate.where(staff_id: current_staff.id).find(params[:id])
    end

    def set_new_document_child
        @document_template = DocumentTemplate.new

        @field_form_value_by_field_def_id_map = {}
        FieldDefLoaderForInput.field_defs.each do |field_def|
            template_field = DocumentTemplateField.new
            template_field.field_def_id = field_def.id
            template_field.staff_id = current_staff.id

            @field_form_value_by_field_def_id_map[field_def.id] = template_field
        end
    end

    def set_exists_document_child
        @field_form_value_by_field_def_id_map = {}

        FieldDefLoaderForInput.field_defs.each do |field_def|
            template_field = @document_template.field_by_field_def_id_map[field_def.id]
            if template_field == nil
                template_field = DocumentTemplateField.new
                template_field.document_template_id = @document_template.id
                template_field.field_def_id = field_def.id
                template_field.staff_id = current_staff.id
            end

            @field_form_value_by_field_def_id_map[field_def.id] = template_field
        end
    end
    
    def set_param_to_list_child
        FieldDefLoaderForInput.field_defs.each do |field_def|
            template_field = @document_template.field_by_field_def_id_map[field_def.id]
            if template_field == nil
                template_field = DocumentTemplateField.new
                template_field.document_template_id = @document_template.id if @document_template.id.present?
                template_field.field_def_id = field_def.id
                template_field.staff_id = current_staff.id
            end

            @field_form_value_by_field_def_id_map[field_def.id] = template_field
        end

        @update_fields = []

        params[:field].keys.each do |field_def_id_string|
            field_param = params[:field][field_def_id_string]
            field_def = FieldDefLoaderForInput.get_field_def(field_def_id_string)
            next if field_def == nil

            template_field = @field_form_value_by_field_def_id_map[field_def.id]
            next if template_field == nil

            field_def.update_pri_field(template_field.get_document_field, field_param)
            template_field.update_by_document_field

            @update_fields << template_field
            @field_form_value_by_field_def_id_map[field_def.id] = template_field
        end
    end

    def request_params
        params.require(:document_template).permit(:name, :description, :publish_to, :input_group_id)
    end

end