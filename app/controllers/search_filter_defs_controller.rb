#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class SearchFilterDefsController < BaseAdminController

    maintenance_data_lock_methods :create, :update, :destroy

    before_action :check_mentenance_mode
    before_action :check_search_filter_def_exists, except: [:index, :new, :create, :param_form]

    def new
        @search_filter_def = SearchFilterDef.new
        @search_filter_def.filter_type = DocumentFilterDef::FILTER_TYPES[:field_value_filter][:id]
    end

    def create
        @search_filter_def = SearchFilterDef.new(request_params)
        @search_filter_def.parent_id = 0

        create_action_block('new', 'search_filter_def', 'フィルタの作成に失敗しました。') do
            if @search_filter_def.save
                lock_search
                lock_pri_search
                redirect_to staff_search_defs_path, notice: 'フィルタを作成しました。'
            else
                render action: 'new'
            end
        end
    end

    def edit
    end

    def update
        @search_filter_def.clear_optional_param
        @search_filter_def.assign_attributes(request_params)

        update_action_block('edit', 'search_filter_def', 'フィルタの更新に失敗しました。') do
            if @search_filter_def.save
                lock_search
                lock_pri_search
                redirect_to staff_search_defs_path, notice: 'フィルタを更新しました。'
            else
                render action: 'edit'
            end
        end
    end

    def destroy
        redirect_block(staff_search_defs_path, 'search_filter_def', 'フィルタの削除に失敗しました。') do
            @search_filter_def.destroy!
            lock_search
            lock_pri_search
            redirect_to staff_search_defs_path, notice: 'フィルタを削除しました。'
        end
    end

    def param_form
        @search_filter_def = SearchFilterDef.new
        @search_filter_def.filter_type = params[:search_filter_def][:filter_type]
    end


private

    def check_search_filter_def_exists
        @search_filter_def = SearchFilterDef.find(params[:id])
    end

    def request_params
        params.require(:search_filter_def).permit(
            :include_filter,
            :filter_type,
            :some_id,
            :filter_value)
    end
end
