#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class CsvController < BaseStaffController
    require 'csv'

    nonmaintenance_data_lock_methods :update, :update_tsv, :update_xml

    before_action :check_not_mentenance_mode, except: [:result]
    before_action :parse_encoding, except: [:result]

    def index
        @created_data_utf_csv_file_name  = get_download_file('created_data_' + Encoding::UTF_8.to_s + col_sep_name(',') + '.csv')
        @created_data_sjis_csv_file_name = get_download_file('created_data_' + Encoding::SJIS.to_s + col_sep_name(',') + '.csv')

        @updated_data_utf_csv_file_name  = get_download_file('updated_data_' + Encoding::UTF_8.to_s + col_sep_name(',') + '.csv')
        @updated_data_sjis_csv_file_name = get_download_file('updated_data_' + Encoding::SJIS.to_s + col_sep_name(',') + '.csv')

        @all_data_utf_csv_file_name  = get_download_file('all_data_' + Encoding::UTF_8.to_s + col_sep_name(',') + '.csv')
        @all_data_sjis_csv_file_name = get_download_file('all_data_' + Encoding::SJIS.to_s + col_sep_name(',') + '.csv')

        @created_data_utf_tsv_file_name  = get_download_file('created_data_' + Encoding::UTF_8.to_s + col_sep_name("\t") + '.csv')
        @created_data_sjis_tsv_file_name = get_download_file('created_data_' + Encoding::SJIS.to_s + col_sep_name("\t") + '.csv')

        @updated_data_utf_tsv_file_name  = get_download_file('updated_data_' + Encoding::UTF_8.to_s + col_sep_name("\t") + '.csv')
        @updated_data_sjis_tsv_file_name = get_download_file('updated_data_' + Encoding::SJIS.to_s + col_sep_name("\t") + '.csv')

        @all_data_utf_tsv_file_name  = get_download_file('all_data_' + Encoding::UTF_8.to_s + col_sep_name("\t") + '.csv')
        @all_data_sjis_tsv_file_name = get_download_file('all_data_' + Encoding::SJIS.to_s + col_sep_name("\t") + '.csv')
    end

    def test
        csv_file = params[:csv_file]

        if csv_file.blank?
           redirect_to staff_csv_url, alert: 'ファイルがありません'
           return
        end

        csv_content = csv_file.read.force_encoding(@encoding).scrub.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => '?')

        test_common(csv_content)
    end

    def update
        begin
            csv_file = params[:csv_file]
            auto_thumnail = params[:auto_thumnail] == 'true'

            if csv_file.blank?
               redirect_to staff_csv_url, alert: 'ファイルがありません'
               return
            end

            csv_content = csv_file.read.force_encoding(@encoding).scrub.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => '?')

            update_common(csv_content, auto_thumnail)

            redirect_to staff_csv_result_url
        rescue Exception => ex
            logger.error ex.message
            logger.error ex.backtrace.join("\n") if ex.backtrace.present?
            redirect_to staff_csv_url, alert: '更新に失敗しました'
        end
    end

    def test_tsv
        tsv_file = params[:tsv_file]

        if tsv_file.blank?
           redirect_to staff_csv_url(file_format: 'tsv'), alert: 'ファイルがありません'
           return
        end

        csv_content = tsv_file.read.force_encoding(@encoding).scrub.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => '?')

        test_common(csv_content, "\t")
    end

    def update_tsv
        begin
            tsv_file = params[:tsv_file]
            auto_thumnail = params[:auto_thumnail] == 'true'

            if tsv_file.blank?
               redirect_to staff_csv_url(file_format: 'tsv'), alert: 'ファイルがありません'
               return
            end

            csv_content = tsv_file.read.force_encoding(@encoding).scrub.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => '?')

            update_common(csv_content, auto_thumnail, "\t")

            redirect_to staff_csv_result_url(file_format: 'tsv')
        rescue Exception => ex
            logger.error ex.message
            logger.error ex.backtrace.join("\n") if ex.backtrace.present?
            redirect_to staff_csv_url(file_format: 'tsv'), alert: '更新に失敗しました'
        end
    end

    def test_xml
        xml_file = params[:xml_file]

        if xml_file.blank?
           redirect_to staff_csv_url(file_format: 'xml'), alert: 'ファイルがありません'
           return
        end

        doc1 = Nokogiri::XML(xml_file)

        xslt1 = Nokogiri::XSLT(@system_setting.upload_xml_string)
        doc2 = xslt1.transform(doc1)

        test_common(doc2.to_xml(encoding: 'UTF-8'))
    end

    def update_xml
        begin
            xml_file = params[:xml_file]
            auto_thumnail = params[:auto_thumnail] == 'true'

            if xml_file.blank?
               redirect_to staff_csv_url(file_format: 'xml'), alert: 'ファイルがありません'
               return
            end

            doc1 = Nokogiri::XML(xml_file)

            xslt1 = Nokogiri::XSLT(@system_setting.upload_xml_string)
            doc2 = xslt1.transform(doc1)

            update_common(doc2.to_xml(encoding: 'UTF-8'), auto_thumnail)

            redirect_to staff_csv_result_url
        rescue Exception => ex
            logger.error ex.message
            logger.error ex.backtrace.join("\n") if ex.backtrace.present?
            redirect_to staff_csv_url, alert: '更新に失敗しました ' + ex.message
        end
    end

    def result
    end

    def generate_created
        generate_created_common
    end

    def generate_updated
        generate_updated_common
    end

    def generate_all
        generate_all_common
    end

    def generate_created_tsv
        generate_created_common("\t")
    end

    def generate_updated_tsv
        generate_updated_common("\t")
    end

    def generate_all_tsv
        generate_all_common("\t")
    end

    def download_created
        download_created_common
    end

    def download_updated
        download_updated_common
    end

    def download_all
        download_all_common
    end

    def download_created_tsv
        download_created_common("\t")
    end

    def download_updated_tsv
        download_updated_common("\t")
    end

    def download_all_tsv
        download_all_common("\t")
    end

private

    def test_common(data, col_sep = ',')
        @documents = []

        begin
            remove_bom(data)

            @line_count = 0
            CSV.parse(data, col_sep: col_sep, row_sep: "\r\n") do |line|
                break if @line_count > 200
                id = line[0]

                document = PriDocument.new
                if id.present?
                    document = PriDocument.where(id: id).first
                    if document == nil
                        document = PriDocument.new
                        document.errors.add(:base, "IDが設定されていましたが、該当するデータが無いため新規で登録されます")
                    end
                end

                document.from_csv(line)

                document.fields_validation_before(document.csv_fields)
                @documents << document
                @line_count += 1
            end

            render 'test'
        rescue Exception => ex
            render 'test', locals:{ notice: ex.message }
        end
    end

    def update_common(csv_content, auto_thumnail = false, col_sep = ',')
        remove_bom(csv_content)

        if Rails.application.config.use_delayed_job
            Delayed::KickDelayedTask.new(current_staff).delay({:run_at => Rails.application.config.delay_time.from_now}).csv_update(csv_content, col_sep, auto_thumnail)
        else
            Delayed::DelayTask.new.csv_update(current_staff, csv_content, col_sep, auto_thumnail)
        end
    end

    def generate_created_common(col_sep = ',')
        file_name = get_download_file('created_data_' + @encoding.to_s + col_sep_name(col_sep) + '.csv')
        if File.exists?(file_name + '.tmp')
            return redirect_to staff_csv_url, notice: '現在生成中です'
        end

        if File.exists?(file_name)
            File.delete(file_name)
        end

        if Rails.application.config.use_delayed_job
            Delayed::KickDelayedTask.new(current_staff).delay({:run_at => Rails.application.config.delay_time.from_now}).csv_download('creator_staff', col_sep, file_name, @encoding)
        else
            Delayed::DelayTask.new.csv_download(current_staff, 'creator_staff', col_sep, file_name, @encoding)
        end

        return redirect_to staff_csv_url, notice: 'ダウンロードデータの生成を開始しました。生成が完了するとダウンロードリンクが表示されます。'
    end

    def download_created_common(col_sep = ',')
        file_name = get_download_file('created_data_' + @encoding.to_s + col_sep_name(col_sep) + '.csv')

        if File.exists?(file_name)
            send_file file_name, type: 'text/csv', filename: 'creator_staff.csv', disposition: "inline"
        else
            redirect_to staff_csv_url, alert: 'ファイルがありません。'
        end
    end

    def generate_updated_common(col_sep = ',')
        file_name = get_download_file('updated_data_' + @encoding.to_s + col_sep_name(col_sep) + '.csv')
        if File.exists?(file_name + '.tmp')
            return redirect_to staff_csv_url, notice: '現在生成中です'
        end

        if File.exists?(file_name)
            File.delete(file_name)
        end

        if Rails.application.config.use_delayed_job
            Delayed::KickDelayedTask.new(current_staff).delay({:run_at => Rails.application.config.delay_time.from_now}).csv_download('updater_staff', col_sep, file_name, @encoding)
        else
            Delayed::DelayTask.new.csv_download(current_staff, 'updater_staff', col_sep, file_name, @encoding)
        end

        return redirect_to staff_csv_url, notice: 'ダウンロードデータの生成を開始しました。生成が完了するとダウンロードリンクが表示されます。'
    end

    def download_updated_common(col_sep = ',')
        file_name = get_download_file('updated_data_' + @encoding.to_s + col_sep_name(col_sep) + '.csv')

        if File.exists?(file_name)
            send_file file_name, type: 'text/csv', filename: 'updated_data.csv', disposition: "inline"
        else
            redirect_to staff_csv_url, alert: 'ファイルがありません。'
        end
    end

    def generate_all_common(col_sep = ',')
        file_name = get_download_file('all_data_' + @encoding.to_s + col_sep_name(col_sep) + '.csv')
        if File.exists?(file_name + '.tmp')
            return redirect_to staff_csv_url, notice: '現在生成中です'
        end

        if File.exists?(file_name)
            File.delete(file_name)
        end

        if Rails.application.config.use_delayed_job
            Delayed::KickDelayedTask.new(current_staff).delay({:run_at => Rails.application.config.delay_time.from_now}).csv_download(nil, col_sep, file_name, @encoding)
        else
            Delayed::DelayTask.new.csv_download(current_staff, nil, col_sep, file_name, @encoding)
        end

        return redirect_to staff_csv_url, notice: 'ダウンロードデータの生成を開始しました。生成が完了するとダウンロードリンクが表示されます。'
    end

    def download_all_common(col_sep = ',')
        file_name = get_download_file('all_data_' + @encoding.to_s + col_sep_name(col_sep) + '.csv')

        if File.exists?(file_name)
            send_file file_name, type: 'text/csv', filename: 'all_data.csv', disposition: "inline"
        else
            redirect_to staff_csv_url, alert: 'ファイルがありません。'
        end
    end


    def parse_encoding
        case params[:encoding]
        when 's'
            @encoding = Encoding::SJIS
        else
            @encoding = Encoding::UTF_8
        end
    end

    def remove_bom(data)
        data.gsub!("\xEF\xBB\xBF".force_encoding("UTF-8"), '')
    end


    def get_download_file(file_name)
        file_dir = File.join(Rails.application.config.private_file_store_dir, "static", "download_data", current_staff.id.to_s)
        FileUtils.mkdir_p(file_dir)
        file_name = File.join(file_dir, file_name)
        file_name
    end

    def col_sep_name(col_sep)
        if col_sep == ','
            '_comma'
        else
            '_tab'
        end
    end
end
