#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class PeopleController < BaseAdminController
    include FieldLock

    maintenance_data_lock_methods :create, :update, :destroy

    skip_before_action :check_admin_role,   only: [:search]

    before_action :check_mentenance_mode, except: [:search]
    before_action :check_person_exists, except: [:index, :new, :create, :search, :download, :test, :upload]

    def index
        @people = Person.all.page(params[:page]).per(100)
    end

    def show
    end

    def new
        @person = Person.new
    end

    def edit
    end

    def create
        @person = Person.new(request_params)
        @person.set_encrypted_id(@people_setting)

        if Person.where(id: @person.id).size != 0
            @person.errors.add(:id, "IDが重複しています")
            return render action: 'new'
        end

        if @person.save
            redirect_to staff_person_url(@person), notice: '人物を作成しました。'
        else
            render action: 'new'
        end
    end

    def update
        @person.assign_attributes(request_params)
        @person.set_encrypted_id(@people_setting)

        update_action_block('edit', 'person', '人物の更新に失敗しました。') do
            touch_use_data
            if @person.save
                redirect_to staff_person_url(@person), notice: '人物を更新しました。'
            else
                render action: 'edit'
            end
        end
    end

    def destroy
        redirect_block(staff_person_url(@person), 'person', '人物の削除に失敗しました。') do
            touch_use_data
            @person.destroy!
            redirect_to staff_people_url, notice: '人物を削除しました。'
        end
    end

    def search
        people = Person.all
        people.where!(id: params[:id]) if params[:id].present?
        people.where!("family_name_ja LIKE ?", "#{params[:family_name_ja]}%") if params[:family_name_ja].present?
        people.where!("given_name_ja LIKE ?", "#{params[:given_name_ja]}%") if params[:given_name_ja].present?
        people.where!("family_name_en LIKE ?", "#{params[:family_name_en]}%") if params[:family_name_en].present?
        people.where!("given_name_en LIKE ?", "#{params[:given_name_en]}%") if params[:given_name_en].present?
        people.where!("family_name_tra LIKE ?", "#{params[:family_name_tra]}%") if params[:family_name_tra].present?
        people.where!("given_name_tra LIKE ?", "#{params[:given_name_tra]}%") if params[:given_name_tra].present?
        people.limit!(5)

        html = render_to_string partial: 'people/search', locals: { people: people }
        render json: {
            result: 'success',
            html: html,
        }
    end

    def download
        require 'csv'
        parse_encoding

        csv_data = CSV.generate do |csv|
            Person.all.each do |person|
                csv << person.to_csv
            end 
        end 

        headers["Content-Type"] = 'text/csv;charset=' + @encoding.name
        send_data csv_data.encode(@encoding, :invalid => :replace, :undef => :replace, :replace => '?'), :type => 'text/csv', :filename => 'person.csv'
    end

    def test
        require 'csv'
        parse_encoding

        csv_file = params[:csv_file]

        if csv_file.blank?
           redirect_to staff_people_url, notice: 'ファイルがありません'
           return
        end

        csv_content = csv_file.read.force_encoding(@encoding).scrub.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => '?')

        @people = []
        redirect_block(staff_people_url, 'person', '人物の更新に失敗しました。') do
            CSV.parse(csv_content) do |line|
                person = Person.from_csv(@people_setting, line)
                next if person == nil
                @people << person
            end
        end
    end

    def upload
        require 'csv'
        parse_encoding

        csv_file = params[:csv_file]

        if csv_file.blank?
           redirect_to staff_people_url, notice: 'ファイルがありません'
           return
        end

        csv_content = csv_file.read.force_encoding(@encoding).scrub.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => '?')

        redirect_block(staff_people_url, 'person', '人物の更新に失敗しました。') do
            CSV.parse(csv_content) do |line|
                person = Person.from_csv(@people_setting, line)
                next if person == nil
                person.save!
            end

            field_def_ids = FieldDef.where(field_type_id: FieldTypes::RepositoryPerson::TYPE_ID).pluck(:id)
            set_lock_pri_field(field_def_ids)

            redirect_to staff_people_url, notice: '更新しました'
        end
    end

private

    def check_person_exists
        @person = Person.find(params[:id])
    end

    def touch_use_data
        return if lock_field_update? == false

        field_def_ids = []
        FieldDef.where(field_type_id: FieldTypes::RepositoryPerson::TYPE_ID).each do |field_def|
            next if field_def.field_type_class.get_select_value_exists(field_def, @person) == false

            field_def_ids << field_def.id
            document_ids = field_def.field_type_class.get_select_value_relation(field_def, @person).pluck(:document_id)
            document_ids.each_slice(100) do |sub_document_ids|
                PriDocument.unscoped.where(deleted: false).where(id: sub_document_ids).update_all(updated_at: PriDocument.get_now)
                PriDocument.where(id: sub_document_ids).update_all(meta_tag_xml: nil)
            end
        end

        set_lock_pri_field(field_def_ids)
    end

    def lock_field_update?
        @person.family_name_ja_changed? || @person.family_name_en_changed? || @person.family_name_tra_changed? ||
        @person.given_name_ja_changed? || @person.given_name_en_changed? || @person.given_name_tra_changed? ||
        @person.family_name_alt1_changed? || @person.family_name_alt2_changed? || @person.family_name_alt3_changed? ||
        @person.given_name_alt1_changed? || @person.given_name_alt2_changed? || @person.given_name_alt3_changed? ||
        @person.affiliation_ja_changed? || @person.affiliation_en_changed? || @person.id_changed?
    end

    def request_params
        params.require(:person).permit(
            :family_name_ja, :family_name_en, :family_name_tra,
            :given_name_ja, :given_name_en, :given_name_tra,
            :family_name_alt1, :family_name_alt2, :family_name_alt3,
            :given_name_alt1, :given_name_alt2, :given_name_alt3,
            :affiliation_ja, :affiliation_en,
            :id, :enable_gen_url, :url, :key1, :key2, :key3, :key4, :key5, :key6, :key7, :key8, :key9, :key10)
    end

    def parse_encoding
        @encoding = Encoding::UTF_8
    end
end
