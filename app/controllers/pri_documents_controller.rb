#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class PriDocumentsController < BaseStaffController
    include SortablePage

    before_action :check_not_mentenance_mode, except: [:index, :show, :thumnail]

    before_action :check_document_exists, except: [:index, :search, :new, :create]

    before_action :set_document_template,       only: [:new]
    before_action :set_document_attach_files,   only: [:edit, :update, :create_attach_file, :destroy_attach_file]
    before_action :set_new_document_child,      only: [:new, :create]

    before_action :set_exists_document_child,   only: [:undelete, :destroy, :edit, :update]

    def index
        @download_id  = params[:download_id]

        @search_term_exists = false
        search_term_map = {}
        field_index = 1
        SearchDef.all.reorder(:id).each do |search|
            search_term_map[field_index] = params[search.id.to_s] if params[search.id.to_s].present?
            field_index += 1
            @search_term_exists = true if params[search.id.to_s].present?
        end

        params[:sort] ||= 'updated_at:r'
        set_sort_param

        result = PriDocument.search do |s|
            if params[:all].present?
                if params[:include_file] != 'include'
                    s.fulltext params[:all] do
                        fields('text_value_all')
                    end
                else
                    s.fulltext params[:all] do
                        fields('text_value_all_with_file')
                    end
                end
            end

            if params[:updater].present?
                s.with :updater_staff_id, current_staff.id
            end

            if params[:creator].present?
                s.with :creator_staff_id, current_staff.id
            end

            if params[:publish_to].present?
                s.with :publish_to, params[:publish_to]
            end

            field_index = 1
            search_term_map.keys.sort.each do |index|
                search_term = search_term_map[index]
                if search_term.present?
                    s.fulltext search_term do
                        fields('text_value_' + index.to_s)
                    end
                end
            end

            if @sort_reverse
                s.order_by(get_sort_column_name, :desc)
            else
                s.order_by(get_sort_column_name)
            end

            s.paginate page: params[:page], per_page: Kaminari.config.default_per_page
        end

        @result = result.hits
        @documents = result.results
        @count = result.total

        if @download_id.present?
            download(@download_id)
            return
        end
    end

    def search
        search_param = {}

        search_param[:all] = params[:all] if params[:all].present?
        search_param[:include_file] = params[:include_file] if params[:include_file].present?
        search_param[:updater] = params[:updater] if params[:updater].present?
        search_param[:creator] = params[:creator] if params[:creator].present?
        search_param[:publish_to] = params[:publish_to] if params[:publish_to].present?
        SearchDef.all.reorder(:id).each do |search|
            search_param[search.id.to_s] = params[search.id.to_s] if params[search.id.to_s].present?
        end

        redirect_to staff_path(search_param)
    end

    def thumnail
        if @document.has_thumnail?
            send_file @document.thumnail_path, type: 'image/png', filename: 'thumnail', disposition: "inline"
        else
            render text: 'not found', status: 404
        end
    end

    def new
    end

    def create
        @document.assign_attributes(request_params)

        set_param_to_list_child

        @document.updater_staff_id = current_staff.id
        @document.creator_staff_id = current_staff.id

        return render action: 'new' if @document.fields_validation_before(@append_fields) == false

        remove_blank_field
        messages = []

        create_action_block('new', 'document', 'データの作成に失敗しました。') do
            @document.check_oaipmh(@append_fields)
            @document.set_embargo_until(@append_fields)
            @document.save!
            @document.set_self_doi(@oaipmh_setting, @append_fields)

            @append_fields.each do |field|
                field.document_id = @document.id
                field.save!
                messages << field.saved_message
            end

            @document.fields_validation_after(@append_fields)

            @document.save_upload_thumnail_file

            @document.set_search_data(@append_fields)
            @document.set_list_data(@append_fields)
            @document.set_ej_data(@append_fields)

            @document.index!

            message_string = messages.delete_if{|a|a.blank?}.join("\n")
            redirect_to staff_pri_document_url(@document.id), notice: message_string.blank? ? nil : message_string
        end
    end

    def show
    end

    def edit
    end

    def update
        @document.assign_attributes(request_params)
        set_param_to_list_child

        @document.updater_staff_id = current_staff.id

        return render action: 'edit' if @document.fields_validation_before(@append_fields + @update_fields) == false

        remove_blank_field
        messages = []

        update_action_block('edit', 'document', 'データの更新に失敗しました。') do
            @document.check_oaipmh(@update_fields + @append_fields)
            @document.set_embargo_until(@update_fields + @append_fields)

            @document.clear_meta_tag_xml
            if @document.changed?
                @document.save!
            else
                @document.touch
            end
            @document.set_self_doi(@oaipmh_setting, @update_fields + @append_fields)

            @document.save_upload_thumnail_file

            @delete_fields.each do |field|
                field.destroy!
            end
            @append_fields.each do |field|
                field.document_id = @document.id
                field.save!
                messages << field.saved_message
            end
            @update_fields.each do |field|
                field.save!
                messages << field.saved_message
            end

            @document.fields_validation_after(@append_fields + @update_fields)

            @document.set_search_data(@update_fields + @append_fields)
            @document.set_list_data(@update_fields + @append_fields)
            @document.set_ej_data(@update_fields + @append_fields)

            @document.index!

            message_string = messages.delete_if{|a|a.blank?}.join("\n")
            redirect_to staff_pri_document_url(@document.id), notice: message_string.blank? ? nil : message_string
        end
    end

    def destroy
        if PubDocument.where(id: @document.id).size == 0

            update_action_block('edit', 'document', 'データの削除に失敗しました。') do
                Hook::Document.on_delete(@document)
                @document.destroy!
                @document.remove_from_index

                redirect_to staff_url
            end
        else
            update_action_block('edit', 'document', 'データの削除に失敗しました。') do
                @document.deleted = true
                @document.save!

                redirect_to staff_url
            end
        end
    end

    def undelete
        update_action_block('edit', 'document', 'データの復活に失敗しました。') do
            @document.deleted = false
            @document.save!

            redirect_to staff_url
        end
    end

    def change_thumnail
        @field_def = FieldDefLoaderForInput.get_field_def(params[:field_def_id])
        @pri_field_file = @field_def.get_pri_type_class.data_class.where(document_id: @document.id, id: params[:field_id]).first

        @document.set_thumnail(@pri_field_file)

        redirect_to staff_pri_document_url(@document.id)
    end

    def destroy_thumnail
        @document.delete_thumnail_file

        redirect_to staff_pri_document_url(@document.id)
    end

    def create_coverpage
        @field_def = FieldDefLoaderForInput.get_field_def(params[:field_def_id])
        @pri_field_file = @field_def.get_pri_type_class.data_class.where(document_id: @document.id, id: params[:field_id]).first

        @documents = [@document]
        document_xml_string = render_to_string 'view_types/result/documents', formats: [:xml]
        @messge_string = 'カバーページの作成に失敗しました。' if @pri_field_file.create_coverpage(document_xml_string) == false


        redirect_to staff_pri_document_url(@document.id), notice: @messge_string
    end

    def destroy_coverpage
        @field_def = FieldDefLoaderForInput.get_field_def(params[:field_def_id])
        @pri_field_file = @field_def.get_pri_type_class.data_class.where(document_id: @document.id, id: params[:field_id]).first

        @messge_string = 'カバーページの削除に失敗しました。' if @pri_field_file.delete_coverpage == false

        redirect_to staff_pri_document_url(@document.id), notice: @messge_string
    end


    def create_attach_file
        @upload_file = params[:upload_file]

        begin
            save_upload_attach_file

            render partial: 'upload_success'
        rescue => ex
            logger.error ex.message
            logger.error ex.backtrace.join("\n") if ex.backtrace.present?

            render partial: 'upload_fail', locals: { message: 'ファイルの追加に失敗しました。' }
        end
    end

    def destroy_attach_file
        destory_file_name = params[:name]

        begin
            Dir::glob(File.join(@current_dir, '*')).each do |static_file|
                if destory_file_name == File.basename(static_file)
                    File::delete(static_file) if File.exists?(static_file)
                    break;
                end
            end

            render json: {
                result: 'success',
            }
        rescue => ex
            logger.error ex.message
            logger.error ex.backtrace.join("\n") if ex.backtrace.present?

            render json: {
                result: 'fail',
                message: 'ファイルの削除に失敗しました。',
            }
        end
    end

private

    def check_document_exists
        @document = PriDocument.unscoped.find(params[:id])
    end

    def set_document_template
        @document_template = DocumentTemplate.where(id: params[:template_id]).first
    end

    def set_document_attach_files
        @current_dir = @document.document_attach_file_dir
    end

    def set_new_document_child
        @document = PriDocument.new
        if @document_template != nil
            @document = @document_template.get_document
        end

        @field_form_value_by_field_def_id_map = {}
        FieldDefLoaderForInput.field_defs.each do |field_def|
            tmpl_field = field_def.default_pri_field

            if @document_template != nil
                template_field = @document_template.field_by_field_def_id_map[field_def.id]
                tmpl_field = template_field.get_document_field if template_field.present?
            end

            @field_form_value_by_field_def_id_map[field_def.id] = { value_map: nil, values: [tmpl_field], tmpl: tmpl_field }
        end
    end

    def set_exists_document_child
        @field_form_value_by_field_def_id_map = {}

        FieldDefLoaderForInput.field_defs.each do |field_def|
            fields = @document.get_fields(field_def.id)
            tmpl_field = field_def.default_pri_field

            @field_form_value_by_field_def_id_map[field_def.id] = { value_map: nil, values: fields.blank? ? [tmpl_field] : fields, tmpl: tmpl_field }
        end
    end

    def set_param_to_list_child
        FieldDefLoaderForInput.field_defs.each do |field_def|
            field_map = @document.fields_by_id_by_field_def_id_map[field_def.id]
            @field_form_value_by_field_def_id_map[field_def.id][:value_map] = (field_map.blank? ? {} : field_map)
            @field_form_value_by_field_def_id_map[field_def.id][:values] = []
        end

        @append_fields = []
        @delete_rc_fields = {}
        @delete_fields = []
        @update_fields = []

        params[:field].keys.each do |field_def_id_string|
            sort_index = 1
            field_def = FieldDefLoaderForInput.get_field_def(field_def_id_string)
            next if field_def == nil

            params[:field][field_def_id_string].keys.each do |field_id_string|
                field_param = params[:field][field_def_id_string][field_id_string]

                if field_id_string.start_with?("new_")
                    field = field_def.new_pri_field(field_param)
                    field.sort_index = sort_index

                    @append_fields << field
                    @field_form_value_by_field_def_id_map[field_def.id][:values] << field
                else
                    field_id = field_id_string.to_i
                    field = @field_form_value_by_field_def_id_map[field_def.id][:value_map][field_id]
                    next if field == nil

                    field_def.update_pri_field(field, field_param)
                    field.sort_index = sort_index

                    if field.deleted == 'true' || field.blank_value?
                        @delete_rc_fields[field_def.id] ||= { field_def: field_def, values: [] }
                        @delete_rc_fields[field_def.id][:values] << field
                    else
                        @update_fields << field
                    end
                    @field_form_value_by_field_def_id_map[field_def.id][:values] << field
                end
                sort_index += 1
            end
        end

        @delete_rc_fields.values.each do |delete_rc_field|
            field_def = delete_rc_field[:field_def]

            delete_all = @field_form_value_by_field_def_id_map[field_def.id][:values].size == delete_rc_field[:values].size

            if @document.must_field?(field_def) && delete_all

                first_empty = delete_rc_field[:values].find{|a| a.deleted != 'true' }
                if first_empty == nil
                    @update_fields += delete_rc_field[:values].take(1)
                    @delete_fields += delete_rc_field[:values].drop(1)
                else
                    @update_fields << first_empty
                    @delete_fields += delete_rc_field[:values].reject{|a| a == first_empty }
                end
            else
                @delete_fields += delete_rc_field[:values]
            end
        end
    end

    def remove_blank_field
        @append_fields.reject!{|a| a.blank_value? }
    end

    def save_upload_attach_file
        return if @upload_file.blank?

        local_file_path = File.join(@current_dir, @upload_file.original_filename)

        File.delete(local_file_path) if File.exists?(local_file_path)

        if @upload_file.size > 10.megabyte
            raise DataCheckException, 'ファイルのサイズは最大で10MBです。'
        end

        f = File.open(local_file_path, "wb")
        f.write(@upload_file.read)
        f.close
    end

    def request_params
        params.require(:pri_document).permit(:publish_to, :input_group_id, :upload_thumnail_file, :upload_thumnail_file_delete, :self_doi_type,
            :self_doi_jalc_manu_input, :self_doi_jalc_auto_input, :self_doi_crossref_auto_input)
    end

end
