#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class SearchFieldDefsController < BaseAdminController

    maintenance_data_lock_methods :create, :update, :destroy

    before_action :check_mentenance_mode
    before_action :check_search_def_exists
    before_action :check_search_field_def_exists, except: [:index, :new, :create]

    def new
        @search_field_def = SearchFieldDef.new
    end

    def create
        @search_field_def = SearchFieldDef.new(request_params)
        @search_field_def.search_def_id = @search_def.id

        create_action_block('new', 'search_field_def', '検索対象項目の作成に失敗しました。') do
            if @search_field_def.save
                lock_search
                lock_pri_search
                redirect_to staff_search_def_url(@search_def), notice: '検索対象項目を作成しました。'
            else
                render action: 'new'
            end
        end
    end

    def edit
    end

    def update
        @search_field_def.assign_attributes(request_params)

        update_action_block('edit', 'search_field_def', '検索対象項目の更新に失敗しました。') do
            if @search_field_def.save
                lock_search
                lock_pri_search
                redirect_to staff_search_def_url(@search_def), notice: '検索対象項目を更新しました。'
            else
                render action: 'edit'
            end
        end
    end

    def destroy
        redirect_block(staff_search_def_url(@search_def), 'search_field_def', '検索対象項目の削除に失敗しました。') do
            @search_field_def.destroy!
            lock_search
            lock_pri_search
            redirect_to staff_search_def_url(@search_def), notice: '検索対象項目を削除しました。'
        end
    end

private

    def check_search_def_exists
        @search_def = SearchDef.find(params[:search_def_id])
    end

    def check_search_field_def_exists
        @search_field_def = @search_def.fields.find(params[:id])
    end

    def request_params
        params.require(:search_field_def).permit(:field_def_id)
    end
end
