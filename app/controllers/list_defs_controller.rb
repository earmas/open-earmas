#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ListDefsController < BaseAdminController

    maintenance_data_lock_methods :create, :update, :destroy

    before_action :check_mentenance_mode
    before_action :check_list_def_exists, except: [:index, :new, :create]

    def new
        @list_def = ListDef.new
        @list_def.set_default_child
        @list_def.set_default_value
    end

    def create
        @list_def = ListDef.new(request_params)
        create_action_block('new', 'list_def', 'リストの作成に失敗しました。') do
            if @list_def.save
                lock_list(@list_def.id)
                lock_pri_list(@list_def.id)
                redirect_to staff_list_def_url(@list_def), notice: 'リストを作成しました。'
            else
                render action: 'new'
            end
        end
    end

    def show
        @list_field_defs = @list_def.list_fields
        @list_filter_defs = @list_def.filter_fields
        @list_result_fields = @list_def.result_fields
    end

    def edit
        @list_def.set_default_child
        @list_def.set_default_value
    end

    def update
        @list_def.assign_attributes(request_params)

        if @list_def.save
#            Rails.cache.clear
            redirect_to staff_list_def_url(@list_def), notice: 'リストを更新しました。'
        else
            render action: 'edit'
        end
    end

    def destroy
        redirect_block(staff_list_def_url(@list_def), 'list_def', 'リストの削除に失敗しました。') do
            check_use_data
            @list_def.destroy
            redirect_to staff_list_defs_url, notice: 'リストを削除しました。'
        end
    end

private

    def check_use_data
        if LeftMenuDef.where(menu_type: LeftMenuDef::TYPE_LIST_DEF, menu_id: @list_def.id).count != 0
            raise DataCheckException, "左メニューで使用中です" 
        end

        if SystemSetting.where(top_page_type: LeftMenuDef::TYPE_LIST_DEF, top_page_id: @list_def.id).count != 0
            raise DataCheckException, "トップページで使用中です" 
        end
    end

    def check_list_def_exists
        @list_def = ListDef.includes(:result_html_set_def, :detail_html_set_def).find(params[:id])
    end

    def request_params
        params.require(:list_def).permit(
            :code,
            :visible,
            :show_others_link_type,
            :caption_ja,
            :caption_en,
            :result_view_type_id,
            :result_sub_header_view_type_id,
            :result_sub_footer_view_type_id,
            { result_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            :detail_view_type_id,
            :detail_sub_header_view_type_id,
            :detail_sub_footer_view_type_id,
            { detail_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            :sortable_field_id,
            :sort_reverse,
            :rss_xml_string)
    end
end
