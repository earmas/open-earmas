#--
# Open Earmas
#
# Copyright (C) 2014 Hiroshima University, ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class EjDetailFieldsController < BaseAdminController
    include OrderHolder

    maintenance_data_lock_methods :create, :update, :destroy, :up, :down

    before_action :check_mentenance_mode
    before_action :check_ej_def_exists
    before_action :check_ej_detail_field, except: [:index, :new, :create]

    def new
        @ej_detail_field = EjDetailField.new
    end

    def edit
    end

    def create
        @ej_detail_field = EjDetailField.new(request_params)
        @ej_detail_field.ej_def_id = @ej_def.id
        @ej_detail_field.sort_index = EjDetailField.where(ej_def_id: @ej_def.id).maximum(:sort_index)
        @ej_detail_field.sort_index = @ej_detail_field.sort_index && @ej_detail_field.sort_index + 1 || 1

        if @ej_detail_field.save
            redirect_to staff_ej_def_url(@ej_def), notice: '詳細画面での表示項目を作成しました。'
        else
            render action: 'new'
        end
    end

    def update
        @ej_detail_field.assign_attributes(request_params)

        if @ej_detail_field.save
            redirect_to staff_ej_def_url(@ej_def), notice: '詳細画面での表示項目を更新しました。'
        else
            render action: 'edit'
        end
    end

    def destroy
        @ej_detail_field.destroy
        redirect_to staff_ej_def_url(@ej_def), notice: '詳細画面での表示項目を削除しました。'
    end

    def up
        move_up(@ej_def.detail_fields.to_a, @ej_detail_field)

        redirect_to staff_ej_def_url(@ej_def), notice: '詳細画面での表示項目を移動しました。'
    end

    def down
        move_down(@ej_def.detail_fields.to_a, @ej_detail_field)

        redirect_to staff_ej_def_url(@ej_def), notice: '詳細画面での表示項目を移動しました。'
    end

private

    def check_ej_detail_field
        @ej_detail_field = EjDetailField.find(params[:id])
    end

    def check_ej_def_exists
        @ej_def = EjDef.find(params[:ej_def_id])
    end

    def request_params
        params.require(:ej_detail_field).permit(:ej_defs_id, :ja_field_def_id, :en_field_def_id, :en_instead_ja, :ja_instead_en)
    end
end
