#--
# Open Earmas
#
# Copyright (C) 2014 Hiroshima University, ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Pri::JournalsController < BasePriController
    include JournalPage

    before_action :check_ej_journal_def_exists, only: [:journal, :voliss, :article, :all_index, :static_page]
    before_action :check_ej_word_exists, only: [:voliss, :article]
    before_action :set_toc,              only: [:journal, :voliss]
    before_action :check_document_exists, only: [:article]
    before_action :check_static_page_exists, only: [:static_page]

    before_action :param_init
    before_action :set_page_obj

    layout 'journals'
end
