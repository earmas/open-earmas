#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class AuthersMailsController < BaseAdminController

    def index
        @authers_file = File.join(Rails.application.config.private_static_file_dir, 'count_mail', 'authers.csv')
        @mail_text_file = File.join(Rails.application.config.private_static_file_dir, 'count_mail', 'mail_text.txt')
    end

    def show
        types = {
            title_ja_download: 'タイトル(ダウンロード)',
            title_ja_access: 'タイトル(アクセス)',
        }

        mail_text_file = File.join(Rails.application.config.private_static_file_dir, 'count_mail', 'mail_text.txt')

        mail_text = File.read(mail_text_file)
        mail_text_array = mail_text.split("\n", 3)
        p from_addr = mail_text_array[0]
        title = mail_text_array[1]
        mail_body_tmpl = mail_text_array[2]

        download_count_text = "\n"

        today = Time.zone.now.yesterday


        day = today.months_ago(3)
        month = day.strftime("%Y-%m")


        3.times do
            download_count_text << "------------------- " + month + " -------------------\n\n"
            log_data = Logs::LogCountData.new.load_person_log(params[:id], month)
            log_data.build_title_map

            types.each do |name, value|
                download_count_text << "\n" + value + "\n\n"
                log_data.get_count_map(name).each do |k, v|
                    download_count_text << v.to_s + "\t" + decode_url(k) + "\n"
                end
            end
            download_count_text << "\n"

            day = day.next_month
            month = day.strftime("%Y-%m")
        end
        download_count_text << "------------------- ここまで -------------------\n"


        @download_count_text = download_count_text
        @mail_text_array = mail_text_array
    end

    def send_mail
        begin
            if Rails.application.config.use_delayed_job
                Delayed::KickDelayedTask.new(current_staff).delay({:run_at => Rails.application.config.delay_time.from_now}).send_authers_mail
            else
                load 'lib/tasks/log/send_download_count_mail.rb'
            end

            redirect_to staff_authers_mails_path, notice: '送信しました'
        rescue => ex
            redirect_to staff_authers_mails_path, notice: '送信に失敗しました'
        end
    end

private

    def decode_url(value)
        begin
            return URI.decode(value).scrub
        rescue => ex
            return value
        end
    end

end
