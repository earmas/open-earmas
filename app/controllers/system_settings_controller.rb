#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class SystemSettingsController < BaseAdminController

    maintenance_data_lock_methods :update, :update_top_page

    before_action :check_mentenance_mode, except: [:index, :maintenance_mode_on]

    def index
    end

    def edit
        @system_setting.set_default_value
    end

    def update
        @system_setting.assign_attributes(system_setting_params)

        update_action_block('edit', 'system_setting', 'システム設定の更新に失敗しました。') do
            need_lock = @system_setting.enable_oaipmh_changed? && @system_setting.enable_oaipmh == true
            need_meta_tag_xml_clear = @system_setting.meta_info_xml_string_changed?

            if @system_setting.save
                lock_pri_oaipmh if need_lock
                if need_meta_tag_xml_clear
                    PriDocument.update_all(meta_tag_xml: nil)
                    PubDocument.update_all(meta_tag_xml: nil)
                end
                redirect_to staff_system_settings_url, notice: 'システム設定を更新しました。'
            else
                render action: 'edit'
            end
        end
    end

    def maintenance_mode_on
        if set_maintenance_mode
            redirect_to staff_system_settings_url
        else
            redirect_to staff_system_settings_url, alert: '他の処理がロックしています。'
        end
    end

    def maintenance_mode_off
        return show_invalid if check_any_lock
        return if set_maintenance_data_lock == false

        begin
            unset_maintenance_mode
        rescue => ex
            Delayed::Worker.logger.error ex.message
        ensure
            unset_data_lock
        end

        redirect_to staff_system_settings_url
    end

    def pub_reindex
        return show_invalid if @system_setting.pub_locking? == false
        return if set_maintenance_data_lock == false

        if Rails.application.config.use_delayed_job
            Delayed::KickDelayedTask.new(current_staff).delay({:run_at => Rails.application.config.delay_time.from_now}).pub_reindex
        else
            Delayed::DelayTask.new.pub_reindex(current_staff)
        end

        redirect_to staff_system_settings_url
    end

    def pri_reindex
        return show_invalid if @system_setting.pri_locking? == false
        return if set_maintenance_data_lock == false

        if Rails.application.config.use_delayed_job
            Delayed::KickDelayedTask.new(current_staff).delay({:run_at => Rails.application.config.delay_time.from_now}).pri_reindex
        else
            Delayed::DelayTask.new.pri_reindex(current_staff)
        end

        redirect_to staff_system_settings_url
    end

    def clear_cache
        return if set_maintenance_data_lock == false

        if Rails.application.config.use_delayed_job
            Delayed::KickDelayedTask.new(current_staff).delay({:run_at => Rails.application.config.delay_time.from_now}).clear_cache
        else
            Delayed::DelayTask.new.clear_cache(current_staff)
        end

        redirect_to staff_system_settings_url
    end

private

    def system_setting_params
        params.require(:system_setting).permit(:enable_oaipmh, :enable_coverpage, :id_conv_format, :id_conv_pattern, :meta_info_xml_string, :upload_xml_string, :search_api_xml_string)
    end

end
