#--
# Open Earmas
#
# Copyright (C) 2014 Hiroshima University, ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class EjJournalDefsController < BaseAdminController
    include OrderHolder, ViewStateHelper, LocaleHelper

    maintenance_data_lock_methods :create, :update, :destroy

    before_action :check_mentenance_mode
    before_action :check_ej_def_exists
    before_action :check_ej_journal_def_exists, except: [:index, :new, :create]

    def new
        @ej_journal_def = EjJournalDef.new
        @ej_journal_def.set_default_child
    end

    def create
        @ej_journal_def = EjJournalDef.new(request_params)
        @ej_journal_def.ej_def_id = @ej_def.id

        if @ej_journal_def.save
            redirect_to staff_ej_def_ej_journal_def_url(@ej_def, @ej_journal_def), notice: '各雑誌を作成しました。'
        else
            render action: 'new'
        end
    end

    def show
    end

    def edit
        @ej_journal_def.set_default_child
    end

    def update
        @ej_journal_def.assign_attributes(request_params)

        begin
            if @ej_journal_def.save
    #            Rails.cache.clear
                redirect_to staff_ej_def_ej_journal_def_url(@ej_def, @ej_journal_def), notice: '各雑誌を更新しました。'
            else
                render action: 'edit'
            end
        rescue => ex
            @ej_journal_def.errors.add(:base, ex.message)
            render action: 'edit'
        end
    end

    def destroy
        @ej_journal_def.destroy
        redirect_to staff_ej_def_url(@ej_def), notice: '各雑誌を削除しました。'
    end

private

    def check_ej_def_exists
        @ej_def = EjDef.find(params[:ej_def_id])
    end

    def check_ej_journal_def_exists
        @ej_journal_def = @ej_def.journal_defs.find(params[:id])
    end

    def request_params
        params.require(:ej_journal_def).permit(
            :code, :value, :visible, :css_string, :upload_thumnail_file, :journals_html_ja, :journals_html_en,
            :journal_view_type_id,
            :journal_sub_header_view_type_id,
            :journal_sub_footer_view_type_id,
            { journal_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            :article_view_type_id,
            :article_sub_header_view_type_id,
            :article_sub_footer_view_type_id,
            { article_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            :journal_all_index_view_type_id,
            :journal_all_index_sub_header_view_type_id,
            :journal_all_index_sub_footer_view_type_id,
            { journal_all_index_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            { journal_top_html_set_def_attributes:  [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            { voliss_list_html_set_def_attributes:  [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            { article_list_html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            { static_list_html_set_def_attributes:  [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] },
            :css_string)

    end
end
