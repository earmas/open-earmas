#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Pub::Api::SyslinkController < ApplicationController
    include DocumentsHelper, ViewStateHelper, SortablePage

    skip_before_filter :set_locale
    skip_before_filter :verify_authenticity_token, only: :post_data


    def post_data
        api_uid = Rails.application.config.try(:api_uid)
        api_pass = Rails.application.config.try(:api_pass)

        return render text: "NG\nlogin was failed" if api_uid.blank?
        return render text: "NG\nlogin was failed" if api_pass.blank?
        return render text: "NG\nlogin was failed" if api_uid != params[:uid] || api_pass != params[:pass]



        api_user_email = Rails.application.config.try(:api_user_email)
        return render text: "NG\nuser not found" if api_user_email.blank?

        staff = Staff.find_by(email: api_user_email)

        @document = PriDocument.new
        @document.publish_to = PriDocument::PRIVATE_TYPE

        api_post_param = Rails.application.config.try(:api_post_param)
        return render text: "NG\napi was not ready" if api_post_param.blank?

        @append_fields = api_post_param.constantize.new.set_param_to_list_child(params)

        @document.updater_staff_id = staff.id
        @document.creator_staff_id = staff.id

        messages = []

        begin
            ActiveRecord::Base.transaction do
                @document.check_oaipmh(@append_fields)
                @document.set_embargo_until(@append_fields)
                @document.save!

                @append_fields.each do |field|
                    field.document_id = @document.id
                    field.save!
                    messages << field.saved_message
                end

                @document.fields_validation_after(@append_fields)

                @document.save_upload_thumnail_file

                @document.set_search_data(@append_fields)
                @document.set_list_data(@append_fields)
                @document.set_ej_data(@append_fields)

                @document.index!

                message_string = messages.delete_if{|a|a.blank?}.join("\n")
                render text: "OK"
            end
        rescue DataCheckException => ex
            logger.error ex.message
            logger.error ex.backtrace.join("\n") if ex.backtrace.present?
            render text: "NG\n" + ex.message
        rescue => ex
            logger.error ex.message
            logger.error ex.backtrace.join("\n") if ex.backtrace.present?
            render text: "NG\n" + ex.message
        end

    end


    def get_data
        api_uid = Rails.application.config.try(:api_uid)
        api_pass = Rails.application.config.try(:api_pass)
        api_key = Rails.application.config.try(:api_key)

        if params[:key].present? && api_key.present?
            return render text: "NG\napi key was invalid" if api_key != params[:key]
        else
            return render text: "NG\nlogin was failed" if api_uid.blank?
            return render text: "NG\nlogin was failed" if api_pass.blank?
            return render text: "NG\nlogin was failed" if api_uid != params[:uid] || api_pass != params[:pass]
        end

        creator_search_field_name = Rails.application.config.creator_search_field_name

        api_get_to_csv = Rails.application.config.try(:api_get_to_csv)
        return render text: "NG\napi was not ready" if api_get_to_csv.blank?
        api_get_to_csv_obj = api_get_to_csv.constantize.new


        query = nil
        if params[:query].present?
            query_params = params[:query].split(':')

            if query_params.size == 2
                query_field_name = query_params[0]
                query_field_value_string = query_params[1]

                search_def = SearchDef.where(code: query_field_name).first

                if search_def.present?
                    search_index = SearchDef.reorder(:id).where('id <= ?', search_def.id).count

                    query_field_values = query_field_value_string.split(',')
                    query_field_value_string = query_field_values.join('" OR "')
                    query_field_value_string = '"' + query_field_value_string + '"' if query_field_value_string.present?

                    query = { search_index: search_index, search_value: query_field_value_string }
                end
            end
        end


        return render text: "NG\nno pid" if params[:pid].blank?

        creator_search_def = SearchDef.where(code: creator_search_field_name).first
        return render text: "NG\napi was not ready" if creator_search_def.blank?

        creator_search_index = SearchDef.reorder(:id).where('id <= ?', creator_search_def.id).count



        set_sort_param


        result = PubDocument.search do |s|

            s.fulltext params[:pid] do
                fields('text_value_' + creator_search_index.to_s)
            end

            if query.present?
                s.fulltext query[:search_value] do
                    fields('text_value_' + query[:search_index].to_s)
                end
            end

            if @sort_reverse
                s.order_by(get_sort_column_name, :desc)
            else
                s.order_by(get_sort_column_name, :asc)
            end

            s.adjust_solr_params do |sunspot_params|
                sunspot_params[:start] = params[:offset].to_i if params[:offset].present?
                sunspot_params[:rows] = params[:limit].to_i if params[:limit].present?
            end

            s.with :limited, false
        end

        @result = result.hits
        @documents = result.results
        @count = result.total

        @encoding = Encoding::UTF_8

        headers["Content-Type"] = 'text/plain;charset=' + @encoding.name
        self.response.headers['Last-Modified'] = Time.now.ctime.to_s


        disp_field_defs = []
        (params[:field] || '').split(',').each do | disp_field_code |
            disp_field_def = FieldDefLoaderForInput.field_defs_by_code_by_input_group_id(nil, disp_field_code)
            next if disp_field_def.blank?

            disp_field_defs << disp_field_def.first
        end


        self.response_body = Enumerator.new do |yielder|
            @documents.each do |document|
                csv_rows = []
                disp_field_defs.each do | disp_field_def |
                    if document.field_empty?(disp_field_def.id)
                        csv_rows << ''
                        next
                    end

                    csv_row_fields = []

                    api_get_to_csv_obj.field_to_csv(csv_row_fields, document, disp_field_def)

                    csv_field_string = csv_row_fields.join(',')
                    csv_rows << csv_field_string
                end

                yielder << CSV.generate_line(csv_rows, col_sep: ',', force_quotes: true).encode(@encoding, :invalid => :replace, :undef => :replace, :replace => '?')
            end
        end

    end


    def get_co_data
        api_uid = Rails.application.config.try(:api_uid)
        api_pass = Rails.application.config.try(:api_pass)
        api_key = Rails.application.config.try(:api_key)

        if params[:key].present? && api_key.present?
            return render text: "NG\napi key was invalid" if api_key != params[:key]
        else
            return render text: "NG\nlogin was failed" if api_uid.blank?
            return render text: "NG\nlogin was failed" if api_pass.blank?
            return render text: "NG\nlogin was failed" if api_uid != params[:uid] || api_pass != params[:pass]
        end

        builder_path = Rails.application.config.api_get_co_builder
        if File.exists?(builder_path) == false
            return render text: "NG\napi key disable"
        end

        co_search_field_name = Rails.application.config.co_search_field_name
        @co_search_field_def = FieldDefLoaderForInput.field_defs_by_code_by_input_group_id(nil, co_search_field_name).try(:first)

        if @co_search_field_def.blank?
            return render text: "NG\napi key disable"
        end

        @include_co_data = params[:co] == 'true'



        creator_search_field_name = Rails.application.config.creator_search_field_name

        api_get_to_csv = Rails.application.config.try(:api_get_to_csv)
        return render text: "NG\napi was not ready" if api_get_to_csv.blank?
        api_get_to_csv_obj = api_get_to_csv.constantize.new


        return render text: "NG\nno pid" if params[:pid].blank?

        creator_search_def = SearchDef.where(code: creator_search_field_name).first
        return render text: "NG\napi was not ready" if creator_search_def.blank?

        creator_search_index = SearchDef.reorder(:id).where('id <= ?', creator_search_def.id).count

        result = PubDocument.search do |s|

            s.fulltext params[:pid] do
                fields('text_value_' + creator_search_index.to_s)
            end

            if params[:keyword].present?
                s.fulltext params[:keyword] do
                    fields('text_value_all')
                end
            end

            s.adjust_solr_params do |sunspot_params|
                sunspot_params[:rows] = 2000
            end

            s.with :limited, false
        end

        @result = result.hits
        @documents = result.results
        @count = result.total

        render builder_path, :content_type => 'text/xml', formats: :xml
    end

    def id_search
        api_uid = Rails.application.config.try(:api_uid)
        api_pass = Rails.application.config.try(:api_pass)
        api_key = Rails.application.config.try(:api_key)

        if params[:key].present? && api_key.present?
            return render text: "NG\napi key was invalid" if api_key != params[:key]
        else
            return render text: "NG\nlogin was failed" if api_uid.blank?
            return render text: "NG\nlogin was failed" if api_pass.blank?
            return render text: "NG\nlogin was failed" if api_uid != params[:uid] || api_pass != params[:pass]
        end

        api_get_to_csv = Rails.application.config.try(:api_get_to_csv)
        return render text: "NG\napi was not ready" if api_get_to_csv.blank?
        api_get_to_csv_obj = api_get_to_csv.constantize.new


        search_def = SearchDef.where(code: params[:field]).first
        search_index = SearchDef.reorder(:id).where('id <= ?', search_def.id).count if search_def.present?

        result = PubDocument.search do |s|

            if search_def.present?
                s.fulltext params[:value] do
                    fields('text_value_' + search_index.to_s)
                end
            end

            s.adjust_solr_params do |sunspot_params|
                sunspot_params[:rows] = 2000
            end

            s.with :limited, false
        end

        @result = result.hits
        @documents = result.results
        @count = result.total

        @encoding = Encoding::UTF_8

        headers["Content-Type"] = 'text/plain;charset=' + @encoding.name
        self.response.headers['Last-Modified'] = Time.now.ctime.to_s


        disp_field_def = FieldDefLoaderForInput.field_defs_by_code_by_input_group_id(nil, params[:field]).try(:first)


        self.response_body = Enumerator.new do |yielder|
            @documents.each do |document|
                csv_rows = []
                csv_rows << document.id
                csv_rows << disp_field_def.code

                if document.field_empty?(disp_field_def.id)
                    csv_rows << ''
                else
                    csv_row_fields = []

                    api_get_to_csv_obj.field_to_csv(csv_row_fields, document, disp_field_def)
                    csv_row_fields.each do |csv_row_field|
                        csv_rows << csv_row_field
                    end
                end

                yielder << CSV.generate_line(csv_rows, col_sep: "\t").encode(@encoding, :invalid => :replace, :undef => :replace, :replace => '?')
            end
        end

        if @documents.size == 0
            self.response_body = 'not found'
        end
    end

end

