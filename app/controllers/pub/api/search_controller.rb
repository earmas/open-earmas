#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Pub::Api::SearchController < ApplicationController
    include ViewStateHelper

    skip_before_filter :set_locale

    def document
        return show_not_found if @system_setting.search_api_xml_string.blank?

        o_param = params[:o].to_i
        if o_param <= 1 || o_param.blank?
            o_param = 1
        end

        p_param = params[:p].to_i
        if p_param <= 0 || p_param.blank?
            p_param = 10
        end

        search_term_map = {}
        field_index = 1
        SearchDef.all.reorder(:id).each do |search|
            search_term_map[field_index] = params[search.code.to_s] if params[search.code.to_s].present?
            field_index += 1
        end

        @documents = []

        if search_term_map.present? || params[:all].present?
            result = PubDocument.search do |s|
                if params[:all].present?
                    if params[:include_file] != 'include'
                        s.fulltext params[:all] do 
                            fields('text_value_all')
                        end
                    else
                        s.fulltext params[:all] do 
                            fields('text_value_all_with_file')
                        end
                    end
                end

                search_term_map.keys.sort.each do |index|
                    search_term = search_term_map[index]
                    if search_term.present?
                        s.fulltext search_term do 
                            fields('text_value_' + index.to_s)
                        end
                    end
                end
                s.adjust_solr_params do |sunspot_params|
                    sunspot_params[:start] = o_param - 1
                    sunspot_params[:rows] = p_param
                end
                view_state_search(s)
            end

            @documents = result.results
        end

        xml_string = render_to_string 'view_types/search/documents'
        doc = Nokogiri::XML(xml_string)

        xslt = Nokogiri::XSLT(@system_setting.search_api_xml_string)
        doc = xslt.transform(doc)

        render :text => doc.to_xml(encoding: 'UTF-8')
    end

end
