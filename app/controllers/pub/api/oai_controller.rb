#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Pub::Api::OaiController < ApplicationController
    include OaiHelper

    require 'nokogiri'

    RECORDS_PER_PAGE = 20

    skip_before_action :verify_authenticity_token
    skip_before_filter :set_locale

    before_action :check_oaipmh_enable
    before_action :set_oai_param

    respond_to :xml

    OAI_ERROR = {
        cannotDisseminateFormat: { code: 'cannotDisseminateFormat', message: "The value of the metadataPrefix argument is not supported by the item identified by the value of the identifier argument." },
        idDoesNotExist: { code: 'idDoesNotExist', message: "The value of the identifier argument is unknown or illegal in this repository." },
        badArgument: { code: 'badArgument', message: "The request includes illegal arguments or is missing required arguments." },
        badVerb: { code: 'badVerb', message: "Illegal OAI verb." },
        noMetadataFormats: { code: 'noMetadataFormats', message: "There are no metadata formats available for the specified item." },
        noRecordsMatch: { code: 'noRecordsMatch', message: "The combination of the values of the from, until, set and metadataPrefix arguments results in an empty list." },
        badResumptionToken: { code: 'badResumptionToken', message: "The value of the resumptionToken argument is invalid or expired." },
        noSetHierarchy: { code: 'noSetHierarchy', message: "The repository does not support sets." },
    }

    def check_param
        @oai_param = {}
        verb = params[:verb]

        case verb
        when 'GetRecord'
            @verb = verb
            @oai_param['verb'] = @verb

            check_param_must('identifier')
            check_param_must('metadataPrefix')
            check_param_must_not('from')
            check_param_must_not('until')
            check_param_must_not('set')
            check_param_must_not('resumptionToken')
        when 'Identify'
            @verb = verb
            @oai_param['verb'] = @verb

            check_param_must_not('identifier')
            check_param_must_not('metadataPrefix')
            check_param_must_not('from')
            check_param_must_not('until')
            check_param_must_not('set')
            check_param_must_not('resumptionToken')
        when 'ListIdentifiers'
            @verb = verb
            @oai_param['verb'] = @verb

            check_param_must_not('identifier')
            check_param_must('metadataPrefix')
            check_param_may('from')
            check_param_may('until')
            check_param_may('set')
            check_param_token
        when 'ListMetadataFormats'
            @verb = verb
            @oai_param['verb'] = @verb

            check_param_may('identifier')
            check_param_must_not('metadataPrefix')
            check_param_must_not('from')
            check_param_must_not('until')
            check_param_must_not('set')
            check_param_must_not('resumptionToken')
        when 'ListRecords'
            @verb = verb
            @oai_param['verb'] = @verb

            check_param_must_not('identifier')
            check_param_must('metadataPrefix')
            check_param_may('from')
            check_param_may('until')
            check_param_may('set')
            check_param_token
        when 'ListSets'
            @verb = verb
            @oai_param['verb'] = @verb

            check_param_must_not('identifier')
            check_param_must_not('metadataPrefix')
            check_param_must_not('from')
            check_param_must_not('until')
            check_param_must_not('set')
            check_param_must_not('resumptionToken')
        else
            @error_code = OAI_ERROR[:badVerb]
        end
    end

    def index
        check_param
        return show_error if @error_code.present?

        case @verb
        when 'GetRecord'
            get_record
        when 'Identify'
            identify
        when 'ListIdentifiers'
            list_identifiers
        when 'ListMetadataFormats'
            list_metadata_formats
        when 'ListRecords'
            list_records
        when 'ListSets'
            list_sets
        else
            show_error
        end
    end

    def get_record
        @oaipmh_format = OaipmhFormat.where(metadata_prefix: @metadataPrefix).first
        return error_response(:cannotDisseminateFormat) if @oaipmh_format.blank?

        return error_response(:idDoesNotExist) if @identifier.start_with?(@oaipmh_setting.identifier_prefix) == false

        document_id = @identifier.gsub(Regexp.new(Regexp.escape(@oaipmh_setting.identifier_prefix), Regexp::IGNORECASE), '')
        document_id = @system_setting.public_id_to_document_id(document_id)
        document = PubDocument.unscoped.oaipmh.where(id: document_id).first
        return error_response(:idDoesNotExist) if document.blank?


        @documents = [document]

        xml_string = render_to_string 'view_state/oai/records'
        doc1 = Nokogiri::XML(xml_string)

        xslt1 = Nokogiri::XSLT(@oaipmh_format.xml_string)
        doc2 = xslt1.transform(doc1)

        xslt_string = render_to_string 'view_state/oai/get_record'
        xslt2 = Nokogiri::XSLT(xslt_string)
        doc3 = xslt2.transform(doc2)

        render :text => doc3.to_xml(encoding: 'UTF-8'), :content_type => 'text/xml'
    end

    def identify
        render 'view_state/oai/identify', :content_type => 'text/xml'
    end

    def list_identifiers
        @oaipmh_format = OaipmhFormat.where(metadata_prefix: @metadataPrefix).first
        return error_response_parmit_token(:cannotDisseminateFormat) if @oaipmh_format.blank?

        @document_relation = PubDocument.unscoped.oaipmh.order('id ASC')

        return error_response_parmit_token(:noSetHierarchy) if check_set_enable == false

        return error_response_parmit_token(:badArgument) if check_from_enable == false
        return error_response_parmit_token(:badArgument) if check_until_enable == false
        return error_response_parmit_token(:badArgument) if check_from_until_enable == false

        @document_relation_limited = set_prev_last_id.limit(RECORDS_PER_PAGE)

        @documents = @document_relation_limited
        return error_response_parmit_token(:noRecordsMatch) if @documents.size == 0

        create_next_resumption_token

        render 'view_state/oai/list_identifiers', :content_type => 'text/xml'
    end

    def list_metadata_formats
        if @identifier.present?
            document_id = @identifier.gsub(Regexp.new(Regexp.escape(@oaipmh_setting.identifier_prefix), Regexp::IGNORECASE), '')
            document_id = @system_setting.public_id_to_document_id(document_id)
            document = PubDocument.unscoped.oaipmh.where(id: document_id).first
            return error_response(:idDoesNotExist) if document.blank?
        end

        @metadata_formats = OaipmhFormat.all.reject{|a|a.schema.blank?}
        return error_response(:noMetadataFormats) if @metadata_formats.size == 0

        render 'view_state/oai/list_metadata_formats', :content_type => 'text/xml'
    end

    def list_records
        @oaipmh_format = OaipmhFormat.where(metadata_prefix: @metadataPrefix).first
        return error_response_parmit_token(:cannotDisseminateFormat) if @oaipmh_format.blank?

        @document_relation = PubDocument.unscoped.oaipmh.order('id ASC')

        return error_response_parmit_token(:noSetHierarchy) if check_set_enable == false

        return error_response_parmit_token(:badArgument) if check_from_enable == false
        return error_response_parmit_token(:badArgument) if check_until_enable == false
        return error_response_parmit_token(:badArgument) if check_from_until_enable == false

        @document_relation_limited = set_prev_last_id.limit(RECORDS_PER_PAGE)

        @documents = @document_relation_limited
        return error_response_parmit_token(:noRecordsMatch) if @documents.size == 0

        create_next_resumption_token

        xml_string = render_to_string 'view_state/oai/records'
        doc1 = Nokogiri::XML(xml_string)

        xslt1 = Nokogiri::XSLT(@oaipmh_format.xml_string)
        doc2 = xslt1.transform(doc1)

        xslt_string = render_to_string 'view_state/oai/list_records'
        xslt2 = Nokogiri::XSLT(xslt_string)
        doc3 = xslt2.transform(doc2)

        render :text => doc3.to_xml(encoding: 'UTF-8'), :content_type => 'text/xml'
    end

    def list_sets
        render 'view_state/oai/list_sets', :content_type => 'text/xml'
    end


private

    def check_oaipmh_enable
        return show_invalid if @system_setting.enable_oaipmh == false
    end

    def set_oai_param
        @response_date = Time.zone.now

        @datestamp_start = PubDocument.unscoped.oaipmh.minimum(:min_publish_at)
        @datestamp_start = Time.zone.now if @datestamp_start.blank?

        @datestamp_end = PubDocument.unscoped.oaipmh.maximum(:max_publish_at)
        @datestamp_end = Time.zone.now if @datestamp_end.blank?

        set_default_value = @oaipmh_setting.set_spec_default_value

        @set_values = []
        if @oaipmh_setting.set_spec_custom_select_id.present?
            custom_select = CustomSelect.where(id: @oaipmh_setting.set_spec_custom_select_id).first
            @set_values = custom_select.values.to_a if custom_select.present?
        end

        @set_values << CustomSelectValue.new(value: set_default_value, caption_ja: set_default_value, caption_en: set_default_value)
        @set_values = @set_values.map{|a|a.value = a.value.delete(' ').delete(':'); a}
    end

    def check_param_must(name)
        param = params[name]
        if param.blank?
            @error_code = OAI_ERROR[:badArgument]
        else
            instance_variable_set('@' + name, param)
            @oai_param[name] = param if param.present?
        end
    end

    def check_param_must_not(name)
        param = params[name]
        if param.present?
            @error_code = OAI_ERROR[:badArgument]
        end
    end

    def check_param_may(name)
        param = params[name]
        instance_variable_set('@' + name, param)
        @oai_param[name] = param if param.present?
    end

    def check_param_token
        @resumptionToken = params[:resumptionToken]
        return if @resumptionToken.blank?
        @error_code = nil
        @oai_param[:resumptionToken] = @resumptionToken
        check_param_must_not('identifier')
        check_param_must_not('metadataPrefix')
        check_param_must_not('from')
        check_param_must_not('until')
        check_param_must_not('set')
        check_resumption_token
    end

    def check_resumption_token
        return if @resumptionToken.blank?
        split_tokens = @resumptionToken.split('/', -1)

        @from = split_tokens[0]
        @until = split_tokens[1]
        @set = split_tokens[2]
        @metadataPrefix = split_tokens[3]
        @prev_last_id = split_tokens[4]
    end

    def create_next_resumption_token
        @last_id = @document_relation_limited.last.id.to_s

        @total = @document_relation.size
        @offset = @resumptionToken.blank? ? 0 : @document_relation.where('id <= ?', @prev_last_id).count

        if @document_relation.where('id > ?', @last_id).count > 0
            @next_resumption_token = show_value_blank(@from) + "/" + show_value_blank(@until) + "/" + show_value_blank(@set) + "/" + show_value_blank(@metadataPrefix) + "/" + (@last_id)
        else
            @next_resumption_token = nil
        end
    end

    def set_prev_last_id
        if @prev_last_id.blank?
            @document_relation
        else
            @document_relation.where('id > ?', @prev_last_id)
        end
    end

    def check_set_enable
        return true if @set.blank?
        @set_values.each do |set_value|
            if @set == set_value.value
                @document_relation = @document_relation.where('set_spec @> ARRAY[?]', [@set])
                return true
            end
        end
        return false
    end

    def check_from_enable
        return true if @from.blank?
        begin
            @from_date = DateTime.iso8601(@from).in_time_zone('UTC')
            @document_relation = @document_relation.where('max_publish_at >= ?', @from_date)
            return true
        rescue ArgumentError, NoMethodError
            return false
        end
    end

    def check_until_enable
        return true if @until.blank?
        begin
            @until_date = DateTime.iso8601(@until).in_time_zone('UTC')
            @org_until_date = @until_date

            if @until.size > 10
                @document_relation = @document_relation.where('min_publish_at <= ?', @until_date)
            else
                @until_date = @org_until_date + 1.day
                @document_relation = @document_relation.where('min_publish_at < ?', @until_date)
            end
            return true
        rescue ArgumentError, NoMethodError
            return false
        end
    end

    def check_from_until_enable
        return true if @from_date.blank? || @org_until_date.blank?
        return false if @from_date > @org_until_date
        return false if @from.size != @until.size

        return true
    end

    def error_response(error_code)
        @error_code = OAI_ERROR[error_code]
        return show_error
    end

    def error_response_parmit_token(error_code)
        if @resumptionToken.present?
            @error_code = OAI_ERROR[:badResumptionToken]
            return show_error
        else
            @error_code = OAI_ERROR[error_code]
            return show_error
        end
    end

    def show_error
        render 'view_state/oai/error'
    end
end
