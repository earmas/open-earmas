#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Pub::Api::InfoController < ApplicationController
    include ViewStateHelper

    skip_before_filter :set_locale

    before_action :check_list_exists, only: [:list]

    def index
        render :json => {
            total: { count: view_state_relation(:Document).all.size },
        }
    end

    def oaipmh
        render :json => {
            total: { count: view_state_relation(:Document).where(oaipmh: true).size },
        }
    end
    def oaipmh_year
        today = Time.zone.now.yesterday
        @last_12_month = today.months_ago(12)

        render :json => {
            total: { count: view_state_relation(:Document).where(oaipmh: true).where('min_publish_at > ?', @last_12_month).size },
        }
    end
    def oaipmh_month
        if params[:month]
            param_date = Time.zone.parse(params[:month] + '-01')

            @start_date = param_date.beginning_of_month
            @end_date = param_date.end_of_month
            data_count = view_state_relation(:Document).where(oaipmh: true)
                .where('min_publish_at > ? and min_publish_at < ? ', @start_date, @end_date).size
        else
            today = Time.zone.now.yesterday
            @end_date = today.months_ago(1)
            data_count = view_state_relation(:Document).where(oaipmh: true)
                .where('min_publish_at > ?', @end_date).size
        end

        render :json => {
            total: { count: data_count },
        }
    end



    def list
        render :json => {
            total: { count: @list_def.pub_list_data.size },
        }
    end


private

    def check_list_exists
        @list_def = ListDef.where(code: params[:list_def_code]).includes(:list_fields).first
        return show_not_found if @list_def.blank?
        return show_not_found if view_state_visible(@list_def) == false
    end

end
