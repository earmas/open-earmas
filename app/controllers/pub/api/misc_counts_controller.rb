#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Pub::Api::MiscCountsController < ApplicationController
    include DocumentsHelper, ViewStateHelper

    skip_before_filter :set_locale

    before_action :set_document


    def show
        misc_count_name = Rails.application.config.try(:misc_count_name)

        misc_counts = MiscCount.where(document_id: @document_ids)

        json_data = { label: {}, data: {}, }
        json_data[:label][:count_1] = misc_count_name[0] if misc_count_name[0].present?
        json_data[:label][:count_2] = misc_count_name[1] if misc_count_name[1].present?
        json_data[:label][:count_3] = misc_count_name[2] if misc_count_name[2].present?
        json_data[:label][:count_4] = misc_count_name[3] if misc_count_name[3].present?
        json_data[:label][:count_5] = misc_count_name[4] if misc_count_name[4].present?
        json_data[:label][:count_6] = misc_count_name[5] if misc_count_name[5].present?
        json_data[:label][:count_7] = misc_count_name[6] if misc_count_name[6].present?
        json_data[:label][:count_8] = misc_count_name[7] if misc_count_name[7].present?
        json_data[:label][:count_9] = misc_count_name[8] if misc_count_name[8].present?

        misc_counts.each do |misc_count|
            json_data[:data][misc_count.document_id] = {
                count_1: misc_count.count_1 || 0,
                count_2: misc_count.count_2 || 0,
                count_3: misc_count.count_3 || 0,
                count_4: misc_count.count_4 || 0,
                count_5: misc_count.count_5 || 0,
                count_6: misc_count.count_6 || 0,
                count_7: misc_count.count_7 || 0,
                count_8: misc_count.count_8 || 0,
                count_9: misc_count.count_9 || 0,
                count_1_url: misc_count.count_1_url || '',
                count_2_url: misc_count.count_2_url || '',
                count_3_url: misc_count.count_3_url || '',
                count_4_url: misc_count.count_4_url || '',
                count_5_url: misc_count.count_5_url || '',
                count_6_url: misc_count.count_6_url || '',
                count_7_url: misc_count.count_7_url || '',
                count_8_url: misc_count.count_8_url || '',
                count_9_url: misc_count.count_9_url || '',
            }
        end

        render :json => json_data
    end


private

    def set_document
        ids = params[:id].split(',')
        @document_ids = view_state_relation(:Document).unscoped.where(deleted: false).where(id: ids).pluck(:id)
    end

end
