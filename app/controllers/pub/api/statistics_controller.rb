#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Pub::Api::StatisticsController < ApplicationController
    include DocumentsHelper, ViewStateHelper

    skip_before_filter :set_locale

    before_action :set_month,           only: [:all_month, :document_month, :person_month]
    before_action :set_last_month,      only: [:all_monthly, :document_monthly, :person_monthly]

    before_action :set_document,    only: [:document, :document_month, :document_monthly]
    before_action :set_person,      only: [:person, :person_month, :person_monthly]


    # last 12 month
    def all
        @data = Logs::LogCountData.new.load_all_log

        render_all_result
    end

    # last 12 month
    def document
        @data = Logs::LogCountData.new.load_document_log(@document.id)

        render_document_result
    end

    # last 12 month
    def person
        @data = Logs::LogCountData.new.load_person_log(@person.id)

        render_person_result
    end

    def all_month
        @data = Logs::LogCountData.new.load_all_log(@month)

        render_all_result
    end

    def document_month
        @data = Logs::LogCountData.new.load_document_log(@document.id, @month)

        render_document_result
    end

    def person_month
        @data = Logs::LogCountData.new.load_person_log(@person.id, @month)

        render_person_result
    end

    def all_monthly
        @data = Logs::LogCountData.new.load_all_log(@month)

        render_all_result
    end

    def document_monthly
        @data = Logs::LogCountData.new.load_document_log(@document.id, @month)

        render_document_result
    end

    def person_monthly
        @data = Logs::LogCountData.new.load_person_log(@person.id, @month)

        render_person_result
    end


private

    def set_month
        date = Time.zone.parse(params[:month] + '-01')
        @month = date.strftime("%Y-%m")
    end

    def set_last_month
        date = Time.zone.now
        @month = date.strftime("%Y-%m")
    end

    def set_document
        @document = view_state_relation(:Document).find(params[:id])
    end

    def set_person
        @person = Person.find_by!(encrypted_id: params[:id])
    end

    def token_enable?
        return false if @access_count_setting.api_token.blank?

        params[:token] == @access_count_setting.api_token
    end

    def create_document_map
        document_map = {
            total: [],
            access: [],
            download: [],
        }

        @data.get_count_map(:id).each do |k, v|
            next if k == 'other'
            document_map[:total] << {
                count: v,
                title_ja: @data.get_count_map(:id_title_ja)[k] || '',
                title_en: @data.get_count_map(:id_title_en)[k] || '',
                url: get_public_document_id_url(k),
            }
        end
        @data.get_count_map(:id_access).each do |k, v|
            next if k == 'other'
            document_map[:access] << {
                count: v,
                title_ja: @data.get_count_map(:id_title_ja)[k] || '',
                title_en: @data.get_count_map(:id_title_en)[k] || '',
                url: get_public_document_id_url(k),
            }
        end
        @data.get_count_map(:id_download).each do |k, v|
            next if k == 'other'
            document_map[:download] << {
                count: v,
                title_ja: @data.get_count_map(:id_title_ja)[k] || '',
                title_en: @data.get_count_map(:id_title_en)[k] || '',
                url: get_public_document_id_url(k),
            }
        end
        document_map
    end

    def render_all_result
        json_data = {
            total: { count: @data.total },
            access: { count: @data.get_count_map(:access_log)['yes'] || 0 },
            download: { count: @data.get_count_map(:download_log)['yes'] || 0 },
            docuument: create_document_map,
        }

        json_data[:remote_host] = { count: @data.get_count_map(:remote_host) } if token_enable?

        render :json => json_data
    end

    def render_document_result
        json_data = {
            title_ja: get_document_title_strings(@document, '')[:ja],
            title_en: get_document_title_strings(@document, '')[:en],
            url: get_public_document_url(@document),
            total: { count: @data.total },
            access: { count: @data.get_count_map(:access_log)['yes'] || 0 },
            download: { count: @data.get_count_map(:download_log)['yes'] || 0 },
        }

        json_data[:remote_host] = { count: @data.get_count_map(:remote_host) } if token_enable?

        render :json => json_data
    end

    def render_person_result
        json_data = {
            name_ja: [@person.family_name_ja, @person.given_name_ja].reject{|a|a.blank?}.join(' '),
            name_en: [@person.given_name_en, @person.family_name_en].reject{|a|a.blank?}.join(' '), 
            total: { count: @data.total },
            access: { count: @data.get_count_map(:access_log)['yes'] || 0 },
            download: { count: @data.get_count_map(:download_log)['yes'] || 0 },
            docuument: create_document_map,
        }

        json_data[:remote_host] = { count: @data.get_count_map(:remote_host) } if token_enable?

        render :json => json_data
    end

end
