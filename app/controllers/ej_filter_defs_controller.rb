#--
# Open Earmas
#
# Copyright (C) 2014 Hiroshima University, ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class EjFilterDefsController < BaseAdminController

    maintenance_data_lock_methods :create, :update, :destroy

    before_action :check_mentenance_mode
    before_action :check_ej_def_exists
    before_action :check_ej_filter_def_exists, except: [:index, :new, :create, :param_form]

    def new
        @ej_filter_def = EjFilterDef.new
        @ej_filter_def.filter_type = DocumentFilterDef::FILTER_TYPES[:field_value_filter][:id]
    end

    def create
        @ej_filter_def = EjFilterDef.new(request_params)
        @ej_filter_def.parent_id = @ej_def.id

        create_action_block('new', 'ej_filter_def', 'フィルタの作成に失敗しました。') do
            if @ej_filter_def.save
                lock_ej(@ej_def.id)
                lock_pri_ej(@ej_def.id)
                redirect_to staff_ej_def_url(@ej_def), notice: 'フィルタを作成しました。'
            else
                render action: 'new'
            end
        end
    end

    def edit
    end

    def update
        @ej_filter_def.clear_optional_param
        @ej_filter_def.assign_attributes(request_params)

        update_action_block('edit', 'ej_filter_def', 'フィルタの更新に失敗しました。') do
            if @ej_filter_def.save
                lock_ej(@ej_def.id)
                lock_pri_ej(@ej_def.id)
                redirect_to staff_ej_def_url(@ej_def), notice: 'フィルタを更新しました。'
            else
                render action: 'edit'
            end
        end
    end

    def destroy
        redirect_block(staff_ej_def_url(@ej_def), 'ej_filter_def', 'フィルタの削除に失敗しました。') do
            @ej_filter_def.destroy!
            lock_ej(@ej_def.id)
            lock_pri_ej(@ej_def.id)
            redirect_to staff_ej_def_url(@ej_def), notice: 'フィルタを削除しました。'
        end
    end

    def param_form
        @ej_filter_def = EjFilterDef.new
        @ej_filter_def.filter_type = params[:ej_filter_def][:filter_type]
    end


private

    def check_ej_def_exists
        @ej_def = EjDef.find(params[:ej_def_id])
    end

    def check_ej_filter_def_exists
        @ej_filter_def = @ej_def.filter_fields.find(params[:id])
    end

    def request_params
        params.require(:ej_filter_def).permit(
            :include_filter,
            :filter_type,
            :some_id,
            :filter_value)
    end
end
