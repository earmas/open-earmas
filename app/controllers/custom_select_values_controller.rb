#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class CustomSelectValuesController < BaseAdminController
    include OrderHolder, FieldLock

    maintenance_data_lock_methods :create, :update, :destroy, :up, :down

    skip_before_action :check_admin_role,   only: [:search]

    before_action :check_mentenance_mode, except: [:search]
    before_action :set_custom_select
    before_action :set_custom_select_value, except: [:index, :new, :create, :search]

    def new
        @custom_select_value = CustomSelectValue.new
    end

    def create
        @custom_select_value = CustomSelectValue.new(request_params)
        @custom_select_value.custom_select_id = @custom_select.id
        @custom_select_value.sort_index = CustomSelectValue.where(custom_select_id: @custom_select.id).maximum(:sort_index)
        @custom_select_value.sort_index = @custom_select_value.sort_index && @custom_select_value.sort_index + 1 || 1

        if @custom_select_value.save
            redirect_to staff_custom_select_url(@custom_select), notice: '選択肢を作成しました。'
        else
            render action: 'new'
        end
    end

    def update
        @custom_select_value.assign_attributes(request_params)

        update_action_block('edit', 'custom_select_value', '選択肢の更新に失敗しました。') do
            touch_use_data_for_update
            if @custom_select_value.save
#                Rails.cache.clear
                redirect_to staff_custom_select_url(@custom_select), notice: '選択肢を更新しました。'
            else
                render action: 'edit'
            end
        end
    end

    def destroy
        redirect_block(staff_custom_select_url(@custom_select), 'custom_select_value', '選択肢の削除に失敗しました。') do
            touch_use_data_for_delete
            @custom_select_value.destroy!
            redirect_to staff_custom_select_url(@custom_select), notice: '選択肢を削除しました。'
        end
    end

    def up
        ActiveRecord::Base.transaction do
            move_up_no_transaction(@custom_select.values.to_a, @custom_select_value)
            check_field_list_update
        end

        redirect_to staff_custom_select_url(@custom_select), notice: '選択肢を移動しました。'
    end

    def down
        ActiveRecord::Base.transaction do
            move_down_no_transaction(@custom_select.values.to_a, @custom_select_value)
            check_field_list_update
        end

        redirect_to staff_custom_select_url(@custom_select), notice: '選択肢を移動しました。'
    end

    def search
        custom_select_values = CustomSelectValue.where(custom_select_id: @custom_select.id)
        custom_select_values.where!(value: params[:value]) if params[:value].present?
        custom_select_values.where!("caption_ja LIKE ?", "#{params[:caption_ja]}%") if params[:caption_ja].present?
        custom_select_values.where!("caption_en LIKE ?", "#{params[:caption_en]}%") if params[:caption_en].present?
        custom_select_values.limit!(5)

        html = render_to_string partial: 'custom_select_values/search', locals: { custom_select_values: custom_select_values }
        render json: {
            result: 'success',
            html: html,
        }
    end

private

    def touch_use_data_for_delete
        FieldDefLoader.field_defs.each do |field_def|
            next if field_def.field_type_class.get_select_id(field_def) != @custom_select_value.custom_select_id

            if field_def.field_type_class.get_select_value_exists(field_def, @custom_select_value) && field_def.field_type_id != FieldTypes::RepositoryTitle::TYPE_ID
                raise DataCheckException, "この選択項目の値は使用中です" 
            end
        end

        touch_use_data_for_update
    end

    def touch_use_data_for_update
        field_def_ids = []

        FieldDefLoader.field_defs.each do |field_def|
            next if field_def.field_type_class.get_select_id(field_def) != @custom_select_value.custom_select_id
            next if field_def.field_type_class.get_select_value_exists(field_def, @custom_select_value) == false

            field_def_ids << field_def.id
            document_ids_relation = field_def.field_type_class.get_select_value_relation(field_def, @custom_select_value).pluck(:document_id)
            document_ids_relation.each_slice(100) do |sub_document_ids|
                PriDocument.unscoped.where(deleted: false).where(id: sub_document_ids).update_all(updated_at: PriDocument.get_now)
                PriDocument.where(id: sub_document_ids).update_all(meta_tag_xml: nil)
            end
        end

        set_lock_pri_field(field_def_ids)
    end

    def check_field_update
        field_def_ids = []

        FieldDefLoader.field_defs.each do |field_def|
            next if field_def.field_type_class.get_select_id(field_def) != @custom_select_value.custom_select_id
            next if field_def.field_type_class.get_select_value_exists(field_def, @custom_select_value) == false

            field_def_ids << field_def.id
        end

        set_lock_pri_field(field_def_ids)
    end

    def check_field_list_update
        field_def_ids = []

        FieldDefLoader.field_defs.each do |field_def|
            next if field_def.field_type_class.get_select_id(field_def) != @custom_select_value.custom_select_id
            next if field_def.field_type_class.get_select_value_exists(field_def, @custom_select_value) == false

            field_def_ids << field_def.id
        end

        set_lock_list(field_def_ids)
    end

    def set_custom_select
        @custom_select = CustomSelect.find(params[:custom_select_id])
    end

    def set_custom_select_value
        @custom_select_value = @custom_select.values.find(params[:id])
    end

    def request_params
        params.require(:custom_select_value).permit(:value, :caption_ja, :caption_en)
    end
end
