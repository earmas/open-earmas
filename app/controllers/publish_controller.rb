#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class PublishController < BaseAdminController
    include PublishHelper

    before_action :check_not_mentenance_mode

    before_action :set_nonmaintenance_data_lock,    only: [:publish, :reset_publish]

    def index
        @document = PriDocument.new
        @documents = PriDocument.publish_query.order('pri_documents.id ASC').page(params[:page])
    end

    def publish
        begin
            if Rails.application.config.use_delayed_job
                Delayed::KickDelayedTask.new(current_staff).delay({:run_at => Rails.application.config.delay_time.from_now}).publish(params)
            else
                Delayed::DelayTask.new.publish(current_staff, params)
            end
            redirect_to staff_publish_url

        rescue Exception => ex
            unset_data_lock
            logger.error ex.message
            redirect_to staff_publish_url, alert: '公開に失敗しました'
        end
    end

    def reset_publish
        now_time = PriDocument.get_now
        begin
            PriDocument.where.not(publish_to: PriDocument::PRIVATE_TYPE).except(:includes).update_all(updated_at: now_time)
            redirect_to staff_publish_url
        rescue Exception => ex
            logger.error ex.message
            redirect_to staff_publish_update_all_data_path, alert: '変更に失敗しました'
        ensure
            unset_data_lock
        end
    end

    def search
        redirect_to staff_publish_searched_path(trim_param(params))
    end

    def searched
        parse_publish_param(params)

        @document = PriDocument.new
        @documents = find_by_publish_document_query.order('pri_documents.id asc').page(params[:page])

        render 'index'
    end

private

    def trim_param(params)
        trimed_params = {}
        trimed_params[:input_group_id] = params[:input_group_id] if params[:input_group_id].present?

        trimed_params[:updater_staff_id] = params[:updater_staff_id] if params[:updater_staff_id].present?
        trimed_params[:creator_staff_id] = params[:creator_staff_id] if params[:creator_staff_id].present?

        trimed_params[:created_at_start] = params[:created_at_start] if params[:created_at_start].present?
        trimed_params[:created_at_end] = params[:created_at_end] if params[:created_at_end].present?

        trimed_params[:updated_at_start] = params[:updated_at_start] if params[:updated_at_start].present?
        trimed_params[:updated_at_end] = params[:updated_at_end] if params[:updated_at_end].present?

        trimed_params[:except_ids] = params[:except_ids] if params[:except_ids].present?

        trimed_params
    end

end
