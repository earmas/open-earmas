#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class BaseStaffController < ApplicationController

    before_action :authenticate_staff!
    before_action :check_not_ej_editor
    before_action :set_view_state_pri

    layout :staff_layout

    def staff_layout
        if current_staff.ej_editor?
            'ej_editor'
        else
            'staff'
        end
    end

    def check_not_ej_editor
        return show_invalid if current_staff.ej_editor?
    end

    def set_view_state_pri
        ViewState.to_pri
    end

    def self.maintenance_data_lock_methods(*names)
        @need_lock_method = names
        prepend MaintenanceDataLock
    end

    def self.get_maintenance_data_lock_method
        @need_lock_method
    end

    module MaintenanceDataLock
        def self.prepended(mod)
            mod.get_maintenance_data_lock_method.each do |name|
                define_method(name) do
                    return if set_maintenance_data_lock == false
                    begin
                        super()
                    ensure
                        unset_data_lock
                    end
                end
            end
        end
    end

    def self.nonmaintenance_data_lock_methods(*names)
        @need_lock_method = names
        prepend NonMaintenanceDataLock
    end

    def self.get_nonmaintenance_data_lock_method
        @need_lock_method
    end

    module NonMaintenanceDataLock
        def self.prepended(mod)
            mod.get_nonmaintenance_data_lock_method.each do |name|
                define_method(name) do
                    return if set_nonmaintenance_data_lock == false
                    begin
                        super()
                    ensure
                        unset_data_lock
                    end
                end
            end
        end
    end

end
