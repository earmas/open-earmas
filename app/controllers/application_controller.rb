#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ApplicationController < ActionController::Base
    include SystemSettingHelper

    protect_from_forgery with: :exception

    before_action :set_locale
    before_action :set_view_state
    before_action :set_system_setting
    before_action :set_theme

    def set_system_setting
        begin
            ActiveRecord::Base.transaction do
                @system_setting = SystemSetting.all.first
                if @system_setting.blank?
                    @system_setting = SystemSetting.create_default
                end

                if @system_setting.enable_oaipmh
                    @oaipmh_setting = OaipmhSetting.all.first
                    if @oaipmh_setting.blank?
                        @oaipmh_setting = OaipmhSetting.create_default
                    end
                end

                @search_setting = SearchSetting.all.first
                if @search_setting.blank?
                    @search_setting = SearchSetting.create_default
                end

                @people_setting = PeopleSetting.all.first
                if @people_setting.blank?
                    @people_setting = PeopleSetting.create_default
                end

                @access_count_setting = AccessCountSetting.all.first
                if @access_count_setting.blank?
                    @access_count_setting = AccessCountSetting.create_default(request.host_with_port)
                end
            end
        rescue => ex
            logger.error ex.message
            return show_invalid
        end
    end

    def set_theme
        @page_title = ''
        return if @system_setting.theme_class.blank?
        prepend_view_path File.join('vendor', 'views', @system_setting.theme_class.dir_name)
    end

    def set_view_state
        FieldDefLoader.clear
        ViewState.to_pub
    end

    def check_oaipmh_enable
        return show_invalid if @system_setting.enable_oaipmh != true
    end

    def check_coverpage_enable
        return show_invalid if @system_setting.enable_coverpage != true
    end

    def show_not_found
        respond_to do |format|
            format.nolayout { render text: 'ページがありません。', status: 404 }
            format.html     { render file: 'shared/not_found', status: 404 }
            format.js       { render json: { result: 'fail', message: 'ページがありません。' } }
            format.xml      { render text: 'NotFound', status: 404, :content_type => 'text/html' }
        end
        return false
    end

    def show_invalid
        respond_to do |format|
            format.nolayout { render text: '不正な操作です。', status: 403 }
            format.html     { render file: 'shared/invalid', status: 403 }
            format.js       { render json: { result: 'fail', message: '不正な操作です。' } }
            format.xml      { render text: 'Forbidden', status: 403, :content_type => 'text/html' }
        end
        return false
    end

    def show_locked
        respond_to do |format|
            format.nolayout { render text: 'メンテナンス中です。', status: 503 }
            format.html     { render file: 'shared/locked', status: 503 }
            format.js       { render json: { result: 'fail', message: 'メンテナンス中です。' } }
            format.xml      { render text: 'Service Unavailable', status: 503, :content_type => 'text/html' }
        end
        return false
    end

    def after_sign_in_path_for(resource) 
        case resource
        when Staff then staff_path(locale: I18n.locale)
        end
    end

    def set_locale
        if params[:logined] != 'user' && staff_signed_in?
            sign_out current_staff
        end
        if params[:locale].blank?
            extracted_locale = extract_locale_from_accept_language_header
            I18n.locale = extracted_locale && (I18n::available_locales.include? extracted_locale.to_sym) ? extracted_locale : I18n.default_locale
            redirect_to url_for(locale: I18n.locale)
        else
            I18n.locale = params[:locale]
        end
    end

    def url_options
        { locale: I18n.locale, logined: (current_staff.present? ? 'user' : '') }.merge(super)
    end

    def add_locale_suffix(value)
        value.to_s + '_' + I18n.locale.to_s
    end

private

    def extract_locale_from_accept_language_header
        request.env['HTTP_ACCEPT_LANGUAGE'] && request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
    end

    def create_action_block(action, object_name, message, &block)
        begin
            ActiveRecord::Base.transaction do
                block.call
            end
        rescue DataCheckException => ex
            self.instance_variable_set('@' + object_name, self.instance_variable_get('@' + object_name).dup)
            self.instance_variable_get('@' + object_name).errors.add(:base, ex.message)
            render action: action
        rescue => ex
            logger.error ex.message
            self.instance_variable_set('@' + object_name, self.instance_variable_get('@' + object_name).dup)
            self.instance_variable_get('@' + object_name).errors.add(:base, message)
            render action: action
        end
    end

    def update_action_block(action, object_name, message, &block)
        begin
            ActiveRecord::Base.transaction do
                block.call
            end
        rescue DataCheckException => ex
            self.instance_variable_get('@' + object_name).errors.add(:base, ex.message)
            render action: action
        rescue => ex
            logger.error ex.message
            self.instance_variable_get('@' + object_name).errors.add(:base, message)
            render action: action
        end
    end

    def redirect_block(path, object_name, message, &block)
        begin
            ActiveRecord::Base.transaction do
                block.call
            end
        rescue DataCheckException => ex
            redirect_to path, flash: { alert: ex.message }
        rescue ActiveRecord::RecordInvalid => ex
            messages = self.instance_variable_get('@' + object_name).errors.full_messages
            message = messages.first if messages.present?

            redirect_to path, flash: { alert: message }
        rescue => ex
            logger.error ex.message
            logger.error ex.backtrace.join("\n") if ex.backtrace.present?

            redirect_to path, flash: { alert: message }
        end
    end
end
