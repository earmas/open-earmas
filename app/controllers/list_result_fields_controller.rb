#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ListResultFieldsController < BaseAdminController
    include OrderHolder

    maintenance_data_lock_methods :create, :update, :destroy, :up, :down

    before_action :check_mentenance_mode
    before_action :check_list_def_exists
    before_action :check_list_result_field, except: [:index, :new, :create]

    def new
        @list_result_field = ListResultField.new
    end

    def edit
    end

    def create
        @list_result_field = ListResultField.new(request_params)
        @list_result_field.list_def_id = @list_def.id
        @list_result_field.sort_index = ListResultField.where(list_def_id: @list_def.id).maximum(:sort_index)
        @list_result_field.sort_index = @list_result_field.sort_index && @list_result_field.sort_index + 1 || 1

        if @list_result_field.save
#            Rails.cache.clear
            redirect_to staff_list_def_url(@list_def), notice: '結果画面での表示項目を作成しました。'
        else
            render action: 'new'
        end
    end

    def update
        @list_result_field.assign_attributes(request_params)

        if @list_result_field.save
#            Rails.cache.clear
            redirect_to staff_list_def_url(@list_def), notice: '結果画面での表示項目を更新しました。'
        else
            render action: 'edit'
        end
    end

    def destroy
        @list_result_field.destroy
#        Rails.cache.clear
        redirect_to staff_list_def_url(@list_def), notice: '結果画面での表示項目を削除しました。'
    end

    def up
        move_up(@list_def.result_fields.to_a, @list_result_field)
#        Rails.cache.clear

        redirect_to staff_list_def_url(@list_def), notice: '結果画面での表示項目を移動しました。'
    end

    def down
        move_down(@list_def.result_fields.to_a, @list_result_field)
#        Rails.cache.clear

        redirect_to staff_list_def_url(@list_def), notice: '結果画面での表示項目を移動しました。'
    end

private

    def check_list_result_field
        @list_result_field = ListResultField.find(params[:id])
    end

    def check_list_def_exists
        @list_def = ListDef.find(params[:list_def_id])
    end

    def request_params
        params.require(:list_result_field).permit(:list_defs_id, :field_def_id)
    end
end
