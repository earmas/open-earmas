#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class CustomSelectsController < BaseAdminController

    maintenance_data_lock_methods :create, :update, :destroy

    before_action :check_mentenance_mode
    before_action :set_custom_select, except: [:index, :new, :create]

    def index
        @custom_selects = CustomSelect.all
    end

    def new
        @custom_select = CustomSelect.new
    end

    def create
        @custom_select = CustomSelect.new(request_params)

        if @custom_select.save
            redirect_to staff_custom_select_url(@custom_select), notice: '選択項目を作成しました。'
        else
            render action: 'new'
        end
    end

    def show
        @custom_select_values = CustomSelectValue.where(custom_select_id: @custom_select.id)
    end

    def edit
    end

    def update
        if @custom_select.update(request_params)
            redirect_to staff_custom_select_url(@custom_select), notice: '選択項目を更新しました。'
        else
            render action: 'edit'
        end
    end

    def destroy
        redirect_block(staff_custom_select_url(@custom_select), 'custom_select', '選択項目の削除に失敗しました。') do
            check_use_data
            @custom_select.destroy
            redirect_to staff_custom_selects_url, notice: '選択項目を削除しました。'
        end
    end

private

    def check_use_data
        FieldDefLoader.field_defs.each do |field_def|
            if field_def.field_type_class.get_select_id(field_def) == @custom_select.id
                raise DataCheckException, "この選択項目は使用中です" 
            end
        end

        if @oaipmh_setting.present? && @oaipmh_setting.set_spec_custom_select_id == @custom_select.id
            raise DataCheckException, "この選択項目はOAI-PMH設定で使用中です" 
        end
    end

    def set_custom_select
        @custom_select = CustomSelect.find(params[:id])
    end

    def request_params
        params.require(:custom_select).permit(:name)
    end

end
