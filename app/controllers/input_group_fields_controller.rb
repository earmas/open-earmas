#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class InputGroupFieldsController < BaseAdminController
    include OrderHolder

    maintenance_data_lock_methods :create, :update, :destroy, :up, :down

    before_action :check_mentenance_mode
    before_action :check_input_group
    before_action :check_input_group_field, except: [:index, :new, :create]

    def new
        @input_group_field = InputGroupField.new
    end

    def edit
    end

    def create
        @input_group_field = InputGroupField.new(request_params)
        @input_group_field.input_group_id = @input_group.id
        @input_group_field.sort_index = InputGroupField.where(input_group_id: @input_group.id).maximum(:sort_index)
        @input_group_field.sort_index = @input_group_field.sort_index && @input_group_field.sort_index + 1 || 1

        if @input_group_field.save
#            Rails.cache.clear
            redirect_to staff_input_group_url(@input_group), notice: 'データグループ項目を作成しました。'
        else
            render action: 'new'
        end
    end

    def update
        @input_group_field.assign_attributes(request_params)

        if @input_group_field.save
#            Rails.cache.clear
            redirect_to staff_input_group_url(@input_group), notice: 'データグループ項目を更新しました。'
        else
            render action: 'edit'
        end
    end

    def destroy
        @input_group_field.destroy
#        Rails.cache.clear
        redirect_to staff_input_group_url(@input_group), notice: 'データグループ項目を削除しました。'
    end

    def up
        move_up(@input_group.fields.to_a, @input_group_field)
#        Rails.cache.clear

        redirect_to staff_input_group_url(@input_group), notice: 'データグループ項目を移動しました。'
    end

    def down
        move_down(@input_group.fields.to_a, @input_group_field)
#        Rails.cache.clear

        redirect_to staff_input_group_url(@input_group), notice: 'データグループ項目を移動しました。'
    end

private

    def check_input_group_field
        @input_group_field = InputGroupField.find(params[:id])
    end

    def check_input_group
        @input_group = InputGroup.find(params[:input_group_id])
    end

    def request_params
        params.require(:input_group_field).permit(:input_groups_id, :caption_ja, :caption_en, :description, :field_def_id, :detail, :result, :must)
    end
end
