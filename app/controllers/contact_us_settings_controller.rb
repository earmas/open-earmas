#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ContactUsSettingsController < BaseAdminController

    def index
        @contact_us_setting = ContactUsSetting.all.first
    end

    def new
        @contact_us_setting = ContactUsSetting.new
        @contact_us_setting.set_default_child
    end

    def create
        @contact_us_setting = ContactUsSetting.new(request_params)

        if @contact_us_setting.save
            redirect_to staff_contact_us_settings_url, notice: 'メールフォーム管理が作成されました。'
        else
            render action: 'new'
        end
    end

    def edit
        @contact_us_setting = ContactUsSetting.all.first
        @contact_us_setting.set_default_child
    end

    def update
        @contact_us_setting = ContactUsSetting.all.first
        @contact_us_setting.assign_attributes(request_params)

        if @contact_us_setting.save
            redirect_to staff_contact_us_settings_url, notice: 'メールフォーム管理が更新されました。'
        else
            render action: 'edit'
        end
    end

private

    def request_params
        params.require(:contact_us_setting).permit(
            :subject,
            :from_address,
            :to_address,
            :caption_ja,
            :caption_en,
            { html_set_def_attributes: [:id, :body_top_html_string_ja, :body_top_html_string_en, :body_bottom_html_string_ja, :body_bottom_html_string_en, :header_parts_id, :footer_parts_id, :right_side_parts_id] })
    end

end
