#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class DownloadFormatsController < BaseAdminController

    before_action :set_download_format, only:[:show, :edit, :update, :destroy]

    def index
        @download_formats = DownloadFormat.all
    end

    def show
    end

    def new
        @download_format = DownloadFormat.new
    end

    def edit
    end

    def create
        @download_format = DownloadFormat.new(request_params)

        respond_to do |format|
            if @download_format.save
                format.html { redirect_to staff_download_formats_url, notice: 'ダウンロードフォーマットを作成しました。' }
                format.json { render action: 'show', status: :created, location: @download_format }
            else
                format.html { render action: 'new' }
                format.json { render json: @download_format.errors, status: :unprocessable_entity }
            end
        end
    end

    def update
        respond_to do |format|
            if @download_format.update(request_params)
                format.html { redirect_to staff_download_formats_url, notice: 'ダウンロードフォーマットを更新しました。' }
                format.json { head :no_content }
            else
                format.html { render action: 'edit' }
                format.json { render json: @download_format.errors, status: :unprocessable_entity }
            end
        end
    end

    def destroy
        @download_format.destroy
        respond_to do |format|
            format.html { redirect_to staff_download_formats_url, notice: 'ダウンロードフォーマットを削除しました。' }
            format.json { head :no_content }
        end
    end


private

    def set_download_format
        @download_format = DownloadFormat.find(params[:id])
    end

    def request_params
        params.require(:download_format).permit(:caption_ja, :caption_en, :xml_string)
    end

end
