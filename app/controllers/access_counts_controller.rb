#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class AccessCountsController < BaseStaffController

    require 'csv'

    def index
    end

    def month
        @data = Logs::LogCountData.new.load_all_log(params[:month])
        set_log_types
    end

    def month_download
        @data = Logs::LogCountData.new.load_all_log(params[:month])
        set_log_types


        csv_data = CSV.generate do |csv|
            csv << ['項目/件数', '値']
            csv << ['合計', @data.total]

            @datas.each do |data|
                csv << [data[:type], '']

                data[:values].each do |name, value|
                    csv << [value, decode_url(name)]
                end
            end
        end 

        headers["Content-Type"] = 'text/csv;charset=UTF-8'
        send_data csv_data, :type => 'text/csv', :filename => 'access_log_download.csv'
    end

    def person_month
        @datas = []
        LogFileAttach.get_person_log_dirs.each do |parson_id_dir|
            parson_id = File.basename(parson_id_dir)

            person = Person.get_value_encrypted(parson_id)
            next if person.blank?

            data = Logs::LogCountData.new.load_person_log(person.id, params[:month])

            @datas << {
                id: person.id,
                name_ja: [person.family_name_ja, person.given_name_ja].reject{|a|a.blank?}.join(' '),
                name_en: [person.given_name_en, person.family_name_en].reject{|a|a.blank?}.join(' '), 
                total: { count: data.total },
                access: { count: data.get_count_map(:access_log)['yes'] || 0 },
                download: { count: data.get_count_map(:download_log)['yes'] || 0 },
            }
        end
    end

    def person_month_download
        csv_data = CSV.generate do |csv|
            csv << ['ID', '名前(日本語)','名前(英語)', '合計', 'アクセス数', 'ダウンロード数']

            LogFileAttach.get_person_log_dirs.each do |parson_id_dir|
                parson_id = File.basename(parson_id_dir)

                person = Person.get_value_encrypted(parson_id)
                next if person.blank?

                data = Logs::LogCountData.new.load_person_log(person.id, params[:month])

                csv << [
                    person.id,
                    ([person.family_name_ja, person.given_name_ja].reject{|a|a.blank?}.join(' ')),
                    ([person.given_name_en, person.family_name_en].reject{|a|a.blank?}.join(' ')),
                    (data.total),
                    (data.get_count_map(:access_log)['yes'] || 0),
                    (data.get_count_map(:download_log)['yes'] || 0)
                ]
            end
        end 

        headers["Content-Type"] = 'text/csv;charset=UTF-8'
        send_data csv_data, :type => 'text/csv', :filename => 'persion_log_download.csv'
    end

private

    def set_log_types
        @data.build_title_map

        @datas = []

        types = {
            remote_host: 'アクセス元',
            user_agent: 'ユーザエージェント',
            id: 'ID',
            title_ja: 'タイトル',
            search_query: '検索内容',
            search_words: '検索語',
            bytes: 'バイト数 (4桁単位で切り捨て)',
            url: 'URL',
            referer_domain: 'リファラドメイン',
            referer: 'リファラ',
            access_log: 'アクセスログかどうか',
            download_log: 'ダウンロードログかどうか',
        }

        types.each do |name, value|
            set_log_type(value, name)
        end
    end

    def set_log_type(name, type)
        data = { type: name, values: {} }
        @data.get_count_map(type).each do |k, v|
            data[:values][k] = v
        end
        @datas << data
    end

    def decode_url(value)
        begin
            return URI.decode(value).scrub
        rescue => ex
            return value
        end
    end

end


