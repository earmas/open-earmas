#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class HtmlPartsDefsController < BaseAdminController

    before_action :check_html_parts_def_exists, except: [:index, :new, :create]

    def index
        @html_parts_defs = HtmlPartsDef.all
    end

    def new
        @html_parts_def = HtmlPartsDef.new
    end

    def create
        @html_parts_def = HtmlPartsDef.new(request_params)

        if @html_parts_def.save
            redirect_to staff_html_parts_defs_url, notice: '画面部品を作成しました。'
        else
            render action: 'new'
        end
    end

    def edit
    end

    def update
        @html_parts_def.assign_attributes(request_params)

        if @html_parts_def.save
            redirect_to staff_html_parts_defs_url, notice: '画面部品を更新しました。'
        else
            render action: 'edit'
        end
    end

    def destroy
        redirect_block(staff_html_parts_defs_url, 'html_parts_def', '画面部品の削除に失敗しました。') do
            check_use_data
            @html_parts_def.destroy
            redirect_to staff_html_parts_defs_url, notice: '画面部品を削除しました。'
        end
    end

private

    def check_use_data
        if HtmlSetDef.where('header_parts_id = ? or footer_parts_id = ? or right_side_parts_id = ?', @html_parts_def.id, @html_parts_def.id, @html_parts_def.id).count != 0
            raise DataCheckException, "使用中です"
        end
    end

    def check_html_parts_def_exists
        @html_parts_def = HtmlPartsDef.find(params[:id])
    end

    def request_params
        params.require(:html_parts_def).permit(
            :name,
            :html_string_ja,
            :html_string_en)
    end
end
