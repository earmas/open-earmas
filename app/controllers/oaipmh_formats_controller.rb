#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class OaipmhFormatsController < BaseAdminController

    maintenance_data_lock_methods :create, :update, :destroy

    before_action :check_mentenance_mode
    before_action :check_oaipmh_enable
    before_action :set_oaipmh_format, only:[:show, :edit, :update, :destroy]

    def index
        @oaipmh_formats = OaipmhFormat.all
    end

    def show
    end

    def new
        @oaipmh_format = OaipmhFormat.new
    end

    def edit
    end

    def create
        @oaipmh_format = OaipmhFormat.new(request_params)

        if @oaipmh_format.save
            redirect_to staff_oaipmh_settings_url, notice: 'フォーマットを作成しました。'
        else
            render action: 'new'
        end
    end

    def update
        @oaipmh_format.assign_attributes(request_params)

        if @oaipmh_format.save
            redirect_to staff_oaipmh_settings_url, notice: 'フォーマットを更新しました。'
        else
            render action: 'edit'
        end
    end

    def destroy
        @oaipmh_format.destroy
        redirect_to staff_oaipmh_settings_url, notice: 'フォーマットを削除しました。'
    end


private

    def check_oaipmh_enable
        return show_invalid if @system_setting.enable_oaipmh == false
    end

    def set_oaipmh_format
        @oaipmh_format = OaipmhFormat.find(params[:id])
    end

    def request_params
        params.require(:oaipmh_format).permit(:metadata_prefix, :schema, :metadata_namespace, :xml_string)
    end

end
