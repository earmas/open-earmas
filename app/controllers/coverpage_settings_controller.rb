#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class CoverpageSettingsController < BaseAdminController

    maintenance_data_lock_methods :update

    before_action :check_mentenance_mode
    before_action :check_coverpage_enable
    before_action :set_coverpage_setting

    def index
    end

    def edit
    end

    def update
        @coverpage_setting.cover_page_xml_string = params[:coverpage_setting][:cover_page_xml_string]

        if @coverpage_setting.save
            redirect_to staff_coverpage_settings_path, notice: 'カバーページの設定が更新されました。'
        else
            render action: 'edit'
        end
    end

private

    def set_coverpage_setting
        @coverpage_setting = CoverpageSetting.new
    end

end
