#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class SortableFieldsController < BaseAdminController
    include OrderHolder

    maintenance_data_lock_methods :create, :update, :destroy, :up, :down

    before_action :check_mentenance_mode
    before_action :check_sortable_field, except: [:index, :new, :create]

    def index
        @sortable_field = SortableField.where(input_group_id: @input_group.id)
    end

    def show
    end

    def new
        @sortable_field = SortableField.new
    end

    def create
        @sortable_field = SortableField.new(request_params)
        @sortable_field.sort_index = SortableField.maximum(:sort_index)
        @sortable_field.sort_index = @sortable_field.sort_index && @sortable_field.sort_index + 1 || 1

        if SortableField.all.size >= 3
            @sortable_field.errors.add(:base, "並び順は３個までです")
            return render action: 'new'
        end

        create_action_block('new', 'sortable_field', '並び順として使用するデータ項目の作成に失敗しました。') do
            if @sortable_field.save
                lock_sort
                lock_pri_sort
                if SortableField.all.size > 3
                    raise DataCheckException, '並び順は３個までです'
                end

                redirect_to staff_field_defs_url, notice: '並び順として使用するデータ項目を作成しました。'
            else
                render action: 'new'
            end
        end
    end

    def edit
    end

    def update
        @sortable_field.assign_attributes(request_params)

        update_action_block('edit', 'sortable_field', '並び順として使用するデータ項目の更新に失敗しました。') do
            if @sortable_field.save
                lock_sort
                lock_pri_sort
                redirect_to staff_field_defs_url, notice: '並び順として使用するデータ項目を更新しました。'
            else
                render action: 'edit'
            end
        end
    end

    def destroy
        redirect_block(staff_field_defs_url, 'sortable_field', '並び順として使用するデータ項目の削除に失敗しました。') do
            check_use_data
            @sortable_field.destroy!
            lock_sort
            lock_pri_sort
            redirect_to staff_field_defs_url, notice: '並び順として使用するデータ項目を削除しました。'
        end
    end

    def up
        move_up(SortableField.all.to_a, @sortable_field)

        redirect_to staff_field_defs_url, notice: '並び順として使用するデータ項目を移動しました。'
    end

    def down
        move_down(SortableField.all.to_a, @sortable_field)

        redirect_to staff_field_defs_url, notice: '並び順として使用するデータ項目を移動しました。'
    end

private

    def check_use_data
        if ListDef.where(sortable_field_id: @sortable_field.id).count != 0
            raise DataCheckException, "一覧で使用中です"
        end
    end

    def check_sortable_field
        @sortable_field = SortableField.find(params[:id])
    end

    def request_params
        params.require(:sortable_field).permit(:field_def_id)
    end
end
