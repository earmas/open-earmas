#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module SortablePage extend ActiveSupport::Concern
    include LocaleHelper, Download

private 

    def get_default_code(sortable_field_id)
        return if sortable_field_id.blank?

        case sortable_field_id
        when -1
            'id'
        when -2
            'updated_at'
        else
            SortableField.find(sortable_field_id).field_def.code
        end
    end

    def set_sort_param
        if params[:sort].present?
            sort_field_code  = params[:sort].split(':')
            @sort_field_code  = sort_field_code[0]
            @sort_reverse = sort_field_code.size == 2 && sort_field_code[1] == 'r' || false
        else
            @sort_field_code  = 'id'
            @sort_reverse  = true
        end
    end

    def get_sort_column_name
        case @sort_field_code
        when 'id'
            'id'
        when 'updated_at'
            'updated_at'
        else
            @sort_field_index = nil
            SortableField.get_all.each_with_index do |sortable_field, index|
                @sort_field_index = (index + 1) if sortable_field.field_def.code == @sort_field_code
            end

            return 'id' if @sort_field_index.blank?
            set_locale_suffix('sort_value') + '_' + @sort_field_index.to_s
        end
    end
end

