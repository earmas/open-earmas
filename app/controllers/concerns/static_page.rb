#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module StaticPage extend ActiveSupport::Concern
    include ViewStateHelper, LocaleHelper

    def index
        @param1 = params[:param1]
        @param2 = params[:param2]
        @param3 = params[:param3]
        @param4 = params[:param4]
        @param5 = params[:param5]
        @page_title = show_caption(@page_obj) + ' - ' + @page_title
        render file: 'view_state/static_pages/index'
    end

private

    def check_static_page_def_exists
        @static_page_def = StaticPageDef.where(code: params[:static_page_def_code]).first
        return show_not_found if @static_page_def.blank?
        return show_not_found if view_state_visible(@static_page_def) == false
    end

    def set_page_obj
        @page_obj = @static_page_def
    end

end
