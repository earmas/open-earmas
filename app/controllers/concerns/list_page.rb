#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module ListPage extend ActiveSupport::Concern
    include ViewStateHelper, SortablePage, ListHelper, Download, DocumentsHelper

    SKIP_VALUE = '--'

private

    def check_list_exists
        @list_def = ListDef.where(code: params[:list_def_code]).includes(:list_fields).first
        return show_not_found if @list_def.blank?
        return show_not_found if view_state_visible(@list_def) == false
    end

    def set_page_obj
        @page_obj = @list_def
    end

    def param_init
        @document_id  = params[:id]
        @download_id  = params[:download_id]
        @search_query  = params[:search_query]

        @level_params = []
        create_level_params(params)
        @level = @level_params.size

        set_sort_param
        set_list_default_sort
        set_volume
    end

    def set_level(params)
        @level = 0
        5.times do |index|
            level = index + 1
            level_sym = ('level' + level.to_s).to_sym
            if params[level_sym].present?
                @level = level
            end
        end
    end

    def create_level_params(params)
        set_level(params)

        return if @level == 0

        list_word_relation = view_state_list_word_relation(@level).where(list_def_id: @list_def.id)

        @level.times do |index|
            level = index + 1
            level_sym = ('level' + level.to_s).to_sym

            if params[level_sym] == SKIP_VALUE
                list_word_relation.where!('value' + level.to_s => '')
            else
                list_word_relation.where!('value' + level.to_s => params[level_sym])
            end
        end

        list_word = list_word_relation.first
        @level.times do |index|
            if list_word.blank?
                @level_params[index] = ListWordSkip.new
            else
                @level_params[index] = list_word.get_level_list_word(index)
            end
        end
    end

    def set_list_default_sort
        return if params[:sort].present?

        @sort_field_code = get_default_code(@list_def.sortable_field_id)
        @sort_reverse = @list_def.sort_reverse
    end

    def set_volume
        if params[:v].present?
            @volume = params[:v].to_i 
            @volume = 10 if @volume > 10
        end
    end

    def set_list_word_param_condition(relation)
        @level.times do |index|
            level = index + 1
            if @level_params[index].value == ListPage::SKIP_VALUE
                relation.where!('value' + level.to_s => '')
            else
                relation.where!('value' + level.to_s => @level_params[index].value)
            end
        end

        if @prev_list_field_def != nil && @prev_list_field_def.prefix_list?
            relation.where!('value' + (@level + 1).to_s + '_prefix' => @level_params[@level - 1].value)
        end

        sort_field = set_locale_suffix('sort_value') + (@level + 1).to_s

        if @list_field_def.sort_reverse
            relation.order!(sort_field => :desc)
        else
            relation.order!(sort_field => :asc)
        end
    end

    def set_list_datum_param_condition(relation)
        @level.times do |index|
            level = index + 1
            if @level_params[index].value == ListPage::SKIP_VALUE
                relation = relation.where('level' + level.to_s + ' = \'{}\'')
            else
                relation = relation.where('level' + level.to_s + ' @> ARRAY[?]', [@level_params[index].value])
            end
        end
        relation
    end
    def set_list_datum_param_condition_with_sort(relation)
        relation = set_list_datum_param_condition(relation)

        sort_column_name = get_sort_column_name 

        sort_column_name = 'document_id' if sort_column_name == 'id'
        sort_column_name = 'document_updated_at' if sort_column_name == 'updated_at'

        if @sort_reverse
            relation.order(sort_column_name => :desc)
        else
            relation.order(sort_column_name => :asc)
        end
    end

    def set_list_title
        @page_title = show_caption(@page_obj) + ' - ' + @page_title

        if @level_params.present? && @level_params.size >= 1
            @page_title = show_level_params(@level_params) + ' - ' + @page_title
        end
    end

    def list_level
        @current_list_field_def = @list_def.get_field(@level)
        @prev_list_field_def = @list_def.get_field(@level)
        @list_field_def = @list_def.get_field(@level + 1)

        set_list_title

        if @list_field_def.blank?

            if @document_id.present?
                @document = view_state_relation(:Document).find(@document_id)
                @page_obj.page_type = @page_obj.class::DETAIL_PAGE
                set_meta_info
                set_page_title_by_document
                render file: 'view_state/lists/show', formats: [:html]
                return
            end

            if @download_id.present?
                @documents = set_list_datum_param_condition_with_sort(view_state_relation(:ListDatum).where(list_def_id: @list_def.id)).limit(Download::LIMIT).map{|a| a.document }
                download(@download_id)
                return
            end

            if @volume.present?
                @result = set_list_datum_param_condition_with_sort(view_state_relation(:ListDatum).includes(:document).where(list_def_id: @list_def.id)).limit(@volume)
            else
                @result = set_list_datum_param_condition_with_sort(view_state_relation(:ListDatum).includes(:document).where(list_def_id: @list_def.id))
                if @list_def.result_view_type_hash[:result_all] != true
                    @result = @result.page(params[:page])
                end
            end

            @page_obj.page_type = @page_obj.class::RESULT_PAGE

            respond_to do |format|
                format.html do
                    @documents = @result.map{|a| a.document }
                    render file: 'view_state/lists/result'
                end
                format.nolayout do
                    @documents = @result.map{|a| a.document }
                    render file: 'view_state/lists/result_nolayout', layout: false, formats: [:html]
                end
                format.rss do
                    @result.reorder!(updated_at: :desc)
                    @documents = @result.map{|a| a.document }
                    rss
                end
            end
        else
            if @list_field_def.prefix_list?
                @param_relation = set_list_word_param_condition(view_state_list_word_relation(@level + 1).where(list_def_id: @list_def.id))
            else
                @param_relation = set_list_word_param_condition(view_state_list_word_relation(@level + 1).where(list_def_id: @list_def.id)).page(params[:page])
            end

            @list_words = []
            @param_relation.each do |list_word|
                @list_words << list_word.get_level_list_word(@level)
            end

            if @list_words.size <= 1 && params[:page].blank?
                if @level < @list_def.get_max_level.size
                    if @list_words.size == 0
                        return redirect_to gen_next_list_path(@level_params, ListWordSkip.new)
                    else
                        return redirect_to gen_next_list_path(@level_params, @list_words.first)
                    end
                end

                # ここに到達するには次のフィールドがある事が前提。だとすると、上の分岐で全部飛ぶから到達し得ないのではないか
                if @document_id.present?
                    @document = view_state_relation(:Document).find(@document_id)
                    @page_obj.page_type = @page_obj.class::DETAIL_PAGE
                    set_meta_info
                    set_page_title_by_document
                    render file: 'view_state/lists/show', formats: [:html]
                    return
                end

                if @download_id.present?
                    @documents = set_list_datum_param_condition_with_sort(view_state_relation(:ListDatum).where(list_def_id: @list_def.id)).limit(Download::LIMIT).map{|a| a.document }
                    download(@download_id)
                    return
                end

                if @volume.present?
                    @result = set_list_datum_param_condition_with_sort(view_state_relation(:ListDatum).includes(:document).where(list_def_id: @list_def.id)).limit(@volume)
                else
                    @result = set_list_datum_param_condition_with_sort(view_state_relation(:ListDatum).includes(:document).where(list_def_id: @list_def.id))
                    if @list_def.result_view_type_hash[:result_all] != true
                        @result = @result.page(params[:page])
                    end
                end

                @page_obj.page_type = @page_obj.class::RESULT_PAGE

                respond_to do |format|
                    format.html do
                        @documents = @result.map{|a| a.document }
                        render file: 'view_state/lists/result'
                    end
                    format.nolayout do
                        @documents = @result.map{|a| a.document }
                        render file: 'view_state/lists/result_nolayout', layout: false, formats: [:html]
                    end
                    format.rss do
                        @result.reorder!(updated_at: :desc)
                        @documents = @result.map{|a| a.document }
                        rss
                    end
                end
            else
                if @search_query.present?
                    @param_relation = @param_relation.where(set_locale_suffix(:caption) + (@level + 1).to_s + ' LIKE ?' , "#{@search_query}%")

                    @list_words = []
                    @param_relation.each do |list_word|
                        @list_words << list_word.get_level_list_word(@level)
                    end
                end

                @page_obj = @list_field_def
                @page_obj.page_type = @page_obj.class::LIST_PARAM_PAGE

                respond_to do |format|
                    format.html do
                        render file: 'view_state/lists/index'
                    end
                    format.nolayout do
                        show_not_found
                    end
                    format.rss do
                        show_not_found
                    end
                end
            end
        end
    end

    alias_method :index,  :list_level
    alias_method :level1, :list_level
    alias_method :level2, :list_level
    alias_method :level3, :list_level
    alias_method :level4, :list_level
    alias_method :level5, :list_level

    public :index, :level1, :level2, :level3, :level4, :level5

end

