#--
# Open Earmas
#
# Copyright (C) 2014 Hiroshima University, ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module JournalPage extend ActiveSupport::Concern
    include ViewStateHelper, Download, DocumentsHelper, LocaleHelper

    SKIP_VALUE = '--'

    def all_index
        all_journal_index = @ej_journal_def.all_journal_index
        if all_journal_index.blank?
            return show_invalid
        end

        @json = ActiveSupport::JSON.decode(all_journal_index)

        @ej_word_map = {}
        view_state_word_relation(:EjWord).where(ej_def_id: @ej_def.id, journal: @ej_journal_def.value).each do |ej_word|
            @ej_word_map[ej_word.id] = ej_word
        end

        @document_map = {}
        view_state_relation(:EjDatum).where(ej_def_id: @ej_def.id, journal: @ej_journal_def.value).includes(:document).each do |datum|
            @document_map[@ej_def.toc_index(datum.volume, datum.issue)] ||= {}
            @document_map[@ej_def.toc_index(datum.volume, datum.issue)][datum.document_id] = { datum: datum, used: false }
        end

        render file: 'view_state/journals/all_index'
    end

    def journal
        render file: 'view_state/journals/journal'
    end

    def voliss
        render file: 'view_state/journals/journal'
    end

    def article
        render file: 'view_state/journals/article'
    end

    def static_page
        render file: 'view_state/journals/static_page'
    end

private

    def check_ej_journal_def_exists
        @ej_journal_def = EjJournalDef.where(code: params[:ej_journal_code]).first
        return show_not_found if @ej_journal_def.blank?
        return show_not_found if view_state_visible(@ej_journal_def) == false
        @page_obj = @ej_journal_def
        @page_obj.page_type = @page_obj.class::JOURNAL_PAGE

        @ej_def = @ej_journal_def.ej_def
        return show_not_found if @ej_def.blank?

        @ej_words = view_state_word_relation(:EjWord).where(ej_def_id: @ej_def.id, journal: @ej_journal_def.value)
        @ej_words.order!(set_locale_suffix('publish_date_sort_value') => :desc, volume: :desc, issue: :desc)

        @ej_word = @ej_words.first
        @ej_word_first = true
        @ej_word_is_newest = true

        @ej_date_first = @ej_words.first.try(:publish_date)
        @ej_date_last = @ej_words.last.try(:publish_date)
        @ej_data_count = view_state_word_relation(:EjWord).where(ej_def_id: @ej_def.id, journal: @ej_journal_def.value).sum(:document_count)
        @ej_volume_count = view_state_word_relation(:EjWord).distinct.where(ej_def_id: @ej_def.id, journal: @ej_journal_def.value).where.not(volume: '').count(:volume)
        @ej_issue_count = view_state_word_relation(:EjWord).distinct.where(ej_def_id: @ej_def.id, journal: @ej_journal_def.value).count

        @journal_base_css_string = @ej_def.css_string
        @journal_css_string = @ej_journal_def.css_string
        @journal_attach_file_dir = @ej_journal_def.journal_attach_file_dir
    end

    def check_ej_word_exists
        replace_skip_value_to_blank
        @ej_word_is_newest = false

        if @ej_word.volume == @journal_volume && @ej_word.issue == @journal_issue
            @ej_word_is_newest = true
        end

        @ej_word = view_state_word_relation(:EjWord).where(ej_def_id: @ej_def.id, journal: @ej_journal_def.value, volume: @journal_volume, issue: @journal_issue).first
        @ej_word_first = false
        return show_not_found if @ej_word.blank?
    end

    def set_toc
        all_journal_index = @ej_journal_def.all_journal_index
        if all_journal_index.present?
            @json = ActiveSupport::JSON.decode(all_journal_index)
            if @ej_word.blank?
                @json = []
                return
            end
            @json = @json[@ej_word.toc_index]

            @ej_word_map = {}
            view_state_word_relation(:EjWord).where(ej_def_id: @ej_def.id, journal: @ej_journal_def.value).each do |ej_word|
                @ej_word_map[ej_word.id] = ej_word
            end

            @document_map = {}
            @ej_word.get_data_relation.order(set_locale_suffix('sort_value') => :asc).includes(:document).each do |datum|
                @document_map[datum.document_id] = { datum: datum, used: false }
            end
        end
    end

    def check_document_exists
        @ej_datum = view_state_relation(:EjDatum).where(ej_def_id: @ej_def.id, journal: @ej_journal_def.value, volume: @journal_volume, issue: @journal_issue, document_id: params[:id]).includes(:document).first
        return show_not_found if @ej_datum.blank?

        @document = @ej_datum.document
        return show_not_found if @document.blank?


        @page_obj.page_type = @page_obj.class::ARTICLE_PAGE
    end

    def check_static_page_exists
        @ej_static_page_def = @ej_journal_def.static_page_defs.where(code: params[:page_code]).first
        return show_not_found if @ej_static_page_def.blank?

        @page_obj = @ej_static_page_def
    end


    def set_page_obj
    end

    def param_init
        @download_id  = params[:download_id]
    end

    def replace_skip_value_to_blank
        @journal_volume = params[:volume]
        if @journal_volume == JournalPage::SKIP_VALUE
            @journal_volume = ''
        end

        @journal_issue = params[:issue]
        if @journal_issue == JournalPage::SKIP_VALUE
            @journal_issue = ''
        end
    end

end

