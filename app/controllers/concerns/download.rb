#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module Download extend ActiveSupport::Concern

    LIMIT = 200

    require 'nokogiri'
    require 'csv'

private

    def download(download_id_string)

        (download_id, encoding) = download_id_string.split(':')

        case encoding
        when 's'
            @encoding = Encoding::SJIS
        else
            @encoding = Encoding::UTF_8
        end

        download_format = DownloadFormat.find_by(id: download_id)
        return show_not_found if download_format.blank?

        documents_xml_string = render_to_string 'view_types/result/documents', formats: [:xml]
        xml_doc = Nokogiri::XML(documents_xml_string)

        xslt = Nokogiri::XSLT(download_format.xml_string)
        result = xslt.transform(xml_doc)

        headers["Content-Type"] = 'text/plain;charset=' + @encoding.name

        if download_format.xml_string.include?('omit-xml-declaration="no"') || download_format.xml_string.include?('omit-xml-declaration=\'no\'')
            send_data result.to_xml(encoding: @encoding.to_s).encode(@encoding, :invalid => :replace, :undef => :replace, :replace => '?'), :type => 'text/xml', :filename => 'download.xml'
        else
            send_data result.text.encode(@encoding, :invalid => :replace, :undef => :replace, :replace => '?'), :type => 'text/plain', :filename => 'download.txt'
        end
    end

    def rss
        return show_not_found if @list_def.rss_xml_string.blank?

        documents_xml_string = render_to_string 'view_types/result/documents', formats: [:xml]
        doc1 = Nokogiri::XML(documents_xml_string)

        xslt1 = Nokogiri::XSLT(@list_def.rss_xml_string)
        doc2 = xslt1.transform(doc1)

        xslt_string = render_to_string 'view_state/lists/rss', formats: [:xml]

        xslt2 = Nokogiri::XSLT(xslt_string)
        result = xslt2.transform(doc2)

        headers["Content-Type"] = 'application/rss+xml;charset=UTF-8'
        render text: result.to_xml(encoding: 'UTF-8')
    end

    def set_meta_info
        return if @system_setting.meta_info_xml_string.blank?

        if @document.meta_tag_xml.blank?
            @documents = [@document]

            documents_xml_string = render_to_string 'view_types/result/documents', formats: [:xml]
            xml_doc = Nokogiri::XML(documents_xml_string)
            xslt = Nokogiri::XSLT(@system_setting.meta_info_xml_string)
            @document.update_columns(meta_tag_xml: xslt.transform(xml_doc).to_xml(encoding: 'UTF-8'))
        end

        @meta_info =  @document.meta_tag_xml.try(:html_safe)
    end

end

