#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module ContactUs extend ActiveSupport::Concern
    include ViewStateHelper, LocaleHelper

    def index
        @contact_mail = ContactMail.new

        @page_title = show_caption(@page_obj) + ' - '
        render file: 'view_state/contact_us/index', formats: [:html]
    end

    def send_mail
        @contact_mail = ContactMail.new

        @contact_mail.email = params[:contact_mail][:email]
        @contact_mail.body = params[:contact_mail][:body]

        if @contact_mail.valid?
            @mailer = ContactMailer.contact_us(@contact_us_setting.from_address, @contact_us_setting.to_address, @contact_us_setting.subject, @contact_mail)
            @mailer.deliver

            redirect_to view_state_path_gen(:result_contact_us_url).call, send_email: @contact_mail.email, send_body: @contact_mail.body
        else
            @page_title = show_caption(@page_obj) + ' - '
            render file: 'view_state/contact_us/index', formats: [:html]
        end
    end

    def result
        @page_title = show_caption(@page_obj) + ' - '
        render file: 'view_state/contact_us/result', formats: [:html]
    end

private 

    def check_contact_us_setting
        @contact_us_setting = ContactUsSetting.all.first
        return show_not_found if @contact_us_setting == nil
        return show_not_found if @contact_us_setting.from_address.blank? || @contact_us_setting.to_address.blank? || @contact_us_setting.subject.blank?
        @page_obj = @contact_us_setting
    end


end
