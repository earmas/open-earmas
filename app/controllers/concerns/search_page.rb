#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module SearchPage extend ActiveSupport::Concern
    include ViewStateHelper, SortablePage, DocumentsHelper

    def index
        @page_title = '検索結果 - ' + @page_title

        @search_term_exists = false
        search_term_map = {}
        field_index = 1
        SearchDef.all.reorder(:id).each do |search|
            search_term_map[field_index] = params[search.code.to_s] if params[search.code.to_s].present?
            field_index += 1
            @search_term_exists = true if params[search.code.to_s].present?
        end

        if params[:id].present?
            @document = view_state_class_name_gen(:Document).find(params[:id])

            @page_obj.page_type = @page_obj.class::DETAIL_PAGE
            set_meta_info
            set_page_title_by_document
            return render file: 'view_state/searches/show', formats: [:html]
        end

        set_sort_param

        result = view_state_class_name_gen(:Document).search do |s|
            if params[:all].present?
                if params[:include_file] != 'include'
                    s.fulltext params[:all] do
                        fields('text_value_all')
                    end
                else
                    s.fulltext params[:all] do
                        fields('text_value_all_with_file')
                    end
                end
            end

            search_term_map.keys.sort.each do |index|
                search_term = search_term_map[index]
                if search_term.present?
                    s.fulltext search_term do
                        fields('text_value_' + index.to_s)
                    end
                end
            end

            view_state_search(s)

            if @sort_reverse
                s.order_by(get_sort_column_name, :desc)
            else
                s.order_by(get_sort_column_name, :asc)
            end

            if params[:download_id].present?
                s.paginate :page => 1, :per_page => Download::LIMIT
            else
                s.paginate page: params[:page], per_page: Kaminari.config.default_per_page
            end
        end

        @result = result.hits
        @documents = result.results
        @count = result.total

        if params[:download_id].present?
            download(params[:download_id])
            return
        end

        @page_obj.page_type = @page_obj.class::RESULT_PAGE

        render file: 'view_state/searches/index'
    end

    def search
        search_param = {}

        search_param[:all] = params[:all] if params[:all].present?
        search_param[:include_file] = params[:include_file] if params[:include_file].present?
        SearchDef.all.reorder(:id).each do |search|
            search_param[search.code.to_s] = params[search.code.to_s] if params[search.code.to_s].present?
        end

        redirect_to view_state_path_gen(:search_path).call(search_param)
    end

private 

    def set_page_obj
        @page_obj = @search_setting
    end

end

