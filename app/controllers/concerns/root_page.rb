#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module RootPage extend ActiveSupport::Concern
    include ListPage, SortablePage

    def index
        if params[:id].present?
            @document = view_state_relation(:Document).find(@system_setting.public_id_to_document_id(params[:id]))

            set_meta_info
            set_page_title_by_document

            input_group = FieldDefLoader.get_input_group(@document.input_group_id)
            if input_group == nil
                @page_obj = @system_setting
            else
                @page_obj = input_group
            end

            render file: 'view_state/items/show', formats: [:html]
            return
        end

        case @system_setting.top_page_type
        when LeftMenuDef::TYPE_LIST_DEF

            @list_def = ListDef.find(@system_setting.top_page_id)
            return render file: 'view_state/root/index' if @list_def.blank?

            if view_state_visible(@list_def) == false
                render file: 'view_state/root/index'
                return
            end

            @page_obj = @list_def

            @level_params = []
            @level = 0

            set_sort_param
            set_list_default_sort
            set_volume

            list_level

        when LeftMenuDef::TYPE_STATIC_PAGE_DEF
            @static_page_def = StaticPageDef.find_by(id: @system_setting.top_page_id)
            return render file: 'view_state/root/index' if @static_page_def.blank?


            if view_state_visible(@static_page_def) == false
                render file: 'view_state/root/index'
                return
            end

            @page_obj = @static_page_def
            @page_title = show_caption(@page_obj) + ' - ' + @page_title

            render file: 'view_state/static_pages/index'

        else
            render file: 'view_state/root/index'
        end
    end

    def thumnail
        if @document.has_thumnail?
            send_file @document.thumnail_path, type: 'image/jpg', filename: 'thumnail', disposition: "inline"
        else
            show_not_found
        end
    end

    def attach
        file_name = params[:name]

        file = File.join(@document.document_attach_file_dir, file_name)
        if File.exists?(file)
            send_file file, filename: file_name, disposition: "inline"
        else
            show_not_found
        end
    end

private

    def check_document_exists
        @document = view_state_relation(:Document).find(params[:id])
    end

end