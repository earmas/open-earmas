#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module OrderHolder extend ActiveSupport::Concern

private

    def move_up(items, current_item, field_name = 'sort_index')
        ActiveRecord::Base.transaction do
            move_up_no_transaction(items, current_item, field_name)
        end
    end

    def move_down(items, current_item, field_name = 'sort_index')
        ActiveRecord::Base.transaction do
            move_down_no_transaction(items, current_item, field_name)
        end
    end

    def move_up_no_transaction(items, current_item, field_name = 'sort_index')
        current_index = 0
        items.each_with_index do |item, index|
            current_index = index if item.id == current_item.id
        end

        return if current_index <= 0

        new_sort_index = items[current_index - 1].send(field_name.to_s)
        current_sort_index = current_item.send(field_name.to_s)

        current_item.send(field_name.to_s + '=', 0)
        current_item.save!

        items[current_index - 1].send(field_name.to_s + '=', current_sort_index)
        items[current_index - 1].save!
        current_item.send(field_name.to_s + '=', new_sort_index)
        current_item.save!


        items.insert(current_index - 1, items.delete_at(current_index))

        new_index = 1
        items.each do |item|
            item.send(field_name.to_s + '=', new_index)
            item.save! if item.send(field_name.to_s + '_changed?')
            new_index += 1
        end
    end

    def move_down_no_transaction(items, current_item, field_name = 'sort_index')
        current_index = 0
        items.each_with_index do |item, index|
            current_index = index if item.id == current_item.id
        end

        return if current_index >= items.size - 1

        new_sort_index = items[current_index + 1].send(field_name.to_s)
        current_sort_index = current_item.send(field_name.to_s)

        current_item.send(field_name.to_s + '=', 0)
        current_item.save!

        items[current_index + 1].send(field_name.to_s + '=', current_sort_index)
        items[current_index + 1].save!
        current_item.send(field_name.to_s + '=', new_sort_index)
        current_item.save!


        items.insert(current_index + 1, items.delete_at(current_index))

        new_index = 1
        items.each do |item|
            item.send(field_name.to_s + '=', new_index)
            item.save! if item.send(field_name.to_s + '_changed?')
            new_index += 1
        end
    end
end

