#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FieldLock extend ActiveSupport::Concern

private

    def set_lock_pri_field(field_def_ids, include_sort = true)
        return if field_def_ids.blank?
        lock_pri_sort if SortableField.where(field_def_id: field_def_ids).count != 0 && include_sort

        lock_pri_search if SearchFieldDef.where(field_def_id: field_def_ids).count != 0
        lock_pri_search if SearchFilterDef.where(parent_type: DocumentFilterDef::SEARCH_TYPE, filter_type: [DocumentFilterDef::FILTER_TYPES[:field_value_filter][:id], DocumentFilterDef::FILTER_TYPES[:field_key_filter][:id]], some_id: field_def_ids).count != 0


        list_ids = ListFieldDef.where(field_def_id: field_def_ids).pluck(:list_def_id)
        list_ids |= ListFilterDef.where(parent_type: DocumentFilterDef::LIST_TYPE, filter_type: [DocumentFilterDef::FILTER_TYPES[:field_value_filter][:id], DocumentFilterDef::FILTER_TYPES[:field_key_filter][:id]], some_id: field_def_ids).pluck(:parent_id)

        lock_pri_lists(list_ids) if list_ids.present?


        ej_ids = EjDef.where(journal_field_def_id: field_def_ids).pluck(:id)
        ej_ids |= EjDef.where(volume_field_def_id: field_def_ids).pluck(:id)
        ej_ids |= EjDef.where(issue_field_def_id: field_def_ids).pluck(:id)
        ej_ids |= EjDef.where(section_field_def_id: field_def_ids).pluck(:id)
        ej_ids |= EjDef.where(publish_date_field_def_id: field_def_ids).pluck(:id)
        ej_ids |= EjDef.where(sort_field_def_id: field_def_ids).pluck(:id)
        ej_ids |= EjFilterDef.where(parent_type: DocumentFilterDef::EJ_TYPE, filter_type: [DocumentFilterDef::FILTER_TYPES[:field_value_filter][:id], DocumentFilterDef::FILTER_TYPES[:field_key_filter][:id]], some_id: field_def_ids).pluck(:parent_id)

        lock_pri_ejs(ej_ids) if ej_ids.present?


        lock_pri_oaipmh if OaipmhFilterDef.where(parent_type: DocumentFilterDef::OAIPMH_TYPE, filter_type: [DocumentFilterDef::FILTER_TYPES[:field_value_filter][:id], DocumentFilterDef::FILTER_TYPES[:field_key_filter][:id]], some_id: field_def_ids).count != 0
    end

    def set_lock_list(field_def_ids, include_sort = true)
        return if field_def_ids.blank?
        if SortableField.where(field_def_id: field_def_ids).count != 0 && include_sort
            lock_sort
            lock_pri_sort
        end

        list_ids = ListFieldDef.where(field_def_id: field_def_ids).pluck(:list_def_id)

        if list_ids.present?
            lock_lists(list_ids)
            lock_pri_lists(list_ids)
        end

        ej_ids = EjDef.where(volume_field_def_id: field_def_ids).pluck(:id)
        ej_ids |= EjDef.where(issue_field_def_id: field_def_ids).pluck(:id)
        ej_ids |= EjDef.where(publish_date_field_def_id: field_def_ids).pluck(:id)
        ej_ids |= EjDef.where(sort_field_def_id: field_def_ids).pluck(:id)

        if ej_ids.present?
            lock_ejs(ej_ids)
            lock_pri_ejs(ej_ids)
        end
    end

end
