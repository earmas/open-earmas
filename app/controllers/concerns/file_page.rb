#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FilePage extend ActiveSupport::Concern
    include ViewStateHelper

    def show
        return redirect_to (root_url.chop + public_dir_file_to_url_path(@field_file.file_path)) if ViewState.pri? == false

        if @field_file.image?
            send_file @field_file.file_path, type: @field_file.mime, filename: @field_file.org_file_name, disposition: "inline"
        else
            send_file @field_file.file_path, type: @field_file.mime, filename: @field_file.org_file_name
        end
    end

    def thumnail
        return redirect_to (root_url.chop + public_dir_file_to_url_path(@field_file.thumnail_path)) if ViewState.pri? == false

        if @field_file.has_thumnail?
            send_file @field_file.thumnail_path, type: 'image/jpg', filename: @field_file.org_file_name, disposition: "inline"
        else
            render text: 'not found', status: 404
        end
    end

    def show_first
        return redirect_to (root_url.chop + public_dir_file_to_url_path(@field_file.file_path)) if ViewState.pri? == false

        if @field_file.image?
            send_file @field_file.file_path, type: @field_file.mime, filename: @field_file.org_file_name, disposition: "inline"
        else
            send_file @field_file.file_path, type: @field_file.mime, filename: @field_file.org_file_name
        end
    end

    def thumnail_first
        return redirect_to (root_url.chop + public_dir_file_to_url_path(@field_file.thumnail_path)) if ViewState.pri? == false

        if @field_file.has_thumnail?
            send_file @field_file.thumnail_path, type: 'image/jpg', filename: @field_file.org_file_name, disposition: "inline"
        else
            render text: 'not found', status: 404
        end
    end

private

    def set_field_file
        field_def = FieldDefLoaderForInput.get_field_def(params[:field_def_id])
        @field_file = field_def.get_type_class_by_view_state.data_class.find(params[:id])

        @document = view_state_relation(:Document).find(params[:document_id])

        return show_invalid if @field_file.document_id != @document.id
    end

    def set_first_file
        @document = view_state_relation(:Document).find(params[:id])

        fields = []
        @document.detail_field_defs.each do |field_def|
            next if @document.field_empty?(field_def.id)
            next if field_def.field_type_class.file_holder? == false

            fields += @document.get_fields(field_def.id)
        end

        return show_not_found if fields.size == 0

        @field_file = fields.first
    end

end