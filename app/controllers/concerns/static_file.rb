#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module StaticFile extend ActiveSupport::Concern

    def index
    end

    def create
        @upload_files = params[:upload_file]

        begin
            save_upload_file
            redirect_to({ action: 'index', dir: @sub_dir_param, only_path: false }, notice: 'ファイルをアップロードしました。')
        rescue DataCheckException => ex
            redirect_to({ action: 'index', dir: @sub_dir_param, only_path: false }, alert: ex.message)
        rescue => ex
            logger.error ex.message
            logger.error ex.backtrace.join("\n") if ex.backtrace.present?

            redirect_to({ action: 'index', dir: @sub_dir_param, only_path: false }, alert: message)
        end
    end

    def destroy
        destory_file_name = params[:name]

        Dir::glob(File.join(@current_dir, '*')).each do |static_file|
            if destory_file_name == File.basename(static_file)
                File::delete(static_file) if File.exists?(static_file)
                break;
            end
        end

        redirect_to({ action: :index, dir: @sub_dir_param, only_path: false })
    end

    def create_directory
        if paths = params[:name].match(/^[a-zA-Z0-9_]+$/)
            FileUtils.mkdir(File.join(@current_dir, paths[0]))

            redirect_to({ action: :index, dir: @sub_dir_param, only_path: false }, notice: 'ディレクトリを作成しました')
        else
            redirect_to({ action: :index, dir: @sub_dir_param, only_path: false }, alert: '不正なディレクトリ名です。')
        end
    end

private

    def set_directory
        current_dir_path = params[:dir]
        @sub_dirs = []
        @sub_dir_param = nil
        @upper_sub_dir_param = nil
        return if current_dir_path.blank?

        current_dir_path.split(',').each do |name|
            hits = Dir::glob(File.join(@current_dir, "*/")).select{|a| File.basename(a) == name}

            return show_not_found if hits.blank?

            @current_dir = File.join(@current_dir, name)
            @sub_dirs << name
            @current_dir_name = name
        end
        @sub_dir_param = @sub_dirs.join(',') if @sub_dirs.present?
        @upper_sub_dir_param = @sub_dirs[0..-2].join(',') if @sub_dirs.size >= 2
        @upper_sub_dir_param = nil if @upper_sub_dir_param.blank?

        @static_file_url += @sub_dirs.join('/') + '/'
    end

    def save_upload_file
        return if @upload_files.blank?

        @upload_files.each do |upload_file|
            local_file_path = File.join(@current_dir, upload_file.original_filename)

            File.delete(local_file_path) if File.exists?(local_file_path)

            if upload_file.size > 10.megabyte
                raise DataCheckException, 'ファイルのサイズは最大で10MBです。'
            end

            f = File.open(local_file_path, "wb")
            f.write(upload_file.read)
            f.close
        end
    end

    def delete_dir(dir)
        Dir::glob(File.join(dir, '*')).each do |child|
            if File.directory?(child)
                delete_dir(child)
            else
                File.delete(child)
            end
        end

        Dir.rmdir(dir)
    end

end