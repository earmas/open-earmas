class SelfDoiController < BaseAdminController

    before_action :check_not_mentenance_mode

    before_action :set_nonmaintenance_data_lock,    only: [:check]

    def index
        @documents = PriDocument.where(self_doi_checked: false).order('pri_documents.id ASC').page(params[:page])
    end


    def check
        begin
            if Rails.application.config.use_delayed_job
                Delayed::KickDelayedTask.new(current_staff).delay({:run_at => Rails.application.config.delay_time.from_now}).check_sel_doi
            else
                Delayed::DelayTask.new.check_sel_doi(current_staff)
            end
            redirect_to staff_self_doi_url

        rescue Exception => ex
            unset_data_lock
            logger.error ex.message
            redirect_to staff_publish_url, alert: '公開に失敗しました'
        end
    end

private

end
