#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ListFilterDefsController < BaseAdminController

    maintenance_data_lock_methods :create, :update, :destroy

    before_action :check_mentenance_mode
    before_action :check_list_def_exists
    before_action :check_list_filter_def_exists, except: [:index, :new, :create, :param_form]

    def new
        @list_filter_def = ListFilterDef.new
        @list_filter_def.filter_type = DocumentFilterDef::FILTER_TYPES[:field_value_filter][:id]
    end

    def create
        @list_filter_def = ListFilterDef.new(request_params)
        @list_filter_def.parent_id = @list_def.id

        create_action_block('new', 'list_filter_def', 'フィルタの作成に失敗しました。') do
            if @list_filter_def.save
                lock_list(@list_def.id)
                lock_pri_list(@list_def.id)
                redirect_to staff_list_def_url(@list_def), notice: 'フィルタを作成しました。'
            else
                render action: 'new'
            end
        end
    end

    def edit
    end

    def update
        @list_filter_def.clear_optional_param
        @list_filter_def.assign_attributes(request_params)

        update_action_block('edit', 'list_filter_def', 'フィルタの更新に失敗しました。') do
            if @list_filter_def.save
                lock_list(@list_def.id)
                lock_pri_list(@list_def.id)
                redirect_to staff_list_def_url(@list_def), notice: 'フィルタを更新しました。'
            else
                render action: 'edit'
            end
        end
    end

    def destroy
        redirect_block(staff_list_def_url(@list_def), 'list_filter_def', 'フィルタの削除に失敗しました。') do
            @list_filter_def.destroy!
            lock_list(@list_def.id)
            lock_pri_list(@list_def.id)
            redirect_to staff_list_def_url(@list_def), notice: 'フィルタを削除しました。'
        end
    end

    def param_form
        @list_filter_def = ListFilterDef.new
        @list_filter_def.filter_type = params[:list_filter_def][:filter_type]
    end


private

    def check_list_def_exists
        @list_def = ListDef.find(params[:list_def_id])
    end

    def check_list_filter_def_exists
        @list_filter_def = @list_def.filter_fields.find(params[:id])
    end

    def request_params
        params.require(:list_filter_def).permit(
            :include_filter,
            :filter_type,
            :some_id,
            :filter_value)
    end
end
