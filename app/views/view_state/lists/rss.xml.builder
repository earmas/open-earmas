xml.instruct!
xmlobj = xml.tag!("xsl:stylesheet",
    'version' => "1.0",
    'xmlns:xsl' => "http://www.w3.org/1999/XSL/Transform"
) do

    xml.tag! 'xsl:output', method: "xml"

    xml.tag! 'xsl:template', match: "/" do
        xml.rss("version"    => '2.0',
                "xmlns:dc"   => "http://purl.org/dc/elements/1.1/",
                "xmlns:atom" => "http://www.w3.org/2005/Atom") do
            xml.channel do
                xml.title show_system_title
                xml.description show_system_title + " - " + show_caption(@list_def)
                xml.link gen_current_list_url(@level_params)

                xml.tag! 'xsl:apply-templates', select: "records"
            end
        end
    end

    xml.tag! 'xsl:template', match: "records" do
        xml.tag! 'xsl:apply-templates', select: "record"
    end

    xml.tag! 'xsl:template', match: "record" do
        xml.item do
            xml.tag! 'xsl:copy-of', select: "child::node()"
        end
    end
end

