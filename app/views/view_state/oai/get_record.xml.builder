xml.instruct!
xmlobj = xml.tag!("xsl:stylesheet",
    'version' => "1.0",
    'xmlns:xsl' => "http://www.w3.org/1999/XSL/Transform",
    "xmlns:oai" => "http://www.openarchives.org/OAI/2.0/", 
    "xmlns" => "http://www.openarchives.org/OAI/2.0/", 
    "exclude-result-prefixes" => "oai"
) do

    xml.tag! 'xsl:output', method: "xml"
    xml.tag! 'xsl:namespace-alias', "stylesheet-prefix" => "#default", "result-prefix" => "oai"

    xml.tag! 'xsl:template', match: "/" do
        xml.tag!('OAI-PMH',
            "xmlns" => "http://www.openarchives.org/OAI/2.0/",
            "xmlns:xsi" => "http://www.w3.org/2001/XMLSchema-instance",
            "xsi:schemaLocation" => "http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd"
        ) do
            xml.responseDate show_oai_date(@response_date)
            xml.request (request.protocol + request.host_with_port + request.path), @oai_param
            xml.tag! 'xsl:apply-templates', select: "oai:records"
        end
    end

    xml.tag! 'xsl:template', match: "oai:records" do
        xml.GetRecord do
            xml.tag! 'xsl:apply-templates', select: "oai:record"
        end
    end

    xml.tag! 'xsl:template', match: "oai:record" do
        xml.record do
            xml.tag! 'xsl:copy-of', select: "child::node()"
        end
    end
end

