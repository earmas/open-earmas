xml.instruct!
xmlobj = xml.tag!('OAI-PMH',
    "xmlns" => "http://www.openarchives.org/OAI/2.0/",
    "xmlns:xsi" => "http://www.w3.org/2001/XMLSchema-instance",
    "xsi:schemaLocation" => "http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd"
) do
    xml.responseDate show_oai_date(@response_date)
    xml.request (request.protocol + request.host_with_port + request.path), @oai_param
    xml.ListIdentifiers do
        @documents.each do |document|
            if document.oaipmh_deleted
                xml.header status: "deleted" do
                    xml.identifier show_oai_identifier(document)
                    xml.datestamp show_oai_publish_date(document)
                    document.set_spec.each do |set_spec|
                        xml.setSpec set_spec
                    end
                end
            else
                xml.header do
                    xml.identifier show_oai_identifier(document)
                    xml.datestamp show_oai_publish_date(document)
                    document.set_spec.each do |set_spec|
                        xml.setSpec set_spec
                    end
                end
            end
        end

        if @next_resumption_token.present?
            xml.resumptionToken @next_resumption_token, completeListSize: @total, cursor: @offset
        end
        if @next_resumption_token.blank? && @resumptionToken.present?
            xml.resumptionToken nil, completeListSize: @total, cursor: @offset
        end
    end
end


