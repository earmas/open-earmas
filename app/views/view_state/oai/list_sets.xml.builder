xml.instruct!
xmlobj = xml.tag!('OAI-PMH',
    "xmlns" => "http://www.openarchives.org/OAI/2.0/",
    "xmlns:xsi" => "http://www.w3.org/2001/XMLSchema-instance",
    "xsi:schemaLocation" => "http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd"
) do
    xml.responseDate show_oai_date(@response_date)
    xml.request (request.protocol + request.host_with_port + request.path), @oai_param
    xml.ListSets do
        @set_values.each do |set_value|
            xml.set do
                xml.setSpec set_value.value
                xml.setName set_value.caption_en
            end
        end
    end
end
