xml.instruct!
xmlobj = xml.tag!('OAI-PMH',
    "xmlns" => "http://www.openarchives.org/OAI/2.0/",
    "xmlns:xsi" => "http://www.w3.org/2001/XMLSchema-instance",
    "xsi:schemaLocation" => "http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd"
) do
    xml.responseDate show_oai_date(@response_date)
    xml.request (request.protocol + request.host_with_port + request.path), @oai_param
    xml.Identify do

        xml.repositoryName @oaipmh_setting.repository_name
        xml.baseURL @oaipmh_setting.base_url
        xml.protocolVersion "2.0"
        xml.adminEmail @oaipmh_setting.admin_email
        xml.earliestDatestamp show_oai_date(@datestamp_start)
        xml.deletedRecord "transient"
        xml.granularity "YYYY-MM-DDThh:mm:ssZ"
        xml.compression "deflate"


    end
end
