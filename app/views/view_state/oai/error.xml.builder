xml.instruct!
xmlobj = xml.tag!('OAI-PMH',
    "xmlns" => "http://www.openarchives.org/OAI/2.0/",
    "xmlns:xsi" => "http://www.w3.org/2001/XMLSchema-instance",
    "xsi:schemaLocation" => "http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd"
) do
    xml.responseDate show_oai_date(@response_date)
    xml.request (request.protocol + request.host_with_port + request.path), @oai_param
    xml.error @error_code[:message], code: @error_code[:code]
end
