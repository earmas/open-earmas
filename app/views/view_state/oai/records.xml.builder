xml.instruct!
xmlobj = xml.records do
    @documents.each do |document|
    cache [view_state_cache_name, locale_name, document] do
        xml.record do
            if document.oaipmh_deleted
                xml.header "xmlns" => "http://www.openarchives.org/OAI/2.0/", status: "deleted" do
                    xml.identifier show_oai_identifier(document)
                    xml.datestamp show_oai_publish_date(document)
                    document.set_spec.each do |set_spec|
                        xml.setSpec set_spec
                    end
                end
            else
                xml.header "xmlns" => "http://www.openarchives.org/OAI/2.0/" do
                    xml.identifier show_oai_identifier(document)
                    xml.datestamp show_oai_publish_date(document)
                    document.set_spec.each do |set_spec|
                        xml.setSpec set_spec
                    end
                end
                xml.metadata do
                    xml.system_uri get_public_document_url(document)
                    xml.system_created_at show_oai_date(document.created_at)
                    xml.system_updated_at show_oai_date(document.updated_at)
                    xml.system_publish_at show_oai_publish_date(document)
                    xml.system_date_created_at show_utc_date(document.created_at)
                    xml.system_date_updated_at show_utc_date(document.updated_at)
                    xml.system_date_publish_at show_utc_publish_date(document)

                    if document.self_doi_jalc_active
                        xml.self_doi_jalc document.show_self_doi
                    end
                    if document.self_doi_crossref_active
                        xml.self_doi_crossref document.show_self_doi
                    end

                    document.detail_field_defs.each do |field_def|
                        next if document.field_empty?(field_def.id) && field_def.field_type_class.must_show_field?(field_def, document) == false
                        render_view_state_xml_field(field_def, document, xml)
                    end
                end
            end
        end
    end
    end
end
