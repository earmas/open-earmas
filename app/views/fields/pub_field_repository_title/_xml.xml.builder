fields.each do |field|
    xml.tag!(field_def.code.gsub(/\s+/, "")) do
        xml.captionJa field.caption_ja
        xml.captionEn field.caption_en
    end
end
