fields.each do |field|
    xml.tag!(field_def.code.gsub(/\s+/, "")) do
        xml.name show_caption(field)
        xml.value1 field.value1
        xml.value2 field.value2
        xml.value3 field.value3
        xml.value4 field.value4
        xml.value5 field.value5
    end
end
