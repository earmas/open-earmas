fields.each do |field|
    xml.tag!(field_def.code.gsub(/\s+/, "")) do
        xml.name field.org_file_name
        xml.title field.title
        xml.mime field.mime
        xml.size field.size
        xml.embargo field.embargo
        xml.url view_state_path_gen(:file_url).call(field.document_id, field.field_def_id, field.id)
    end
end

