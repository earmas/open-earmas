fields.each do |field|
    xml.tag! field_def.code.gsub(/\s+/, ""), field.value1.strftime(FieldTypes::SimpleDate::DATE_FORMAT)
end
