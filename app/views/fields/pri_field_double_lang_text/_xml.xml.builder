fields.each do |field|
    xml.tag!(field_def.code.gsub(/\s+/, "")) do
        xml.valueJa field.value1
        xml.valueEn field.value2
    end
end
