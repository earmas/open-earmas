fields.each do |field|
    xml.tag!(field_def.code.gsub(/\s+/, "")) do
        xml.familyName field.value1
        xml.givenName field.value2
    end
end
