fields.each do |field|
    xml.tag! field_def.code.gsub(/\s+/, ""), field_def.field_type_class.gen_pmh_data(field_def, field)
end
