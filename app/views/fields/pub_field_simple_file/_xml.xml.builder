fields.each do |field|
    next if field.publish_type != PubFieldFileDatum::PUBLISH_STD
    xml.tag!(field_def.code.gsub(/\s+/, "")) do
        xml.name field.org_file_name
        xml.title field.title
        xml.mime field.mime
        xml.size field.size
        xml.embargo field.embargo
        xml.url root_url.chop + public_dir_file_to_url_path(field.file_path)
    end
end

