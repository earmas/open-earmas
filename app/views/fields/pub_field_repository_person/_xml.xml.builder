fields.each do |field|
    xml.tag!(field_def.code.gsub(/\s+/, "")) do

        person = field.get_master_value
        if person != nil
            xml.personId person.encrypted_id
        end

        xml.familyNameJa field.family_name_ja
        xml.givenNameJa field.given_name_ja
        xml.familyNameEn field.family_name_en
        xml.givenNameEn field.given_name_en
        xml.familyNameTra field.family_name_tra
        xml.givenNameTra field.given_name_tra

        xml.familyNameAlt field.family_name_alt1
        xml.givenNameAlt field.given_name_alt1
        xml.familyNameAlt field.family_name_alt2
        xml.givenNameAlt field.given_name_alt2
        xml.familyNameAlt field.family_name_alt3
        xml.givenNameAlt field.given_name_alt3

        xml.affiliationJa field.affiliation_ja
        xml.affiliationEn field.affiliation_en
    end
end
