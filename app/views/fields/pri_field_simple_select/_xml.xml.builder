fields.each do |field|
    value = get_custom_select_for_xml(field)
    xml.tag!(field_def.code.gsub(/\s+/, "")) do
        xml.value value[:value]
        xml.captionJa value[:caption_ja]
        xml.captionEn value[:caption_en]
    end
end
