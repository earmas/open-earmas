xml.instruct!
xmlobj = xml.records do
    xml.totalHits @total_hits
    @documents.each do |document|
        xml.record do
            xml.system_uri get_document_url(document)
            xml.public_uri get_public_document_url(document)
            xml.perm_uri get_perm_document_url(document)
            xml.system_created_at show_oai_date(document.created_at)
            xml.system_updated_at show_oai_date(document.updated_at)
            xml.system_publish_at show_oai_publish_date(document)
            xml.system_date_created_at show_utc_date(document.created_at)
            xml.system_date_updated_at show_utc_date(document.updated_at)
            xml.system_date_publish_at show_utc_publish_date(document)
            document.detail_field_defs.each do |field_def|
                next if document.field_empty?(field_def.id) && field_def.field_type_class.must_show_field?(field_def, document) == false
                render_view_state_xml_field(field_def, document, xml)
            end
        end
    end
end


