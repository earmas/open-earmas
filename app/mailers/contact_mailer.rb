class ContactMailer < ActionMailer::Base
    def contact_us(from_address, to_address, subject, message)
        @message = message
        mail(from: from_address, to: to_address, subject: subject)
    end
end

