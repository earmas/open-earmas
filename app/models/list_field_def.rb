#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ListFieldDef < ActiveRecord::Base
    include MultiPageSetting, ViewTypeHolder, ViewStateHelper

    SORT_TYPE_VALUE = 1
    SORT_TYPE_KEY   = 2

    LIST_FIELD_TYPE_STD = 1
    LIST_FIELD_TYPE_PREFIX = 2

    default_scope { order(:sort_index) }

    belongs_to :list_def
    belongs_to :field_def

    belongs_to :list_param_html_set_def, class_name: :HtmlSetDef, :foreign_key => :list_param_html_set_def_id, dependent: :delete
    accepts_nested_attributes_for :list_param_html_set_def

    validate  :duplicate_field_def_id

    def prefix_list?
        list_field_type == LIST_FIELD_TYPE_PREFIX
    end

    def get_list_words(prev_list_field, field)
        if list_field_type == LIST_FIELD_TYPE_PREFIX
            [
                {
                    value_prefix: field.get_list_param_prefix_ja,
                    value: get_list_param_value_ja(field),
                    sort_value_ja: get_list_param_value_ja(field),
                    sort_value_en: get_list_param_value_ja(field),
                    caption_ja: get_list_param_value_ja(field),
                    caption_en: get_list_param_value_ja(field),
                },{
                    value_prefix: field.get_list_param_prefix_en,
                    value: get_list_param_value_en(field),
                    sort_value_ja: get_list_param_value_en(field),
                    sort_value_en: get_list_param_value_en(field),
                    caption_ja: get_list_param_value_en(field),
                    caption_en: get_list_param_value_en(field),
                }
            ].reject{|a|a[:value].blank? || (prev_list_field != nil && prev_list_field.list_field_type == LIST_FIELD_TYPE_PREFIX && a[:value_prefix].blank?)}
        else
            if prev_list_field != nil && prev_list_field.list_field_type == LIST_FIELD_TYPE_PREFIX
                [
                    {
                        value_prefix: field.get_list_param_prefix_ja,
                        value: field.get_list_param,
                        sort_value_ja: get_list_sort_value_ja(field),
                        sort_value_en: get_list_sort_value_en(field),
                        caption_ja: get_list_caption_ja(field),
                        caption_en: get_list_caption_en(field),
                    }, {
                        value_prefix: field.get_list_param_prefix_en,
                        value: field.get_list_param,
                        sort_value_ja: get_list_sort_value_ja(field),
                        sort_value_en: get_list_sort_value_en(field),
                        caption_ja: get_list_caption_ja(field),
                        caption_en: get_list_caption_en(field),
                    }
                ].reject{|a|a[:value_prefix].blank?}
            else
                [
                    {
                        value_prefix: '',
                        value: field.get_list_param,
                        sort_value_ja: get_list_sort_value_ja(field),
                        sort_value_en: get_list_sort_value_en(field),
                        caption_ja: get_list_caption_ja(field),
                        caption_en: get_list_caption_en(field),
                    }
                ]
            end
        end
    end

    def get_list_param_value_ja(field)
        return field.get_list_param_prefix_ja if list_field_type == LIST_FIELD_TYPE_PREFIX

        field.get_list_param
    end

    def get_list_param_value_en(field)
        return field.get_list_param_prefix_en if list_field_type == LIST_FIELD_TYPE_PREFIX

        field.get_list_param
    end

    def get_list_param_values(field)
        [get_list_param_value_ja(field), get_list_param_value_en(field)].reject{|a|a.blank?}.uniq
    end

    def get_list_sort_value_ja(field)
        return field.get_list_param_prefix_ja if list_field_type == LIST_FIELD_TYPE_PREFIX

        if sort_type == SORT_TYPE_KEY
            field.get_sort_key_value
        else
            field.get_sort_value_ja
        end
    end
    def get_list_sort_value_en(field)
        return field.get_list_param_prefix_en if list_field_type == LIST_FIELD_TYPE_PREFIX

        if sort_type == SORT_TYPE_KEY
            field.get_sort_key_value
        else
            field.get_sort_value_en
        end
    end

    def get_list_caption_ja(field)
        return field.get_list_param_prefix_ja if list_field_type == LIST_FIELD_TYPE_PREFIX

        field.get_list_caption_with_unit_ja
    end

    def get_list_caption_en(field)
        return field.get_list_param_prefix_en if list_field_type == LIST_FIELD_TYPE_PREFIX

        field.get_list_caption_with_unit_en
    end

    def caption_ja
        list_def.caption_ja
    end
    def caption_en
        list_def.caption_en
    end

    def result_view_type_id
        list_def.result_view_type_id
    end
    def result_html_set_def
        list_def.result_html_set_def
    end

    def detail_view_type_id
        list_def.detail_view_type_id
    end
    def detail_html_set_def
        list_def.detail_html_set_def
    end

    def set_default_child
        list_param_html_set_def.present? || build_list_param_html_set_def
        list_param_html_set_def.header_parts || list_param_html_set_def.build_header_parts
        list_param_html_set_def.footer_parts || list_param_html_set_def.build_footer_parts
        list_param_html_set_def.right_side_parts || list_param_html_set_def.build_right_side_parts
    end

private

    def duplicate_field_def_id
        if ListFieldDef.where.not(id: id).where(list_def_id: list_def_id, field_def_id: field_def_id, list_field_type: list_field_type).size != 0
            errors.add(:field_def_id, "が重複しています")
        end
    end
end
