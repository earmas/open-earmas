#--
# Open Earmas
# 
# Copyright (C) 2014 Hiroshima University, ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class EjJournalDef < ActiveRecord::Base
    include MultiPageSetting, EjViewTypeHolder

    require 'RMagick'

    THUMNAIL_WIDHT  = 200
    THUMNAIL_HEIGHT = 200

    default_scope { order(:id) }

    belongs_to :ej_def
    has_many :static_page_defs, class_name: :EjStaticPageDef, dependent: :delete_all

    belongs_to :journal_html_set_def,           class_name: :HtmlSetDef, :foreign_key => :journal_html_set_def_id,            dependent: :delete
    belongs_to :article_html_set_def,           class_name: :HtmlSetDef, :foreign_key => :article_html_set_def_id,            dependent: :delete
    belongs_to :journal_all_index_html_set_def, class_name: :HtmlSetDef, :foreign_key => :journal_all_index_html_set_def_id,  dependent: :delete
    accepts_nested_attributes_for :journal_html_set_def
    accepts_nested_attributes_for :article_html_set_def
    accepts_nested_attributes_for :journal_all_index_html_set_def

    belongs_to :journal_top_html_set_def,   class_name: :HtmlSetDef, :foreign_key => :journal_top_html_set_def_id,   dependent: :delete
    belongs_to :voliss_list_html_set_def,   class_name: :HtmlSetDef, :foreign_key => :voliss_list_html_set_def_id,   dependent: :delete
    belongs_to :article_list_html_set_def,  class_name: :HtmlSetDef, :foreign_key => :article_list_html_set_def_id,  dependent: :delete
    belongs_to :static_list_html_set_def,   class_name: :HtmlSetDef, :foreign_key => :static_list_html_set_def_id,   dependent: :delete
    accepts_nested_attributes_for :journal_top_html_set_def
    accepts_nested_attributes_for :voliss_list_html_set_def
    accepts_nested_attributes_for :article_list_html_set_def
    accepts_nested_attributes_for :static_list_html_set_def

    validates :code, uniqueness: true, presence: true
    validates :value, presence: true

    after_save :save_upload_thumnail_file
    after_destroy :delete_upload_files

    attr_accessor :upload_thumnail_file

    def set_default_child
        journal_html_set_def.present? || build_journal_html_set_def
        journal_html_set_def.header_parts || journal_html_set_def.build_header_parts
        journal_html_set_def.footer_parts || journal_html_set_def.build_footer_parts
        journal_html_set_def.right_side_parts || journal_html_set_def.build_right_side_parts

        article_html_set_def.present? || build_article_html_set_def
        article_html_set_def.header_parts || article_html_set_def.build_header_parts
        article_html_set_def.footer_parts || article_html_set_def.build_footer_parts
        article_html_set_def.right_side_parts || article_html_set_def.build_right_side_parts

        journal_all_index_html_set_def.present? || build_journal_all_index_html_set_def
        journal_all_index_html_set_def.header_parts || journal_all_index_html_set_def.build_header_parts
        journal_all_index_html_set_def.footer_parts || journal_all_index_html_set_def.build_footer_parts
        journal_all_index_html_set_def.right_side_parts || journal_all_index_html_set_def.build_right_side_parts

        journal_top_html_set_def.present?  || build_journal_top_html_set_def
        voliss_list_html_set_def.present?  || build_voliss_list_html_set_def
        article_list_html_set_def.present? || build_article_list_html_set_def
        static_list_html_set_def.present?  || build_static_list_html_set_def
    end

    def old_thumnail_path
        File.join(thumnail_dir, 'thumnail')
    end

    def new_thumnail_path
        File.join(thumnail_dir, 'thumnail.png')
    end

    def thumnail_path
        return new_thumnail_path if File.exists?(new_thumnail_path)
        old_thumnail_path
    end

    def has_thumnail?
        return true if File.exists?(new_thumnail_path)
        File.exists?(old_thumnail_path)
    end

    def journal_dir
        file_dir = File.join(Rails.application.config.public_file_store_dir, "journal", id.to_s)
        FileUtils.mkdir_p(file_dir)
        file_dir
    end

    def thumnail_dir
        file_dir = File.join(journal_dir, "journal_thumnail_file")
        FileUtils.mkdir_p(file_dir)
        file_dir
    end

    def journal_attach_file_dir
        file_dir = File.join(journal_dir, "journal_attach_file")
        FileUtils.mkdir_p(file_dir)
        file_dir
    end

    def delete_upload_files
        delete_dir(thumnail_dir)
        delete_dir(journal_attach_file_dir)
    end

    def save_upload_thumnail_file
        return if upload_thumnail_file.blank?

        File.delete(new_thumnail_path) if File.exists?(new_thumnail_path)
        File.delete(old_thumnail_path) if File.exists?(old_thumnail_path)

        if upload_thumnail_file.size > 1.megabyte
            raise DataCheckException, 'ファイルのサイズは最大で1MBです。'
        end

        if upload_thumnail_file.content_type.start_with?('image/') == false
            raise DataCheckException, '画像ではありません。'
        end

        ilist = Magick::ImageList.new
        ilist.from_blob(upload_thumnail_file.read)
        ilist.format = "PNG"
        width = ilist.columns < THUMNAIL_WIDHT ? ilist.columns : THUMNAIL_WIDHT
        ratio = width.to_f / ilist.columns.to_f

        height = ilist.rows.to_f * ratio

        ilist = ilist.coalesce
        ilist.each do |frame|
            frame.resize_to_fill!(width, height)
        end

        ilist = ilist.optimize_layers(Magick::OptimizeLayer)

        f = File.open(new_thumnail_path, "wb")
        f.write(ilist.to_blob)
        f.close

        self.thumnail_size = File.size(new_thumnail_path)
        self.thumnail_mime = upload_thumnail_file.content_type
        self.update_columns(thumnail_size: self.thumnail_size, thumnail_mime: self.thumnail_mime)

        self.upload_thumnail_file = nil
    end

private

    def delete_dir(dir)
        Dir::glob(File.join(dir, '*')).each do |child|
            if File.directory?(child)
                delete_dir(child)
            else
                File.delete(child)
            end
        end

        Dir.rmdir(dir)
    end

end
