#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ListDef < ActiveRecord::Base
    include MultiPageSetting, ViewTypeHolder

    require 'nokogiri'

    SHOW_OTHERS_LINK_HIDDEN = 1
    SHOW_OTHERS_LINK_SHOW = 2

    REVERSE_BOOL_VALUE = ['降順', '昇順']
    RSS_FILE_PATH = 'lib/default_files/rss/rss.xml'

    has_many :list_fields,    -> { order(:sort_index) },    class_name: :ListFieldDef,                                 dependent: :destroy
    has_many :filter_fields,  -> { order(:id) },            class_name: :ListFilterDef, :foreign_key => :parent_id,    dependent: :delete_all
    has_many :result_fields,    -> { order(:sort_index) },  class_name: :ListResultField,                              dependent: :delete_all
    belongs_to :sortable_field,  -> { includes(:field_def) }

    belongs_to :result_html_set_def, class_name: :HtmlSetDef, :foreign_key => :result_html_set_def_id, dependent: :delete
    belongs_to :detail_html_set_def, class_name: :HtmlSetDef, :foreign_key => :detail_html_set_def_id, dependent: :delete
    accepts_nested_attributes_for :result_html_set_def
    accepts_nested_attributes_for :detail_html_set_def

    has_many :pri_list_data,         dependent: :delete_all
    has_many :pri_list_level1_words, dependent: :delete_all
    has_many :pri_list_level2_words, dependent: :delete_all
    has_many :pri_list_level3_words, dependent: :delete_all
    has_many :pri_list_level4_words, dependent: :delete_all
    has_many :pri_list_level5_words, dependent: :delete_all

    has_many :pub_list_data,         dependent: :delete_all
    has_many :pub_list_level1_words, dependent: :delete_all
    has_many :pub_list_level2_words, dependent: :delete_all
    has_many :pub_list_level3_words, dependent: :delete_all
    has_many :pub_list_level4_words, dependent: :delete_all
    has_many :pub_list_level5_words, dependent: :delete_all

    validates :caption_ja, presence: true
    validates :caption_en, presence: true
    validates :code, uniqueness: true, format: { with: /\A[A-Za-z][A-Za-z0-9_\-]+[A-Za-z0-9]\z/, message: "は半角英数字[A-Za-z][A-Za-z0-9_\-]+[A-Za-z0-9]で入力してください。" }
    validate  :xslt_is_valid

    def check_document_filter(document, fields)
        filter_fields.each do |filter_field|
            return false if filter_field.match_document(document, fields) == false
        end
        return true
    end
    def check_field_filter(document, field)
        filter_fields.each do |filter_field|
            return false if filter_field.match_field(document, field) == false
        end
        return true
    end

    def get_field(level)
        set_field_level if @level_fields == nil
        @level_fields[level - 1]
    end

    def get_max_level
        set_field_level if @level_fields == nil
        @level_fields.size
    end

    def set_default_child
        result_html_set_def.present? || build_result_html_set_def
        result_html_set_def.header_parts || result_html_set_def.build_header_parts
        result_html_set_def.footer_parts || result_html_set_def.build_footer_parts
        result_html_set_def.right_side_parts || result_html_set_def.build_right_side_parts
        detail_html_set_def.present? || build_detail_html_set_def
        detail_html_set_def.header_parts || detail_html_set_def.build_header_parts
        detail_html_set_def.footer_parts || detail_html_set_def.build_footer_parts
        detail_html_set_def.right_side_parts || detail_html_set_def.build_right_side_parts
    end

    def default_result_field_defs
        return @default_result_field_defs if @default_result_field_defs.present?

        @default_result_field_defs = result_field_defs

        @default_result_field_defs = FieldDefLoader.result_field_defs.reject{|a| ViewState.pri? == false && a.to_pub == false } if @default_result_field_defs.size == 0

        @default_result_field_defs
    end

    def result_field_defs
        return @result_field_defs if @result_field_defs.present?

        @result_field_defs = []
        result_fields.each do |result_field|
            field_def = FieldDefLoader.get_field_def(result_field.field_def_id)
            next if field_def == nil
            next if ViewState.pri? == false && field_def.to_pub == false

            @result_field_defs << field_def
        end

        @result_field_defs
    end

    def set_default_value
        self.rss_xml_string = File.open(RSS_FILE_PATH).read if self.rss_xml_string.blank?
    end

    def xslt_is_valid
        return true if rss_xml_string.blank?
        begin
            Nokogiri::XSLT.parse(rss_xml_string)
        rescue => ex
            errors.add(:rss_xml_string, "不正なXSLTです : " + ex.message)
            return false
        end
    end

private

    def set_field_level
        @level_fields = []
        list_fields.each do |list_field|
            if @level_fields[0].blank?
                @level_fields[0] = list_field
                next
            end
            if @level_fields[1].blank?
                @level_fields[1] = list_field
                next
            end
            if @level_fields[2].blank?
                @level_fields[2] = list_field
                next
            end
            if @level_fields[3].blank?
                @level_fields[3] = list_field
                next
            end
            if @level_fields[4].blank?
                @level_fields[4] = list_field
                next
            end
        end
    end

end
