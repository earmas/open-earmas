#--
# Open Earmas
# 
# Copyright (C) 2014 Hiroshima University, ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class PubEjDatum < ActiveRecord::Base

    belongs_to :document, class_name: :PubDocument, foreign_key: 'document_id'
    belongs_to :ej_def

    after_destroy :remove_ej_word

    def blank_all?
        journal.blank? && volume.blank? && issue.blank? && publish_date.blank?
    end

private

    def remove_ej_word
        PubEjWord.where(id: ej_word_ids).each do |ej_word|
            ej_word.document_count -= 1
            if ej_word.document_count <= 0
                ej_word.destroy!
            else
                ej_word.save!
            end
        end
    end

end
