#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class PriDocument < ActiveRecord::Base
    include DocumentCommon, DocumentEj, PriPubModel, SelfDoiCheck

    require 'RMagick'

    PRIVATE_TYPE = 0
    LIMITED_TYPE = 1
    PUBLIC_TYPE  = 2

    default_scope          { where(deleted: false).includes(                                             :doc_field1, :doc_field2, :doc_field3, :doc_field4, :doc_field5, :doc_field6, :doc_field7, :doc_field8, :doc_field9) }
    scope :publishable, -> { where(self_doi_checked: true).where('(publish_to != ? or publish_at is not null)', PRIVATE_TYPE).includes(:doc_field1, :doc_field2, :doc_field3, :doc_field4, :doc_field5, :doc_field6, :doc_field7, :doc_field8, :doc_field9) }

    has_one :pub_document, primary_key: 'id', foreign_key: 'id'

    has_many :doc_field1,   -> { order(:sort_index, :id) },  class_name: :PriFieldText1Datum,   foreign_key: 'document_id', dependent: :delete_all
    has_many :doc_field2,   -> { order(:sort_index, :id) },  class_name: :PriFieldText2Datum,   foreign_key: 'document_id', dependent: :delete_all
    has_many :doc_field3,   -> { order(:sort_index, :id) },  class_name: :PriFieldNumberDatum,  foreign_key: 'document_id', dependent: :delete_all
    has_many :doc_field4,   -> { order(:sort_index, :id) },  class_name: :PriFieldFileDatum,    foreign_key: 'document_id', dependent: :destroy
    has_many :doc_field5,   -> { order(:sort_index, :id) },  class_name: :PriFieldNavDatum,     foreign_key: 'document_id', dependent: :delete_all
    has_many :doc_field6,   -> { order(:sort_index, :id) },  class_name: :PriFieldText9Datum,   foreign_key: 'document_id', dependent: :delete_all
    has_many :doc_field7,   -> { order(:sort_index, :id) },  class_name: :PriFieldDateDatum,    foreign_key: 'document_id', dependent: :delete_all
    has_many :doc_field8,   -> { order(:sort_index, :id) },  class_name: :PriFieldRepositoryPersonDatum,    foreign_key: 'document_id', dependent: :delete_all
    has_many :doc_field9,   -> { order(:sort_index, :id) },  class_name: :PriFieldRepositoryTitleDatum,     foreign_key: 'document_id', dependent: :delete_all

    belongs_to :creator_staff, class_name: :Staff, foreign_key: 'creator_staff_id'
    belongs_to :updater_staff, class_name: :Staff, foreign_key: 'updater_staff_id'

    after_destroy :remove_list_index
    after_destroy :delete_upload_files

    attr_accessor :upload_thumnail_file
    attr_accessor :upload_thumnail_file_delete

    attr_accessor :text_value_all
    attr_accessor :text_value_all_with_file
    attr_accessor :text_value_1
    attr_accessor :text_value_2
    attr_accessor :text_value_3
    attr_accessor :text_value_4
    attr_accessor :text_value_5
    attr_accessor :text_value_6
    attr_accessor :text_value_7
    attr_accessor :text_value_8
    attr_accessor :text_value_9
    attr_accessor :text_value_10
    attr_accessor :text_value_11
    attr_accessor :text_value_12
    attr_accessor :text_value_13
    attr_accessor :text_value_14
    attr_accessor :text_value_15
    attr_accessor :text_value_16
    attr_accessor :text_value_17
    attr_accessor :text_value_18
    attr_accessor :text_value_19
    attr_accessor :text_value_20

    attr_accessor :sort_value_ja_1
    attr_accessor :sort_value_en_1
    attr_accessor :sort_value_ja_2
    attr_accessor :sort_value_en_2
    attr_accessor :sort_value_ja_3
    attr_accessor :sort_value_en_3

    searchable :auto_index => false, :auto_remove => false do
        text :text_value_all
        text :text_value_all_with_file
        text :text_value_1
        text :text_value_2
        text :text_value_3
        text :text_value_4
        text :text_value_5
        text :text_value_6
        text :text_value_7
        text :text_value_8
        text :text_value_9
        text :text_value_10
        text :text_value_11
        text :text_value_12
        text :text_value_13
        text :text_value_14
        text :text_value_15
        text :text_value_16
        text :text_value_17
        text :text_value_18
        text :text_value_19
        text :text_value_20

        string :sort_value_ja_1
        string :sort_value_en_1
        string :sort_value_ja_2
        string :sort_value_en_2
        string :sort_value_ja_3
        string :sort_value_en_3

        integer :id
        integer :updater_staff_id
        integer :creator_staff_id
        integer :publish_to
        time :updated_at
    end

    def fields_validation_before(fields)
        valid = true
        fields.each do |field|
            result = field.fields_validation_before(self)
            valid &&= result
        end
        valid
    end
    def fields_validation_after(fields)
        valid = true
        fields.each do |field|
            result = field.fields_validation_after(self)
            valid &&= result
        end
        valid
    end

    def document_dir
        if Rails.application.config.try(:use_prefix_dir)
            number_data_dir = (id / 10000).to_s
            file_dir = File.join(Rails.application.config.private_file_store_dir, "private", number_data_dir, id.to_s)
        else
            file_dir = File.join(Rails.application.config.private_file_store_dir, "private", id.to_s)
        end

        FileUtils.mkdir_p(file_dir)
        file_dir
    end

    def must_field?(field_def)
        if get_input_group == nil
            field_def.must
        else
            input_group_field = get_input_group_field(field_def.id)
            input_group_field != nil && input_group_field.must
        end
    end

    def delete_thumnail_file
        self.thumnail_size = 0
        self.thumnail_mime = nil
        self.update_columns(thumnail_size: 0, thumnail_mime: nil)
        File.delete(new_thumnail_path) if File.exists?(new_thumnail_path)
        File.delete(old_thumnail_path) if File.exists?(old_thumnail_path)
    end

    def save_upload_thumnail_file
        if upload_thumnail_file_delete == '1'
            delete_thumnail_file
        end

        return if upload_thumnail_file.blank?

        File.delete(new_thumnail_path) if File.exists?(new_thumnail_path)
        File.delete(old_thumnail_path) if File.exists?(old_thumnail_path)

        if upload_thumnail_file.size > 1.megabyte
            raise DataCheckException, 'ファイルのサイズは最大で1MBです。'
        end

        if upload_thumnail_file.content_type.start_with?('image/') == false
            raise DataCheckException, '画像ではありません。'
        end

        ilist = Magick::ImageList.new
        ilist.from_blob(upload_thumnail_file.read)
        ilist.format = "PNG"
        width = ilist.columns < THUMNAIL_WIDHT ? ilist.columns : THUMNAIL_WIDHT
        ratio = width.to_f / ilist.columns.to_f

        height = ilist.rows.to_f * ratio

        ilist = ilist.coalesce
        ilist.each do |frame|
            frame.resize_to_fill!(width, height)
        end

        ilist = ilist.optimize_layers(Magick::OptimizeLayer)
        f = File.open(new_thumnail_path, "wb")
        f.write(ilist.to_blob)
        f.close

        self.thumnail_size = File.size(new_thumnail_path)
        self.thumnail_mime = upload_thumnail_file.content_type
        self.update_columns(thumnail_size: self.thumnail_size, thumnail_mime: self.thumnail_mime)

        self.upload_thumnail_file = nil
    end

    def create_list_datum
        list_datum = pri_pub_class_name(:ListDatum).new
        list_datum.publish_to = publish_to
        list_datum
    end

    def get_list_word_class(level)
        ('PriListLevel' + level.to_s + 'Word').constantize
    end

    def set_list_word(param_patterns, list_def, level)
        set_list_word_common(param_patterns, list_def, level)
    end

    def create_ej_datum
        ej_datum = pri_pub_class_name(:EjDatum).new
        ej_datum.publish_to = publish_to
        ej_datum
    end

    def get_ej_word_class
        PriEjWord
    end

    def set_ej_word(param_pattern, ej_def)
        set_ej_word_common(param_pattern, ej_def)
    end

    def check_oaipmh(fields)
        self.oaipmh = false
        self.set_spec = []
        return if SystemSetting.all.first.enable_oaipmh == false

        self.set_spec = set_oaipmh_set_spec(fields)

        OaipmhFilterDef.all.each do |oaipmh_filter_def|
            return if oaipmh_filter_def.match_document(self, fields) == false
        end
        return if publish_to == LIMITED_TYPE

        self.oaipmh = true
    end

    def set_oaipmh_set_spec(fields)
        oaipmh_setting = OaipmhSetting.all.first
        return [] if oaipmh_setting == nil

        values = []
        if oaipmh_setting.set_spec_custom_select_id.present?
            fields.each do |field|
                select_id = field.get_field_def.field_type_class.get_select_id(field.get_field_def)
                next if select_id != oaipmh_setting.set_spec_custom_select_id

                set_value = field.get_oaipmh_set_value
                values << set_value if set_value.present?
            end
        end

        values = values.reject{|a|a.blank?}

        values << oaipmh_setting.set_spec_default_value if values.blank?

        values = values.map{|a|a.delete(' ').delete(':')}
        values
    end

    def get_embargo_until(fields, now_time)
        min_embargo_until = nil

        fields.each do |field|
            field_embargo = field.get_field_type.get_embargo_until(field)
            next if field_embargo == nil
            next if field_embargo < now_time

            if min_embargo_until == nil || field_embargo < min_embargo_until
                min_embargo_until = field_embargo
            end
        end
        min_embargo_until
    end

    def set_embargo_until(fields)
        self.embargo_until = get_embargo_until(fields, Time.zone.today)
    end

    def to_csv
        csv_rows = [id.to_s, InputGroup.get_input_group_name(input_group_id)]

        FieldDefLoaderForInput.csv_fields.each do |field_def|
            next if field_empty?(field_def.id)

            csv_row_fields = []
            get_fields(field_def.id).each do |field|
                csv_row_fields << field.to_csv_field
            end

            csv_field_string = csv_row_fields.join('|')
            csv_rows[field_def.csv_column + 1] = csv_field_string
        end
        csv_rows
    end

    def from_csv(csv_rows)
        self.input_group_id = InputGroup.get_input_group_id(csv_rows[1])

        @csv_field_map = {}
        @csv_fields = []

        FieldDefLoaderForInput.csv_fields.each do |field_def|
            sort_index = 1

            csv_field_string = csv_rows[field_def.csv_column + 1]
            next if csv_field_string.blank?

            csv_row_fields = csv_field_string.split('|', -1)

            csv_row_fields.each do |csv_row_field|
                next if csv_row_field.blank?
                field = field_def.default_pri_field
                field.field_def_id = field_def.id
                field.from_csv_field(csv_row_field)
                field.sort_index = sort_index

                @csv_field_map[field_def.id] = [] if @csv_field_map[field_def.id].blank?
                @csv_field_map[field_def.id] << field
                @csv_fields << field
                sort_index += 1
            end
        end
    end

    def csv_field_map
        @csv_field_map
    end

    def csv_fields
        @csv_fields
    end

    def self.publish_query
        # publishable
        # 公開日がある または private ではない


        # 公開日がない または 公開日後に更新されている
        # または
        # 公開日がある かつ エンバーゴ到達後

        PriDocument.unscoped.publishable
            .where('(publish_at is null or publish_at < pri_documents.updated_at) or (publish_at is not null and ? > embargo_until)', get_today)
    end

    def publish_at_history
        []
    end

    def get_pub_document
        if @get_pub_document_called != true
            @cached_pub_document = PubDocument.unscoped.where(id: id).first
            @get_pub_document_called = true
        end

        return @cached_pub_document
    end

    def publish(staff, auto_commit = true)
        pub_document = get_pub_document


        if deleted || publish_to == PRIVATE_TYPE # pri_documentを公開から非公開に変えたか、削除したか
            return if pub_document == nil # 公開データが無いことはありえない

            Hook::Document.on_delete(self)

            if pub_document.oaipmh
                # 公開データが非公開になり、oai_pmhの対象から外れた
                destroy_oaipmh_document(pub_document, staff)
            else
                # oai_pmhの対象でない場合は消しても問題ない。
                destroy_pub_document(pub_document, staff)
            end

            # 公開データを作ったら削除した場合はもうpri_documentは不要なので消す。
            if deleted
                self.destroy!
                self.remove_from_index
            end

            # 非公開に変更した場合、公開日は消す。消さないと、publishableに引っかかるのでまたpublishで対象になる。
            PriDocument.unscoped.publishable.where(id: id).update_all(publish_at: nil, publisher_staff_id: staff.id)

        else # pri_documentが公開になっている場合

            Hook::Document.on_publish(self)

            if pub_document != nil && pub_document.oaipmh # すでに公開済でoaipmhの対象である場合
                if oaipmh
                    # 特に変化がないので普通に更新
                    save_oaipmh_document(pub_document, staff, auto_commit)
                else
                    # 公開データはそのままで、oai_pmhの対象から外れた
                    save_deleted_oaipmh_document(pub_document, staff, auto_commit)
                end
            else # まだ公開してない場合か、oaipmhの対象ではない場合
                if oaipmh
                    # oaipmhの対象ではなかったものが対象になったか、初めてのoaipmhデータ公開
                    save_oaipmh_document(pub_document, staff, auto_commit)
                else
                    # 普通の処理
                    save_pub_document(pub_document, staff, auto_commit)
                end
            end
        end
    end

    def set_thumnail(pri_field_file)
        if pri_field_file.field_has_thumnail?
            FileUtils.cp pri_field_file.thumnail_path, self.new_thumnail_path

            self.thumnail_size = File.size(self.new_thumnail_path)
            self.thumnail_mime = 'image/png'
        end
    end

    def self.get_now
        self.default_timezone == :utc ? Time.now.utc : Time.now
    end

    def self.get_today
        get_now.to_date
    end

    def self.parse_time(time)
        if self.default_timezone == :utc
            Time.zone.parse(time)
        else
            Time.zone.parse(time).in_time_zone('UTC')
        end
    end

    def clear_meta_tag_xml
        self.meta_tag_xml = nil
    end

private

    def fields_has_value?(fields)
        return false if fields.blank?

        fields.reject{|a| a.blank_value?}.size > 0
    end

    def remove_list_index
        PriListDatum.where(document_id: id).destroy_all
        PriEjDatum.where(document_id: id).destroy_all
    end

    def destroy_pub_document(pub_document, staff)
        pub_document.destroy!
        pub_document.remove_from_index
    end

    def add_publish_at(pub_document, now_time)
        if pub_document.publish_at_history.blank?
            pub_document.publish_at_history = [now_time.to_s]
        else
            pub_document.publish_at_history += [now_time.to_s]
        end

        pub_document.max_publish_at = PriDocument.parse_time(pub_document.publish_at_history.max)
        pub_document.min_publish_at = PriDocument.parse_time(pub_document.publish_at_history.min)
    end

    def copy_to_pub_self_doi(pub_document)
        pub_document.self_doi_type = self_doi_type
        pub_document.self_doi = self_doi
    end

    # 公開データが非公開になり、oai_pmhの対象から外れた
    def destroy_oaipmh_document(pub_document, staff)
        now_time = PriDocument.get_now

        pub_document.set_spec = set_spec

        add_publish_at(pub_document, now_time)
        copy_to_pub_self_doi(pub_document)

        pub_document.oaipmh = true
        pub_document.oaipmh_deleted = true

        pub_document.deleted = true
        pub_document.delete_upload_files
        pub_document.save!
        pub_document.delete_all_field
        pub_document.remove_from_index
    end

    # 公開データはそのままで、oai_pmhの対象から外れた
    def save_deleted_oaipmh_document(pub_document, staff, auto_commit)
        pub_document.oaipmh = true
        pub_document.oaipmh_deleted = true
        pub_document.deleted = false

        save_document_common(pub_document, staff, auto_commit)
    end

    def save_pub_document(pub_document, staff, auto_commit)
        if pub_document.blank?
            pub_document = PubDocument.new
            pub_document.id = id
        end

        pub_document.oaipmh = false
        pub_document.oaipmh_deleted = false
        pub_document.deleted = false

        save_document_common(pub_document, staff, auto_commit)
    end

    def save_oaipmh_document(pub_document, staff, auto_commit)
        if pub_document.blank?
            pub_document = PubDocument.new
            pub_document.id = id
        end

        pub_document.oaipmh = true
        pub_document.oaipmh_deleted = false
        pub_document.deleted = false

        save_document_common(pub_document, staff, auto_commit)
    end

    def save_document_common(pub_document, staff, auto_commit)
        now_time = PriDocument.get_now

        pub_document.input_group_id = input_group_id
        pub_document.set_spec = set_spec

        add_publish_at(pub_document, now_time)
        copy_to_pub_self_doi(pub_document)

        pub_document.delete_upload_files if pub_document.present?

        pub_document.limited        = publish_to == LIMITED_TYPE
        pub_document.thumnail_mime  = thumnail_mime
        pub_document.thumnail_size  = thumnail_size

        pub_document.clear_meta_tag_xml
        pub_document.save!
        pub_document.delete_all_field

        FileUtils.cp_r thumnail_dir, pub_document.document_dir
        FileUtils.cp_r document_attach_file_dir, pub_document.document_dir


        all_fields = []
        pri_all_fields = reget_all_fields
        pri_all_fields.each do |pri_field|
            next if FieldDefLoader.get_field_def(pri_field.field_def_id).to_pub == false
            pub_field = pri_field.to_public_field
            next if pub_field == nil

            pub_field.id            = pri_field.id
            pub_field.field_def_id  = pri_field.field_def_id
            pub_field.document_id   = pub_document.id
            pub_field.sort_index    = pri_field.sort_index
            pub_field.save!

            all_fields << pub_field
        end

        pub_document.set_search_data(all_fields)
        pub_document.set_list_data(all_fields)
        pub_document.set_ej_data(all_fields)

        if auto_commit
            pub_document.index!
        else
            pub_document.index
        end

        new_embargo_until = get_embargo_until(pri_all_fields, now_time)
        PriDocument.where(id: id).update_all(publish_at: now_time, publisher_staff_id: staff.id, embargo_until: new_embargo_until)
    end

end
