#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class DocumentTemplate < ActiveRecord::Base

    default_scope { order(:name) }

    has_many :fields, class_name: :DocumentTemplateField, dependent: :delete_all

    belongs_to :staff

    validates :name, presence: true

    def get_document
        return @document if @document != nil

        @document = PriDocument.new
        @document.input_group_id = input_group_id
        @document.publish_to = publish_to

        @document
    end

    def field_by_field_def_id_map
        return @field_by_field_def_id_map if @field_by_field_def_id_map != nil
        @field_by_field_def_id_map = {}

        fields.each do |field| 
            @field_by_field_def_id_map[field.field_def_id] = field
        end

        @field_by_field_def_id_map
    end

end
