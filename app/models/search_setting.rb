#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class SearchSetting < ActiveRecord::Base
    include MultiPageSetting, ViewTypeHolder

    default_scope { order(:id) }

    belongs_to :result_html_set_def,  class_name: :HtmlSetDef, :foreign_key => :result_html_set_def_id, dependent: :delete
    belongs_to :detail_html_set_def,  class_name: :HtmlSetDef, :foreign_key => :detail_html_set_def_id, dependent: :delete
    accepts_nested_attributes_for :result_html_set_def
    accepts_nested_attributes_for :detail_html_set_def

    def self.create_default
        @search_setting = SearchSetting.new
        @search_setting.result_view_type_id = 1
        @search_setting.detail_view_type_id = 1

        @search_setting.detail_sub_header_view_type_id = 1
        @search_setting.detail_sub_footer_view_type_id = 1

        @search_setting.save!
    end

    def set_default_child
        result_html_set_def.present? || build_result_html_set_def
        result_html_set_def.header_parts || result_html_set_def.build_header_parts
        result_html_set_def.footer_parts || result_html_set_def.build_footer_parts
        result_html_set_def.right_side_parts || result_html_set_def.build_right_side_parts
        detail_html_set_def.present? || build_detail_html_set_def
        detail_html_set_def.header_parts || detail_html_set_def.build_header_parts
        detail_html_set_def.footer_parts || detail_html_set_def.build_footer_parts
        detail_html_set_def.right_side_parts || detail_html_set_def.build_right_side_parts
    end

    def default_result_field_defs
        return @default_result_field_defs if @default_result_field_defs.present?

        @default_result_field_defs = result_field_defs

        @default_result_field_defs = FieldDefLoader.result_field_defs.reject{|a| ViewState.pri? == false && a.to_pub == false } if @default_result_field_defs.size == 0

        @default_result_field_defs
    end

    def result_field_defs
        return @result_field_defs if @result_field_defs.present?

        @result_field_defs = []

        SearchResultField.all.each do |result_field|
            field_def = FieldDefLoader.get_field_def(result_field.field_def_id)
            next if field_def == nil
            next if ViewState.pri? == false && field_def.to_pub == false

            @result_field_defs << field_def
        end

        @result_field_defs
    end
end
