#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class InputGroup < ActiveRecord::Base
    include ViewTypeHolder

    ALL_CAPTION = { ja: '全て', en: 'all' }

    default_scope { order(:sort_index) }

    has_many :fields,  -> { order(:sort_index, :id) }, class_name: :InputGroupField, dependent: :delete_all

    belongs_to :detail_html_set_def,    class_name: :HtmlSetDef, :foreign_key => :detail_html_set_def_id, dependent: :delete
    accepts_nested_attributes_for :detail_html_set_def

    validates :name, presence: true

    def set_default_child
        detail_html_set_def.present? || build_detail_html_set_def
        detail_html_set_def.header_parts || detail_html_set_def.build_header_parts
        detail_html_set_def.footer_parts || detail_html_set_def.build_footer_parts
        detail_html_set_def.right_side_parts || detail_html_set_def.build_right_side_parts
    end

    def html_set_def
        detail_html_set_def
    end


    def self.get_input_group_name(input_group_id)
        input_group = FieldDefLoader.get_input_group(input_group_id)
        if input_group.blank?
            ALL_CAPTION[:ja]
        else
            input_group.name
        end
    end
    def self.get_input_group_id(input_group_name)
        input_group = FieldDefLoader.get_input_group_map_by_name(input_group_name)
        if input_group.blank?
            0
        else
            input_group.id
        end
    end
end
