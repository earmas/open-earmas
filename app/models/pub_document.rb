#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class PubDocument < ActiveRecord::Base
    include DocumentCommon, DocumentEj, PriPubModel

    self.primary_key = 'id'

    default_scope     { where(deleted: false).includes(:doc_field1, :doc_field2, :doc_field3, :doc_field4, :doc_field5, :doc_field6, :doc_field7, :doc_field8, :doc_field9) }
    scope :oaipmh, -> { where(oaipmh: true).includes(  :doc_field1, :doc_field2, :doc_field3, :doc_field4, :doc_field5, :doc_field6, :doc_field7, :doc_field8, :doc_field9) }

    has_many :doc_field1,   -> { order(:sort_index, :id) },  class_name: :PubFieldText1Datum,        foreign_key: 'document_id', dependent: :delete_all
    has_many :doc_field2,   -> { order(:sort_index, :id) },  class_name: :PubFieldText2Datum,        foreign_key: 'document_id', dependent: :delete_all
    has_many :doc_field3,   -> { order(:sort_index, :id) },  class_name: :PubFieldValueCaptionDatum, foreign_key: 'document_id', dependent: :delete_all
    has_many :doc_field4,   -> { order(:sort_index, :id) },  class_name: :PubFieldFileDatum,         foreign_key: 'document_id', dependent: :destroy
    has_many :doc_field5,   -> { order(:sort_index, :id) },  class_name: :PubFieldNavDatum,          foreign_key: 'document_id', dependent: :delete_all
    has_many :doc_field6,   -> { order(:sort_index, :id) },  class_name: :PubFieldText9Datum,        foreign_key: 'document_id', dependent: :delete_all
    has_many :doc_field7,   -> { order(:sort_index, :id) },  class_name: :PubFieldDateDatum,         foreign_key: 'document_id', dependent: :delete_all
    has_many :doc_field8,   -> { order(:sort_index, :id) },  class_name: :PubFieldRepositoryPersonDatum,       foreign_key: 'document_id', dependent: :delete_all
    has_many :doc_field9,   -> { order(:sort_index, :id) },  class_name: :PubFieldRepositoryTitleDatum,        foreign_key: 'document_id', dependent: :delete_all

    belongs_to :creator_staff, class_name: :Staff, foreign_key: 'creator_staff_id'
    belongs_to :updater_staff, class_name: :Staff, foreign_key: 'updater_staff_id'

    after_destroy :remove_list_index
    after_destroy :delete_upload_files

    attr_accessor :text_value_all
    attr_accessor :text_value_all_with_file
    attr_accessor :text_value_1
    attr_accessor :text_value_2
    attr_accessor :text_value_3
    attr_accessor :text_value_4
    attr_accessor :text_value_5
    attr_accessor :text_value_6
    attr_accessor :text_value_7
    attr_accessor :text_value_8
    attr_accessor :text_value_9
    attr_accessor :text_value_10
    attr_accessor :text_value_11
    attr_accessor :text_value_12
    attr_accessor :text_value_13
    attr_accessor :text_value_14
    attr_accessor :text_value_15
    attr_accessor :text_value_16
    attr_accessor :text_value_17
    attr_accessor :text_value_18
    attr_accessor :text_value_19
    attr_accessor :text_value_20

    attr_accessor :sort_value_ja_1
    attr_accessor :sort_value_en_1
    attr_accessor :sort_value_ja_2
    attr_accessor :sort_value_en_2
    attr_accessor :sort_value_ja_3
    attr_accessor :sort_value_en_3

    searchable :auto_index => false, :auto_remove => false do
        text :text_value_all
        text :text_value_all_with_file
        text :text_value_1
        text :text_value_2
        text :text_value_3
        text :text_value_4
        text :text_value_5
        text :text_value_6
        text :text_value_7
        text :text_value_8
        text :text_value_9
        text :text_value_10
        text :text_value_11
        text :text_value_12
        text :text_value_13
        text :text_value_14
        text :text_value_15
        text :text_value_16
        text :text_value_17
        text :text_value_18
        text :text_value_19
        text :text_value_20

        string :sort_value_ja_1
        string :sort_value_en_1
        string :sort_value_ja_2
        string :sort_value_en_2
        string :sort_value_ja_3
        string :sort_value_en_3

        integer :id
        boolean :limited
        time :updated_at
    end

    def document_dir
        if limited
            if Rails.application.config.try(:use_prefix_dir)
                number_data_dir = (id / 10000).to_s
                file_dir = File.join(Rails.application.config.public_file_store_dir, "limited", number_data_dir, id.to_s)
            else
                file_dir = File.join(Rails.application.config.public_file_store_dir, "limited", id.to_s)
            end
        else
            if Rails.application.config.try(:use_prefix_dir)
                number_data_dir = (id / 10000).to_s
                file_dir = File.join(Rails.application.config.public_file_store_dir, "public", number_data_dir, id.to_s)
            else
                file_dir = File.join(Rails.application.config.public_file_store_dir, "public", id.to_s)
            end
        end
        FileUtils.mkdir_p(file_dir)
        file_dir
    end

    def delete_all_field
        reget_all_fields.each do |field|
            field.destroy!
        end

        remove_list_index
    end

    def create_list_datum
        list_datum = pri_pub_class_name(:ListDatum).new
        list_datum.limited = limited
        list_datum
    end

    def get_list_word_class(level)
        ('PubListLevel' + level.to_s + 'Word').constantize
    end

    def set_list_word(param_patterns, list_def, level)
        new_list_word_ids = set_list_word_common(param_patterns, list_def, level, true)
        if limited == false
            new_list_word_ids += set_list_word_common(param_patterns, list_def, level, false)
        end
        new_list_word_ids
    end

    def create_ej_datum
        ej_datum = pri_pub_class_name(:EjDatum).new
        ej_datum.limited = limited
        ej_datum
    end

    def get_ej_word_class
        PubEjWord
    end

    def set_ej_word(param_pattern, ej_def)
        new_ej_word_ids = set_ej_word_common(param_pattern, ej_def, true)
        if limited == false
            new_ej_word_ids += set_ej_word_common(param_pattern, ej_def, false)
        end
        new_ej_word_ids
    end

    def set_publish_date
        now_time = PriDocument.get_now

        if self.publish_at_history.blank?
            self.publish_at_history = [now_time.to_s]
        else
            self.publish_at_history += [now_time.to_s]
        end
        self.max_publish_at = PriDocument.parse_time(self.publish_at_history.max)
        self.min_publish_at = PriDocument.parse_time(self.publish_at_history.min)
    end

    def clear_meta_tag_xml
        self.meta_tag_xml = nil
    end

private

    def remove_list_index
        PubListDatum.where(document_id: id).destroy_all
        PubEjDatum.where(document_id: id).destroy_all
    end

end
