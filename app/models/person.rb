#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Person < ActiveRecord::Base

    class ThreadValue
        attr_accessor :value_map
        attr_accessor :value_encrypted_map

        def set_value_map
            return if @value_map != nil
            @value_map = {}
            @value_encrypted_map = {}
            Person.all.each do |person|
                @value_map[person.id.to_s] = person
                @value_encrypted_map[person.encrypted_id.to_s] = person
            end
        end
    end


    class << self
        def thread_cache
            Thread.current[:earmas_person] ||= Person::ThreadValue.new
        end

        def get_value(id)
            thread_cache.set_value_map
            thread_cache.value_map[id.to_s]
        end
        def get_value_encrypted(encrypted_id)
            thread_cache.set_value_map
            thread_cache.value_encrypted_map[encrypted_id.to_s]
        end
    end

    self.primary_key = 'id'

    validates :id, presence: true
    validates :family_name_ja, presence: true, :unless => :family_name_en?
    validates :family_name_en, presence: true, :unless => :family_name_ja?

    default_scope { order(:id) }

    def set_encrypted_id(people_setting)
        self.encrypted_id = people_setting.encrypt_data(self.id)
    end

    def gen_url(url, value)
        url.sub('{replace}', value)
    end

    def gen_default_url(url)
        self.url.present? && self.url || url.sub('{replace}', self.encrypted_id.to_s)
    end

    def enable_default_url?(url)
        enable_gen_url && (url.present? || self.url.present?)
    end

    def to_csv
        [id, family_name_ja, family_name_en, family_name_tra, given_name_ja, given_name_en, given_name_tra,
            family_name_alt1, given_name_alt1, family_name_alt2, given_name_alt2, family_name_alt3, given_name_alt3, affiliation_ja, affiliation_en,
            url, key1, key2, key3, key4, key5, key6, key7, key8, key9, key10]
    end

    def self.from_csv(people_setting, csv_values)
        csv_id = csv_values[0]
        return nil if csv_id.blank?

        person = Person.get_value(csv_id)
        if person == nil
            person = Person.new
            person.id = csv_id
        end
        person.set_encrypted_id(people_setting)

        person.family_name_ja   = csv_values[1] || ''
        person.family_name_en   = csv_values[2] || ''
        person.family_name_tra  = csv_values[3] || ''
        person.given_name_ja    = csv_values[4] || ''
        person.given_name_en    = csv_values[5] || ''
        person.given_name_tra   = csv_values[6] || ''

        person.family_name_alt1     = csv_values[7] || ''
        person.given_name_alt1      = csv_values[8] || ''
        person.family_name_alt2     = csv_values[9] || ''
        person.given_name_alt2      = csv_values[10] || ''
        person.family_name_alt3     = csv_values[11] || ''
        person.given_name_alt3      = csv_values[12] || ''

        person.affiliation_ja       = csv_values[13] || ''
        person.affiliation_en       = csv_values[14] || ''

        person.url              = csv_values[15] || ''
        person.key1             = csv_values[16] || ''
        person.key2             = csv_values[17] || ''
        person.key3             = csv_values[18] || ''
        person.key4             = csv_values[19] || ''
        person.key5             = csv_values[20] || ''
        person.key6             = csv_values[21] || ''
        person.key7             = csv_values[22] || ''
        person.key8             = csv_values[23] || ''
        person.key9             = csv_values[24] || ''
        person.key10            = csv_values[25] || ''

        person
    end
end
