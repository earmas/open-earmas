#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ContactUsSetting < ActiveRecord::Base

    default_scope { order(:id) }

    validates :caption_ja, presence: true
    validates :caption_en, presence: true
    validates :subject, presence: true

    belongs_to :html_set_def, class_name: :HtmlSetDef, :foreign_key => :html_set_def_id, dependent: :delete
    accepts_nested_attributes_for :html_set_def

    def set_default_child
        html_set_def.present? || build_html_set_def
        html_set_def.header_parts || html_set_def.build_header_parts
        html_set_def.footer_parts || html_set_def.build_footer_parts
        html_set_def.right_side_parts || html_set_def.build_right_side_parts
    end

end
