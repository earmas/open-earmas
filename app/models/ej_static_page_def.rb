#--
# Open Earmas
# 
# Copyright (C) 2014 Hiroshima University, ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class EjStaticPageDef < ActiveRecord::Base
    include EjViewTypeHolder

    default_scope { order(:sort_index) }

    belongs_to :ej_journal_def

    belongs_to :journal_static_html_set_def,  class_name: :HtmlSetDef, :foreign_key => :journal_static_html_set_def_id, dependent: :delete
    accepts_nested_attributes_for :journal_static_html_set_def

    validates :caption_ja, presence: true
    validates :caption_en, presence: true

    validate  :duplicate_code

    def set_default_child
        journal_static_html_set_def.present? || build_journal_static_html_set_def
        journal_static_html_set_def.header_parts || journal_static_html_set_def.build_header_parts
        journal_static_html_set_def.footer_parts || journal_static_html_set_def.build_footer_parts
        journal_static_html_set_def.right_side_parts || journal_static_html_set_def.build_right_side_parts
    end

    def html_set_def
        journal_static_html_set_def
    end

private

    def duplicate_code
        if EjStaticPageDef.where.not(id: id).where(ej_journal_def_id: ej_journal_def_id, code: code).size != 0
            errors.add(:code, "が重複しています")
        end
    end

end
