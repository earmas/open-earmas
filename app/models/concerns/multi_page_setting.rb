#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module MultiPageSetting extend ActiveSupport::Concern

    LIST_PARAM_PAGE  = 1
    RESULT_PAGE      = 2
    DETAIL_PAGE      = 3
    JOURNAL_PAGE     = 4
    ARTICLE_PAGE     = 5

    attr_accessor :page_type

    def html_set_def
        return list_param_html_set_def if page_type == LIST_PARAM_PAGE
        return result_html_set_def if page_type == RESULT_PAGE
        return journal_html_set_def if page_type == JOURNAL_PAGE
        return article_html_set_def if page_type == ARTICLE_PAGE
        detail_html_set_def
    end
end