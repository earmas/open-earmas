#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module ListWordCommon extend ActiveSupport::Concern

    def get_level_list_word(level)
        if send('value' + (level + 1).to_s) == ''
            return ListWordSkip.new(document_count)
        end

        @level_list_word = LevelListWord.new(level, self)
        @level_list_word
    end


    class LevelListWord

        def initialize(level, list_word)
            @level = level
            @list_word = list_word
        end

        def field_def_id
            @list_word.send('field_def_id' + (@level + 1).to_s)
        end
        def value
            @list_word.send('value' + (@level + 1).to_s)
        end
        def sort_value_ja
            @list_word.send('sort_value_ja' + (@level + 1).to_s)
        end
        def sort_value_en
            @list_word.send('sort_value_en' + (@level + 1).to_s)
        end
        def caption_ja
            @list_word.send('caption_ja' + (@level + 1).to_s)
        end
        def caption_en
            @list_word.send('caption_en' + (@level + 1).to_s)
        end

        def document_count
            @list_word.document_count
        end
    end

end
