#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module ViewTypeHolder extend ActiveSupport::Concern

    def list_param_view_type_hash
        return ViewType::ListParam::VIEW_MAP[1] if list_param_view_type_id.blank?
        ViewType::ListParam::VIEW_MAP[list_param_view_type_id]
    end

    def result_view_type_hash
        return ViewType::Result::VIEW_MAP[1] if result_view_type_id.blank?
        ViewType::Result::VIEW_MAP[result_view_type_id]
    end

    def detail_view_type_hash
        return ViewType::Detail::VIEW_MAP[1] if detail_view_type_id.blank?
        ViewType::Detail::VIEW_MAP[detail_view_type_id]
    end

    def list_param_sub_header_view_type_hash
        return ViewType::ListParam::SUB_HEADER_VIEW_MAP[1] if list_param_sub_header_view_type_id.blank?
        ViewType::ListParam::SUB_HEADER_VIEW_MAP[list_param_sub_header_view_type_id]
    end

    def result_sub_header_view_type_hash
        return ViewType::Result::SUB_HEADER_VIEW_MAP[1] if result_sub_header_view_type_id.blank?
        ViewType::Result::SUB_HEADER_VIEW_MAP[result_sub_header_view_type_id]
    end

    def detail_sub_header_view_type_hash
        return ViewType::Detail::SUB_HEADER_VIEW_MAP[1] if detail_sub_header_view_type_id.blank?
        ViewType::Detail::SUB_HEADER_VIEW_MAP[detail_sub_header_view_type_id]
    end

    def list_param_sub_footer_view_type_hash
        return ViewType::ListParam::SUB_FOOTER_VIEW_MAP[1] if list_param_sub_footer_view_type_id.blank?
        ViewType::ListParam::SUB_FOOTER_VIEW_MAP[list_param_sub_footer_view_type_id]
    end

    def result_sub_footer_view_type_hash
        return ViewType::Result::SUB_FOOTER_VIEW_MAP[1] if result_sub_footer_view_type_id.blank?
        ViewType::Result::SUB_FOOTER_VIEW_MAP[result_sub_footer_view_type_id]
    end

    def detail_sub_footer_view_type_hash
        return ViewType::Detail::SUB_FOOTER_VIEW_MAP[1] if detail_sub_footer_view_type_id.blank?
        ViewType::Detail::SUB_FOOTER_VIEW_MAP[detail_sub_footer_view_type_id]
    end


end