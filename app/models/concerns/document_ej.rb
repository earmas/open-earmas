#--
# Open Earmas
#
# Copyright (C) 2014 Hiroshima University, ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module DocumentEj extend ActiveSupport::Concern

    def set_ej_detail_field_defs(ej_detail_field_defs)
        @stored_ej_detail_field_defs = []

        ej_detail_field_defs.each do |ej_detail_field_def|
            ja_field_def = ej_detail_field_def[:ja]
            en_field_def = ej_detail_field_def[:en]

            ja_field_def = nil if ja_field_def != nil && self.pub_or_lim? && ja_field_def.to_pub == false
            en_field_def = nil if en_field_def != nil && self.pub_or_lim? && en_field_def.to_pub == false

            ja_field_def = nil if ja_field_def != nil && field_contains_input_group?(ja_field_def.id) == false
            en_field_def = nil if en_field_def != nil && field_contains_input_group?(en_field_def.id) == false

            @stored_ej_detail_field_defs << {ja: ja_field_def, en: en_field_def, en_instead_ja: ej_detail_field_def[:en_instead_ja], ja_instead_en: ej_detail_field_def[:ja_instead_en]}
        end

        @stored_ej_detail_field_defs
    end


    def ej_detail_field_defs
        return @stored_ej_detail_field_defs if @stored_ej_detail_field_defs.present?

        FieldDefLoader.detail_field_defs_by_input_group_id(input_group_id).reject{|a| self.pub_or_lim? && a.to_pub == false }.map{|a| {ja: a, en: a, en_instead_ja: false, ja_instead_en: false} }
    end

    def set_ej_result_field_defs(ej_result_field_defs)
        @stored_ej_result_field_defs = []

        ej_result_field_defs.each do |ej_result_field_def|
            ja_field_def = ej_result_field_def[:ja]
            en_field_def = ej_result_field_def[:en]

            ja_field_def = nil if ja_field_def != nil && self.pub_or_lim? && ja_field_def.to_pub == false
            en_field_def = nil if en_field_def != nil && self.pub_or_lim? && en_field_def.to_pub == false

            ja_field_def = nil if ja_field_def != nil && field_contains_input_group?(ja_field_def.id) == false
            en_field_def = nil if en_field_def != nil && field_contains_input_group?(en_field_def.id) == false

            @stored_ej_result_field_defs << {ja: ja_field_def, en: en_field_def, en_instead_ja: ej_result_field_def[:en_instead_ja], ja_instead_en: ej_result_field_def[:ja_instead_en]}
        end

        @stored_ej_result_field_defs
    end

    def ej_result_field_defs
        return @stored_ej_result_field_defs if @stored_ej_result_field_defs.present?

        FieldDefLoader.result_field_defs_by_input_group_id(input_group_id).reject{|a| self.pub_or_lim? && a.to_pub == false }.map{|a| {ja: a, en: a, en_instead_ja: false, ja_instead_en: false} }
    end

    def set_ej_data(fields, ej_def_ids = nil)

        if FieldDefLoader.get_input_group(input_group_id) != nil
            fields = fields.reject{|a| get_input_group_field(a.field_def_id) == nil }
        end

        ej_def_relation = EjDef.all
        ej_def_relation = EjDef.where(id: ej_def_ids) if ej_def_ids != nil

        ej_def_relation.includes(:journal_defs, :filter_fields).each do |ej_def|

            next if ej_def.check_document_filter(self, fields) == false

            ej_datum = create_ej_datum
            ej_datum.ej_def_id  = ej_def.id
            ej_datum.document_id  = id
            ej_datum.document_updated_at  = updated_at
            ej_datum.document_created_at  = created_at

            ej_datum.journal        = ""
            ej_datum.volume         = ""
            ej_datum.issue          = ""
            ej_datum.publish_date   = ""

            param_pattern = {}

            fields.each do |field|
                next if ej_def.check_field_filter(self, field) == false

                if ej_def.journal_field_def_id == field.field_def_id
                    if field.get_list_param.present?
                        ej_datum.journal = field.get_list_param
                        param_pattern[:journal] = field
                    end
                    next
                end

                if ej_def.volume_field_def_id == field.field_def_id
                    if field.get_list_param.present?
                        ej_datum.volume = field.get_list_param
                        param_pattern[:volume] = field
                    end
                    next
                end

                if ej_def.issue_field_def_id == field.field_def_id
                    if field.get_list_param.present?
                        ej_datum.issue = field.get_list_param
                        param_pattern[:issue] = field
                    end
                    next
                end

                if ej_def.publish_date_field_def_id == field.field_def_id
                    if field.get_list_param.present?
                        ej_datum.publish_date = field.get_list_param
                        param_pattern[:publish_date] = field
                    end
                    next
                end

                if ej_def.sort_field_def_id == field.field_def_id
                    ej_datum.sort_value_ja = field.get_sort_value_ja
                    ej_datum.sort_value_en = field.get_sort_value_en
                    next
                end
            end

            next if ej_datum.blank_all?

            old_ej_datum = pri_pub_class_name(:EjDatum).where(ej_def_id: ej_def.id, document_id: id).first
            if old_ej_datum != nil
                old_ej_datum.destroy!
            end

            new_ej_word_ids = set_ej_word(param_pattern, ej_def)
            ej_datum.send('ej_word_ids=', new_ej_word_ids)

            ej_datum.save!
        end
    end

    def set_ej_word_common(param_pattern, ej_def, limited = nil)
        exist_ej_word_relation = get_ej_word_class.where(ej_def_id: ej_def.id)
        if limited != nil
            exist_ej_word_relation.where!(limited: limited)
        end

        if param_pattern[:journal].present?
            exist_ej_word_relation.where!(journal: param_pattern[:journal].get_list_param)
        else
            exist_ej_word_relation.where!(journal: '')
        end
        if param_pattern[:volume].present?
            exist_ej_word_relation.where!(volume: param_pattern[:volume].get_list_param)
        else
            exist_ej_word_relation.where!(volume: '')
        end
        if param_pattern[:issue].present?
            exist_ej_word_relation.where!(issue: param_pattern[:issue].get_list_param)
        else
            exist_ej_word_relation.where!(issue: '')
        end
        if param_pattern[:publish_date].present?
            exist_ej_word_relation.where!(publish_date: param_pattern[:publish_date].get_list_param)
        else
            exist_ej_word_relation.where!(publish_date: '')
        end


        new_ej_word = exist_ej_word_relation.first

        if new_ej_word != nil
            new_ej_word.document_count += 1
        else
            new_ej_word = get_ej_word_class.new
            new_ej_word.ej_def_id  = ej_def.id
            new_ej_word.document_count = 1
            if limited != nil
                new_ej_word.limited = limited
            end
        end

        if param_pattern[:journal].present?
            new_ej_word.journal = param_pattern[:journal].get_list_param
            new_ej_word.journal_caption_ja = param_pattern[:journal].get_list_caption_with_unit_ja
            new_ej_word.journal_caption_en = param_pattern[:journal].get_list_caption_with_unit_en
        end

        if param_pattern[:volume].present?
            new_ej_word.volume = param_pattern[:volume].get_list_param
            new_ej_word.volume_caption_ja = param_pattern[:volume].get_list_caption_with_unit_ja
            new_ej_word.volume_caption_en = param_pattern[:volume].get_list_caption_with_unit_en
        end

        if param_pattern[:issue].present?
            new_ej_word.issue = param_pattern[:issue].get_list_param
            new_ej_word.issue_caption_ja = param_pattern[:issue].get_list_caption_with_unit_ja
            new_ej_word.issue_caption_en = param_pattern[:issue].get_list_caption_with_unit_en
        end

        if param_pattern[:publish_date].present?
            new_ej_word.publish_date = param_pattern[:publish_date].get_list_param
            new_ej_word.publish_date_caption_ja = param_pattern[:publish_date].get_list_caption_with_unit_ja
            new_ej_word.publish_date_caption_en = param_pattern[:publish_date].get_list_caption_with_unit_en
        end

        if param_pattern[:publish_date].present?
            new_ej_word.publish_date_sort_value_ja = param_pattern[:publish_date].get_sort_value_ja
            new_ej_word.publish_date_sort_value_en = param_pattern[:publish_date].get_sort_value_en
        end

        new_ej_word.save!

        [new_ej_word.id]
    end

end
