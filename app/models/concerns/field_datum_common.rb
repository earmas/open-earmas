#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FieldDatumCommon extend ActiveSupport::Concern

    def get_master_value
        return @master_value_cache if @master_value_access == true
        @master_value_cache = get_field_type.get_master_value(self)
        @master_value_access = true
        @master_value_cache
    end

    def get_sort_value_ja
        @sort_value_ja_cache ||= get_field_type.get_sort_value_ja(self)
    end

    def get_sort_value_en
        @sort_value_en_cache ||= get_field_type.get_sort_value_en(self)
    end

    def get_sort_key_value
        @get_sort_key_value_cache ||= get_field_type.get_sort_key_value(self)
    end

    def get_list_caption_ja
        @caption_ja_cache ||= get_field_type.get_list_caption_ja(self)
    end

    def get_list_caption_en
        @caption_en_cache ||= get_field_type.get_list_caption_en(self)
    end

    def get_list_caption_with_unit_ja
        @caption_ja_with_unit_cache ||= get_field_def.set_unit_ja(get_list_caption_ja)
    end

    def get_list_caption_with_unit_en
        @caption_en_with_unit_cache ||= get_field_def.set_unit_en(get_list_caption_en)
    end

    def get_list_param
        return @list_param_cache if @list_param_access == true
        @list_param_cache = to_param_word(get_field_type.get_list_param(self))
        @list_param_access = true
        @list_param_cache
    end

    def get_list_param_prefix_ja
        return @list_param_prefix_ja_cache if @list_param_prefix_ja_access == true
        @list_param_prefix_ja_cache = to_param_word(get_field_type.get_list_param_prefix_ja(self))
        @list_param_prefix_ja_access = true
        @list_param_prefix_ja_cache
    end

    def get_list_param_prefix_en
        return @list_param_prefix_en_cache if @list_param_prefix_en_access == true
        @list_param_prefix_en_cache = to_param_word(get_field_type.get_list_param_prefix_en(self))
        @list_param_prefix_en_access = true
        @list_param_prefix_en_cache
    end

    def get_search_values
        get_field_type.get_search_values(self)
    end

    def get_search_values_with_file
        get_field_type.get_search_values_with_file(self)
    end

    def blank_value?
        get_field_type.blank_value?(self)
    end

    def nil_value?
        get_field_type.nil_value?(self)
    end

    def to_filter_value
        get_field_type.to_filter_value(self)
    end

    def to_filter_key_value
        get_field_type.to_filter_key_value(self)
    end

    def to_csv_field
        get_field_type.to_csv_field(self)
    end

    def field_has_thumnail?
        if get_field_def.field_type_class.file_holder?
            has_thumnail?
        else
            false
        end
    end

private

    def to_param_word(string)
        return string if string.blank?
        string.delete('./')
    end

end
