#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module PriPubModel extend ActiveSupport::Concern

    def pri_pub_class_name(class_name)
        if self.class.name.to_s.start_with?("Pub")
            ('Pub' + class_name.to_s).constantize
        else
            ('Pri' + class_name.to_s).constantize
        end
    end

    def pri?
        self.class.name.to_s.start_with?("Pri")
    end
    def pub_or_lim?
        self.class.name.to_s.start_with?("Pub")
    end
end
