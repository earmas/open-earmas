#--
# Open Earmas
#
# Copyright (C) 2014 Hiroshima University, ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module EjViewTypeHolder extend ActiveSupport::Concern

    def journal_view_type_hash
        return ViewType::Journal::VIEW_MAP[1] if journal_view_type_id.blank?
        ViewType::Journal::VIEW_MAP[journal_view_type_id]
    end
    def article_view_type_hash
        return ViewType::Article::VIEW_MAP[1] if article_view_type_id.blank?
        ViewType::Article::VIEW_MAP[article_view_type_id]
    end
    def journal_all_index_view_type_hash
        return ViewType::JournalAllIndex::VIEW_MAP[1] if journal_all_index_view_type_id.blank?
        ViewType::JournalAllIndex::VIEW_MAP[journal_all_index_view_type_id]
    end
    def journal_static_view_type_hash
        return ViewType::JournalStatic::VIEW_MAP[1] if journal_static_view_type_id.blank?
        ViewType::JournalStatic::VIEW_MAP[journal_static_view_type_id]
    end


    def journal_sub_header_view_type_hash
        return ViewType::Journal::SUB_HEADER_VIEW_MAP[1] if journal_sub_header_view_type_id.blank?
        ViewType::Journal::SUB_HEADER_VIEW_MAP[journal_sub_header_view_type_id]
    end
    def article_sub_header_view_type_hash
        return ViewType::Article::SUB_HEADER_VIEW_MAP[1] if article_sub_header_view_type_id.blank?
        ViewType::Article::SUB_HEADER_VIEW_MAP[article_sub_header_view_type_id]
    end
    def journal_all_index_sub_header_view_type_hash
        return ViewType::JournalAllIndex::SUB_HEADER_VIEW_MAP[1] if journal_all_index_sub_header_view_type_id.blank?
        ViewType::JournalAllIndex::SUB_HEADER_VIEW_MAP[journal_all_index_sub_header_view_type_id]
    end
    def journal_static_sub_header_view_type_hash
        return ViewType::JournalStatic::SUB_HEADER_VIEW_MAP[1] if journal_static_sub_header_view_type_id.blank?
        ViewType::JournalStatic::SUB_HEADER_VIEW_MAP[journal_static_sub_header_view_type_id]
    end


    def journal_sub_footer_view_type_hash
        return ViewType::Journal::SUB_FOOTER_VIEW_MAP[1] if journal_sub_footer_view_type_id.blank?
        ViewType::Journal::SUB_FOOTER_VIEW_MAP[journal_sub_footer_view_type_id]
    end
    def article_sub_footer_view_type_hash
        return ViewType::Article::SUB_FOOTER_VIEW_MAP[1] if article_sub_footer_view_type_id.blank?
        ViewType::Article::SUB_FOOTER_VIEW_MAP[article_sub_footer_view_type_id]
    end
    def journal_all_index_sub_footer_view_type_hash
        return ViewType::JournalAllIndex::SUB_FOOTER_VIEW_MAP[1] if journal_all_index_sub_footer_view_type_id.blank?
        ViewType::JournalAllIndex::SUB_FOOTER_VIEW_MAP[journal_all_index_sub_footer_view_type_id]
    end
    def journal_static_sub_footer_view_type_hash
        return ViewType::JournalStatic::SUB_FOOTER_VIEW_MAP[1] if journal_static_sub_footer_view_type_id.blank?
        ViewType::JournalStatic::SUB_FOOTER_VIEW_MAP[journal_static_sub_footer_view_type_id]
    end

end