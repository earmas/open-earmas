#--
# Open Earmas
#
# Copyright (C) 2014 Hiroshima University, ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module SelfDoiCheck extend ActiveSupport::Concern

    def set_self_doi(oaipmh_setting, fields)
        return if oaipmh_setting == nil

        pub_document = get_pub_document
        if pub_document == nil || pub_document.self_doi_active == false
            self.self_doi = nil

            if self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU && oaipmh_setting.enable_self_jalc_doi
                self.self_doi = self_doi_jalc_manu_input
            end

            if self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_JALC_AUTO && oaipmh_setting.enable_self_jalc_doi
                self.self_doi = oaipmh_setting.self_doi_jalc_prefix.to_s + self_doi_jalc_auto_input.to_s + id.to_s
            end

            if self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_CROS_AUTO && oaipmh_setting.enable_self_crossref_doi
                self.self_doi = oaipmh_setting.self_doi_crossref_prefix.to_s + self_doi_crossref_auto_input.to_s + id.to_s
            end
        end

        self.self_doi_checked = check_self_doi(oaipmh_setting, fields, [])
        self.update_columns(self_doi_checked: self.self_doi_checked, self_doi: self.self_doi)
    end

    def changed_change_self_doi_checked?(fields)
        oaipmh_setting = OaipmhSetting.all.first
        self.self_doi_checked = check_self_doi(oaipmh_setting, fields, [])
        return self.self_doi_checked_changed?
    end


    def check_self_doi(oaipmh_setting, fields = nil, error_data = nil)
        return true if oaipmh_setting == nil
        return true if oaipmh_setting.enable_self_doi == false
        pub_document = get_pub_document

        if pub_document != nil && pub_document.self_doi_active
            # 一度でもDOIを公開されたことがある場合

            case self_doi_type
            when SelfDoiDocument::SELF_DOI_TYPE_JALC_DELETED
                if pub_document.self_doi_jalc_active == false
                    add_self_doi_errors(error_data, '削除するSelfDOIの種類が異なっています。')
                    return false
                end

                return true
            when SelfDoiDocument::SELF_DOI_TYPE_CROS_DELETED
                if pub_document.self_doi_crossref_active == false
                    add_self_doi_errors(error_data, '削除するSelfDOIの種類が異なっています。')
                    return false
                end

                return true
            when SelfDoiDocument::SELF_DOI_TYPE_UNUSED

                # 基本到達しない
                add_self_doi_errors(error_data, '一度公開したSelfDOIは無効にできません。')
                return false

            else
                if pub_document.self_doi_type != self.self_doi_type
                    add_self_doi_errors(error_data, '公開したSelfDOIの種類は変更できません。')
                    return false
                end
                if pub_document.self_doi != self.self_doi
                    add_self_doi_errors(error_data, '公開したSelfDOIの値は変更できません。')
                    return false
                end

                # 念のため値のチェック
                return check_self_doi_value(oaipmh_setting, fields, error_data)
            end

        else
            if self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_JALC_DELETED || self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_CROS_DELETED
                add_self_doi_errors(error_data, 'まだ公開されていないSelfDOIは削除できません。')
                return false
            end

            # 一度も公開されたことがない場合

            if self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_UNUSED
                # 使用しない場合は問題なし
                return true
            else
                # 使用する場合は、値のチェック
                return check_self_doi_value(oaipmh_setting, fields, error_data)
            end
        end
    end

    def gen_fields_by_field_def_id_map(fields)
        fields_map = {}

        fields.each do |field|
            fields_map[field.field_def_id] ||= []
            fields_map[field.field_def_id] << field
        end

        fields_map
    end

    def check_self_doi_value(oaipmh_setting, fields, error_data = nil)
        if fields != nil
            fields_map = gen_fields_by_field_def_id_map(fields)
        else
            fields_map = fields_by_field_def_id_map
        end

        nii_type_fields = fields_map[oaipmh_setting.nii_type_field_def_id]
        if nii_type_fields.blank?
            add_self_doi_errors(error_data, 'SelfDOIで使用されるNII資源タイプがありません。')
            return false
        end
        if nii_type_fields.try(:size) != 1
            add_self_doi_errors(error_data, 'SelfDOIで使用されるNII資源タイプの値は１つのみです')
            return false
        end

        if self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_JALC_AUTO || self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU
            if oaipmh_setting.enable_self_jalc_doi == false
                add_self_doi_errors(error_data, 'jalcのSelfDOIは無効になっています。')
                return false
            end


            nii_type_value = nii_type_fields.first.get_list_caption_en


            title_fields = fields_map[oaipmh_setting.title_field_def_id]
            creator_fields = fields_map[oaipmh_setting.creator_field_def_id]
            start_page_fields = fields_map[oaipmh_setting.start_page_field_def_id]


            case nii_type_value
            when 'Departmental Bulletin Paper', 'Journal Article', 'Article', 'Preprint'
                if fields_has_value?(title_fields) == false
                    add_self_doi_errors(error_data, 'SelfDOIで使用されるタイトルがありません。')
                end
                if fields_has_value?(start_page_fields) == false
                    add_self_doi_errors(error_data, 'SelfDOIで使用される開始ページがありません。')
                end

            when 'Thesis or Dissertation'
                if fields_has_value?(title_fields) == false
                    add_self_doi_errors(error_data, 'SelfDOIで使用されるタイトルがありません。')
                end

            when 'Conference Paper', 'Book', 'Technical Report', 'Research Paper'
                if fields_has_value?(title_fields) == false
                    add_self_doi_errors(error_data, 'SelfDOIで使用されるタイトルがありません。')
                end

            when 'Learning Material'
                if fields_has_value?(title_fields) == false
                    add_self_doi_errors(error_data, 'SelfDOIで使用されるタイトルがありません。')
                end

            when 'Presentation'
                if fields_has_value?(title_fields) == false
                    add_self_doi_errors(error_data, 'SelfDOIで使用されるタイトルがありません。')
                end

            else
                add_self_doi_errors(error_data, 'SelfDOIで使用されるNII資源タイプの値が不正です')
                nii_type_fields.first.errors.add(:base, '値が不正です')
            end

            if self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU
                if self_doi_jalc_manu_input.start_with?(SelfDoiDocument::NDL_DOI_PREFIX) == false
                    add_self_doi_errors(error_data, 'はNDLのDOIの値を入力してください', :self_doi_jalc_manu_input)
                end
            end

            check_self_doi_duplicate(error_data)

            return error_exists(error_data)
        end


        if self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_CROS_AUTO
            if oaipmh_setting.enable_self_crossref_doi == false
                add_self_doi_errors(error_data, 'crossrefのSelfDOIは無効になっています。')
                return false
            end

            title_fields = fields_map[oaipmh_setting.title_field_def_id]
            creator_fields = fields_map[oaipmh_setting.creator_field_def_id]
            publisher_fields = fields_map[oaipmh_setting.publisher_field_def_id]
            issn_fields = fields_map[oaipmh_setting.issn_field_def_id]
            isbn_fields = fields_map[oaipmh_setting.isbn_field_def_id]
            jtitle_fields = fields_map[oaipmh_setting.jtitle_field_def_id]
            start_page_fields = fields_map[oaipmh_setting.start_page_field_def_id]


            case nii_type_value
            when 'Departmental Bulletin Paper', 'Journal Article', 'Article', 'Preprint'
                if fields_has_value?(title_fields) == false
                    add_self_doi_errors(error_data, 'SelfDOIで使用されるタイトルがありません。')
                end
                if fields_has_value?(publisher_field_fields) == false
                    add_self_doi_errors(error_data, 'SelfDOIで使用される公開者がありません。')
                end
                if fields_has_value?(issn_fields) == false
                    add_self_doi_errors(error_data, 'SelfDOIで使用されるISSNがありません。')
                end
                if fields_has_value?(jtitle_fields) == false
                    add_self_doi_errors(error_data, 'SelfDOIで使用される雑誌名がありません。')
                end
                if fields_has_value?(start_page_fields) == false
                    add_self_doi_errors(error_data, 'SelfDOIで使用される開始ページがありません。')
                end

            when 'Conference Paper', 'Book', 'Technical Report', 'Research Paper'
                if fields_has_value?(title_fields) == false
                    add_self_doi_errors(error_data, 'SelfDOIで使用されるタイトルがありません。')
                end
                if fields_has_value?(publisher_field_fields) == false
                    add_self_doi_errors(error_data, 'SelfDOIで使用される公開者がありません。')
                end
                if fields_has_value?(isbn_fields) == false
                    add_self_doi_errors(error_data, 'SelfDOIで使用されるISBNがありません。')
                end
            else
                add_self_doi_errors(error_data, 'SelfDOIで使用されるNII資源タイプの値が不正です')
                nii_type_fields.first.errors.add(:base, '値が不正です')
            end

            check_self_doi_duplicate(error_data)

            return error_exists(error_data)
        end

    end

    def check_self_doi_duplicate(error_data)
        return if self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_UNUSED

        # 削除されている場合はPriDocumentは無い可能性があるのでPubDocumentで検索
        # 今設定されているself_doiでの重複チェック
        if PubDocument.unscoped.where(self_doi: self_doi).where.not(id: id).size > 0 \
        || PriDocument.unscoped.where(self_doi: self_doi).where.not(id: id).size > 0
            add_self_doi_errors(error_data, 'このSelfDOIはすでに使用されています')
        end
    end

    def add_self_doi_errors(error_data, value, name = nil)
        if error_data == nil
            if name == nil
                errors.add(:base, value)
            else
                errors.add(name, value)
            end
        else
            if name == nil
                error_data << (errors.full_message(:base, value))
            else
                error_data << (value)
            end
        end
    end

    def error_exists(error_data)
        if error_data == nil
            errors.size == 0
        else
            error_data.size == 0
        end
    end

end