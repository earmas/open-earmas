#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FileHolder extend ActiveSupport::Concern
    include PdfHelper

    TEXT_FILE_NAME = 'content.txt'
    THUMNAIL_FILE_NAME = 'thumnail.png'

    def image?
        mime.start_with?('image/')
    end

    def pdf?
        mime.end_with?('/pdf')
    end

    def text?
        mime.start_with?('text/')
    end

    def has_file?
        File.exists?(file_path)
    end

    def get_ext(file_name)
        file_name_parts = file_name.split('.')
        return nil if file_name_parts.size < 2
        return file_name_parts.last
    end

    def get_mime_type(file_name)
        match = /\.([^.]+)$/.match(file_name)

        mime_type = nil
        mime_type = Mime::Type.lookup_by_extension(match[1].downcase) if match != nil
        mime_type = 'application/octet-stream' if mime_type.blank?
        mime_type
    end

    def file_path
        File.join(stored_dir_path, org_file_name)
    end

    def delete_upload_file
        delete_dir(stored_dir_path)
    end

    def image_width; 600 end
    def image_height; nil end

    def file_type
        return 'pdf' if pdf?
        return 'image' if image?
#        return 'text' if text?
        'other'
    end

    # thumnail

    def has_thumnail?
        File.exists?(thumnail_path)
    end

    def thumnail_path
        return File.join(stored_dir_path, THUMNAIL_FILE_NAME)
    end

    def create_thumnail
        if pdf?
            return create_thumnail_from_pdf(file_path, thumnail_path, has_coverpage)
        end

        if image?
            return create_thumnail_from_image(file_path, thumnail_path)
        end

        return true
    end

    # text

    def has_text?
        File.exists?(text_file_path)
    end

    def text_file_path
        return File.join(stored_dir_path, TEXT_FILE_NAME)
    end

    def get_text_string
        File.open(text_file_path).read.force_encoding(Encoding::UTF_8).scrub.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => '?').gsub(/[\x01-\x08\x0B\x0C\x0E-\x1F\u0000\uFFFF]/, '')
    end

    def create_text
        if pdf?
            return create_text_from_pdf(file_path, text_file_path)
        end

        if text?
        end

        return true
    end

    # coverpage

    def has_coverpage?
        has_coverpage
    end

    def create_coverpage(document_xml_string)
        cover_page_pdf_path = file_path + '.cover'
        covered_page_pdf_path = file_path + '.covered'

        return false if create_cover_page(document_xml_string, cover_page_pdf_path) == false
        return false if marge_pdf(cover_page_pdf_path, file_path, covered_page_pdf_path) == false


        FileUtils.mv covered_page_pdf_path, file_path
        FileUtils.rm cover_page_pdf_path

        self.has_coverpage = true
        self.save!
    end

    def delete_coverpage
        original_pdf_path = file_path + '.uncovered'

        return false if delete_cover_page(file_path, original_pdf_path) == false

        FileUtils.mv original_pdf_path, file_path

        self.has_coverpage = false
        self.save!
    end

private

    def delete_dir(dir)
        return if File.exists?(dir) == false
        return if File.directory?(dir) == false

        Dir::glob(File.join(dir, '*')).each do |child|
            if File.directory?(child)
                delete_dir(child)
            else
                File.delete(child)
            end
        end

        Dir.rmdir(dir)
    end

end
