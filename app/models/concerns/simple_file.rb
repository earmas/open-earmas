#--
# Open Earmas
#
# Copyright (C) 2014 Hiroshima University, ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module SimpleFile extend ActiveSupport::Concern

    PUBLISH_STD = 1
    PUBLISH_LINK_ONLY = 2
    PUBLISH_NONE = 3
    PUBLISH_LTD = 4

    def publish_type_caption
        case publish_type
        when PUBLISH_STD
            ''
        when PUBLISH_LINK_ONLY
            ' ( リンク公開 ) '
        when PUBLISH_NONE
            ' ( 非公開 ) '
        when PUBLISH_LTD
            ' ( 限定公開 ) '
        end
    end

    def publish_type_caption_to_csv
        case publish_type
        when PUBLISH_STD
            '公開'
        when PUBLISH_LINK_ONLY
            'リンク公開'
        when PUBLISH_NONE
            '非公開'
        when PUBLISH_LTD
            '限定公開'
        end
    end

    def set_publish_type_from_csv(value)
        case value
        when 'リンク公開'
            self.publish_type = PUBLISH_LINK_ONLY
        when '非公開'
            self.publish_type = PUBLISH_NONE
        when '限定公開'
            self.publish_type = PUBLISH_LTD
        else
            self.publish_type = PUBLISH_STD
        end
    end

end