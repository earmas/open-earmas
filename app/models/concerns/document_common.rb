#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module DocumentCommon extend ActiveSupport::Concern
    include SelfDoiDocument

    THUMNAIL_WIDHT  = 200
    THUMNAIL_HEIGHT = 200

    def old_thumnail_path
        File.join(thumnail_dir, 'thumnail')
    end

    def new_thumnail_path
        File.join(thumnail_dir, 'thumnail.png')
    end

    def thumnail_path
        return new_thumnail_path if File.exists?(new_thumnail_path)
        old_thumnail_path
    end

    def has_thumnail?
        return true if File.exists?(new_thumnail_path)
        File.exists?(old_thumnail_path)
    end

    def thumnail_dir
        file_dir = File.join(document_dir, "document_thumnail_file")
        FileUtils.mkdir_p(file_dir)
        file_dir
    end

    def document_attach_file_dir
        file_dir = File.join(document_dir, "document_attach_file")
        FileUtils.mkdir_p(file_dir)
        file_dir
    end

    def delete_upload_files
        delete_dir(thumnail_dir)
        delete_dir(document_attach_file_dir)
    end

    def get_input_group
        FieldDefLoader.get_input_group(input_group_id)
    end

    def field_contains_input_group?(field_def_id)
        if FieldDefLoader.get_input_group(input_group_id) == nil
            FieldDefLoader.get_field_def(field_def_id) != nil
        else
            FieldDefLoader.input_group_field_by_field_def_id(input_group_id, field_def_id) != nil
        end
    end

    def get_input_group_field(field_def_id)
        FieldDefLoader.input_group_field_by_field_def_id(input_group_id, field_def_id)
    end

    def fields_by_field_def_id_map
        return @fields_by_field_def_id_map if @fields_by_field_def_id_map != nil
        @fields_by_field_def_id_map = {}

        self.class.reflect_on_all_associations(:has_many).each do |association|
            self.send(association.name).each do |field|
                @fields_by_field_def_id_map[field.field_def_id] ||= []
                @fields_by_field_def_id_map[field.field_def_id] << field
            end
        end

        @fields_by_field_def_id_map
    end

    def fields_by_id_by_field_def_id_map
        return @fields_by_id_by_field_def_id_map if @fields_by_id_by_field_def_id_map != nil
        @fields_by_id_by_field_def_id_map = {}

        self.class.reflect_on_all_associations(:has_many).each do |association|
            self.send(association.name).each do |field|
                @fields_by_id_by_field_def_id_map[field.field_def_id] ||= {}
                @fields_by_id_by_field_def_id_map[field.field_def_id][field.id] = field
            end
        end

        @fields_by_id_by_field_def_id_map
    end

    def get_all_fields
        return @all_fields if @all_fields != nil

        reget_all_fields
    end

    def reget_all_fields(clear_cache = false)
        @all_fields = []

        self.class.reflect_on_all_associations(:has_many).each do |association|
            self.send(association.name, clear_cache).each do |field|
                @all_fields << field
            end
        end

        @all_fields
    end

    def field_defs
        FieldDefLoader.field_defs_by_input_group_id(input_group_id).reject{|a| self.pub_or_lim? && a.to_pub == false }
    end

    def set_detail_field_defs(detail_field_defs)
        @stored_detail_field_defs = []

        detail_field_defs.each do |field_def|
            next if self.pub_or_lim? && field_def.to_pub == false
            next if field_contains_input_group?(field_def.id) == false
            @stored_detail_field_defs << field_def
        end

        @stored_detail_field_defs
    end


    def detail_field_defs
        return @stored_detail_field_defs if @stored_detail_field_defs.present?

        FieldDefLoader.detail_field_defs_by_input_group_id(input_group_id).reject{|a| self.pub_or_lim? && a.to_pub == false }
    end

    def set_result_field_defs(result_field_defs)
        @stored_result_field_defs = []

        result_field_defs.each do |field_def|
            next if self.pub_or_lim? && field_def.to_pub == false
            next if field_contains_input_group?(field_def.id) == false
            @stored_result_field_defs << field_def
        end

        @stored_result_field_defs
    end

    def result_field_defs
        return @stored_result_field_defs if @stored_result_field_defs.present?

        FieldDefLoader.result_field_defs_by_input_group_id(input_group_id).reject{|a| self.pub_or_lim? && a.to_pub == false }
    end

    def field_empty?(field_def_id)
        return true if field_def_id.blank?
        get_fields(field_def_id).blank?
    end

    def get_fields(field_def_id)
        return nil if field_def_id.blank?
        fields_by_field_def_id_map[field_def_id]
    end

    # list

    def set_list_data(fields, list_def_ids = nil)

        if FieldDefLoader.get_input_group(input_group_id) != nil
            fields = fields.reject{|a| get_input_group_field(a.field_def_id) == nil }
        end

        list_def_relation = ListDef.all
        list_def_relation = ListDef.where(id: list_def_ids) if list_def_ids != nil

        list_def_relation.includes(:list_fields, :filter_fields).each do |list_def|

            next if list_def.check_document_filter(self, fields) == false

            list_datum = create_list_datum
            list_datum.list_def_id  = list_def.id
            list_datum.document_id  = id
            list_datum.document_updated_at  = updated_at
            list_datum.document_created_at  = created_at
            list_datum.level1 = []
            list_datum.level2 = []
            list_datum.level3 = []
            list_datum.level4 = []
            list_datum.level5 = []

            level_list_words1 = []
            level_list_words2 = []
            level_list_words3 = []
            level_list_words4 = []
            level_list_words5 = []


            fields.each do |field|
                next if list_def.check_field_filter(self, field) == false

                SortableField.get_all.each_with_index do |sortable_field, index|
                    level = index + 1

                    if sortable_field.field_def_id == field.field_def_id
                        list_datum.send('sort_value_ja_' + level.to_s + '=', field.get_sort_value_ja)
                        list_datum.send('sort_value_en_' + level.to_s + '=', field.get_sort_value_en)
                    end
                end

                next if list_def.get_field(1).blank?
                if list_def.get_field(1).field_def_id == field.field_def_id
                    if field.get_list_param.present?
                        level_list_words1 += list_def.get_field(1).get_list_words(nil, field)
                        list_datum.level1 += list_def.get_field(1).get_list_param_values(field)
                    end
                end

                next if list_def.get_field(2).blank?
                if list_def.get_field(2).field_def_id == field.field_def_id
                    if field.get_list_param.present?
                        level_list_words2 += list_def.get_field(2).get_list_words(list_def.get_field(1), field)
                        list_datum.level2 += list_def.get_field(2).get_list_param_values(field)
                    end
                end

                next if list_def.get_field(3).blank?
                if list_def.get_field(3).field_def_id == field.field_def_id
                    if field.get_list_param.present?
                        level_list_words3 += list_def.get_field(3).get_list_words(list_def.get_field(2), field)
                        list_datum.level3 += list_def.get_field(3).get_list_param_values(field)
                    end
                end

                next if list_def.get_field(4).blank?
                if list_def.get_field(4).field_def_id == field.field_def_id
                    if field.get_list_param.present?
                        level_list_words4 += list_def.get_field(4).get_list_words(list_def.get_field(3), field)
                        list_datum.level4 += list_def.get_field(4).get_list_param_values(field)
                    end
                end

                next if list_def.get_field(5).blank?
                if list_def.get_field(5).field_def_id == field.field_def_id
                    if field.get_list_param.present?
                        level_list_words5 += list_def.get_field(5).get_list_words(list_def.get_field(4), field)
                        list_datum.level5 += list_def.get_field(5).get_list_param_values(field)
                    end
                end
            end

            list_datum.level1.uniq!
            list_datum.level2.uniq!
            list_datum.level3.uniq!
            list_datum.level4.uniq!
            list_datum.level5.uniq!

            next if list_def.list_fields.size != 0 && list_datum.blank_all?

            param_patterns = {}
            param_patterns[1] = []
            param_patterns[2] = []
            param_patterns[3] = []
            param_patterns[4] = []
            param_patterns[5] = []
            if list_def.get_field(1).present?

                level_list_words1 = [nil] if level_list_words1.size == 0
                level_list_words1.each do |level_list_word1|
                    if list_def.get_field(2).present?

                        level_list_words2 = [nil] if level_list_words2.size == 0
                        level_list_words2.each do |level_list_word2|
                            if list_def.get_field(3).present?

                                level_list_words3 = [nil] if level_list_words3.size == 0
                                level_list_words3.each do |level_list_word3|
                                    if list_def.get_field(4).present?

                                        level_list_words4 = [nil] if level_list_words4.size == 0
                                        level_list_words4.each do |level_list_word4|
                                            if list_def.get_field(5).present?

                                                level_list_words5 = [nil] if level_list_words5.size == 0
                                                level_list_words5.each do |level_list_word5|
                                                    param_patterns[5] << {level1: level_list_word1, level2: level_list_word2, level3: level_list_word3, level4: level_list_word4, level5: level_list_word5}
                                                end
                                            end
                                            param_patterns[4] << {level1: level_list_word1, level2: level_list_word2, level3: level_list_word3, level4: level_list_word4}
                                        end
                                    end
                                    param_patterns[3] << {level1: level_list_word1, level2: level_list_word2, level3: level_list_word3}
                                end
                            end
                            param_patterns[2] << {level1: level_list_word1, level2: level_list_word2}
                        end
                    end
                    param_patterns[1] << {level1: level_list_word1}
                end
            end

            old_list_datum = pri_pub_class_name(:ListDatum).where(list_def_id: list_def.id, document_id: id).first
            if old_list_datum != nil
                old_list_datum.destroy!
            end

            5.times do |index|
                index_level = index + 1
                new_list_word_ids = set_list_word(param_patterns[index_level], list_def, index_level)

                list_datum.send('list_word_level' + index_level.to_s + '_ids=', new_list_word_ids)
            end

            list_datum.save!
        end
    end

    def set_list_word_common(param_patterns, list_def, level, limited = nil)
        new_list_word_ids = []

        param_patterns.each do |param_pattern|
            exist_list_word_relation = get_list_word_class(level).where(list_def_id: list_def.id)
            if limited != nil
                exist_list_word_relation.where!(limited: limited)
            end

            level.times do |index|
                index_level = index + 1
                level_sym = ('level' + index_level.to_s).to_sym
                if list_def.get_field(index_level).present?
                    if param_pattern[level_sym].present?
                        exist_list_word_relation.where!(('value' + index_level.to_s) => param_pattern[level_sym][:value])
                    else
                        exist_list_word_relation.where!(('value' + index_level.to_s) => '')
                    end
                end
            end

            if level > 1
                max_level_sym = ('level' + level.to_s).to_sym
                if param_pattern[max_level_sym].present?
                    exist_list_word_relation.where!(('value' + level.to_s + '_prefix') => param_pattern[max_level_sym][:value_prefix])
                else
                    exist_list_word_relation.where!(('value' + level.to_s + '_prefix') => '')
                end
            end

            new_list_word = exist_list_word_relation.first

            if new_list_word != nil
                if new_list_word_ids.include?(new_list_word.id) == false
                    new_list_word.document_count += 1
                end
            else
                new_list_word = get_list_word_class(level).new
                new_list_word.list_def_id  = list_def.id
                new_list_word.document_count = 1
                if limited != nil
                    new_list_word.limited = limited
                end
            end

            if level > 1
                max_level_sym = ('level' + level.to_s).to_sym
                if param_pattern[max_level_sym].present?
                    new_list_word.send('value' + level.to_s + '_prefix=', param_pattern[max_level_sym][:value_prefix])
                else
                    new_list_word.send('value' + level.to_s + '_prefix=', '')
                end
            end

            level.times do |index|
                index_level = index + 1
                level_sym = ('level' + index_level.to_s).to_sym
                next if param_pattern[level_sym].blank?
                new_list_word.send('value' + index_level.to_s + '=', param_pattern[level_sym][:value])
                new_list_word.send('sort_value_ja' + index_level.to_s + '=', param_pattern[level_sym][:sort_value_ja])
                new_list_word.send('sort_value_en' + index_level.to_s + '=', param_pattern[level_sym][:sort_value_en])
                new_list_word.send('caption_ja' + index_level.to_s + '=', param_pattern[level_sym][:caption_ja])
                new_list_word.send('caption_en' + index_level.to_s + '=', param_pattern[level_sym][:caption_en])
            end

            new_list_word.save!
            new_list_word_ids |= [new_list_word.id]
        end
        new_list_word_ids
    end

    # search

    def set_search_data(fields)
        SearchFilterDef.all.each do |filter_field|
            return if filter_field.match_document(self, fields) == false
        end

        if FieldDefLoader.get_input_group(input_group_id) != nil
            fields = fields.reject{|a| FieldDefLoader.input_group_field_by_field_def_id(input_group_id, a.field_def_id) == nil }
        end

        all_text_values = [id.to_s]
        all_text_values_with_file = [id.to_s]
        fields.each do |field|
            SortableField.get_all.each_with_index do |sortable_field, index|
                if sortable_field.field_def_id == field.field_def_id
                    self.send('sort_value_ja_' + (index + 1).to_s + '=', field.get_sort_value_ja)
                    self.send('sort_value_en_' + (index + 1).to_s + '=', field.get_sort_value_en)
                end
            end

            all_text_values.concat(field.get_search_values)
            all_text_values_with_file.concat(field.get_search_values_with_file)
        end

        SearchDef.limit(20).reorder(:id).includes(:fields).each_with_index do |search_def, index|
            field_values = []

            search_def.fields.each do |search_field|

                fields.each do |field|
                    if search_field.field_def_id == field.field_def_id
                        field_values += field.get_search_values
                    end
                end

                if search_field.field_def.field_type_class.no_value?
                    field_values += search_field.field_def.field_type_class.get_no_value_search_values(search_field.field_def, self)

                    all_text_values.concat(search_field.field_def.field_type_class.get_no_value_search_values(search_field.field_def, self))
                    all_text_values_with_file.concat(search_field.field_def.field_type_class.get_no_value_search_values(search_field.field_def, self))
                end
            end

            self.send('text_value_' + (index + 1).to_s + '=', field_values.reject{|a| a.blank? }.join(' '))
        end

        self.text_value_all = all_text_values.reject{|a| a.blank? }
        self.text_value_all_with_file = all_text_values_with_file.reject{|a| a.blank? }
    end

private

    def delete_dir(dir)
        Dir::glob(File.join(dir, '*')).each do |child|
            if File.directory?(child)
                delete_dir(child)
            else
                File.delete(child)
            end
        end

        Dir.rmdir(dir)
    end

end
