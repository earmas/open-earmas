#--
# Open Earmas
#
# Copyright (C) 2014 Hiroshima University, ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module SelfDoiDocument extend ActiveSupport::Concern

    # 新規追加時
    #
    # pri_document チェック前
    # self_doi_type = SELF_DOI_TYPE_UNUSED 以外
    # self_doi = 何か
    # self_doi_checked = false
    #
    # pri_document チェック後
    # self_doi_type = SELF_DOI_TYPE_UNUSED 以外
    # self_doi = 何か
    # self_doi_checked = true
    #
    # pub_document 公開前
    # self_doi_type = SELF_DOI_TYPE_UNUSED
    # self_doi = 何か
    #
    # pub_document 公開後
    # self_doi_type = SELF_DOI_TYPE_INUSE
    # self_doi = 何か

    # 修正時
    #
    # pri_document チェック前
    # self_doi_type = aaa
    # self_doi_jalc_manu_input = aaa
    # self_doi_jalc_auto_input = aaa
    # self_doi_type = aaa
    # self_doi = 何か
    # self_doi_checked = false
    #
    # pri_document チェック後
    # self_doi_type = aaa
    # self_doi_jalc_manu_input = aaa
    # self_doi_jalc_auto_input = aaa
    # self_doi_type = aaa
    # self_doi = 何か
    # self_doi_checked = true
    #
    # pri_document
    # self_doi_type = aaa
    # self_doi_jalc_manu_input = aaa
    # self_doi_jalc_auto_input = aaa
    # self_doi_type = aaa
    # self_doi = 何か



    # 削除時
    #
    # pri_document
    # self_doi_type = 元の値
    # self_doi = 元の値
    # 削除時でも値はいじらない
    #
    # pub_document 公開前
    # self_doi_type = SELF_DOI_TYPE_INUSE
    # self_doi = 元の値
    #
    # pub_document 公開後
    # self_doi_type = SELF_DOI_TYPE_どちらか_DELETED
    # self_doi = 元の値

    SELF_DOI_TYPE_UNUSED      = 0
    SELF_DOI_TYPE_JALC_AUTO   = 1
    SELF_DOI_TYPE_JALC_MANU   = 2
    SELF_DOI_TYPE_CROS_AUTO   = 5
    SELF_DOI_TYPE_JALC_DELETED     = 8
    SELF_DOI_TYPE_CROS_DELETED     = 9

    NDL_DOI_PREFIX = '10.11501/'
    SELF_DOI_BASE_URL = 'http://doi.org/'

    def self_doi_url
        SELF_DOI_BASE_URL + self_doi.to_s
    end

    def show_self_doi_url
        return '' if self_doi_enable == false
        return '' if self_doi.blank?
        SELF_DOI_BASE_URL + self_doi
    end

    def show_self_doi
        return '' if self_doi_enable == false
        return '' if self_doi.blank?
        self_doi
    end

    # 値があるかどうか oaipmh用に値がない場合でも表示する
    def self_doi_active
        self_doi_type != SelfDoiDocument::SELF_DOI_TYPE_UNUSED
    end

    # 値があるかどうか oaipmh用に値がない場合でも表示する
    def self_doi_jalc_active
        self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_JALC_AUTO || self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU || self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_JALC_DELETED
    end

    # 値があるかどうか oaipmh用に値がない場合でも表示する
    def self_doi_crossref_active
        self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_CROS_AUTO || self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_CROS_DELETED
    end

    def self_doi_enable
        self_doi_type != SelfDoiDocument::SELF_DOI_TYPE_UNUSED && self_doi_type != SelfDoiDocument::SELF_DOI_TYPE_JALC_DELETED && self_doi_type != SelfDoiDocument::SELF_DOI_TYPE_CROS_DELETED
    end

    # 表示してもいいかどうか
    def self_doi_visible(oaipmh_setting)
        return false if oaipmh_setting == nil
        return false if oaipmh_setting.enable_self_doi == false

        self_doi_enable
    end

    # 表示してもいいかどうか
    def self_doi_jalc_visible(oaipmh_setting)
        return false if self_doi_visible(oaipmh_setting) == false
        return false if oaipmh_setting.enable_self_jalc_doi == false
        self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_JALC_AUTO || self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU
    end

    # 表示してもいいかどうか
    def self_doi_crossref_visible(oaipmh_setting)
        return false if self_doi_visible(oaipmh_setting) == false
        return false if oaipmh_setting.enable_self_crossref_doi == false
        self_doi_type == SelfDoiDocument::SELF_DOI_TYPE_CROS_AUTO
    end

end

