#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module LogFileAttach extend ActiveSupport::Concern

    TOTAL_FILE_NAME = 'total.txt'

    def log_dst_dir
        @log_dst_dir ||= File.join(Rails.application.config.log_count_dir, 'dst')
    end

    def log_src_dir
        @log_dst_dir ||= File.join(Rails.application.config.log_count_dir, 'src')
    end

    def all_log_base_dir
        log_dir = File.join(log_dst_dir, 'all')
        FileUtils.mkdir_p(log_dir)
        log_dir
    end

    def all_log_file(month)
        get_month_log_file(all_log_base_dir, month)
    end

    def all_month
        Dir::glob(File.join(all_log_base_dir, '*.txt')).select{|a|File.file?(a)}.map{|a| File.basename(a).gsub('.txt', '') }
    end

    def person_log_base_dir
        log_dir = File.join(log_dst_dir, 'person')
        FileUtils.mkdir_p(log_dir)
        log_dir
    end

    def person_log_dir(person_id)
        log_dir = File.join(person_log_base_dir, person_id.to_s)
        FileUtils.mkdir_p(log_dir)
        log_dir
    end

    def document_log_base_dir
        log_dir = File.join(log_dst_dir, 'id')
        FileUtils.mkdir_p(log_dir)
        log_dir
    end

    def document_log_dir(document_id)
        if document_id.to_i == 0
            log_dir = File.join(document_log_base_dir, 'other')
            FileUtils.mkdir_p(log_dir)
            log_dir
        else
            number_data_dir = (document_id.to_i / 10000).to_s
            log_dir = File.join(document_log_base_dir, number_data_dir, document_id.to_s)
            FileUtils.mkdir_p(log_dir)
            log_dir
        end
    end

    def person_log_file(person_id, month)
        log_dir = person_log_dir(person_id)
        get_month_log_file(log_dir, month)
    end

    def document_log_file(document_id, month)
        log_dir = document_log_dir(document_id)
        get_month_log_file(log_dir, month)
    end

    def get_person_log_dirs
        Dir::glob(File.join(person_log_base_dir, '*')).select{|a|File.directory?(a)}
    end

    def get_document_log_dirs
        Dir::glob(File.join(document_log_base_dir, '*', '*')).select{|a|File.directory?(a)}
    end


private

    def get_month_log_file(dir, month)
        FileUtils.mkdir_p(dir)
        if month.blank?
            File.join(dir, TOTAL_FILE_NAME)
        else
            File.join(dir, month.delete('./').to_s + '.txt')
        end
    end

    extend(self)
    def self.included(base)
        base.extend(self)
    end
end