#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class PeopleSetting < ActiveRecord::Base

    default_scope { order(:id) }

    def self.create_default
        people_setting = PeopleSetting.new
        people_setting.save!
        people_setting
    end

    def locale_key_param(number)
        if I18n.locale == :ja
            send("key#{number}_param_ja")
        else
            send("key#{number}_param_en")
        end
    end

    def enable_crypt?
        Rails.application.config.try(:crypt_external) || (cipher_type.present? && cipher_iv.present? && cipher_key.present?)
    end

    def encrypt_data(data)
        return data if data.blank? || enable_crypt? == false

        return encrypt_external_program(data) if Rails.application.config.try(:crypt_external)

        begin
            cipher = OpenSSL::Cipher.new(cipher_type)
            cipher.encrypt
            cipher.iv = cipher_iv
            cipher.key = cipher_key
            cipher.update(data)
            (cipher.final).unpack("H*")[0]
        rescue Exception => ex
            data
        end
    end

    def decrypt_data(data)
        return data if data.blank? || enable_crypt? == false

        return decrypt_external_program(data) if Rails.application.config.try(:crypt_external)

        begin
            cipher = OpenSSL::Cipher.new(cipher_type)
            cipher.decrypt
            cipher.iv = cipher_iv
            cipher.key = cipher_key
            cipher.update([data].pack("H*")) + cipher.final
        rescue Exception => ex
            data
        end
    end


    def encrypt_external_program(value)
        begin
            stdout_str, stderr_str, status = Open3.capture3("#{Rails.application.config.encrypt_external_program} #{value}")
            return value if stdout_str.blank?
            stdout_str
        rescue Exception => ex
            value
        end
    end
    def decrypt_external_program(value)
        begin
            stdout_str, stderr_str, status = Open3.capture3("#{Rails.application.config.decrypt_external_program} #{value}")
            return value if stdout_str.blank?
            stdout_str
        rescue Exception => ex
            value
        end
    end

end
