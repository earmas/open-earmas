class Hook::Document
    def self.on_publish(pri_document)
        hook_class = Rails.application.config.try(:publish_hook)
        return if hook_class.blank?

        hook_class.constantize.new.call_hook(pri_document)
    end


    def self.on_delete(pri_document)
        hook_class = Rails.application.config.try(:delete_hook)
        return if hook_class.blank?

        hook_class.constantize.new.call_hook(pri_document)
    end

end