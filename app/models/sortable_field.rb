#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class SortableField < ActiveRecord::Base

    class ThreadValue
        attr_accessor :values

        def set_values
            return if @values != nil
            @values = []
            SortableField.limit(3).includes(:field_def).to_a.each do |sortable_field|
                @values << sortable_field
            end

        end
    end


    class << self
        def thread_cache
            Thread.current[:earmas_sortable_field] ||= SortableField::ThreadValue.new
        end

        def get_all
            thread_cache.set_values
            thread_cache.values
        end
    end


    default_scope { order(:sort_index, :id) }

    belongs_to :field_def

    validate  :duplicate_field_def_id

private

    def duplicate_field_def_id
        if SortableField.where.not(id: id).where(field_def_id: field_def_id).size != 0
            errors.add(:field_def_id, "が重複しています")
        end
    end

end
