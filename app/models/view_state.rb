#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module ViewState

    PUB = 1
    LIM = 2
    PRI = 3

    class ThreadValue
        attr_accessor :state
    end


    class << self
        def thread_value
            Thread.current[:earmas_view_state] ||= ViewState::ThreadValue.new
        end

        def to_pub
            thread_value.state = PUB
        end
        def to_lim
            thread_value.state = LIM
        end
        def to_pri
            thread_value.state = PRI
        end

        def pub?
            thread_value.state == PUB
        end
        def lim?
            thread_value.state == LIM
        end
        def pri?
            thread_value.state == PRI
        end
    end
end
