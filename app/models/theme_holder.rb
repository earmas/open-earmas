#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module ThemeHolder

    module_function

    # レイアウトとスタイルシートを設定する

    @@theme_map = nil

    def set_all_theme_model
        @@theme_map = {}
        Rails.application.config.theme_models.each do |field_class_name|
            theme_class = ('Theme::' + field_class_name.camelize).constantize
            @@theme_map[theme_class::THEME_ID] = theme_class
        end
    end

    def get_theme_map
        if @@theme_map.nil? && Rails.application.config.theme_models.present?
            set_all_theme_model
        end
        @@theme_map
    end

end
