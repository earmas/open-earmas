#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class DocumentFilterDef < ActiveRecord::Base

    OAIPMH_TYPE = 1
    LIST_TYPE   = 2
    SEARCH_TYPE = 3
    EJ_TYPE     = 4

    FILTER_TYPES = {
        field_value_filter: {
            id: 1,
            caption: { ja: 'データ値フィルタ', en: 'field value filter' },
        },
        field_key_filter: {
            id: 2,
            caption: { ja: 'データキーフィルタ', en: 'field key filter' },
        },
        input_group_filter: {
            id: 5,
            caption: { ja: 'データグループフィルタ', en: 'input group filter' },
        },
    }

    scope :oaipmh, -> { where(list_def_id: 0) }

    validates :include_filter, inclusion: {:in => [true, false], message: 'は必須です。'}
    validates :some_id,        presence: { message: 'は必須です。'}

    validate  :check_filter_value_regex

    def match_document(document, fields)
        case filter_type
        when FILTER_TYPES[:field_value_filter][:id]
            match_document_field_value(document, fields)
        when FILTER_TYPES[:field_key_filter][:id]
            match_document_field_key(document, fields)
        when FILTER_TYPES[:input_group_filter][:id]
            match_input_group(document, fields)
        else
            return false
        end
    end

    def match_field(document, field)
        case filter_type
        when FILTER_TYPES[:field_value_filter][:id]
            match_field_value(document, field)
        when FILTER_TYPES[:field_key_filter][:id]
            match_field_key(document, field)
        when FILTER_TYPES[:input_group_filter][:id]
            match_input_group(document, field)
        else
            return false
        end
    end

    def clear_optional_param
        self.filter_type = nil
        self.some_id = nil
        self.filter_value = nil
    end

    def show_filter_type_caption
        case filter_type
        when FILTER_TYPES[:field_value_filter][:id]
            FILTER_TYPES[:field_value_filter][:caption]
        when FILTER_TYPES[:field_key_filter][:id]
            FILTER_TYPES[:field_key_filter][:caption]
        when FILTER_TYPES[:input_group_filter][:id]
            FILTER_TYPES[:input_group_filter][:caption]
        else
            return nil
        end
    end

private

    def check_filter_value_regex
        begin
            Regexp.new(filter_value)
            return true
        rescue Exception => ex
            errors.add(:filter_value, " 無効な値です "  + ex.message)
            return false
        end
    end

    def match_document_field_value(document, fields)
        @filter_value_regex ||= Regexp.new(filter_value)

        if include_filter
            # 含む　はどれかにあればいい
            fields.each do |field|
                return true if field.field_def_id == some_id && @filter_value_regex.match(field.to_filter_value) != nil
            end
            return false
        else
            # 含まない　は一つもあってはいけない
            fields.each do |field|
                return false if field.field_def_id == some_id && @filter_value_regex.match(field.to_filter_value) != nil
            end
            return true
        end
    end

    def match_document_field_key(document, fields)
        @filter_value_regex ||= Regexp.new(filter_value)

        if include_filter
            # 含む　はどれかにあればいい
            fields.each do |field|
                return true if field.field_def_id == some_id && @filter_value_regex.match(field.to_filter_key_value) != nil
            end
            return false
        else
            # 含まない　は一つもあってはいけない
            fields.each do |field|
                return false if field.field_def_id == some_id && @filter_value_regex.match(field.to_filter_key_value) != nil
            end
            return true
        end
    end

    def match_field_value(document, field)
        @filter_value_regex ||= Regexp.new(filter_value)
        return true if field.field_def_id != some_id

        if include_filter
            @filter_value_regex.match(field.to_filter_value) != nil
        else
            @filter_value_regex.match(field.to_filter_value) == nil
        end
    end

    def match_field_key(document, field)
        @filter_value_regex ||= Regexp.new(filter_value)
        return true if field.field_def_id != some_id

        if include_filter
            @filter_value_regex.match(field.to_filter_key_value) != nil
        else
            @filter_value_regex.match(field.to_filter_key_value) == nil
        end
    end

    def match_input_group(document, field)
        if include_filter
            document.input_group_id == some_id
        else
            document.input_group_id != some_id
        end
    end
end
