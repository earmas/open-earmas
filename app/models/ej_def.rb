#--
# Open Earmas
# 
# Copyright (C) 2014 Hiroshima University, ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class EjDef < ActiveRecord::Base

    require 'nokogiri'

    REVERSE_BOOL_VALUE = ['降順', '昇順']
    RSS_FILE_PATH = 'lib/default_files/rss/rss.xml'

    has_many :journal_defs,    -> { order(:id) },           class_name: :EjJournalDef,                               dependent: :destroy
    has_many :filter_fields,   -> { order(:id) },           class_name: :EjFilterDef, :foreign_key => :parent_id,    dependent: :delete_all
    has_many :result_fields,   -> { order(:sort_index) },   class_name: :EjResultField,                              dependent: :delete_all
    has_many :detail_fields,   -> { order(:sort_index) },   class_name: :EjDetailField,                              dependent: :delete_all

    belongs_to :journal_field_def,       class_name: :FieldDef, :foreign_key => :journal_field_def_id
    belongs_to :volume_field_def,        class_name: :FieldDef, :foreign_key => :volume_field_def_id
    belongs_to :issue_field_def,         class_name: :FieldDef, :foreign_key => :issue_field_def_id
    belongs_to :publish_date_field_def,  class_name: :FieldDef, :foreign_key => :publish_date_field_def_id
    belongs_to :section_field_def,       class_name: :FieldDef, :foreign_key => :section_field_def_id
    belongs_to :sort_field_def,          class_name: :FieldDef, :foreign_key => :sort_field_def_id

    belongs_to :start_page_field_def,    class_name: :FieldDef, :foreign_key => :start_page_field_def_id
    belongs_to :end_page_field_def,      class_name: :FieldDef, :foreign_key => :end_page_field_def_id


    has_many :pri_ej_data,  dependent: :delete_all
    has_many :pri_ej_words, dependent: :delete_all

    has_many :pub_ej_data,  dependent: :delete_all
    has_many :pub_ej_words, dependent: :delete_all

    validate  :xslt_is_valid

    def check_document_filter(document, fields)
        filter_fields.each do |filter_field|
            return false if filter_field.match_document(document, fields) == false
        end
        return true
    end
    def check_field_filter(document, field)
        filter_fields.each do |filter_field|
            return false if filter_field.match_field(document, field) == false
        end
        return true
    end

    def get_field(level)
        set_field_level if @level_fields == nil
        @level_fields[level - 1]
    end

    def get_max_level
        set_field_level if @level_fields == nil
        @level_fields.size
    end

    def default_result_field_defs
        return @default_result_field_defs if @default_result_field_defs.present?

        @default_result_field_defs = result_field_defs

        if @default_result_field_defs.size == 0
            @default_result_field_defs = FieldDefLoader.result_field_defs.reject{|a| ViewState.pri? == false && a.to_pub == false }.map{|a| {ja: a, en: a} }
        end

        @default_result_field_defs
    end

    def result_field_defs
        return @result_field_defs if @result_field_defs.present?

        @result_field_defs = []
        result_fields.each do |result_field|
            ja_field_def = FieldDefLoader.get_field_def(result_field.ja_field_def_id)
            en_field_def = FieldDefLoader.get_field_def(result_field.en_field_def_id)
            ja_field_def = nil if ja_field_def != nil && ViewState.pri? == false && ja_field_def.to_pub == false
            en_field_def = nil if en_field_def != nil && ViewState.pri? == false && en_field_def.to_pub == false
            next if ja_field_def == nil && en_field_def == nil

            @result_field_defs << {ja: ja_field_def, en: en_field_def, en_instead_ja: result_field.en_instead_ja, ja_instead_en: result_field.ja_instead_en}
        end

        @result_field_defs
    end

    def detail_field_defs
        return @detail_field_defs if @detail_field_defs.present?

        @detail_field_defs = []
        detail_fields.each do |detail_field|
            ja_field_def = FieldDefLoader.get_field_def(detail_field.ja_field_def_id)
            en_field_def = FieldDefLoader.get_field_def(detail_field.en_field_def_id)
            ja_field_def = nil if ja_field_def != nil && ViewState.pri? == false && ja_field_def.to_pub == false
            en_field_def = nil if en_field_def != nil && ViewState.pri? == false && en_field_def.to_pub == false
            next if ja_field_def == nil && en_field_def == nil

            @detail_field_defs << {ja: ja_field_def, en: en_field_def, en_instead_ja: detail_field.en_instead_ja, ja_instead_en: detail_field.ja_instead_en}
        end

        @detail_field_defs
    end

    def set_default_value
        self.rss_xml_string = File.open(RSS_FILE_PATH).read if self.rss_xml_string.blank?
    end

    def xslt_is_valid
        return true if rss_xml_string.blank?
        begin
            Nokogiri::XSLT.parse(rss_xml_string)
        rescue => ex
            errors.add(:rss_xml_string, "不正なXSLTです : " + ex.message)
            return false
        end
    end

    def toc_index(volume, issue)
        [volume, issue].join(' - ')
    end

end
