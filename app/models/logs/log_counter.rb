#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Logs::LogCounter
    include LogFileAttach

    COUNT_TIME_DIFF = 60 * 60 * 24

    def initialize(month)
        @all_count = Logs::LogCountData.new
        @id_count_map = {}

        @last_count_datetime_map = {}

        @month = month
    end

    def count(log_data)
        remote_host = log_data.get_value(:remote_host)
        url = log_data.get_value(:url)
        user_agent = log_data.get_value(:user_agent)
        access_key = remote_host.to_s + url.to_s + user_agent.to_s

        time_epoch = log_data.get_value(:timestamp).to_i
        last_time_epoch = @last_count_datetime_map[access_key]

        if last_time_epoch != nil
            time_diff = time_epoch - last_time_epoch
            if time_diff > 0
                @last_count_datetime_map[access_key] = time_epoch
            end

            return if time_diff < COUNT_TIME_DIFF
        else
            @last_count_datetime_map[access_key] = time_epoch
        end


        @all_count.add(log_data)

        id_value = log_data.get_value(:id)
        if id_value.present?
            @id_count_map[id_value] ||= Logs::LogCountData.new

            @id_count_map[id_value].add(log_data)
        end

    end

    def show
        p @month.to_s.rjust(100, '-')
        @all_count.show

        @id_count_map.each do |id, value|
            p id.to_s.rjust(100, '-')
            p @month.to_s.rjust(100, '-')
            value.show
        end
    end

    def write
        @title_map_builder ||= Logs::TitleMapBuilder.new

        @all_count.write(all_log_file(@month), @title_map_builder)

        @id_count_map.each do |id, value|
            value.write(document_log_file(id, @month), @title_map_builder)
        end
    end

end
