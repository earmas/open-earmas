#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Logs::LogParser

    def initialize
        format = '%h %l %u %t "%r" %>s %b "%{Referer}i" "%{User-agent}i" "%{x_forwarded_for}i"'

        log_format_term_map = {}
        log_format_term_map['%h']                       = { regex: '(\S+)', name: :remote_host}
        log_format_term_map['%l']                       = { regex: '(\S+)', name: :client_ident}
        log_format_term_map['%u']                       = { regex: '(\S+)', name: :auth_user_name}
        log_format_term_map['%t']                       = { regex: '\[([^\]]+)\]', name: :timestamp}
        log_format_term_map['"%r"']                     = { regex: '"([^\"]*)"', name: :first_line}
        log_format_term_map['%>s']                      = { regex: '(\S+)', name: :status_code}
        log_format_term_map['%b']                       = { regex: '(\S+)', name: :bytes}
        log_format_term_map['"%{Referer}i"']            = { regex: '"([^\"]*)"', name: :referer}
        log_format_term_map['"%{User-agent}i"']         = { regex: '"([^\"]*)"', name: :user_agent}
        log_format_term_map['"%{x_forwarded_for}i"']    = { regex: '"([^\"]+)?"', name: :x_forwarded_for}

        @log_format_term_names = []

        log_format_term_regex_string = nil

        format.scan(/[^\s]+/).each do |term|
            log_format_term = log_format_term_map[term]
            if log_format_term == nil
                p 'invalid term : ' + term
                exit
            end

            if log_format_term_regex_string == nil
                 log_format_term_regex_string = ''
            else
                 log_format_term_regex_string << '\s+'
            end
            log_format_term_regex_string << log_format_term[:regex]
            @log_format_term_names << log_format_term[:name]
        end

        @log_format_term_regex = Regexp.new(log_format_term_regex_string)

        @log_format_sub_term_map = {}
        @log_format_sub_term_map[:remote_host]      = [{ regex: /(\S+)/, names: [:remote_host] }]
        @log_format_sub_term_map[:client_ident]     = [{ regex: /(\S+)/, names: [:client_ident] }]
        @log_format_sub_term_map[:auth_user_name]   = [{ regex: /(\S+)/, names: [:auth_user_name] }]
        @log_format_sub_term_map[:timestamp]        = [{ regex: /(\S+ [\d\+\-]+)/, names: [:timestamp] }]
        @log_format_sub_term_map[:first_line]       = [
                                                        { regex: /([A-Z]+) (.+) HTTP\/(\d+(?:\.\d+)*)/, names: [:method, :url, :http] },
                                                        { regex: /(.*)/, names: [:value] }
                                                    ]
        @log_format_sub_term_map[:status_code]      = [{ regex: /(\d{3})/, names: [:status_code] }]
        @log_format_sub_term_map[:bytes]            = [{ regex: /(\d+|-)/, names: [:bytes] }]
        @log_format_sub_term_map[:referer]          = [{ regex: /(.*)/, names: [:referer] }]
        @log_format_sub_term_map[:user_agent]       = [{ regex: /(.*)/, names: [:user_agent] }]
        @log_format_sub_term_map[:x_forwarded_for]  = [{ regex: /([\s\d\,\.\:]+)/, names: [:x_forwarded_for] }]

        @localhosts = ['127.0.0.1', '::1']

        @url_checker = Logs::UrlChecker.new
        @bot_checker = AccessCountSetting.all.first.create_bot_checker

        @domain_regex = /^https?\:\/\/([^\.\/]+(?:\.[^\.\/]+)+)/
        @query_string_regex = /\?(.+)/

        @sarch_word_regexs = {
            google: /q=([^&]+)/,
            yahoo:  /p=([^&]+)/,
            bing:   /q=([^&]+)/,
            baidu:  /wd=([^&]+)/,
            yandex: /text=([^&]+)/,
            cinii:  /q=([^&]+)/,
            ask:    /q=([^&]+)/,
            calil:  /q=([^&]+)/,
            ndl:    /any=([^&]+)/,
            AccessCountSetting.all.first.my_url.to_sym => /(?:[^\=]+)=([^&]+)/,
        }

        @system_setting = SystemSetting.all.first
    end

    def parse(line)
        log_terms = line.match(@log_format_term_regex)
        if log_terms == nil
            p 'invalid format : ' + line
            exit
        end

        log_data = Logs::LogData.new

        @log_format_term_names.each_with_index do |log_format_term_name, index|
            log_term = log_terms[index+1]
            next if log_format_term_name == :x_forwarded_for && log_term == nil

            log_format_sub_patterns = @log_format_sub_term_map[log_format_term_name]

            parse_term(log_format_sub_patterns, log_term, log_data)
        end

        log_data
    end

private

    def parse_term(log_format_sub_patterns, log_term, log_data)
        log_format_sub_patterns.each do |log_format_sub_pattern|

            log_format_sub_pattern_names = log_format_sub_pattern[:names]
            log_format_sub_pattern_regex = log_format_sub_pattern[:regex]

            log_sub_terms = log_term.match(log_format_sub_pattern_regex)

            next if log_sub_terms == nil

            parse_sub_term(log_format_sub_pattern_names, log_sub_terms, log_data)
            return
        end
    end

    def parse_sub_term(log_format_sub_term_parts_names, log_sub_terms, log_data)
        log_format_sub_term_parts_names.each_with_index do |name, index|
            log_data_value = log_sub_terms[index+1]

            create_log_date(name, log_data_value, log_data)
        end
    end

    def create_log_date(name, log_data_value, log_data)
        case name
        when :timestamp

            begin
                log_data.set_value(name, DateTime.strptime(log_data_value, "%d/%b/%Y:%H:%M:%S %Z").in_time_zone)
                log_data.set_value(:month, log_data.get_value(name).strftime("%Y-%m"))
            rescue Exception => ex
                p "invalid timestamp " + ex.message.to_s + ' ' + log_data_value.to_s
                throw ex
            end

        when :url

            doc_identifier = @url_checker.get_doc_access_identifier(log_data_value)
            if doc_identifier != nil
                log_data.set_value(:id, @system_setting.public_id_to_document_id(doc_identifier))
                log_data.set_value(:access_log, 'yes')
            end
            doc_identifier = @url_checker.get_doc_download_identifier(log_data_value)
            if doc_identifier != nil
                log_data.set_value(:id, doc_identifier)
                log_data.set_value(:download_log ,'yes')
            end

            if @url_checker.check_not_public(log_data_value)
                log_data.set_value(:public ,'no')
            else
                log_data.set_value(:public ,'yes')
            end

            log_data.set_value(name, log_data_value)

        when :remote_host

            log_data.set_value(name, log_data_value)
            log_data.set_value(:bot, 'yes') if @bot_checker.bot_addr?(log_data_value)

        when :x_forwarded_for

            if log_data_value.delete(' ,.:').strip.present? && @localhosts.include?(log_data.get_value(:remote_host)) 
               log_data.set_value(:remote_host, log_data_value)

                if @bot_checker.bot_addr?(log_data_value) || @bot_checker.bot_names?(log_data.get_value(:user_agent))
                    log_data.set_value(:bot, 'yes')
                else
                    log_data.set_value(:bot, 'no')
                end
            end

        when :user_agent

            log_data.set_value(name, log_data_value)
            log_data.set_value(:bot, 'yes') if @bot_checker.bot_names?(log_data_value)

        when :bytes

            bytes = log_data_value.to_i >> 10

            if bytes > 1024
                bytes >>= 10

                if bytes > 1024
                    bytes >>= 10

                    if bytes > 1024
                        bytes >>= 10
                        bytes <<= 1000
                    end

                    bytes <<= 10
                end

                bytes <<= 10
            end


            log_data.set_value(name, log_data_value.to_i)
            log_data.set_value(:round_bytes, bytes)

        when :referer

            domain_match = @domain_regex.match(log_data_value)
            if domain_match != nil
                log_data.set_value(:referer_domain, domain_match[1])
                parse_query_string(domain_match[1], log_data_value, log_data)
            else
                log_data.set_value(:referer_domain, '-')
            end

            log_data.set_value(name, log_data_value)

        when :value

            p "not url : log value " + log_data_value.to_s

        else

            log_data.set_value(name, log_data_value)

        end
    end

    def parse_query_string(domain, log_data_value, log_data)
        query_string_match = @query_string_regex.match(log_data_value)
        return if query_string_match == nil

        query_string = query_string_match[1]

        @sarch_word_regexs.each do |name, value|
            next if domain.include?(name.to_s) == false

            search_word_match = value.match(query_string)
            if search_word_match
                search_query = search_word_match[1].gsub('+', ' ').gsub('%20', ' ').gsub('%E3%80%80', ' ')
                search_words = search_query.split(' ')

                log_data.set_value(:search_query, search_query)

                search_words.each do |search_word|
                    next if search_word.blank?
                    log_data.add_value(:search_words, search_word)
                end
            end
            return
        end

    end
end

