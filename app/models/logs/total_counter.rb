#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Logs::TotalCounter
    include LogFileAttach

    def count
        walk_all
        walk_document
        walk_person
    end

private

    def walk_all
        build_total(all_log_base_dir)
    end

    def walk_document
        get_document_log_dirs.each do |document_dir|
            build_total(document_dir)
        end
    end

    def walk_person
        get_person_log_dirs.each do |person_dir|
            build_total(person_dir)
        end
    end

    def build_total(dir)
        total_count_data = Logs::LogCountData.new

        Dir::glob(File.join(dir, '*.txt')).select{|a|File.file?(a)}.sort.reverse.take(13).each do |count_file|
            file_name = File.basename(count_file)

            next if file_name == LogFileAttach::TOTAL_FILE_NAME

            count_data = Logs::LogCountData.new.load_file(count_file)
            total_count_data.merge(count_data)
        end

        total_count_file = File.join(dir, LogFileAttach::TOTAL_FILE_NAME)
        total_count_data.write(total_count_file)
    end
end

