#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Logs::TitleMapBuilder
    include DocumentsHelper, LocaleHelper

    def build_map(data)
        set_id_title_maps(data, Hash[data['id']])
    end

private

    def set_id_title_maps(data, id_counts)
        data['id_title_ja'] = {}
        data['id_title_en'] = {}

        documents = PubDocument.where(id: id_counts.keys.reject{|a|a == 'other'})

        documents.each do |document|
            title = get_document_title_strings(document, '')
            data['id_title_ja'][document.id.to_s] = title[:ja]
            data['id_title_en'][document.id.to_s] = title[:en]
        end
    end

end