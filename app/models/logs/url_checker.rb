#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Logs::UrlChecker

    def initialize
        @not_pub_url_regex = Regexp.new('^\/?(ja\/|en\/)?(lim\/|pri\/|staff\/)')

        @doc_access_id_regexs = []
        @doc_access_id_regexs << Regexp.new('^\/?(?:ja\/|en\/)?(\d+)(?!.+file)\/?(?:\?.*)?')
        @doc_access_id_regexs << Regexp.new('^\/?(?:ja\/|en\/)?p\/\d+\/(\d+)\/?(?:\?.*)?')
        @doc_access_id_regexs << Regexp.new('^\/?(?:ja\/|en\/)?metadata\/(\d+)\/?(?:\?.*)?')

        @doc_access_id_regexs << Regexp.new('^\/?(?:ja\/|en\/)?(?:[^\/]+\/)+item\/(\d+)\/?(?:\?.*)?')
        @doc_access_id_regexs << Regexp.new('^\/?(?:ja\/|en\/)?(?:[^\/]+\/)+p\/\d+\/item\/(\d+)\/?(?:\?.*)?')


        @doc_download_id_regexs = []
        @doc_download_id_regexs << Regexp.new('^\/?files\/public\/(\d+)\/\d+\/(?!thumnail\.png)(?:[^\/]+)\/?(?:\?.*)?$')
        @doc_download_id_regexs << Regexp.new('^\/?files\/public\/(?:\d+)\/(\d+)\/\d+\/(?!thumnail\.png)(?:[^\/]+)\/?(?:\?.*)?$')
        @doc_download_id_regexs << Regexp.new('^\/?(?:ja\/|en\/)?(\d+)\/file(?!.+thumnail)\/?(?:\?.*)?')
        @doc_download_id_regexs << Regexp.new('^\/?(?:ja\/|en\/)?file\/(\d+)\/\d+\/\d+(?!.+thumnail)\/?(?:\?.*)?')
    end

    def check_not_public(url)
        url.match(@not_pub_url_regex) != nil
    end

    def get_doc_access_identifier(url)
        @doc_access_id_regexs.each do |doc_access_id_regex|
            match = url.match(doc_access_id_regex)
            return match[1] if match != nil
        end
        return nil
    end
    def get_doc_download_identifier(url)
        @doc_download_id_regexs.each do |doc_download_id_regex|
            match = url.match(doc_download_id_regex)
            return match[1] if match != nil
        end
        return nil
    end
end

