#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Logs::LogCountData
    include LogFileAttach

    DATA_TYPES = [
        :remote_host,
        :month,
        :method,
        :url,
        :http,
        :status_code,
        :round_bytes,
        :bytes,
        :referer,
        :referer_domain,
        :search_query,
        :search_words,
        :user_agent,
        :bot,
        :public,
        :access_log,
        :download_log,
        :id,
        :id_access,
        :id_download,
        :id_title_ja,
        :id_title_en
    ]

    def initialize
        @data = { 'total' => 0 }
        DATA_TYPES.each do |data_type|
            @data[data_type.to_s] ||= Hash.new
        end
    end

    def load_all_total
        load_file(File.join(all_log_base_dir, LogFileAttach::TOTAL_FILE_NAME))
    end

    def load_all_log(month = nil)
        load_file(all_log_file(month))
    end

    def load_person_log(person_id, month = nil)
        load_file(person_log_file(person_id, month))
    end

    def load_document_log(document_id, month = nil)
        load_file(document_log_file(document_id, month))
    end

    def load_file(file)
        @file = file
        if File.exists?(@file)
            @data = JSON.parse(File.read(@file))
        end
        self
    end


    def write(file = nil, title_map_builder = nil)
        file = @file if file == nil
        @file = file
        f = File.open(file, "w")

        sort
        set_other
        title_map_builder.build_map(@data) if title_map_builder != nil

        f.write @data.to_json
        f.close
    end

    def add(log_data)
        return if log_data.get_value(:public) == 'no'
        return if log_data.get_value(:bot) == 'yes'
        return if log_data.get_value(:status_code) != '200'
        return if log_data.get_value(:method) != 'GET'

        @data['total'] += 1

        DATA_TYPES.each do |data_type|
            @data[data_type.to_s] ||= {}

            value = log_data.get_value(data_type)
            next if value.blank?

            if value.class.to_s == 'Array'
                value.each do |sub_value|
                    @data[data_type.to_s][sub_value] ||= 0
                    @data[data_type.to_s][sub_value] += 1
                end
            else
                @data[data_type.to_s][value] ||= 0
                @data[data_type.to_s][value] += 1
            end
        end

        id_value = log_data.get_value(:id)
        if id_value.present?
            if log_data.get_value(:access_log) == 'yes'
                @data[:id_access.to_s][id_value] ||= 0
                @data[:id_access.to_s][id_value] += 1
            end
            if log_data.get_value(:download_log) == 'yes'
                @data[:id_download.to_s][id_value] ||= 0
                @data[:id_download.to_s][id_value] += 1
            end
        end
    end

    def merge(log_count_data)
        @data['total'] += log_count_data.total

        DATA_TYPES.each do |data_type|
            @data[data_type.to_s] ||= {}

            log_count_data.get_count_map(data_type).each do |name, value|
                if data_type.to_s == 'id_title_ja' || data_type.to_s == 'id_title_en'
                    @data[data_type.to_s][name] = value
                else
                    @data[data_type.to_s][name] ||= 0
                    @data[data_type.to_s][name] += value
                end
            end
        end
    end

    def get_count_map(data_type)
        @data[data_type.to_s] || {}
    end

    def build_title_map
        @data['title_ja'] = {}
        @data['title_en'] = {}
        @data['title_ja_access'] = {}
        @data['title_en_access'] = {}
        @data['title_ja_download'] = {}
        @data['title_en_download'] = {}

        get_count_map(:id).each do |k, v|
            title_ja = get_count_map(:id_title_ja)[k] || ''
            if title_ja.blank?
                @data['title_ja']["該当なし : " + k.to_s] = v
            else
                @data['title_ja'][title_ja] = v
            end
            title_en = get_count_map(:id_title_en)[k] || ''
            if title_en.blank?
                @data['title_en']["not found : " + k.to_s] = v
            else
                @data['title_en'][title_en] = v
            end
        end
        get_count_map(:id_access).each do |k, v|
            title_ja = get_count_map(:id_title_ja)[k] || ''
            if title_ja.blank?
                @data['title_ja_access']["該当なし : " + k.to_s] = v
            else
                @data['title_ja_access'][title_ja] = v
            end
            title_en = get_count_map(:id_title_en)[k] || ''
            if title_en.blank?
                @data['title_en_access']["not found : " + k.to_s] = v
            else
                @data['title_en_access'][title_en] = v
            end
        end
        get_count_map(:id_download).each do |k, v|
            title_ja = get_count_map(:id_title_ja)[k] || ''
            if title_ja.blank?
                @data['title_ja_download']["該当なし : " + k.to_s] = v
            else
                @data['title_ja_download'][title_ja] = v
            end
            title_en = get_count_map(:id_title_en)[k] || ''
            if title_en.blank?
                @data['title_en_download']["not found : " + k.to_s] = v
            else
                @data['title_en_download'][title_en] = v
            end
        end
    end

    def show
        return if total == 0
        p "".rjust(100, '#')
        p "".rjust(100, '#')
        p ('###  ' + @file.to_s + '  total ' + total.to_s + '  ').ljust(100, '#')
        p "".rjust(100, '#')
        p "".rjust(100, '#')

        DATA_TYPES.each do |data_type|
            p data_type.to_s.rjust(100, '-')
            @data[data_type.to_s].sort_by{|k,v| v }.reverse.each do |name, value|
                p name.to_s + ' : ' + value.to_s
            end
        end
    end

    def sort
        DATA_TYPES.each do |data_type|
            @data[data_type.to_s] = Hash[@data[data_type.to_s].sort_by{|k,v| v }.reverse]
        end
        self
    end

    def set_other
        DATA_TYPES.each do |data_type|
            border_value = @data[data_type.to_s].values[50]
            next if border_value.blank?
            next if data_type.to_s == 'id_title_ja'
            next if data_type.to_s == 'id_title_en'

            new_hash = {}
            @data[data_type.to_s].each do |k, v|
                if v <= border_value
                    new_hash['other'] ||= 0
                    new_hash['other'] += v
                else
                    new_hash[k] = v
                end
            end
            @data[data_type.to_s] = new_hash
        end
    end

    def total
        @data['total']
    end

end