#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Logs::PersonLogSpliter
    include LogFileAttach

    def split_by_person(month)
        Person.all.each do |person|
            person_log = Logs::LogCountData.new

            document_ids = PubFieldRepositoryPersonDatum.where(person_id: person.id).uniq.pluck(:document_id)
            document_ids.each do |document_id|
                document_log = Logs::LogCountData.new.load_document_log(document_id, month)
                person_log.merge(document_log)
            end

            person_log.write(person_log_file(person.id, month))
        end
    end

end

