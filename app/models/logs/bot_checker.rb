#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Logs::BotChecker

    def initialize(bot_names, bot_addrs, bot_addr_ranges)
        @bot_names = bot_names
        @bot_addrs = bot_addrs
        @bot_addr_ranges = bot_addr_ranges
    end

    def bot_names?(string)
        return true if string == '-'
        @bot_names.each do |bot_word|
            return true if string.downcase.include? bot_word.downcase
        end
        return false
    end


    def bot_addr?(string)
        both_bot = true
        string.split(',').each do |addr_string|
            addr_string = addr_string.strip
            next if addr_string.blank?

            if @bot_addrs.include?(addr_string)
                both_bot &&= true
                next
            end

            begin
                src_ip = IPAddr.new(addr_string)
                both_bot &&= in_addr_range?(src_ip)
            rescue Exception => e
                both_bot &&= false
            end

        end

        return both_bot
    end

private

    def in_addr_range?(src_ip)
        @bot_addr_ranges.each do |bot_addr|
            if bot_addr.include? src_ip
                return true
            end
        end
        return false
    end
end

