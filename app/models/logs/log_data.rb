#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Logs::LogData

    DATA_TYPES = [
        :remote_host,
        :client_ident,
        :auth_user_name,
        :timestamp,
        :month,
        :method,
        :url,
        :http,
        :status_code,
        :round_bytes,
        :bytes,
        :referer,
        :referer_domain,
        :search_query,
        :search_words,
        :user_agent,
        :bot,
        :public,
        :access_log,
        :download_log,
        :id
    ]

    def initialize
        @data = { 'bot' => 'no', 'access_log' => 'no', 'download_log' => 'no', 'search_words' => [] }
    end

    def set_value(data_type, value)
        if DATA_TYPES.include?(data_type) == false
            p 'invalid log data type : ' + data_type.to_s
            return
        end
        @data[data_type.to_s] = value
    end

    def add_value(data_type, value)
        if DATA_TYPES.include?(data_type) == false || @data[data_type.to_s].class.to_s != 'Array'
            p 'invalid log data type : ' + data_type.to_s
            return
        end

        @data[data_type.to_s] << value
    end

    def get_value(data_type)
        @data[data_type.to_s]
    end

end
