#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class CoverpageSetting
    include ActiveModel::Model

    FILE_PATH = 'lib/coverpage/coverpage.xsl'

    require 'nokogiri'

    attr_writer :cover_page_xml_string

    validate  :check_cover_page_xml_string
    define_model_callbacks :save

    before_save :validate_self

    def validate_self
        self.valid?
    end

    def save
        run_callbacks :save do
            File.open(FILE_PATH, 'wb') do |file|
                file.write(cover_page_xml_string)
            end
        end
    end

    def cover_page_xml_string
        if @cover_page_xml_string == nil
            if File.exists?(FILE_PATH) == false
                FileUtils.cp File.expand_path(FILE_PATH + '.sample'), File.expand_path(FILE_PATH)
            end
            @cover_page_xml_string = File.read(FILE_PATH)
        end

        @cover_page_xml_string
    end

private

    def check_cover_page_xml_string
        return true if cover_page_xml_string.blank?
        begin
            Nokogiri::XSLT.parse(cover_page_xml_string)
        rescue => ex
            errors.add(:cover_page_xml_string, "不正なXSLTです : "  + ex.message)
            return false
        end
    end

end
