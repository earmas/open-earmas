#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class PriListDatum < ActiveRecord::Base

    belongs_to :document, class_name: :PriDocument, foreign_key: 'document_id'
    belongs_to :list_def,  -> { includes(:list_fields) }

    after_destroy :remove_list_word

    def blank_all?
        level1.blank? && level2.blank? && level3.blank? && level4.blank? && level5.blank?
    end

private

    def remove_list_word
        5.times do |index|
            index_level = index + 1

            list_word_ids = send('list_word_level' + index_level.to_s + '_ids')

            ('PriListLevel' + index_level.to_s + 'Word').constantize.where(id: list_word_ids).each do |list_word|
                list_word.document_count -= 1
                if list_word.document_count <= 0
                    list_word.destroy!
                else
                    list_word.save!
                end
            end
        end
    end

end
