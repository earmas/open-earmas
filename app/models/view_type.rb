#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module ViewType

    module ListParam
        VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'デフォルトビュー', en: 'default view' }, file_name: 'default' },
            2 => { view_id: 2, caption: { ja: '件数表示無しビュー', en: 'not count view' }, file_name: 'no_count' },
            3 => { view_id: 3, caption: { ja: '頭文字ビュー', en: 'prefix view' }, file_name: 'prefix' },
            4 => { view_id: 4, caption: { ja: '頭文字件数表示無しビュー', en: 'prefix not count view' }, file_name: 'prefix_no_count' },
            5 => { view_id: 5, caption: { ja: '検索付きビュー', en: 'search view' }, file_name: 'search' },
        }
        SUB_HEADER_VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'デフォルトビュー', en: 'default view' }, file_name: 'default' },
            2 => { view_id: 2, caption: { ja: 'なし', en: 'none' }, file_name: 'no' },
        }
        SUB_FOOTER_VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'なし', en: 'none' }, file_name: 'no' },
        }
    end

    module Result
        VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'デフォルトビュー', en: 'default view' }, file_name: 'default', result_all: false },
            2 => { view_id: 2, caption: { ja: 'テーブルビュー', en: 'table view' }, file_name: 'table', result_all: false },
            3 => { view_id: 3, caption: { ja: 'ナビビュー', en: 'nav view' }, file_name: 'nav', result_all: true },
            4 => { view_id: 4, caption: { ja: 'ニュースビュー', en: 'news view' }, file_name: 'news', result_all: false },
            5 => { view_id: 5, caption: { ja: 'ボックスビュー', en: 'box view' }, file_name: 'box', result_all: false },
            6 => { view_id: 6, caption: { ja: 'ピックアップビュー', en: 'pickup view' }, file_name: 'pickup', result_all: false },
        }
        SUB_HEADER_VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'デフォルトビュー', en: 'default view' }, file_name: 'default' },
            2 => { view_id: 2, caption: { ja: 'なし', en: 'none' }, file_name: 'no' },
            3 => { view_id: 3, caption: { ja: '画像文書付', en: 'image attach' }, file_name: 'image_attach' },
            4 => { view_id: 4, caption: { ja: '画像文書付(最初のパラメタ)', en: 'image attach (first param)' }, file_name: 'image_attach_first' },
            5 => { view_id: 5, caption: { ja: '画像文書付(最後のパラメタ)', en: 'image attach (last param)' }, file_name: 'image_attach_last' },
        }
        SUB_FOOTER_VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'なし', en: 'none' }, file_name: 'no' },
        }
    end

    module Detail
        VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'デフォルトビュー', en: 'default view' }, file_name: 'default' },
            2 => { view_id: 2, caption: { ja: 'テーブルビュー', en: 'table view' }, file_name: 'table' },
            3 => { view_id: 3, caption: { ja: 'ナビビュー', en: 'nav view' }, file_name: 'nav' },
            4 => { view_id: 4, caption: { ja: 'ニュースビュー', en: 'news view' }, file_name: 'news' },
            5 => { view_id: 5, caption: { ja: 'ワンブロックビュー', en: 'one block view' }, file_name: 'one_block' },
            6 => { view_id: 6, caption: { ja: 'コンテンツビュー', en: 'contents view' }, file_name: 'contents' },
        }
        SUB_HEADER_VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'デフォルトビュー', en: 'default view' }, file_name: 'default' },
            2 => { view_id: 2, caption: { ja: 'なし', en: 'none' }, file_name: 'no' },
            3 => { view_id: 3, caption: { ja: 'サムネイルなし', en: 'no thumnail' }, file_name: 'no_thumnail' },
            4 => { view_id: 4, caption: { ja: '画像文書付', en: 'image attach' }, file_name: 'image_attach' },
            5 => { view_id: 5, caption: { ja: '画像文書付(最初のパラメタ)', en: 'image attach (first param)' }, file_name: 'image_attach_first' },
            6 => { view_id: 6, caption: { ja: '画像文書付(最後のパラメタ)', en: 'image attach (last param)' }, file_name: 'image_attach_last' },
            7 => { view_id: 7, caption: { ja: 'ナビビュー', en: 'nav view' }, file_name: 'nav' },
            8 => { view_id: 8, caption: { ja: 'ニュースビュー', en: 'news view' }, file_name: 'news' },
        }
        SUB_FOOTER_VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'なし', en: 'none' }, file_name: 'no' },
            2 => { view_id: 2, caption: { ja: 'facebookコメント', en: 'facebook comment' }, file_name: 'facebook_comment' },
        }
    end

    module Journal
        VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'デフォルトビュー', en: 'default view' }, file_name: 'default' },
            2 => { view_id: 2, caption: { ja: 'サムネイルなしビュー', en: 'no thumnail view' }, file_name: 'no_thumnail' },
            3 => { view_id: 3, caption: { ja: 'amoビュー', en: 'amo view' }, file_name: 'amo' },
            4 => { view_id: 4, caption: { ja: 'ouビュー', en: 'ou view' }, file_name: 'ou' },
        }
        SUB_HEADER_VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'デフォルトビュー', en: 'default view' }, file_name: 'default' },
        }
        SUB_FOOTER_VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'なし', en: 'none' }, file_name: 'no' },
            2 => { view_id: 2, caption: { ja: 'facebookコメント', en: 'facebook comment' }, file_name: 'facebook_comment' },
        }
    end

    module Article
        VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'デフォルトビュー', en: 'default view' }, file_name: 'default' },
            2 => { view_id: 2, caption: { ja: 'amoビュー', en: 'amo view' }, file_name: 'amo' },
            3 => { view_id: 3, caption: { ja: 'ouビュー', en: 'ou view' }, file_name: 'ou' },
        }
        SUB_HEADER_VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'デフォルトビュー', en: 'default view' }, file_name: 'default' },
        }
        SUB_FOOTER_VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'なし', en: 'none' }, file_name: 'no' },
            2 => { view_id: 2, caption: { ja: 'facebookコメント', en: 'facebook comment' }, file_name: 'facebook_comment' },
        }
    end

    module JournalAllIndex
        VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'デフォルトビュー', en: 'default view' }, file_name: 'default' },
            2 => { view_id: 2, caption: { ja: 'amoビュー', en: 'amo view' }, file_name: 'amo' },
        }
        SUB_HEADER_VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'デフォルトビュー', en: 'default view' }, file_name: 'default' },
        }
        SUB_FOOTER_VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'なし', en: 'none' }, file_name: 'no' },
        }
    end

    module JournalStatic
        VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'デフォルトビュー', en: 'default view' }, file_name: 'default' },
            2 => { view_id: 2, caption: { ja: 'amoビュー', en: 'amo view' }, file_name: 'amo' },
        }
        SUB_HEADER_VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'デフォルトビュー', en: 'default view' }, file_name: 'default' },
        }
        SUB_FOOTER_VIEW_MAP = {
            1 => { view_id: 1, caption: { ja: 'なし', en: 'none' }, file_name: 'no' },
        }
    end

end
