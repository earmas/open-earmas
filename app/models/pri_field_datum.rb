#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class PriFieldDatum < ActiveRecord::Base
    include FieldDatumCommon

	self.abstract_class = true

    belongs_to :document, class_name: :PriDocument, foreign_key: 'document_id'

    belongs_to :field_def

    attr_accessor :deleted

    def get_oaipmh_set_value
        get_field_type.get_oaipmh_set_value(self)
    end

    def must?(document)
        if document.get_input_group == nil
            get_field_def.must
        else
            input_group_field = document.get_input_group_field(field_def_id)
            input_group_field != nil && input_group_field.must
        end
    end

    def fields_validation_before(document)
        get_field_type.fields_validation_before(self, document)
    end

    def fields_validation_after(document)
        get_field_type.fields_validation_after(self, document)
    end

    def get_search_document_ids(limit)
        get_field_type.get_search_document_ids(self, limit)
    end

    def get_search_document_relation(relation)
        get_field_type.get_search_document_relation(relation, self)
    end

    def from_csv_field(csv_string)
        return if csv_string == nil

        get_field_type.from_csv_field(self, csv_string)
    end

    def to_public_field
        get_field_type.to_public_field(self)
    end

    def get_exist_values(relation)
        get_field_type.get_exist_values(self, relation)
    end

    def get_all_exist_values
        get_field_type.get_all_exist_values(self)
    end

    def new_count_field(value) 
        get_field_type.new_count_field(value)
    end

    def update_values(relation, search_field)
        get_field_type.update_values(self, relation, search_field)
    end

    def update_field_values(relation, new_field)
        get_field_type.update_field_values(self, relation, new_field)
    end

    def get_field_def
        FieldDefLoader.get_field_def(field_def_id)
    end

    def get_field_type
        get_field_def.field_type_class::Pri
    end

    def saved_message
        nil
    end

end
