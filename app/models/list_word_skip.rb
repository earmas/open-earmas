#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class ListWordSkip

    attr_accessor :document_count

    def initialize(document_count = 0)
        @document_count = document_count
    end

    def value
        ListPage::SKIP_VALUE
    end
    def sort_value_ja
        Earmas::SORT_MAX
    end
    def sort_value_en
        Earmas::SORT_MAX
    end
    def caption_ja
        '値なし'
    end
    def caption_en
        'empty value'
    end

end
