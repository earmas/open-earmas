#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class PubFieldFileDatum < PubFieldDatum
    include FileHolder, SimpleFile

    self.primary_key = 'id'

    attr_accessor :private_stored_dir_name
    attr_accessor :private_file_path
    attr_accessor :private_thumnail_file_path
    attr_accessor :private_text_file_path

    before_save :save_upload_file
    after_destroy :delete_upload_file

    def get_new_stored_dir_path(dir_name)
        if document.limited || self.publish_type == PUBLISH_LTD
            if Rails.application.config.try(:use_prefix_dir)
                number_data_dir = (document_id / 10000).to_s
                file_dir = File.join(Rails.application.config.public_file_store_dir, "limited", number_data_dir, document_id.to_s, dir_name)
            else
                file_dir = File.join(Rails.application.config.public_file_store_dir, "limited", document_id.to_s, dir_name)
            end
        else
            if Rails.application.config.try(:use_prefix_dir)
                number_data_dir = (document_id / 10000).to_s
                file_dir = File.join(Rails.application.config.public_file_store_dir, "public", number_data_dir, document_id.to_s, dir_name)
            else
                file_dir = File.join(Rails.application.config.public_file_store_dir, "public", document_id.to_s, dir_name)
            end
        end
        FileUtils.mkdir_p(file_dir)
        file_dir
    end


    def save_upload_file
        new_stored_dir_path = get_new_stored_dir_path(private_stored_dir_name)
        return if stored_dir_path == new_stored_dir_path

        if stored_dir_path.present?
            delete_upload_file
        end

        self.stored_dir_path = new_stored_dir_path

        if private_file_path.present?
            FileUtils.cp private_file_path, file_path
        end
        if private_thumnail_file_path.present?
            FileUtils.cp private_thumnail_file_path, thumnail_path
        end
        if private_text_file_path.present?
            FileUtils.cp private_text_file_path, text_file_path
        end
    end
end
