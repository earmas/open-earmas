#--
# Open Earmas
# 
# Copyright (C) 2014 Hiroshima University, ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class PubEjWord < ActiveRecord::Base

    paginates_per 100

    def get_data_relation
        if limited
            PubEjDatum.where(ej_def_id: ej_def_id, journal: journal, volume: volume, issue: issue, publish_date: publish_date)
        else
            PubEjDatum.where(limited: false, ej_def_id: ej_def_id, journal: journal, volume: volume, issue: issue, publish_date: publish_date)
        end
    end

    def toc_index
        [volume, issue].join(' - ')
    end
end
