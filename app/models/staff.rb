#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Staff < ActiveRecord::Base
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    # devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable, :lockable

    default_scope { order(:id) }

    validate  :check_ej_editor_role

    ROLE_BOOL_VALUE = ['管理', '一般']

    ROLE_ADMIN = 9
    ROLE_STAFF = 1
    ROLE_EJ_EDITOR = 2

    def admin?
        role == ROLE_ADMIN
    end

    def ej_editor?
        role == ROLE_EJ_EDITOR
    end

    def editable_ej_journal_def
        EjJournalDef.find_by(id: ej_journal_def_id)
    end

    def role_caption
        case role
        when ROLE_ADMIN
            '管理者'
        when ROLE_STAFF
            '一般ユーザ'
        when ROLE_EJ_EDITOR
            'EJ編集者'
        end
    end

private

    def check_ej_editor_role
        if role == ROLE_EJ_EDITOR && editable_ej_journal_def == nil
            errors.add(:ej_journal_def_id, "を指定してください。")
        end
    end

end
