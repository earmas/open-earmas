#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class OaipmhFormat < ActiveRecord::Base
    require 'nokogiri'

    OAI_DC_FILE_PATH = 'lib/default_files/oaipmh/oai_dc.xml'
    JUNII2_FILE_PATH = 'lib/default_files/oaipmh/junii2.xml'

    validate :xslt_is_valid


    def xslt_is_valid
        begin
            Nokogiri::XSLT.parse(xml_string)
        rescue => ex
            errors.add(:xml_string, "不正なXSLTです : " + ex.message)
            return false
        end
    end

end
