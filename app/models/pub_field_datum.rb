#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class PubFieldDatum < ActiveRecord::Base
    include FieldDatumCommon

    self.abstract_class = true

    belongs_to :document, class_name: :PubDocument, foreign_key: 'document_id'

    belongs_to :field_def

    def get_field_def
        FieldDefLoader.get_field_def(field_def_id)
    end

    def get_field_type
        get_field_def.field_type_class::Pub
    end

end
