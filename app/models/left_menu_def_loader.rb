#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class LeftMenuDefLoader

    class ThreadValue
        attr_accessor :menus, :menus_id_type_map, :menus_id_map, :menus_parent_map, :menus_root, :list_defs

        def init
            return if @menus != nil
            @menus = []
            @menus_id_type_map = {}
            @menus_id_map = {}
            @menus_parent_map = {}
            @menus_root = []

            LeftMenuDef.all.order(:parent_menu, :sort_index).each do |menu_def|
                @menus << menu_def
                @menus_id_map[menu_def.id] = menu_def

                if menu_def.parent_menu.blank?
                    @menus_root << menu_def
                else
                    @menus_parent_map[menu_def.parent_menu] ||= []
                    @menus_parent_map[menu_def.parent_menu] << menu_def
                end

                @menus_id_type_map[menu_def.menu_type] ||= {}
                @menus_id_type_map[menu_def.menu_type][menu_def.menu_id] ||= []
                @menus_id_type_map[menu_def.menu_type][menu_def.menu_id] << menu_def
            end

            list_def_ids = []
            static_page_def_ids = []

            @menus.each do |menu|
                case menu.menu_type
                when LeftMenuDef::TYPE_LIST_DEF
                    list_def_ids << menu.menu_id
                when LeftMenuDef::TYPE_STATIC_PAGE_DEF
                    static_page_def_ids << menu.menu_id
                end
            end

            @list_defs = { LeftMenuDef::TYPE_LIST_DEF => {}, LeftMenuDef::TYPE_STATIC_PAGE_DEF => {} }
            ListDef.where(id: list_def_ids).each do |list_def|
                @list_defs[LeftMenuDef::TYPE_LIST_DEF][list_def.id] = list_def
            end
            StaticPageDef.where(id: static_page_def_ids).each do |static_page_def|
                @list_defs[LeftMenuDef::TYPE_STATIC_PAGE_DEF][static_page_def.id] = static_page_def
            end
        end
        @menus
    end


    class << self
        def thread_cache
            Thread.current[:earmas_left_menu] ||= ThreadValue.new
        end

        def get_menu_by_parent(parent_menu)
            thread_cache.init
            if parent_menu.blank?
                thread_cache.menus_root
            else
                thread_cache.menus_parent_map[parent_menu] || []
            end
        end

        def get_menu_by_type_id(menu_type, menu_id)
            thread_cache.init
            thread_cache.menus_id_type_map[menu_type].try(:[], menu_id) || []
        end

        def get_menu_by_id(id)
            thread_cache.init
            thread_cache.menus_id_map[id]
        end

        def get_list_def(id)
            thread_cache.init
            thread_cache.list_defs[LeftMenuDef::TYPE_LIST_DEF].try(:[], id.to_i)
        end

        def get_static_page_def(id)
            thread_cache.init
            thread_cache.list_defs[LeftMenuDef::TYPE_STATIC_PAGE_DEF].try(:[], id.to_i)
        end

    end


end
