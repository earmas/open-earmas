#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class OaipmhSetting < ActiveRecord::Base

    default_scope { order(:id) }

    validates :set_spec_default_value, presence: true
    validate :check_self_doi_setting

    belongs_to :title_field_def,       class_name: :FieldDef, foreign_key: :title_field_def_id
    belongs_to :creator_field_def,     class_name: :FieldDef, foreign_key: :creator_field_def_id
    belongs_to :publisher_field_def,   class_name: :FieldDef, foreign_key: :publisher_field_def_id
    belongs_to :issn_field_def,        class_name: :FieldDef, foreign_key: :issn_field_def_id
    belongs_to :isbn_field_def,        class_name: :FieldDef, foreign_key: :isbn_field_def_id
    belongs_to :jtitle_field_def,      class_name: :FieldDef, foreign_key: :jtitle_field_def_id
    belongs_to :start_page_field_def,  class_name: :FieldDef, foreign_key: :start_page_field_def_id
    belongs_to :nii_type_field_def,    class_name: :FieldDef, foreign_key: :nii_type_field_def_id



    def self.create_default
        oaipmh_setting = OaipmhSetting.new
        oaipmh_setting.repository_name = 'テストデータベース'
        oaipmh_setting.base_url = 'http://test.test'
        oaipmh_setting.admin_email = 'test@example.jp'
        oaipmh_setting.identifier_prefix = 'oai:enut.jp:sample/'

        oaipmh_setting.set_spec_custom_select_id = nil
        oaipmh_setting.set_spec_default_value = 'all'

        oaipmh_setting.save!

        if File.exists?(OaipmhFormat::OAI_DC_FILE_PATH)
            oaipmh_format = OaipmhFormat.new
            oaipmh_format.metadata_prefix = 'oai_dc'
            oaipmh_format.schema = 'http://www.openarchives.org/OAI/2.0/oai_dc.xsd'
            oaipmh_format.metadata_namespace = 'http://www.openarchives.org/OAI/2.0/oai_dc/'
            oaipmh_format.xml_string = File.open(OaipmhFormat::OAI_DC_FILE_PATH).read
            oaipmh_format.save!
        end

        if File.exists?(OaipmhFormat::JUNII2_FILE_PATH)
            oaipmh_format = OaipmhFormat.new
            oaipmh_format.metadata_prefix = 'junii2'
            oaipmh_format.schema = 'http://irdb.nii.ac.jp/oai/junii2.xsd'
            oaipmh_format.metadata_namespace = 'http://irdb.nii.ac.jp/oai'
            oaipmh_format.xml_string = File.open(OaipmhFormat::JUNII2_FILE_PATH).read
            oaipmh_format.save!
        end
    end

    def enable_self_doi
        enable_self_jalc_doi || enable_self_crossref_doi
    end

private

    def check_self_doi_setting
        self.errors.add_on_empty(:self_doi_jalc_prefix) if self.self_doi_jalc_prefix.blank?           && self.enable_self_jalc_doi
        self.errors.add_on_empty(:self_doi_crossref_prefix) if self.self_doi_crossref_prefix.blank?   && self.enable_self_crossref_doi

        self.errors.add_on_empty(:title_field_def_id) if self.title_field_def_id.blank?           && (self.enable_self_jalc_doi || self.enable_self_crossref_doi)
        self.errors.add_on_empty(:creator_field_def_id) if self.creator_field_def_id.blank?       && (self.enable_self_jalc_doi || self.enable_self_crossref_doi)
        self.errors.add_on_empty(:publisher_field_def_id) if self.publisher_field_def_id.blank?   && (self.enable_self_crossref_doi)
        self.errors.add_on_empty(:issn_field_def_id) if self.issn_field_def_id.blank?             && (self.enable_self_crossref_doi)
        self.errors.add_on_empty(:isbn_field_def_id) if self.isbn_field_def_id.blank?             && (self.enable_self_crossref_doi)
        self.errors.add_on_empty(:jtitle_field_def_id) if self.jtitle_field_def_id.blank?         && (self.enable_self_crossref_doi)
        self.errors.add_on_empty(:start_page_field_def_id) if self.start_page_field_def_id.blank? && (self.enable_self_jalc_doi || self.enable_self_crossref_doi)
        self.errors.add_on_empty(:nii_type_field_def_id) if self.nii_type_field_def_id.blank?     && (self.enable_self_jalc_doi || self.enable_self_crossref_doi)
    end
end
