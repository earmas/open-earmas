#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class PriFieldFileDatum < PriFieldDatum
    include FileHolder, SimpleFile

    attr_accessor :upload_file, :csv_upload_file

    validates :publish_type, :inclusion => {:in => [1, 2, 3, 4]}

    before_create :save_upload_file
    after_destroy :delete_upload_file

    def stored_dir_path
        if Rails.application.config.try(:use_prefix_dir)
            number_data_dir = (document_id / 10000).to_s
            File.join(Rails.application.config.private_file_store_dir, "private", number_data_dir, document_id.to_s, stored_dir_name)
        else
            File.join(Rails.application.config.private_file_store_dir, "private", document_id.to_s, stored_dir_name)
        end
    end

    def save_upload_file
        return if stored_dir_name.present?
        raise DataCheckException, 'ファイルがありません。' if upload_file.blank? && csv_upload_file.blank?

        save_form_upload_file if upload_file.present?
        save_csv_upload_file if csv_upload_file.present?
    end

    def get_csv_uploaded_file_path(document_id = nil)
        file_dir = File.join(Rails.application.config.private_file_store_dir, "static", "uploaded")
        FileUtils.mkdir_p(file_dir)

        file = File.join(file_dir, csv_upload_file)

        # 更新時の処理

        if document_id.present? && File.exists?(file) == false
            PriFieldFileDatum.where(document_id: document_id).each do |exist_field|
                if File.basename(csv_upload_file) == exist_field.org_file_name
                    self.mime = exist_field.mime
                    self.org_file_name = exist_field.org_file_name
                    return exist_field.file_path
                end
            end
        else
            file
        end
    end

    def saved_message
        @saved_message
    end

private

    def get_new_stored_file_path(dir_name, original_filename)
        if Rails.application.config.try(:use_prefix_dir)
            number_data_dir = (document_id / 10000).to_s
            file_dir = File.join(Rails.application.config.private_file_store_dir, "private", number_data_dir, document_id.to_s, dir_name)
        else
            file_dir = File.join(Rails.application.config.private_file_store_dir, "private", document_id.to_s, dir_name)
        end

        FileUtils.mkdir_p(file_dir)
        File.join(file_dir, original_filename)
    end

    def save_form_upload_file
        self.org_file_name = upload_file.original_filename # must set first

        dir_name = Time.now.in_time_zone.strftime("%Y%m%d%H%M%S") + Time.now.in_time_zone.usec.to_s

        new_stored_file_path = get_new_stored_file_path(dir_name, org_file_name)

        if upload_file.size > Rails.application.config.upload_file_size_max.megabyte
            raise DataCheckException, 'ファイルのサイズは最大で' + Rails.application.config.upload_file_size_max.to_s + 'MBです。'
        end

        File.open(new_stored_file_path, "wb") do |file|
            file.write(upload_file.read)
        end

        self.stored_dir_name = dir_name
        self.size = File.size(new_stored_file_path)
        self.mime = get_mime_type(org_file_name).to_s

        @saved_message = []
        @saved_message << 'サムネイルの作成に失敗しました。' if create_thumnail == false
        @saved_message << '全文の抽出に失敗しました。' if create_text == false
    end

    def save_csv_upload_file
        self.org_file_name = File.basename(csv_upload_file) if self.org_file_name.blank?
        mime_type = get_mime_type(csv_upload_file)
        mime_type = self.mime if self.mime.present?

        dir_name = Time.now.in_time_zone.strftime("%Y%m%d%H%M%S") + Time.now.in_time_zone.usec.to_s

        new_stored_file_path = get_new_stored_file_path(dir_name, org_file_name)

        upload_file =  File.new(get_csv_uploaded_file_path(document_id), "r")

        if upload_file.size > Rails.application.config.upload_file_size_max.megabyte.megabyte
            raise DataCheckException, 'ファイルのサイズは最大で' + Rails.application.config.upload_file_size_max.to_s + 'MBです。'
        end

        File.open(new_stored_file_path, "wb") do |file|
            file.write(upload_file.read)
        end

        self.stored_dir_name = dir_name
        self.size = File.size(new_stored_file_path)
        self.mime = mime_type.to_s


        @saved_message = []
        @saved_message << 'サムネイルの作成に失敗しました。' if create_thumnail == false
        @saved_message << '全文の抽出に失敗しました。' if create_text == false
    end

end
