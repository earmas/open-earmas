#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Delayed::DelayTask
    include BatchHelper, SystemSettingHelper, PublishHelper

    def clear_cache(staff)
        begin
            Delayed::Worker.logger.debug("Start Claer Cache")

            Rails.cache.clear

            Delayed::Worker.logger.debug("End Clear Cache")
        rescue => ex
            Delayed::Worker.logger.error ex.message
            ex.backtrace.each{|a| Delayed::Worker.logger.error a }
        ensure
            unset_data_lock
        end
    end

    def get_object_stats
        return_value = Hash.new
        ObjectSpace::each_object(Object) do |my_object|
            unless return_value[my_object.class.to_s.downcase].nil?
                return_value[my_object.class.to_s.downcase][:count] += 1
            else
                return_value[my_object.class.to_s.downcase] = Hash.new
                return_value[my_object.class.to_s.downcase][:name] = my_object.class
                return_value[my_object.class.to_s.downcase][:count] = 1
            end
        end
        return_value.sort_by {|k,v| -v[:count]}
    end

    def show_memory
        @mem_check_index ||= 0
        @mem_check_index += 1
        return if @mem_check_index % 1000 != 0
        Delayed::Worker.logger.debug "============================================="
        get_object_stats.take(20).each do |stat|
           Delayed::Worker.logger.debug "#{stat.at(1)[:count]}                 #{stat.at(1)[:name]}"
        end
        Delayed::Worker.logger.debug "============================================="
    end

    def send_authers_mail(staff)
        load 'lib/tasks/log/send_download_count_mail.rb'
    end

    def update_people_setting(staff)
        begin
            people_setting = PeopleSetting.all.first
            Delayed::Worker.logger.debug("Start Update People Setting job")

            FieldDefLoader.clear

            Person.all.find_each(batch_size: 100) do |person|
                person.encrypted_id = people_setting.encrypt_data(person.id)
                person.save!
            end

            Delayed::Worker.logger.debug("End Update People Setting job")
        rescue => ex
            Delayed::Worker.logger.error ex.message
            ex.backtrace.each{|a| Delayed::Worker.logger.error a }
        ensure
            unset_data_lock
        end
    end

    def check_sel_doi(staff)
        begin
            Delayed::Worker.logger.debug("Start Check SelfDOI job")

            FieldDefLoader.clear

            oaipmh_setting = OaipmhSetting.all.first
            PriDocument.where(self_doi_checked: false).order('pri_documents.id ASC').find_each(batch_size: 100) do |document|
                if document.check_self_doi(oaipmh_setting)
                    document.update_column(:self_doi_checked, true)
                end
            end

            Delayed::Worker.logger.debug("End Check SelfDOI job")
        rescue => ex
            Delayed::Worker.logger.error ex.message
            ex.backtrace.each{|a| Delayed::Worker.logger.error a }
        ensure
            unset_data_lock
        end
    end

    def publish(staff, params)
        begin
            Delayed::Worker.logger.debug("Start Publish job")

            FieldDefLoader.clear

            parse_publish_param(params)

            find_by_publish_document_query.find_each(batch_size: 100) do |document|
                document.publish(staff, false)
                show_memory
            end

            Sunspot.commit

            Delayed::Worker.logger.debug("End Publish job")
        rescue => ex
            Delayed::Worker.logger.error ex.message
            ex.backtrace.each{|a| Delayed::Worker.logger.error a }
        ensure
            unset_data_lock
        end
    end

    def field_value_update(staff, params)
        begin
            Delayed::Worker.logger.debug("Start Field Value Update job")
            FieldDefLoader.clear

            parse_field_value_update_search_param(params)
            parse_field_value_update_new_value_param(params)

            if @active_search_field_map.size == 0
                Delayed::Worker.logger.debug("End Field Value Update job. no proc")
                return
            end

            if @active_search_field_map.size > 1
                Delayed::Worker.logger.debug("End Field Value Update job. no proc")
                return
            end

            @active_search_field_map.first.update_field_values(PriDocument.all, @new_field)

            Delayed::Worker.logger.debug("End Field Value Update job")
        rescue => ex
            Delayed::Worker.logger.error ex.message
            ex.backtrace.each{|a| Delayed::Worker.logger.error a }
        ensure
            unset_data_lock
        end
    end


    def update_values(staff, params)
        begin
            Delayed::Worker.logger.debug("Start Batch job")
            FieldDefLoader.clear

            parse_search_param(params)
            parse_new_value_param(params)

            query_document_ids = get_query_document_ids(nil)

            if query_document_ids.blank?
                Delayed::Worker.logger.debug("End Batch job. no proc")
                return
            end

            query_document_ids.each_slice(100) do |sub_query_document_ids|
                update_value(sub_query_document_ids)
            end

            Delayed::Worker.logger.debug("End Batch job")
        rescue => ex
            Delayed::Worker.logger.error ex.message
            ex.backtrace.each{|a| Delayed::Worker.logger.error a }
        ensure
            unset_data_lock
        end
    end

    def update_value(query_document_ids)
        relation = find_by_document_query(query_document_ids)

        if @new_input_group_id.present?
            PriDocument.unscoped.where(deleted: false).where(id: relation).update_all(input_group_id: @new_input_group_id)
            @search_input_group_id = @new_input_group_id if @search_input_group_id.present?
        end

        if @new_publish_to.present?
            PriDocument.unscoped.where(deleted: false).where(id: relation).update_all(publish_to: @new_publish_to)
            @search_publish_to = @new_publish_to if @search_publish_to.present?
        end

        if @new_self_doi_type.present?
            PriDocument.unscoped.where(deleted: false).where(id: relation).update_all(self_doi_type: @new_self_doi_type, self_doi_checked: false)
            @search_self_doi_type = @new_self_doi_type if @search_self_doi_type.present?
        end

        if @new_updater_staff_id.present?
            PriDocument.unscoped.where(deleted: false).where(id: relation).update_all(updater_staff_id: @new_updater_staff_id)
            @search_updater_staff_id = @new_updater_staff_id if @search_updater_staff_id.present?
        end

        if @new_creator_staff_id.present?
            PriDocument.unscoped.where(deleted: false).where(id: relation).update_all(creator_staff_id: @new_creator_staff_id)
            @search_creator_staff_id = @new_creator_staff_id if @new_creator_staff_id.present?
        end

        @new_field_map.values.each do |field|
            next if field.blank_value?
            field.update_values(relation, @search_field_map[field.field_def_id])
        end

        PriDocument.unscoped.where(deleted: false).where(id: relation).update_all(updated_at: PriDocument.get_now)
    end

    def csv_download(staff, target, col_sep, file_name, encoding)
        require 'csv'

        if File.exists?(file_name + '.tmp')
            return
        end
        FieldDefLoader.clear

        File.open(file_name + '.tmp', "w", encoding: encoding) do |csv|
            if target == 'creator_staff'
                PriDocument.where(creator_staff_id: staff.id).find_each(batch_size: 100) do |document|
                    csv << CSV.generate_line(document.to_csv, col_sep: col_sep, row_sep: "\r\n").encode(encoding, :invalid => :replace, :undef => :replace, :replace => '?')
                end
            elsif target == 'updater_staff'
                PriDocument.where(updater_staff_id: staff.id).find_each(batch_size: 100) do |document|
                    csv << CSV.generate_line(document.to_csv, col_sep: col_sep, row_sep: "\r\n").encode(encoding, :invalid => :replace, :undef => :replace, :replace => '?')
                end
            else
                PriDocument.find_each(batch_size: 100) do |document|
                    csv << CSV.generate_line(document.to_csv, col_sep: col_sep, row_sep: "\r\n").encode(encoding, :invalid => :replace, :undef => :replace, :replace => '?')
                end
            end
        end

        FileUtils.mv file_name + '.tmp', file_name
    end

    def csv_update(staff, csv_content, col_sep, auto_thumnail)
        require 'csv'

        begin
            FieldDefLoader.clear

            Delayed::Worker.logger.debug("Start CSV Update job")

            @documents = []
            CSV.parse(csv_content, col_sep: col_sep, row_sep: "\r\n") do |line|
                id = line[0]

                document = PriDocument.where(id: id).first
                if document == nil
                    document = PriDocument.new
                end

                document.updater_staff_id = staff.id
                document.creator_staff_id = staff.id

                document.from_csv(line)
                if document.fields_validation_before(document.csv_fields) == false
                    document.errors.full_messages.each do |msg|
                        Delayed::Worker.logger.error msg
                    end
                    next
                end

                @documents << document
                show_memory
            end

            ActiveRecord::Base.transaction do
                @documents.each do |document|
                    document.check_oaipmh(document.csv_fields)

                    if document.changed? || document.new_record?
                        document.save!
                    else
                        document.touch
                    end

                    exists_fields = document.reget_all_fields

                    thumnail_field = nil
                    document.csv_fields.each do |csv_new_field|
                        csv_new_field.document_id = document.id
                        csv_new_field.save!

                        thumnail_field = csv_new_field if (thumnail_field == nil && csv_new_field.field_has_thumnail?)
                    end

                    document.fields_validation_after(document.csv_fields)
                    document.set_thumnail(thumnail_field) if thumnail_field != nil && auto_thumnail == true

                    exists_fields.each do |exists_field|
                        exists_field.destroy!
                    end

                    document.set_search_data(document.csv_fields)
                    document.set_list_data(document.csv_fields)
                    document.set_ej_data(document.csv_fields)
                    document.index

                    show_memory
                end
            end
            Delayed::Worker.logger.debug("End CSV Update job")
        rescue => ex
            Delayed::Worker.logger.error ex.message
            ex.backtrace.each{|a| Delayed::Worker.logger.error a }
        ensure
            unset_data_lock
        end
    end

    def pub_reindex(staff)
        system_setting = SystemSetting.all.first

        locked_list_ids = get_pub_list_ids(system_setting)
        locked_ej_ids = get_pub_ej_ids(system_setting)

        begin
            FieldDefLoader.clear

            if system_setting.pub_sort_locking?
                PubListDatum.delete_all
                PubListLevel1Word.delete_all
                PubListLevel2Word.delete_all
                PubListLevel3Word.delete_all
                PubListLevel4Word.delete_all
                PubListLevel5Word.delete_all

                PubEjDatum.delete_all
                PubEjWord.delete_all
            else
                if locked_list_ids.present?
                    PubListDatum.where(list_def_id: locked_list_ids).delete_all
                    PubListLevel1Word.where(list_def_id: locked_list_ids).delete_all
                    PubListLevel2Word.where(list_def_id: locked_list_ids).delete_all
                    PubListLevel3Word.where(list_def_id: locked_list_ids).delete_all
                    PubListLevel4Word.where(list_def_id: locked_list_ids).delete_all
                    PubListLevel5Word.where(list_def_id: locked_list_ids).delete_all
                end

                if locked_ej_ids.present?
                    PubEjDatum.where(ej_def_id: locked_ej_ids).delete_all if locked_ej_ids.present?
                    PubEjWord.where(ej_def_id: locked_ej_ids).delete_all if locked_ej_ids.present?
                end
            end

            PubDocument.find_each(batch_size: 100) do |document|
                if system_setting.pub_oaipmh_locking?
                    document.set_publish_date
                    document.save!
                end

                all_field = document.reget_all_fields
                if system_setting.pub_sort_locking?
                    document.set_list_data(all_field)
                    document.set_ej_data(all_field)
                    document.set_search_data(all_field)
                    document.index
                else
                    document.set_list_data(all_field, locked_list_ids) if locked_list_ids.present?
                    document.set_ej_data(all_field, locked_ej_ids) if locked_ej_ids.present?

                    if system_setting.pub_search_locking?
                        document.set_search_data(all_field)
                        document.index
                    end
                end
            end

            Sunspot.commit

            ActiveRecord::Base.transaction do
                unlock_sort
                unlock_ej
                unlock_list
                unlock_search
                unlock_oaipmh
            end
        rescue => ex
            Delayed::Worker.logger.error ex.message
            ex.backtrace.each{|a| Delayed::Worker.logger.error a }
        ensure
            unset_data_lock
        end
    end

    def pri_reindex(staff)
        system_setting = SystemSetting.all.first

        locked_pri_list_ids = get_pri_list_ids(system_setting)
        locked_pri_ej_ids = get_pri_ej_ids(system_setting)

        begin
            FieldDefLoader.clear

            if system_setting.pri_sort_locking?
                PriListDatum.delete_all
                PriListLevel1Word.delete_all
                PriListLevel2Word.delete_all
                PriListLevel3Word.delete_all
                PriListLevel4Word.delete_all
                PriListLevel5Word.delete_all

                PriEjDatum.delete_all
                PriEjWord.delete_all
            else
                if locked_pri_list_ids.present?
                    PriListDatum.where(list_def_id: locked_pri_list_ids).delete_all
                    PriListLevel1Word.where(list_def_id: locked_pri_list_ids).delete_all
                    PriListLevel2Word.where(list_def_id: locked_pri_list_ids).delete_all
                    PriListLevel3Word.where(list_def_id: locked_pri_list_ids).delete_all
                    PriListLevel4Word.where(list_def_id: locked_pri_list_ids).delete_all
                    PriListLevel5Word.where(list_def_id: locked_pri_list_ids).delete_all
                end

                if locked_pri_ej_ids.present?
                    PriEjDatum.where(ej_def_id: locked_pri_ej_ids).delete_all
                    PriEjWord.where(ej_def_id: locked_pri_ej_ids).delete_all
                end
            end

            PriDocument.find_each(batch_size: 100) do |document|
                all_field = document.reget_all_fields

                if system_setting.pri_oaipmh_locking?
                    document.check_oaipmh(all_field)
                    document.save!
                end

                if system_setting.pri_sort_locking?
                    document.set_list_data(all_field)
                    document.set_ej_data(all_field)
                    document.set_search_data(all_field)

                    document.index
                else
                    document.set_list_data(all_field, locked_pri_list_ids) if locked_pri_list_ids.present?
                    document.set_ej_data(all_field, locked_pri_ej_ids) if locked_pri_ej_ids.present?

                    if system_setting.pri_search_locking?
                        document.set_search_data(all_field)
                        document.index
                    end
                end
            end
            Sunspot.commit

            ActiveRecord::Base.transaction do
                unlock_pri_sort
                unlock_pri_list
                unlock_pri_ej
                unlock_pri_search
                unlock_pri_oaipmh
            end
        rescue => ex
            Delayed::Worker.logger.error ex.message
            ex.backtrace.each{|a| Delayed::Worker.logger.error a }
        ensure
            unset_data_lock
        end
    end

    def get_pub_list_ids(system_setting)
        locked_list_ids = system_setting.locked_list_ids
        locked_list_ids.uniq! if locked_list_ids.present?
        locked_list_ids
    end

    def get_pub_ej_ids(system_setting)
        locked_ej_ids = system_setting.locked_ej_ids
        locked_ej_ids.uniq! if locked_ej_ids.present?
        locked_ej_ids
    end

    def get_pri_list_ids(system_setting)
        locked_pri_list_ids = system_setting.locked_pri_list_ids
        locked_pri_list_ids.uniq! if locked_pri_list_ids.present?
        locked_pri_list_ids
    end

    def get_pri_ej_ids(system_setting)
        locked_pri_ej_ids = system_setting.locked_pri_ej_ids
        locked_pri_ej_ids.uniq! if locked_pri_ej_ids.present?
        locked_pri_ej_ids
    end



    def pri_reindex_all(staff)
        begin
            PriDocument.find_each(batch_size: 100) do |document|
                all_field = document.reget_all_fields
                document.set_list_data(all_field)
                document.set_ej_data(all_field)
                document.set_search_data(all_field)
                document.index

                document.check_oaipmh(all_field)
                document.changed_change_self_doi_checked?(all_field)
                document.save!
            end
            Sunspot.commit
        rescue => ex
            Delayed::Worker.logger.error ex.message
            ex.backtrace.each{|a| Delayed::Worker.logger.error a }
        ensure
            unset_data_lock
        end
    end

end
