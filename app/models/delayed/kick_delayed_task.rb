#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class Delayed::KickDelayedTask

    def initialize(staff)
        @staff = staff
    end

    def clear_cache
        Delayed::DelayTask.new.clear_cache(@staff)
    end

    def send_authers_mail
        Delayed::DelayTask.new.send_authers_mail(@staff)
    end

    def update_people_setting
        Delayed::DelayTask.new.update_people_setting(@staff)
    end

    def check_sel_doi
        Delayed::DelayTask.new.check_sel_doi(@staff)
    end

    def publish(params)
        Delayed::DelayTask.new.publish(@staff, params)
    end

    def update_values(params)
        Delayed::DelayTask.new.update_values(@staff, params)
    end

    def field_value_update(params)
        Delayed::DelayTask.new.field_value_update(@staff, params)
    end

    def csv_download(target, col_sep, file_name, encoding)
        Delayed::DelayTask.new.csv_download(@staff, target, col_sep, file_name, encoding)
    end

    def csv_update(csv_content, col_sep, auto_thumnail)
        Delayed::DelayTask.new.csv_update(@staff, csv_content, col_sep, auto_thumnail)
    end

    def pub_reindex
        Delayed::DelayTask.new.pub_reindex(@staff)
    end

    def pri_reindex
        Delayed::DelayTask.new.pri_reindex(@staff)
    end

    def pri_reindex_all
        Delayed::DelayTask.new.pri_reindex_all(@staff)
    end

end