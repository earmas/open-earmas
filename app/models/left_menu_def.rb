#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class LeftMenuDef < ActiveRecord::Base

    default_scope { order(:parent_menu, :sort_index, :id) }

	TYPE_LIST_DEF        = 1
    TYPE_STATIC_PAGE_DEF = 2
    TYPE_BLANK           = 3
    TYPE_LABEL           = 4
    TYPE_LINK           = 5

    def has_child
        menu_type == TYPE_LIST_DEF || menu_type == TYPE_STATIC_PAGE_DEF
    end

    def clear_optional_param
        self.menu_type = nil
        self.menu_id = nil
    end

end
