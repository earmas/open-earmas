#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class HtmlSetDef < ActiveRecord::Base

    default_scope { includes(:header_parts,:footer_parts,:right_side_parts).order(:id) }

    belongs_to :header_parts,       class_name: 'HtmlPartsDef', :foreign_key => :header_parts_id
    belongs_to :footer_parts,       class_name: 'HtmlPartsDef', :foreign_key => :footer_parts_id
    belongs_to :right_side_parts,   class_name: 'HtmlPartsDef', :foreign_key => :right_side_parts_id

end
