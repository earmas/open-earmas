#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class FieldDef < ActiveRecord::Base

    default_scope { order(:sort_index) }

    @@field_type_map = nil

    has_many :template_fields, class_name: :DocumentTemplateField, dependent: :delete_all

    validate  :check_field_param
    validates :code, uniqueness: true, format: { with: /\A[A-Za-z][A-Za-z0-9_]+\z/, message: "は半角英数字で入力してください。" }
    validates :caption_ja, presence: true
    validates :caption_en, presence: true


    YES_NO_BOOL_VALUE = ['○', '×']

    def self.set_all_field_model
        @@field_type_map = {}
        Rails.application.config.field_type_models.each do |field_class_name|
            field_class = ('FieldTypes::' + field_class_name.camelize).constantize
            @@field_type_map[field_class::TYPE_ID] = field_class
        end
    end

    def self.get_field_type_map
        if @@field_type_map.nil? && Rails.application.config.field_type_models.present?
            set_all_field_model
        end
        @@field_type_map
    end

    def descriptions
        @descriptions ||= description.split(',', -1)

        @descriptions
    end

    def clear_optional_param
        self.default_value = nil
        self.in_field_delim = nil

        self.field_param_id1 = nil
        self.field_param_id2 = nil
        self.field_param_id3 = nil
        self.field_param_text1 = nil
        self.field_param_text2 = nil
        self.field_param_text3 = nil

        self.sort_conv_pattern = nil
        self.sort_conv_format = nil
    end

    def field_type_class
        self.class.get_field_type_map[field_type_id]
    end

    def get_pri_view_dir
        get_pri_type_class.get_view_dir
    end

    def get_pub_view_dir
        get_pub_type_class.get_view_dir
    end

    def empty_pri_field
        field = get_pri_type_class.empty_field(self)
        field.field_def_id = id
        field
    end

    def default_pri_field
        field = get_pri_type_class.default_field(self)
        field.field_def_id = id
        field
    end

    def new_pri_field(param)
        field = get_pri_type_class.new_field(param)
        field.field_def_id = id
        field
    end

    def update_pri_field(field, param)
        get_pri_type_class.update_field(field, param)
    end

    def get_pri_type_class
        field_type_class::Pri
    end

    def get_pub_type_class
        field_type_class::Pub
    end

    def get_type_class_by_view_state
        if ViewState.pri?
            get_pri_type_class
        else
            get_pub_type_class
        end
    end

    def set_unit_ja(value)
        return '' if value.blank?
        result = ''
        result << unit_prefix_ja.to_s if unit_prefix_ja.present?
        result << value.to_s
        result << unit_suffix_ja.to_s  if unit_suffix_ja.present?
        result
    end
    def set_unit_en(value)
        return '' if value.blank?
        result = ''
        result << unit_prefix_en.to_s if unit_prefix_en.present?
        result << value.to_s
        result << unit_suffix_en.to_s if unit_suffix_en.present?
        result
    end
    def set_unit_by_locale(value)
        if I18n.locale == :ja
            set_unit_ja(value)
        else
            set_unit_en(value)
        end
    end

    def get_sort_conv_pattern_regex
        @sort_conv_pattern_regex ||= Regexp.new(sort_conv_pattern)
    end

    def create_sort_value(value)
        return value if sort_conv_pattern.blank? && sort_conv_format.blank?

        regex_for_sort = sort_conv_pattern.present? && get_sort_conv_pattern_regex || nil

        value_for_sort = value
        if regex_for_sort != nil
            match = regex_for_sort.match(value_for_sort)
            if match != nil
                if sort_conv_format.present?
                    converted_value = match.to_a.drop(1).map{|a| a == nil ? '' : a }.map{|a| convert_format(a) }.join('')
                    value_for_sort = converted_value.present? && converted_value || value_for_sort
                else
                    converted_value = match.to_a.drop(1).map{|a| a == nil ? '' : a }.join('')
                    value_for_sort = converted_value.present? && converted_value || value_for_sort
                end
            end
        else
            if sort_conv_format.present?
                converted_value = convert_format(value_for_sort)
                value_for_sort = converted_value.present? && converted_value || value_for_sort
            end
        end

        value_for_sort
    end

    def convert_format(value)
        begin
            sprintf(sort_conv_format, value)
        rescue Exception => ex
            Rails.logger.error ex.message
            return value
        end
    end

private

    def check_field_param
        field_type_class.check_field_param(self)
    end

end
