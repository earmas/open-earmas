#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class SystemSetting < ActiveRecord::Base
    include ViewTypeHolder

    require 'nokogiri'

    default_scope { order(:id) }

    alias_attribute :menu_type, :top_page_type
    alias_attribute :menu_id, :top_page_id

    validates :top_page_type, format: { with: /\A[12]+\z/, message: "は不正な選択肢です。" }, allow_nil: true
    validate  :check_top_page_value
    validate  :check_meta_info_xml_string
    validate  :check_upload_xml_string
    validate  :check_search_api_xml_string

    belongs_to :detail_html_set_def,  class_name: :HtmlSetDef, :foreign_key => :detail_html_set_def_id
    accepts_nested_attributes_for :detail_html_set_def

    OAI_PMH_BOOL_VALUE = ['有効', '無効']
    META_INFO_FILE_PATH = 'lib/default_files/meta_info/meta_info.xml'

    def self.create_default
        system_setting = SystemSetting.new
        system_setting.enable_oaipmh = false
        system_setting.sytem_name_ja = 'テストデータベース'
        system_setting.sytem_name_en = 'Test Database'

        system_setting.detail_view_type_id = 1
        system_setting.detail_sub_header_view_type_id = 1
        system_setting.detail_sub_footer_view_type_id = 1

        system_setting.locked_list_ids = []
        system_setting.locked_ej_ids = []

        system_setting.save!
        system_setting
    end

    def theme_class
        return nil if theme_id.blank?
        ThemeHolder.get_theme_map[theme_id]
    end

    def clear_optional_param
        self.top_page_type = nil
        self.top_page_id = nil
    end

    def set_default_child
        detail_html_set_def.present? || build_detail_html_set_def
        detail_html_set_def.header_parts || detail_html_set_def.build_header_parts
        detail_html_set_def.footer_parts || detail_html_set_def.build_footer_parts
        detail_html_set_def.right_side_parts || detail_html_set_def.build_right_side_parts
    end

    def set_default_value
        self.meta_info_xml_string = File.open(META_INFO_FILE_PATH).read if self.meta_info_xml_string.blank?
    end

    def html_set_def
        detail_html_set_def
    end


    def get_id_conv_pattern_regex
        @id_conv_pattern_regex ||= Regexp.new(id_conv_pattern)
    end

    def public_id_to_document_id(value)
        return value if id_conv_pattern.blank?

        regex_for_id = get_id_conv_pattern_regex || nil

        id_value = value
        if regex_for_id != nil
            match = regex_for_id.match(id_value)
            if match != nil
                id_value = match.to_a.drop(1).map{|a| a == nil ? '' : a }.join('')
            end
        end
        
        id_value
    end

    def document_id_to_public_id(value)
        return value.to_s if id_conv_format.blank?

        begin
            sprintf(id_conv_format, value)
        rescue Exception => ex
            logger.error ex.message
            return value.to_s
        end
    end

    def pub_locking?
        field_locked_at != nil ||
        list_locked_at != nil ||
        ej_locked_at != nil ||
        search_locked_at != nil || 
        oaipmh_locked_at != nil
    end

    def pri_locking?
        pri_field_locked_at != nil ||
        pri_list_locked_at != nil ||
        pri_ej_locked_at != nil ||
        pri_search_locked_at != nil || 
        pri_oaipmh_locked_at != nil
    end

    def pub_sort_locking?
        field_locked_at.present?
    end
    def pub_list_locking?
        list_locked_at.present?
    end
    def pub_ej_locking?
        ej_locked_at.present?
    end
    def pub_search_locking?
        search_locked_at.present?
    end
    def pub_oaipmh_locking?
        oaipmh_locked_at.present?
    end

    def pri_sort_locking?
        pri_field_locked_at.present?
    end
    def pri_list_locking?
        pri_list_locked_at.present?
    end
    def pri_ej_locking?
        pri_ej_locked_at.present?
    end
    def pri_search_locking?
        pri_search_locked_at.present?
    end
    def pri_oaipmh_locking?
        pri_oaipmh_locked_at.present?
    end

private

    def check_meta_info_xml_string
        return true if meta_info_xml_string.blank?
        begin
            Nokogiri::XSLT.parse(meta_info_xml_string)
        rescue => ex
            errors.add(:meta_info_xml_string, "不正なXSLTです : "  + ex.message)
            return false
        end
    end

    def check_upload_xml_string
        return true if upload_xml_string.blank?
        begin
            Nokogiri::XSLT.parse(upload_xml_string)
        rescue => ex
            errors.add(:upload_xml_string, "不正なXSLTです : "  + ex.message)
            return false
        end
    end

    def check_search_api_xml_string
        return true if search_api_xml_string.blank?
        begin
            Nokogiri::XSLT.parse(search_api_xml_string)
        rescue => ex
            errors.add(:search_api_xml_string, "不正なXSLTです : "  + ex.message)
            return false
        end
    end

    def check_top_page_value
        if top_page_type.present? && top_page_id.blank?
            errors.add(:top_page_id, "対象を選択してください")
        end
    end

end
