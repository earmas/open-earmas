#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class SearchDef < ActiveRecord::Base

    default_scope { order(:sort_index) }

    has_many :fields, class_name: :SearchFieldDef, dependent: :destroy

    validate  :duplicate_code
    validates :code, uniqueness: true, format: { with: /\A[A-Za-z][A-Za-z0-9_]+\z/, message: "は半角英数字で入力してください。" }
    validates :caption_ja, presence: true
    validates :caption_en, presence: true

private

    def duplicate_code
        if code == 'all'
            errors.add(:code, "allは使用できません")
        end 
    end
    
end
