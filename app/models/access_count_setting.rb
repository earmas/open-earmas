#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class AccessCountSetting < ActiveRecord::Base

    BOT_FILTER_BOOL_VALUE = ['有効', '無効']

    validate  :check_bot_address

    def self.create_default(url)
        access_count_setting = AccessCountSetting.new
        access_count_setting.my_url = url
        access_count_setting.filter_bot = true
        access_count_setting.bot_names = [
            "spider",
            "bot",
            "crawler",
            "slurp",
            "Fetcher",
            "froute.jp",
            "BaiduMobaider",
            "Ask Jeeves",
            "ichiro",
            "ndl-japan-warp",
            "Preview",
            "Yandex",
            "reader",
            "Manager",
            "telnet",
        ].join("\n")

        access_count_setting.bot_address = '127.0.0.1/32'

        access_count_setting.save!
        access_count_setting
    end

    def create_bot_checker
        bot_names_array = []
        bot_names.split("\n").each do |bot_name|
            next if bot_name.strip.blank?
            bot_names_array << bot_name.strip
        end

        bot_addrs_array = []
        bot_addr_ranges_array = []
        bot_address.split("\n").each do |bot_addr|
            next if bot_addr.strip.blank?

            striped_bot_addr = bot_addr.strip
            if striped_bot_addr.include?('/')
                bot_addr_ranges_array << IPAddr.new(striped_bot_addr)
            else
                bot_addrs_array << striped_bot_addr
            end
        end

        Logs::BotChecker.new(bot_names_array, bot_addrs_array, bot_addr_ranges_array)
    end

private

    def check_bot_address
        return true if bot_address.blank?
        begin
            bot_address.split("\n").each do |bot_addr|
                next if bot_addr.strip.blank?
                IPAddr.new(bot_addr.strip)
            end
        rescue => ex
            errors.add(:bot_address, "不正なアドレス指定があります。 : "  + ex.message)
            return false
        end
    end

end
