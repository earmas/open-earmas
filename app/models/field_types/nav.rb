#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FieldTypes::Nav
    include FieldTypes::FieldTypeBase

    TYPE_ID = 6

    def caption_ja
        'ナビゲーション'
    end

    def caption_en
        'navigation'
    end

    def check_field_param(field_def)
        if field_def.in_field_delim.blank?
            field_def.errors.add(:in_field_delim, "CSVでの項目区切り文字は必須です")
        end
        if field_def.field_param_id1.blank?
            field_def.errors.add(:field_param_id1, "選択肢は必須です")
        end
        if field_def.field_param_id2.present? \
        || field_def.field_param_id3.present? \
        || field_def.field_param_text1.present? \
        || field_def.field_param_text2.present? \
        || field_def.field_param_text3.present? \
        || field_def.check_format.present? \
        || field_def.sort_conv_pattern.present? \
        || field_def.sort_conv_format.present?
            field_def.errors.add(:base, "不要なパラメタがあります。")
        end
    end

    def lock_field_update?(field_def)
        false
    end

    def file_holder?
        false
    end

    def no_batch?
        false
    end

    def get_select_id(field_def)
        field_def.field_param_id1
    end

    # get_select_id が trueになる場合のみ
    def get_select_value_exists(field_def, custom_select_value)
        get_select_value_relation(field_def, custom_select_value).count != 0
    end

    # get_select_id が trueになる場合のみ
    def get_select_value_relation(field_def, custom_select_value)
        Pri::data_class.where(field_def_id: field_def.id, some_id: custom_select_value.id)
    end

    NO_VALUE = "値なし"

    extend(self)
    def self.included(base)
        base.extend(self)
    end

    module Common

        def get_key_value(datum)
            datum.send(get_key_name)
        end

        def get_sort_key_value(datum)
            custom_select_value = datum.get_master_value
            return Earmas::SORT_MAX if custom_select_value == nil

            add_layer_suffix(Earmas.int32_to_s(custom_select_value.sort_index), datum)
        end

        def add_layer_suffix(value, datum)
            value + Earmas.int32_to_s(datum.value1) + Earmas.int32_to_s(datum.value2) + Earmas.int32_to_s(datum.value3) + Earmas.int32_to_s(datum.value4) + Earmas.int32_to_s(datum.value5)
        end

        def nil_value?(datum)
            get_key_value(datum).nil? && datum.value1.nil? && datum.value2.nil? && datum.value3.nil? && datum.value4.nil? && datum.value5.nil?
        end

        def to_filter_key_value(datum)
            custom_select_value = datum.get_master_value

            custom_select_value.present? && custom_select_value.value || ''
        end

        def get_prev_next_nav_field(datum)
            prev_field = nil
            hit = false
            next_field = nil

            data_class.where(get_key_name => get_key_value(datum)).order(get_key_name, :value1, :value2, :value3, :value4, :value5).each do |field|
                if field.id == datum.id
                    hit = true
                    next
                end
                if hit
                    next_field = field
                    break
                else
                    prev_field = field
                end
            end
            { prev_field: prev_field, next_field: next_field }
        end

        def get_upper_nav_field(datum)
            parent_level = datum.nav_level
            parent_level.pop

            data_class.where(get_key_name => get_key_value(datum))
                .where(value1: parent_level[1] || 0)
                .where(value2: parent_level[2] || 0)
                .where(value3: parent_level[3] || 0)
                .where(value4: parent_level[4] || 0)
                .where(value5: parent_level[5] || 0).first
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

    module Pri
        include FieldTypes::FieldTypeBase::Pri
        include Common

        def get_key_name
            'some_id'
        end

        def get_master_value(datum)
            return nil if datum.some_id.blank?
            CustomSelectValue.get_value(datum.some_id)
        end

        def get_sort_value_ja(datum)
            custom_select_value = datum.get_master_value
            return Earmas::SORT_MAX if custom_select_value == nil

            add_layer_suffix(to_full_sort_number(datum.get_list_caption_ja), datum)
        end

        def get_sort_value_en(datum)
            custom_select_value = datum.get_master_value
            return Earmas::SORT_MAX if custom_select_value == nil

            add_layer_suffix(to_full_sort_number(datum.get_list_caption_en), datum)
        end

        def get_list_caption_ja(datum)
            custom_select_value = datum.get_master_value

            if custom_select_value == nil
                "#{NO_VALUE}(#{datum.some_id})"
            else
                caption = custom_select_value.caption_ja
                caption = custom_select_value.caption_en if caption.blank?
                caption = [caption, datum.value1, datum.value2, datum.value3, datum.value4, datum.value5].reject{|a|a.blank?}.join('-')
                caption
            end
        end
        def get_list_caption_en(datum)
            custom_select_value = datum.get_master_value

            if custom_select_value == nil
                "#{NO_VALUE}(#{datum.some_id})"
            else
                caption = custom_select_value.caption_en
                caption = custom_select_value.caption_ja if caption.blank?
                caption = [caption, datum.value1, datum.value2, datum.value3, datum.value4, datum.value5].reject{|a|a.blank?}.join('-')
                caption
            end
        end

        def get_list_param(datum)
            custom_select_value = datum.get_master_value

            if custom_select_value == nil
                ["#{NO_VALUE}(#{datum.some_id})", datum.value1, datum.value2, datum.value3, datum.value4, datum.value5].join('-')
            else
                [custom_select_value.value, datum.value1, datum.value2, datum.value3, datum.value4, datum.value5].join('-')
            end
        end

        def get_list_param_prefix_ja(datum)
            get_list_param(datum)[0] || ''
        end

        alias_method :get_list_param_prefix_en, :get_list_param_prefix_ja

        def get_search_values(datum)
            custom_select_value = datum.get_master_value
            values = [datum.value1, datum.value2, datum.value3, datum.value4, datum.value5]

            if custom_select_value.present?
                values << custom_select_value.caption_ja
                values << custom_select_value.caption_en
                values << custom_select_value.value
            end
            values.reject{|a| a == 0 }.reject{|a|a.blank?}.uniq
        end

        def get_oaipmh_set_value(datum)
            custom_select_value = datum.get_master_value
            custom_select_value && custom_select_value.value || nil
        end

        def blank_value?(datum)
            (datum.some_id.blank? || datum.some_id == 0) &&
            (datum.value1.blank? || datum.value1 == 0) &&
            (datum.value2.blank? || datum.value2 == 0) &&
            (datum.value3.blank? || datum.value3 == 0) &&
            (datum.value4.blank? || datum.value4 == 0) &&
            (datum.value5.blank? || datum.value5 == 0)
        end

        def fields_validation_before(datum, document)
            if blank_value?(datum) == false && datum.some_id.blank?
                datum.errors.add(:some_id, "名前は必須です")
            end

            if blank_value?(datum) == false && datum.value1 == 0
                datum.errors.add(:value1, "1層目は必須です")
            end

            if datum.must?(document) && blank_value?(datum)
                document.errors.add(:base, "必須項目が未入力です")
                datum.errors.add(:some_id, "名前は必須です")
                datum.errors.add(:value1, "1層目は必須です")
            end

            datum.errors.blank?
        end

        def fields_validation_after(datum, document)
        end

        def new_field(param)
            data_class.new(param.permit(:some_id, :value1, :value2, :value3, :value4, :value5, :deleted))
        end

        def update_field(datum, param)
            datum.assign_attributes(param.permit(:some_id, :value1, :value2, :value3, :value4, :value5, :deleted))
        end

        def to_csv_field(datum)
            custom_select_value = datum.get_master_value
            value = custom_select_value && custom_select_value.value || "#{NO_VALUE}(#{datum.some_id})"

            delim = datum.get_field_def.in_field_delim
            [value, datum.value1, datum.value2, datum.value3, datum.value4, datum.value5].join(delim)
        end

        def from_csv_field(datum, csv_string)
            delim = datum.get_field_def.in_field_delim
            csv_values = csv_string.split(delim, -1)

            custom_select_id = datum.get_field_def.field_param_id1

            custom_select_value = CustomSelectValue.where(custom_select_id: custom_select_id, value: csv_values[0]).first
            if custom_select_value.present?
                datum.some_id = custom_select_value.id # some_idが無いとvalidationで引っかかる
            end

            datum.value1 = csv_values[1] || ''
            datum.value2 = csv_values[2] || ''
            datum.value3 = csv_values[3] || ''
            datum.value4 = csv_values[4] || ''
            datum.value5 = csv_values[5] || ''
        end

        alias_method :to_filter_value, :to_csv_field

        def to_public_field(datum)
            pub_field = Pub.data_class.where(id: datum.id).first
            if pub_field.blank?
                pub_field = Pub.data_class.new
            end

            custom_select_value = datum.get_master_value

            pub_field.value         = custom_select_value && custom_select_value.value || "#{NO_VALUE}"
            pub_field.caption_ja    = custom_select_value && custom_select_value.caption_ja || "#{NO_VALUE}"
            pub_field.caption_en    = custom_select_value && custom_select_value.caption_en || "#{NO_VALUE}"
            pub_field.value1        = datum.value1
            pub_field.value2        = datum.value2
            pub_field.value3        = datum.value3
            pub_field.value4        = datum.value4
            pub_field.value5        = datum.value5
            pub_field
        end

        def get_search_document_ids(datum, limit)
            return nil if blank_value?(datum)

            relation = data_class.where(field_def_id: datum.field_def_id)

            relation = relation.where(some_id: datum.some_id) if (datum.some_id.present? && datum.some_id != 0)
            relation = relation.where(value1:  datum.value1)  if (datum.value1.present? && datum.value1 != 0)
            relation = relation.where(value2:  datum.value2)  if (datum.value2.present? && datum.value2 != 0)
            relation = relation.where(value3:  datum.value3)  if (datum.value3.present? && datum.value3 != 0)
            relation = relation.where(value4:  datum.value4)  if (datum.value4.present? && datum.value4 != 0)
            relation = relation.where(value5:  datum.value5)  if (datum.value5.present? && datum.value5 != 0)

            relation.limit(limit).pluck(:document_id)
        end

        def get_search_document_relation(relation, datum)
            relation = relation.joins(:doc_field5)

            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.some_id = ?)", datum.field_def_id, datum.some_id) if datum.some_id.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.value1 = ?)", datum.field_def_id, datum.value1) if datum.value1.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.value2 = ?)", datum.field_def_id, datum.value2) if datum.value2.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.value3 = ?)", datum.field_def_id, datum.value3) if datum.value3.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.value4 = ?)", datum.field_def_id, datum.value4) if datum.value4.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.value5 = ?)", datum.field_def_id, datum.value5) if datum.value5.present?

            relation
        end

        def get_search_field_relation(relation, datum)
            relation = relation.where("(field_def_id = ? AND some_id = ?)", datum.field_def_id, datum.some_id) if datum.some_id.present?
            relation = relation.where("(field_def_id = ? AND value1 = ?)", datum.field_def_id, datum.value1) if datum.value1.present?
            relation = relation.where("(field_def_id = ? AND value2 = ?)", datum.field_def_id, datum.value2) if datum.value2.present?
            relation = relation.where("(field_def_id = ? AND value3 = ?)", datum.field_def_id, datum.value3) if datum.value3.present?
            relation = relation.where("(field_def_id = ? AND value4 = ?)", datum.field_def_id, datum.value4) if datum.value4.present?
            relation = relation.where("(field_def_id = ? AND value5 = ?)", datum.field_def_id, datum.value5) if datum.value5.present?
            relation
        end

        def get_value_exists_document_ids(field_def, limit)
            return data_class.where(field_def_id: field_def.id)
                .where.not(name: nil).order(:name, :value1, :value2, :value3, :value4, :value5)
                .limit(limit).pluck(:document_id)
        end

        def get_exist_values(datum, relation)
            return [] if blank_value?(datum)

            data_class.where(document_id: relation, field_def_id: datum.field_def_id).group(:some_id, :value1, :value2, :value3, :value4, :value5).count
        end

        def get_all_exist_values(datum)
            return [] if blank_value?(datum)

            relation = data_class.joins(:document).where('pri_documents.deleted = false')

            get_search_field_relation(relation, datum).group(:some_id, :value1, :value2, :value3, :value4, :value5).count
        end

        # get_exist_values と 対応
        def new_count_field(value)
            field = data_class.new
            field.some_id = value[0]
            field.value1  = value[1]
            field.value2  = value[2]
            field.value3  = value[3]
            field.value4  = value[4]
            field.value5  = value[5]
            field
        end

        def update_values(datum, relation, search_field)
            update_value = {}

            update_value[:some_id] = datum.some_id if (datum.some_id.present? && datum.some_id != 0)
            update_value[:value1]  = datum.value1  if (datum.value1.present? && datum.value1 != 0)
            update_value[:value2]  = datum.value2  if (datum.value2.present? && datum.value2 != 0)
            update_value[:value3]  = datum.value3  if (datum.value3.present? && datum.value3 != 0)
            update_value[:value4]  = datum.value4  if (datum.value4.present? && datum.value4 != 0)
            update_value[:value5]  = datum.value5  if (datum.value5.present? && datum.value5 != 0)

            data_class.where(document_id: relation, field_def_id: datum.field_def_id)
                .update_all(update_value)
        end

        def update_field_values(datum, relation, new_field)
            update_value = {}

            relation = get_search_field_relation(data_class.includes(:document), datum)

            if new_field.some_id.present?
                update_value[:some_id] = new_field.some_id
                update_value[:value1] = new_field.value1
                update_value[:value2] = new_field.value2
                update_value[:value3] = new_field.value3
                update_value[:value4] = new_field.value4
                update_value[:value5] = new_field.value5

                update_field_values_and_update_document(relation, update_value)
            else
                destory_field_values_and_update_document(relation, update_value)
            end
        end

        def data_class
            'PriFieldNavDatum'.constantize
        end

        def get_view_dir
            'pri_field_nav'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end


    module Pub
        include FieldTypes::FieldTypeBase::Pub
        include Common

        def get_key_name
            'value'
        end

        def get_master_value(datum)
            return nil if datum.value.blank?
            custom_select_value = CustomSelectValue.get_value_by_custom_select_id_value(datum.get_field_def.field_param_id1, datum.value)
        end

        def get_sort_value_ja(datum)
            add_layer_suffix(to_full_sort_number(datum.get_list_caption_ja), datum)
        end

        def get_sort_value_en(datum)
            add_layer_suffix(to_full_sort_number(datum.get_list_caption_en), datum)
        end

        def get_list_caption_ja(datum)
            caption = datum.caption_ja
            caption = datum.caption_en if caption.blank?
            caption = [caption, datum.value1, datum.value2, datum.value3, datum.value4, datum.value5].reject{|a|a.blank?}.join('-')
            caption
        end
        def get_list_caption_en(datum)
            caption = datum.caption_en
            caption = datum.caption_ja if caption.blank?
            caption = [caption, datum.value1, datum.value2, datum.value3, datum.value4, datum.value5].reject{|a|a.blank?}.join('-')
            caption
        end

        def get_list_param(datum)
            [datum.value, datum.value1, datum.value2, datum.value3, datum.value4, datum.value5].join('-')
        end

        def get_list_param_prefix_ja(datum)
            get_list_param(datum)[0] || ''
        end

        alias_method :get_list_param_prefix_en, :get_list_param_prefix_ja

        def get_search_values(datum)
            [datum.value1, datum.value2, datum.value3, datum.value4, datum.value5, datum.value, datum.caption_ja, datum.caption_en].reject{|a|a.blank?}.uniq
        end

        def blank_value?(datum)
            datum.value.blank? && datum.value1.blank? && datum.value2.blank? && datum.value3.blank? && datum.value4.blank? && datum.value5.blank?
        end

        def to_csv_field(datum)
            delim = datum.get_field_def.in_field_delim
            [datum.value, datum.value1, datum.value2, datum.value3, datum.value4, datum.value5].join(delim)
        end

        alias_method :to_filter_value, :to_csv_field

        def data_class
            'PubFieldNavDatum'.constantize
        end

        def get_view_dir
            'pub_field_nav'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

end
