#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FieldTypes::SimpleFile
    include FieldTypes::FieldTypeBase

    TYPE_ID = 4

    def caption_ja
        'シンプルファイル'
    end

    def caption_en
        'simple file'
    end

    def check_field_param(field_def)
        if field_def.in_field_delim.blank?
            field_def.errors.add(:in_field_delim, "CSVでの項目区切り文字は必須です")
        end
        if field_def.default_value.present? \
        || field_def.field_param_id1.present? \
        || field_def.field_param_id2.present? \
        || field_def.field_param_id3.present? \
        || field_def.field_param_text1.present? \
        || field_def.field_param_text2.present? \
        || field_def.field_param_text3.present? \
        || field_def.check_format.present? \
        || field_def.sort_conv_pattern.present? \
        || field_def.sort_conv_format.present?
            field_def.errors.add(:base, "不要なパラメタがあります。")
        end
    end

    def lock_field_update?(field_def)
        false
    end

    def file_holder?
        true
    end

    def no_batch?
        true
    end

    def get_select_id(field_def)
        nil
    end

    extend(self)
    def self.included(base)
        base.extend(self)
    end

    module Common

        def get_search_values(datum)
            [datum.title, datum.org_file_name]
        end

        def get_search_values_with_file(datum)
            if datum.has_text?
                [datum.title, datum.org_file_name, datum.get_text_string].reject{|a|a.blank?}.uniq
            else
                [datum.title, datum.org_file_name]
            end
        end

        def get_list_caption_ja(datum)
            if datum.title.present?
                datum.title
            else
                datum.org_file_name
            end
        end

        alias_method :get_list_caption_en, :get_list_caption_ja

        def get_list_param(datum)
            if datum.title.present?
                datum.title
            else
                datum.org_file_name
            end
        end

        def get_list_param_prefix_ja(datum)
            get_list_param(datum)[0] || ''
        end

        alias_method :get_list_param_prefix_en, :get_list_param_prefix_ja

        def to_csv_field(datum)
            delim = datum.get_field_def.in_field_delim
            [datum.org_file_name, datum.title, datum.embargo, datum.publish_type_caption_to_csv].join(delim)
        end

        alias_method :to_filter_value, :to_csv_field

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

    module Pri
        include FieldTypes::FieldTypeBase::Pri
        include Common

        def get_oaipmh_set_value(datum)
            datum.org_file_name
        end

        def get_embargo_until(datum)
            datum.embargo
        end

        def blank_value?(datum)
            datum.upload_file.blank? && datum.csv_upload_file.blank? && datum.org_file_name.blank? && datum.stored_dir_name.blank?
        end

        def nil_value?(datum)
            datum.upload_file.blank? && datum.csv_upload_file.blank? && datum.org_file_name.nil? && datum.stored_dir_name.nil?
        end

        def fields_validation_before(datum, document)
            if (datum.title.present? || datum.embargo.present?) && datum.upload_file.blank? && datum.csv_upload_file.blank? && datum.stored_dir_name.blank?
                document.errors.add(:base, "ファイルを選択して下さい")
                datum.errors.add(:upload_file, "ファイルを選択して下さい")
            end

            if datum.csv_upload_file.present? && File.exists?(datum.get_csv_uploaded_file_path(document.id)) == false
                document.errors.add(:base, "ファイルがありません")
                datum.errors.add(:upload_file, "ファイルがありません")
            end

            if datum.upload_file.present? && [FileHolder::TEXT_FILE_NAME, FileHolder::THUMNAIL_FILE_NAME].include?(datum.upload_file.original_filename)
                document.errors.add(:base, "ファイル名が不正です。ファイル名を変更してください。")
                datum.errors.add(:upload_file, "ファイル名が不正です。ファイル名を変更してください。")
            end
            if datum.csv_upload_file.present? && [FileHolder::TEXT_FILE_NAME, FileHolder::THUMNAIL_FILE_NAME].include?(File.basename(datum.csv_upload_file))
                document.errors.add(:base, "ファイル名が不正です。ファイル名を変更してください。")
                datum.errors.add(:upload_file, "ファイル名が不正です。ファイル名を変更してください。")
            end

            if datum.must?(document) && blank_value?(datum)
                document.errors.add(:base, "必須項目が未入力です")
                datum.errors.add(:upload_file, "ファイルを選択して下さい")
            end

            datum.errors.blank?
        end

        def fields_validation_after(datum, document)
        end

        def default_field(field_def)
            data_class.new
        end

        def new_field(param)
            data_class.new(param.permit(:upload_file, :title, :publish_type, :embargo, :deleted))
        end

        def update_field(datum, param)
            datum.assign_attributes(param.permit(:title, :publish_type, :embargo, :deleted))
        end

        def from_csv_field(datum, csv_string)
            delim = datum.get_field_def.in_field_delim
            csv_values = csv_string.split(delim, -1)

            datum.csv_upload_file = csv_values[0]
            datum.title = csv_values[1] || ''
            datum.embargo = csv_values[2] || ''
            datum.set_publish_type_from_csv(csv_values[3]) || PriFieldFileDatum::PUBLISH_STD
        end

        def to_public_field(datum)
            return nil if datum.publish_type == PriFieldFileDatum::PUBLISH_NONE
            return nil if datum.embargo.present? && datum.embargo > Time.zone.today

            pub_field = Pub.data_class.where(id: datum.id).first
            if pub_field.blank?
                pub_field = Pub.data_class.new
            end

            pub_field.title = datum.title
            pub_field.publish_type = datum.publish_type
            pub_field.embargo = datum.embargo

            pub_field.org_file_name = datum.org_file_name
            pub_field.mime = datum.mime
            pub_field.size = datum.size
            pub_field.private_stored_dir_name = datum.stored_dir_name
            pub_field.private_file_path = datum.file_path
            pub_field.private_thumnail_file_path = datum.thumnail_path if datum.has_thumnail?
            pub_field.private_text_file_path = datum.text_file_path if datum.has_text?
            pub_field
        end

        def data_class
            'PriFieldFileDatum'.constantize
        end

        def get_view_dir
            'pri_field_simple_file'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end


    module Pub
        include FieldTypes::FieldTypeBase::Pub
        include Common

        def blank_value?(datum)
            datum.org_file_name.blank? || datum.stored_dir_name.blank?
        end

        def nil_value?(datum)
            datum.org_file_name.nil? && datum.stored_dir_name.nil?
        end

        def data_class
            'PubFieldFileDatum'.constantize
        end

        def get_view_dir
            'pub_field_simple_file'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

end


