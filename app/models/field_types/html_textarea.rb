#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FieldTypes::HtmlTextarea
    include FieldTypes::SimpleText

    TYPE_ID = 8

    def caption_ja
        'テキストHTML'
    end

    def caption_en
        'text html'
    end

    def check_field_param(field_def)
        if field_def.in_field_delim.present? \
        || field_def.field_param_id1.present? \
        || field_def.field_param_id2.present? \
        || field_def.field_param_id3.present? \
        || field_def.field_param_text1.present? \
        || field_def.field_param_text2.present? \
        || field_def.field_param_text3.present? \
        || field_def.check_format.present? \
        || field_def.sort_conv_pattern.present? \
        || field_def.sort_conv_format.present?
            field_def.errors.add(:base, "不要なパラメタがあります。")
        end
    end

    def lock_field_update?(field_def)
        false
    end

    extend(self)
    def self.included(base)
        base.extend(self)
    end

    module Common

        def get_sort_value_ja(datum)
            datum.id
        end

        alias_method :get_sort_value_en, :get_sort_value_ja

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

    module Pri
        include FieldTypes::SimpleText::Pri
        include Common

        def get_view_dir
            'pri_field_html_textarea'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end


    module Pub
        include FieldTypes::SimpleText::Pub
        include Common

        def get_view_dir
            'pub_field_html_textarea'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

end
