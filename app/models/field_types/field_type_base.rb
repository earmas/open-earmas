#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FieldTypes::FieldTypeBase

    def file_holder?
        false
    end

    def no_batch?
        false
    end

    def no_value?
        false
    end

    def must_show_field?(field_def, document)
        false
    end

    def get_select_id(field_def)
        nil
    end

    def get_url(document, field_def, datum)
        nil
    end

    extend(self)
    def self.included(base)
        base.extend(self)
    end

    module Pri
        include LocaleHelper

        def get_master_value
            nil
        end

        def get_search_values_with_file(datum)
            get_search_values(datum)
        end

        def get_sort_value_ja(datum)
            return Earmas::SORT_MAX if datum.get_list_caption_ja.blank?
            to_sort_number(datum.get_field_def.create_sort_value(datum.get_list_caption_ja))
        end

        def get_sort_value_en(datum)
            return Earmas::SORT_MAX if datum.get_list_caption_en.blank?
            to_sort_number(datum.get_field_def.create_sort_value(datum.get_list_caption_en))
        end

        def get_sort_key_value(datum)
            ''
        end

        def to_filter_key_value(datum)
            ''
        end

        def default_field(field_def)
            datum = data_class.new
            datum.field_def_id = field_def.id
            from_csv_field(datum, field_def.default_value) if field_def.default_value.present?
            datum
        end

        def empty_field(field_def)
            datum = data_class.new
            datum.field_def_id = field_def.id
            datum
        end

        def get_embargo_until(datum)
            nil
        end

        def update_field_values_and_update_document(relation, update_value)
            relation.each do |field|
                document = field.document
                next if document.deleted

                field.update_columns(update_value)

                exists_fields = document.reget_all_fields(true)
                document.check_oaipmh(exists_fields)
                document.save!
                document.set_self_doi(OaipmhSetting.all.first, exists_fields)
            end
        end

        def destory_field_values_and_update_document(relation, update_value)
            relation.each do |field|
                document = field.document
                next if document.deleted

                field.destroy!

                exists_fields = document.reget_all_fields(true)
                document.check_oaipmh(exists_fields)
                document.save!
                document.set_self_doi(OaipmhSetting.all.first, exists_fields)
            end
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end


    module Pub
        include LocaleHelper

        def get_master_value
            nil
        end

        def get_search_values_with_file(datum)
            get_search_values(datum)
        end

        def get_sort_value_ja(datum)
            to_sort_number(datum.get_field_def.create_sort_value(datum.get_list_caption_ja))
        end

        def get_sort_value_en(datum)
            to_sort_number(datum.get_field_def.create_sort_value(datum.get_list_caption_en))
        end

        def get_sort_key_value(datum)
            ''
        end

        def to_filter_key_value(datum)
            ''
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

end
