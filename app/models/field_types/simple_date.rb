#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FieldTypes::SimpleDate
    include FieldTypes::SimpleText

    TYPE_ID = 10
    DATE_FORMAT = "%Y-%m-%d"

    def caption_ja
        'シンプル日付'
    end

    def caption_en
        'simple date'
    end

    def check_field_param(field_def)
        if field_def.in_field_delim.present? \
        || field_def.field_param_id1.present? \
        || field_def.field_param_id2.present? \
        || field_def.field_param_id3.present? \
        || field_def.field_param_text1.present? \
        || field_def.field_param_text2.present? \
        || field_def.field_param_text3.present? \
        || field_def.check_format.present? \
        || field_def.sort_conv_pattern.present? \
        || field_def.sort_conv_format.present?
            field_def.errors.add(:base, "不要なパラメタがあります。")
        end
    end

    def lock_field_update?(field_def)
        false
    end

    extend(self)
    def self.included(base)
        base.extend(self)
    end

    module Common

        def get_sort_value_ja(datum)
            datum.value1.to_i
        end

        alias_method :get_sort_value_en, :get_sort_value_ja

        def get_list_caption_ja(datum)
            datum.value1.strftime(DATE_FORMAT)
        end

        alias_method :get_list_caption_en, :get_list_caption_ja

        def get_list_param(datum)
            datum.get_list_caption_ja
        end

        def get_search_values(datum)
            [datum.value1.strftime(DATE_FORMAT)].reject{|a|a.blank?}.uniq
        end

        def blank_value?(datum)
            datum.value1.blank?
        end

        def nil_value?(datum)
            datum.value1.nil?
        end

        def to_csv_field(datum)
            return '' if blank_value?(datum)

            datum.value1.strftime(DATE_FORMAT)
        end

        alias_method :to_filter_value, :to_csv_field

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

    module Pri
        include FieldTypes::SimpleText::Pri
        include Common

        def get_oaipmh_set_value(datum)
            datum.value1.strftime(DATE_FORMAT)
        end

        def fields_validation_before(datum, document)
            if datum.must?(document) && blank_value?(datum)
                document.errors.add(:base, "必須項目が未入力です")
                datum.errors.add(:value1, "値を入力してください")
            end

            datum.errors.blank?
        end

        def fields_validation_after(datum, document)
        end

        def default_field(field_def)
            datum = data_class.new
            datum.field_def_id = field_def.id
            datum.value1 = Time.zone.now
            from_csv_field(datum, field_def.default_value) if field_def.default_value.present?
            datum
        end

        def new_field(param)
            data_class.new(param.permit(:value1, :deleted))
        end

        def update_field(datum, param)
            datum.assign_attributes(param.permit(:value1, :deleted))
        end

        def from_csv_field(datum, csv_string)
            begin
                datum.value1 = Time.zone.parse(csv_string)
            rescue => ex
                datum.value1 = nil
            end
        end

        def to_public_field(datum)
            pub_field = Pub.data_class.where(id: datum.id).first
            if pub_field.blank?
                pub_field = Pub.data_class.new
            end

            pub_field.value1 = datum.value1
            pub_field
        end

        def data_class
            'PriFieldDateDatum'.constantize
        end

        def get_view_dir
            'pri_field_simple_date'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end


    module Pub
        include FieldTypes::SimpleText::Pub
        include Common

        def data_class
            'PubFieldDateDatum'.constantize
        end

        def get_view_dir
            'pub_field_simple_date'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

end
