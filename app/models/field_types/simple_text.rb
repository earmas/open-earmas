#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FieldTypes::SimpleText
    include FieldTypes::FieldTypeBase

    TYPE_ID = 1

    SHOW_AS_LINK = 1

    def caption_ja
        'シンプルテキスト'
    end

    def caption_en
        'simple text'
    end

    def check_field_param(field_def)
        if field_def.in_field_delim.present? \
        || field_def.field_param_id2.present? \
        || field_def.field_param_id3.present? \
        || field_def.field_param_text1.present? \
        || field_def.field_param_text2.present? \
        || field_def.field_param_text3.present?
            field_def.errors.add(:base, "不要なパラメタがあります。")
        end
    end

    def lock_field_update?(field_def)
        false
    end

    def file_holder?
        false
    end

    def no_batch?
        false
    end

    def get_select_id(field_def)
        nil
    end

    def get_url(document, field_def, datum)
        if field_def.field_param_id1 == FieldTypes::SimpleText::SHOW_AS_LINK
            datum.value1
        else
            nil
        end
    end

    extend(self)
    def self.included(base)
        base.extend(self)
    end

    module Common

        def get_list_caption_ja(datum)
            datum.value1
        end

        alias_method :get_list_caption_en, :get_list_caption_ja

        def get_list_param(datum)
            datum.value1
        end

        def get_list_param_prefix_ja(datum)
            get_list_param(datum)[0] || ''
        end

        alias_method :get_list_param_prefix_en, :get_list_param_prefix_ja

        def get_search_values(datum)
            [datum.value1]
        end

        def blank_value?(datum)
            datum.value1.blank?
        end

        def nil_value?(datum)
            datum.value1.nil?
        end

        def to_csv_field(datum)
            datum.value1
        end

        alias_method :to_filter_value, :to_csv_field

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

    module Pri
        include FieldTypes::FieldTypeBase::Pri
        include Common

        def get_oaipmh_set_value(datum)
            datum.value1
        end

        def fields_validation_before(datum, document)
            if datum.must?(document) && blank_value?(datum)
                document.errors.add(:base, '必須項目が未入力です。')
                datum.errors.add(:value1, '値を入力してください。')
            end

            if blank_value?(datum) == false && datum.value1.match(datum.get_field_def.check_format) == nil
                document.errors.add(:base, '入力内容が不正です')
                datum.errors.add(:value1, datum.get_field_def.check_format + 'にマッチする値を入力してください。')
            end

            datum.errors.blank?
        end

        def fields_validation_after(datum, document)
        end

        def default_field(field_def)
            datum = data_class.new
            datum.field_def_id = field_def.id
            from_csv_field(datum, field_def.default_value) if field_def.default_value.present?
            datum
        end

        def new_field(param)
            data_class.new(param.permit(:value1, :deleted))
        end

        def update_field(datum, param)
            datum.assign_attributes(param.permit(:value1, :deleted))
        end

        def from_csv_field(datum, csv_string)
            datum.value1 = csv_string
        end

        def to_public_field(datum)
            pub_field = Pub.data_class.where(id: datum.id).first
            if pub_field.blank?
                pub_field = Pub.data_class.new
            end

            pub_field.value1 = datum.value1
            pub_field
        end

        def get_search_document_ids(datum, limit)
            return nil if blank_value?(datum)
            return data_class.where(field_def_id: datum.field_def_id)
                .where(value1: datum.value1)
                .limit(limit).pluck(:document_id)
        end

        def get_search_document_relation(relation, datum)
            relation = relation.joins(:doc_field1)

            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.value1 = ?)", datum.field_def_id, datum.value1) if datum.value1.present?

            relation
        end

        def get_search_field_relation(relation, datum)
            relation = relation.where("(field_def_id = ? AND value1 = ?)", datum.field_def_id, datum.value1) if datum.value1.present?
            relation
        end

        def get_exist_values(datum, relation)
            return [] if blank_value?(datum)

            data_class.where(document_id: relation, field_def_id: datum.field_def_id).group(:value1).count
        end

        def get_all_exist_values(datum)
            return [] if blank_value?(datum)

            relation = data_class.joins(:document).where('pri_documents.deleted = false')

            get_search_field_relation(relation, datum).group(:value1).count
        end

        # get_exist_values と 対応
        def new_count_field(value)
            field = data_class.new
            field.value1 = value
            field
        end

        def update_values(datum, relation, search_field)
            if datum.value1.present?
                data_class.where(document_id: relation, field_def_id: datum.field_def_id)
                    .update_all(value1: datum.value1)
            end
        end

        def update_field_values(datum, relation, new_field)
            update_value = {}

            relation = get_search_field_relation(data_class.includes(:document), datum)

            update_value[:value1] = new_field.value1

            update_field_values_and_update_document(relation, update_value)
        end

        def data_class
            'PriFieldText1Datum'.constantize
        end

        def get_view_dir
            'pri_field_simple_text'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end


    module Pub
        include FieldTypes::FieldTypeBase::Pub
        include Common

        def data_class
            'PubFieldText1Datum'.constantize
        end

        def get_view_dir
            'pub_field_simple_text'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

end
