#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FieldTypes::SimplePerson
    include FieldTypes::FieldTypeBase

    TYPE_ID = 2

    def caption_ja
        'シンプル人名'
    end

    def caption_en
        'simple person'
    end

    # field_def.field_param_text1 は、姓名の表記方法 {family}, {given}

    def check_field_param(field_def)
        if field_def.in_field_delim.blank?
            field_def.errors.add(:in_field_delim, "CSVでの項目区切り文字は必須です")
        end
        if field_def.field_param_id1.present? \
        || field_def.field_param_id2.present? \
        || field_def.field_param_id3.present? \
        || field_def.field_param_text2.present? \
        || field_def.field_param_text3.present? \
        || field_def.check_format.present? \
        || field_def.sort_conv_pattern.present? \
        || field_def.sort_conv_format.present?
            field_def.errors.add(:base, "不要なパラメタがあります。")
        end
    end

    def lock_field_update?(field_def)
        field_def.field_param_text1_changed?
    end

    def file_holder?
        false
    end

    def no_batch?
        false
    end

    def get_select_id(field_def)
        nil
    end

    extend(self)
    def self.included(base)
        base.extend(self)
    end

    module Common

        def format_name(datum, family_name, given_name)
            return given_name if family_name.blank?
            return family_name if given_name.blank?

            format = datum.get_field_def.field_param_text1
            if format.blank?
                given_name.to_s + ' ' + family_name.to_s
            else
                format = format.gsub('{family}', family_name.to_s)
                format = format.gsub('{given}', given_name.to_s)
                format
            end
        end

        def get_list_caption_ja(datum)
            format_name(field, field.value1, field.value2)
        end

        alias_method :get_list_caption_en, :get_list_caption_ja

        def get_list_param(datum)
            [datum.value1, datum.value2].reject{|a|a.blank?}.join(',')
        end

        def get_list_param_prefix_ja(datum)
            get_list_param(datum)[0] || ''
        end

        alias_method :get_list_param_prefix_en, :get_list_param_prefix_ja

        def get_search_values(datum)
            [datum.value1, datum.value2, datum.value1.to_s + datum.value2, datum.value2.to_s + datum.value1].reject{|a|a.blank?}.uniq
        end

        def blank_value?(datum)
            datum.value1.blank? && datum.value2.blank?
        end

        def nil_value?(datum)
            datum.value1.nil? && datum.value2.nil?
        end

        def to_csv_field(datum)
            delim = datum.get_field_def.in_field_delim
            [datum.value1, datum.value2].join(delim)
        end

        alias_method :to_filter_value, :to_csv_field

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

    module Pri
        include FieldTypes::FieldTypeBase::Pri
        include Common

        def get_oaipmh_set_value(datum)
            [datum.value1, datum.value2].join(',')
        end

        def fields_validation_before(datum, document)
            if blank_value?(datum) == false && datum.value1.blank?
                document.errors.add(:base, "値を入力してください")
                datum.errors.add(:value1, "最低でも姓は入力してください")
            end

            if datum.must?(document) && blank_value?(datum)
                document.errors.add(:base, "必須項目が未入力です")
                datum.errors.add(:value1, "最低でも姓は入力してください")
            end

            datum.errors.blank?
        end

        def fields_validation_after(datum, document)
        end

        def new_field(param)
            data_class.new(param.permit(:value1, :value2, :deleted))
        end

        def update_field(datum, param)
            datum.assign_attributes(param.permit(:value1, :value2, :deleted))
        end

        def from_csv_field(datum, csv_string)
            delim = datum.get_field_def.in_field_delim
            csv_values = csv_string.split(delim, -1)

            datum.value1 = csv_values[0]
            datum.value2 = csv_values[1] || ''
        end

        def to_public_field(datum)
            pub_field = Pub.data_class.where(id: datum.id).first
            if pub_field.blank?
                pub_field = Pub.data_class.new
            end

            pub_field.value1 = datum.value1
            pub_field.value2 = datum.value2
            pub_field
        end

        def get_search_document_ids(datum, limit)
            return nil if blank_value?(datum)

            relation = data_class.where(field_def_id: datum.field_def_id)

            relation = relation.where(value1: datum.value1) if datum.value1.present?
            relation = relation.where(value2: datum.value2) if datum.value2.present?

            relation.limit(limit).pluck(:document_id)
        end

        def get_search_document_relation(relation, datum)
            relation = relation.joins(:doc_field2)

            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.value1 = ?)", datum.field_def_id, datum.value1) if datum.value1.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.value2 = ?)", datum.field_def_id, datum.value2) if datum.value2.present?

            relation
        end

        def get_search_field_relation(relation, datum)
            relation = relation.where("(field_def_id = ? AND value1 = ?)", datum.field_def_id, datum.value1) if datum.value1.present?
            relation = relation.where("(field_def_id = ? AND value2 = ?)", datum.field_def_id, datum.value2) if datum.value2.present?
            relation
        end

        def get_exist_values(datum, relation)
            return [] if blank_value?(datum)

            data_class.where(document_id: relation, field_def_id: datum.field_def_id).group(:value1, :value2).count
        end

        def get_all_exist_values(datum)
            return [] if blank_value?(datum)

            relation = data_class.joins(:document).where('pri_documents.deleted = false')

            get_search_field_relation(relation, datum).group(:value1, :value2).count
        end

        # get_exist_values と 対応
        def new_count_field(value)
            field = data_class.new
            field.value1 = value[0]
            field.value2 = value[1]
            field
        end

        def update_values(datum, relation, search_field)
            update_value = {}

            update_value[:value1] = datum.value1 if datum.value1.present?
            update_value[:value2] = datum.value2 if datum.value2.present?

            data_class.where(document_id: relation, field_def_id: datum.field_def_id)
                .update_all(update_value)
        end

        def update_field_values(datum, relation, new_field)
            update_value = {}

            relation = get_search_field_relation(data_class.includes(:document), datum)

            update_value[:value1] = new_field.value1
            update_value[:value2] = new_field.value2

            update_field_values_and_update_document(relation, update_value)
        end

        def data_class
            'PriFieldText2Datum'.constantize
        end

        def get_view_dir
            'pri_field_simple_person'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end


    module Pub
        include FieldTypes::FieldTypeBase::Pub
        include Common

        def data_class
            'PubFieldText2Datum'.constantize
        end

        def get_view_dir
            'pub_field_simple_person'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

end
