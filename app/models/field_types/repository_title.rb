#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FieldTypes::RepositoryTitle
    include FieldTypes::FieldTypeBase

    TYPE_ID = 13

    def caption_ja
        'リポジトリタイトル'
    end

    def caption_en
        'repository title'
    end

    def check_field_param(field_def)
        if field_def.field_param_id1.blank?
            field_def.errors.add(:field_param_id1, "選択肢は必須です")
        end
        if field_def.in_field_delim.blank?
            field_def.errors.add(:in_field_delim, "CSVでの項目区切り文字は必須です")
        end
        if field_def.field_param_id2.present? \
        || field_def.field_param_id3.present? \
        || field_def.field_param_text1.present? \
        || field_def.field_param_text2.present? \
        || field_def.field_param_text3.present? \
        || field_def.check_format.present? \
        || field_def.sort_conv_pattern.present? \
        || field_def.sort_conv_format.present?
            field_def.errors.add(:base, "不要なパラメタがあります。")
        end
    end

    def lock_field_update?(field_def)
        field_def.field_param_id1_changed?
    end

    def file_holder?
        false
    end

    def no_batch?
        false
    end

    def get_select_id(field_def)
        field_def.field_param_id1
    end

    # get_select_id が trueになる場合のみ
    def get_select_value_exists(field_def, custom_select_value)
        get_select_value_relation(field_def, custom_select_value).count != 0
    end

    # custom select valueのupdate,delete時のみ
    def get_select_value_relation(field_def, custom_select_value)
        Pri::data_class.where(field_def_id: field_def.id, value: custom_select_value.value)
    end

    extend(self)
    def self.included(base)
        base.extend(self)
    end

    module Common

        def get_master_value(datum)
            return nil if datum.value.blank?
            CustomSelectValue.get_value_by_custom_select_id_value(datum.get_field_def.field_param_id1, datum.value)
        end

        def get_sort_key_value(datum)
            custom_select_value = datum.get_master_value
            return Earmas::SORT_MAX if custom_select_value == nil

            Earmas.int32_to_s(custom_select_value.sort_index)
        end

        def get_list_caption_ja(datum)
            custom_select_value = datum.get_master_value

            if custom_select_value == nil
                caption = datum.caption_ja
                caption = datum.caption_en if caption.blank?
                caption
            else
                caption = custom_select_value.caption_ja
                caption = custom_select_value.caption_en if caption.blank?
                caption
            end
        end
        def get_list_caption_en(datum)
            custom_select_value = datum.get_master_value

            if custom_select_value == nil
                caption = datum.caption_en
                caption = datum.caption_ja if caption.blank?
                caption
            else
                caption = custom_select_value.caption_en
                caption = custom_select_value.caption_ja if caption.blank?
                caption
            end
        end

        def get_list_param(datum)
            custom_select_value = datum.get_master_value

            if custom_select_value == nil
                if datum.caption_en == datum.caption_ja
                    datum.caption_ja
                else
                    [datum.caption_en, datum.caption_ja].reject{|a|a.blank?}.join(',')
                end
            else
                datum.value
            end
        end

        def get_list_param_prefix_ja(datum)
            get_list_caption_ja(datum)[0] || ''
        end

        def get_list_param_prefix_en(datum)
            get_list_caption_en(datum)[0] || ''
        end

        def get_search_values(datum)
            values = [datum.value, datum.caption_ja, datum.caption_en]

            custom_select_value = datum.get_master_value

            if custom_select_value != nil
                values << custom_select_value.value
                values << custom_select_value.caption_ja
                values << custom_select_value.caption_en
            end
            values.reject{|a|a.blank?}.uniq
        end

        def blank_value?(datum)
            datum.value.blank? && datum.caption_ja.blank? && datum.caption_en.blank?
        end

        def nil_value?(datum)
            datum.value.nil? && datum.caption_ja.nil? && datum.caption_en.nil?
        end


        def to_csv_field(datum)
            delim = datum.get_field_def.in_field_delim
            [datum.value, datum.caption_ja, datum.caption_en].join(delim)
        end

        alias_method :to_filter_value, :to_csv_field

        def to_filter_key_value(datum)
            datum.get_master_value.present? && datum.value || ''
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

    module Pri
        include FieldTypes::FieldTypeBase::Pri
        include Common

        def get_oaipmh_set_value(datum)
            caption = datum.caption_ja
            caption = datum.caption_en if caption.blank?

            caption.present? && caption || nil
        end

        def fields_validation_before(datum, document)
            if blank_value?(datum) == false && datum.caption_ja.blank? && datum.caption_en.blank? 
                document.errors.add(:base, "値を入力してください")
                datum.errors.add(:caption_ja, "最低でも日本語名か英語名は入力して下さい")
                datum.errors.add(:caption_en, "")
            end

            if datum.must?(document) && blank_value?(datum)
                document.errors.add(:base, "必須項目が未入力です")
                datum.errors.add(:caption_ja, "最低でも日本語名か英語名は入力して下さい")
                datum.errors.add(:caption_en, "")
            end

            datum.errors.blank?
        end

        def fields_validation_after(datum, document)
        end

        def new_field(param)
            datum = data_class.new(param.permit(:value, :caption_ja, :caption_en, :deleted))
            datum
        end

        def update_field(datum, param)
            datum.assign_attributes(param.permit(:value, :caption_ja, :caption_en, :deleted))
            datum
        end

        def from_csv_field(datum, csv_string)
            delim = datum.get_field_def.in_field_delim
            csv_values = csv_string.split(delim, -1)

            datum.value       = csv_values[0]
            datum.caption_ja  = csv_values[1] || ''
            datum.caption_en  = csv_values[2] || ''
        end

        def to_public_field(datum)
            pub_field = Pub.data_class.where(id: datum.id).first
            if pub_field.blank?
                pub_field = Pub.data_class.new
            end

            pub_field.value = datum.value
            pub_field.caption_ja = datum.caption_ja
            pub_field.caption_en = datum.caption_en

            pub_field
        end

        def get_search_document_ids(datum, limit)
            return nil if blank_value?(datum)

            relation = data_class.where(field_def_id: datum.field_def_id)

            relation = relation.where(value: datum.value) if datum.value.present?
            relation = relation.where(caption_ja: datum.caption_ja) if datum.caption_ja.present?
            relation = relation.where(caption_en: datum.caption_en) if datum.caption_en.present?

            relation.limit(limit).pluck(:document_id)
        end

        def get_search_document_relation(relation, datum)
            relation = relation.joins(:doc_field9)

            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.value = ?)", datum.field_def_id, datum.value) if datum.value.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.caption_ja = ?)", datum.field_def_id, datum.caption_ja) if datum.caption_ja.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.caption_en = ?)", datum.field_def_id, datum.caption_en) if datum.caption_en.present?

            relation
        end

        def get_search_field_relation(relation, datum)
            relation = relation.where("(field_def_id = ? AND value = ?)", datum.field_def_id, datum.value) if datum.value.present?
            relation = relation.where("(field_def_id = ? AND caption_ja = ?)", datum.field_def_id, datum.caption_ja) if datum.caption_ja.present?
            relation = relation.where("(field_def_id = ? AND caption_en = ?)", datum.field_def_id, datum.caption_en) if datum.caption_en.present?
            relation
        end

        def get_exist_values(datum, relation)
            return [] if blank_value?(datum)

            data_class.where(document_id: relation, field_def_id: datum.field_def_id).group(:value,
                :caption_ja, :caption_en).count
        end

        def get_all_exist_values(datum)
            return [] if blank_value?(datum)

            relation = data_class.joins(:document).where('pri_documents.deleted = false')

            get_search_field_relation(relation, datum).group(:value,
                :caption_ja, :caption_en).count
        end

        # get_exist_values と 対応
        def new_count_field(value)
            field = data_class.new
            field.value = value[0]
            field.caption_ja = value[1] || ''
            field.caption_en = value[2] || ''

            field
        end

        def update_values(datum, relation, search_field)
            update_value = {}

            update_value[:value] = datum.value if datum.value.present?
            update_value[:caption_ja] = datum.caption_ja if datum.caption_ja.present?
            update_value[:caption_en] = datum.caption_en if datum.caption_en.present?


            data_class.where(document_id: relation, field_def_id: datum.field_def_id)
                .update_all(update_value)
        end

        def update_field_values(datum, relation, new_field)
            update_value = {}

            relation = get_search_field_relation(data_class.includes(:document), datum)

            update_value[:value] = new_field.value
            update_value[:caption_ja] = new_field.caption_ja
            update_value[:caption_en] = new_field.caption_en

            update_field_values_and_update_document(relation, update_value)
        end

        def data_class
            'PriFieldRepositoryTitleDatum'.constantize
        end

        def get_view_dir
            'pri_field_repository_title'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end


    module Pub
        include FieldTypes::FieldTypeBase::Pub
        include Common

        def data_class
            'PubFieldRepositoryTitleDatum'.constantize
        end

        def get_view_dir
            'pub_field_repository_title'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

end
