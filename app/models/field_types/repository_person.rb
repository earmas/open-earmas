#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FieldTypes::RepositoryPerson
    include FieldTypes::FieldTypeBase

    TYPE_ID = 7

    def caption_ja
        'リポジトリ人名'
    end

    def caption_en
        'repository person'
    end

    # field_def.field_param_text1 は、日本語の姓名の表記方法 {family}, {given}
    # field_def.field_param_text2 は、英語の姓名の表記方法 {family}, {given}

    def check_field_param(field_def)
        if field_def.in_field_delim.blank?
            field_def.errors.add(:in_field_delim, "CSVでの項目区切り文字は必須です")
        end
        if field_def.field_param_id1.present? \
        || field_def.field_param_id2.present? \
        || field_def.field_param_id3.present? \
        || field_def.field_param_text3.present? \
        || field_def.check_format.present? \
        || field_def.sort_conv_pattern.present? \
        || field_def.sort_conv_format.present?
            field_def.errors.add(:base, "不要なパラメタがあります。")
        end
    end

    def lock_field_update?(field_def)
        field_def.field_param_text1_changed? || 
        field_def.field_param_text2_changed?
    end

    def file_holder?
        false
    end

    def no_batch?
        false
    end

    def get_select_id(field_def)
        nil
    end

    def get_person(datum)
        return nil if datum.person_id.blank?
        Person.where(id: datum.person_id).first
    end

    # peopleのupdate,delete時のみ
    def get_select_value_exists(field_def, person)
        get_select_value_relation(field_def, person).count != 0
    end

    # peopleのupdate,delete時のみ
    def get_select_value_relation(field_def, person)
        Pri::data_class.where(field_def_id: field_def.id, person_id: person.id)
    end

    extend(self)
    def self.included(base)
        base.extend(self)
    end

    module Common

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

    module Common

        # 全ての値がnilの可能性があるので注意

        def get_master_value(datum)
            return nil if datum.person_id.blank?
            Person.get_value(datum.person_id)
        end

        def format_name_ja(datum, family_name, given_name)
            return given_name if family_name.blank?
            return family_name if given_name.blank?

            format = datum.get_field_def.field_param_text1
            if format.blank?
                family_name.to_s + ' ' + given_name.to_s
            else
                format = format.gsub('{family}', family_name.to_s)
                format = format.gsub('{given}', given_name.to_s)
                format
            end
        end

        def format_name_en(datum, family_name, given_name)
            return given_name if family_name.blank?
            return family_name if given_name.blank?

            format = datum.get_field_def.field_param_text2
            if format.blank?
                given_name.to_s + ' ' + family_name.to_s
            else
                format = format.gsub('{family}', family_name.to_s)
                format = format.gsub('{given}', given_name.to_s)
                format
            end
        end

        def get_sort_value_ja(datum)
            person = datum.get_master_value

            tra_family_string = nil
            tra_given_string = nil

            if person == nil
                tra_family_string = datum.family_name_tra.strip.to_s
                tra_given_string = datum.given_name_tra.strip.to_s
            else
                tra_family_string = person.family_name_tra.strip.to_s
                tra_given_string = person.given_name_tra.strip.to_s
            end

            if tra_family_string.blank? && tra_given_string.blank?
                Earmas::SORT_MAX + to_sort_number(datum.get_list_caption_ja)
            else
                tra_string = tra_family_string.to_s.ljust(20, ' ') + tra_given_string.to_s
                to_sort_number(tra_string) + to_sort_number(datum.get_list_caption_ja)
            end
        end

        def get_sort_key_value(datum)
            person = datum.get_master_value
            return Earmas::SORT_MAX if person == nil

            datum.person_id
        end

        def get_list_caption_ja(datum)
            person = datum.get_master_value

            if person == nil
                caption = format_name_ja(datum, datum.family_name_ja, datum.given_name_ja)
                caption = format_name_en(datum, datum.family_name_en, datum.given_name_en) if caption.blank?
                caption
            else
                caption = format_name_ja(datum, person.family_name_ja, person.given_name_ja)
                caption = format_name_en(datum, person.family_name_en, person.given_name_en) if caption.blank?
                caption
            end
        end
        def get_list_caption_en(datum)
            person = datum.get_master_value

            if person == nil
                caption = format_name_en(datum, datum.family_name_en, datum.given_name_en)
                caption = format_name_ja(datum, datum.family_name_ja, datum.given_name_ja) if caption.blank?
                caption
            else
                caption = format_name_en(datum, person.family_name_en, person.given_name_en)
                caption = format_name_ja(datum, person.family_name_ja, person.given_name_ja) if caption.blank?
                caption
            end
        end

        def get_list_param(datum)
            person = datum.get_master_value

            if person == nil
                if datum.family_name_ja.to_s == datum.family_name_en.to_s && datum.given_name_ja.to_s == datum.given_name_en.to_s
                    [datum.given_name_ja, datum.family_name_ja].reject{|a|a.blank?}.join(',')
                else
                    [datum.given_name_en, datum.family_name_en, datum.family_name_ja, datum.given_name_ja].reject{|a|a.blank?}.join(',')
                end
            else
                person.encrypted_id.to_s
            end
        end

        def get_list_param_prefix_ja(datum)
            person = datum.get_master_value

            prefix = ''
            if person == nil
                prefix = datum.family_name_tra.strip.upcase[0] if prefix.blank?
                prefix = datum.given_name_tra.strip.upcase[0] if prefix.blank?

                prefix = datum.family_name_ja.strip.upcase[0] if prefix.blank?
                prefix = datum.given_name_ja.strip.upcase[0] if prefix.blank?
            else
                prefix = person.family_name_tra.strip.upcase[0] if prefix.blank?
                prefix = person.given_name_tra.strip.upcase[0] if prefix.blank?

                prefix = person.family_name_ja.strip.upcase[0] if prefix.blank?
                prefix = person.given_name_ja.strip.upcase[0] if prefix.blank?
            end

            prefix
        end

        def get_list_param_prefix_en(datum)
            person = datum.get_master_value

            prefix = ''
            if person == nil
                prefix = datum.family_name_en.strip.upcase[0] if prefix.blank?
                prefix = datum.given_name_en.strip.upcase[0] if prefix.blank?
            else
                prefix = person.family_name_en.strip.upcase[0] if prefix.blank?
                prefix = person.given_name_en.strip.upcase[0] if prefix.blank?
            end

            prefix
        end


        def blank_value?(datum)
            datum.person_id.blank? && datum.family_name_ja.blank? && datum.given_name_ja.blank? &&
                datum.family_name_en.blank? && datum.given_name_en.blank? &&
                datum.family_name_tra.blank? && datum.given_name_tra.blank? &&
                datum.family_name_alt1.blank? && datum.given_name_alt1.blank? &&
                datum.family_name_alt2.blank? && datum.given_name_alt2.blank? &&
                datum.family_name_alt3.blank? && datum.given_name_alt3.blank? &&
                datum.affiliation_ja.blank? && datum.affiliation_en.blank?
        end

        def nil_value?(datum)
            datum.person_id.nil? && datum.family_name_ja.nil? && datum.given_name_ja.nil? &&
                datum.family_name_en.nil? && datum.given_name_en.nil? &&
                datum.family_name_tra.nil? && datum.given_name_tra.nil? &&
                datum.family_name_alt1.nil? && datum.given_name_alt1.nil? &&
                datum.family_name_alt2.nil? && datum.given_name_alt2.nil? &&
                datum.family_name_alt3.nil? && datum.given_name_alt3.nil? &&
                datum.affiliation_ja.nil? && datum.affiliation_en.nil?
        end

        def to_filter_key_value(datum)
            datum.get_master_value.present? && datum.person_id || ''
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

    module Pri
        include FieldTypes::FieldTypeBase::Pri
        include Common

        def get_oaipmh_set_value(datum)
            caption = [datum.given_name_en, datum.family_name_en].reject{|a|a.blank?}.join(' ')
            caption = [datum.family_name_ja, datum.given_name_ja].reject{|a|a.blank?}.join(' ') if caption.blank?

            caption.present? && caption || nil
        end

        def fields_validation_before(datum, document)
            if blank_value?(datum) == false && datum.family_name_ja.blank? && datum.family_name_en.blank? 
                document.errors.add(:base, "値を入力してください")
                datum.errors.add(:family_name_ja, "最低でも日本語姓か英語姓は入力してください")
                datum.errors.add(:family_name_en, "")
            end

            if datum.must?(document) && blank_value?(datum)
                document.errors.add(:base, "必須項目が未入力です")
                datum.errors.add(:family_name_ja, "最低でも日本語姓か英語姓は入力してください")
                datum.errors.add(:family_name_en, "")
            end

            datum.errors.blank?
        end

        def fields_validation_after(datum, document)
        end

        def new_field(param)
            datum = data_class.new(param.permit(:person_id, :family_name_ja, :given_name_ja, :family_name_en, :given_name_en, :family_name_tra, :given_name_tra,
                :family_name_alt1, :given_name_alt1, :family_name_alt2, :given_name_alt2, :family_name_alt3, :given_name_alt3, :affiliation_ja, :affiliation_en, :optional_key1, :optional_key2, :deleted))
            datum
        end

        def update_field(datum, param)
            datum.assign_attributes(param.permit(:person_id, :family_name_ja, :given_name_ja, :family_name_en, :given_name_en, :family_name_tra, :given_name_tra,
                :family_name_alt1, :given_name_alt1, :family_name_alt2, :given_name_alt2, :family_name_alt3, :given_name_alt3, :affiliation_ja, :affiliation_en, :optional_key1, :optional_key2, :deleted))
            datum
        end

        def to_csv_field(datum)
            delim = datum.get_field_def.in_field_delim
            [datum.person_id, datum.family_name_ja, datum.given_name_ja, datum.family_name_en, datum.given_name_en, datum.family_name_tra, datum.given_name_tra,
                datum.family_name_alt1, datum.given_name_alt1, datum.family_name_alt2, datum.given_name_alt2, datum.family_name_alt3, datum.given_name_alt3,
                datum.affiliation_ja, datum.affiliation_en].join(delim)
        end

        def from_csv_field(datum, csv_string)
            delim = datum.get_field_def.in_field_delim
            csv_values = csv_string.split(delim, -1)

            datum.person_id = csv_values[0]
            datum.family_name_ja  = csv_values[1] || ''
            datum.given_name_ja   = csv_values[2] || ''
            datum.family_name_en  = csv_values[3] || ''
            datum.given_name_en   = csv_values[4] || ''
            datum.family_name_tra = csv_values[5] || ''
            datum.given_name_tra  = csv_values[6] || ''

            datum.family_name_alt1 = csv_values[7] || ''
            datum.given_name_alt1  = csv_values[8] || ''
            datum.family_name_alt2 = csv_values[9] || ''
            datum.given_name_alt2  = csv_values[10] || ''
            datum.family_name_alt3 = csv_values[11] || ''
            datum.given_name_alt3  = csv_values[12] || ''

            datum.affiliation_ja  = csv_values[13] || ''
            datum.affiliation_en  = csv_values[14] || ''

            datum.optional_key1  = csv_values[15] || ''
            datum.optional_key2  = csv_values[16] || ''
        end

        def get_search_values(datum)
            values = []
            values << datum.person_id

            values << datum.family_name_ja
            values << datum.given_name_ja
            values << datum.family_name_ja.to_s + datum.given_name_ja.to_s

            values << datum.family_name_en
            values << datum.given_name_en
            values << datum.family_name_en.to_s + datum.given_name_en.to_s

            values << datum.family_name_tra
            values << datum.given_name_tra
            values << datum.family_name_tra.to_s + datum.given_name_tra.to_s

            values << datum.family_name_alt1
            values << datum.given_name_alt1
            values << datum.family_name_alt1.to_s + datum.given_name_alt1.to_s

            values << datum.family_name_alt2
            values << datum.given_name_alt2
            values << datum.family_name_alt2.to_s + datum.given_name_alt2.to_s

            values << datum.family_name_alt3
            values << datum.given_name_alt3
            values << datum.family_name_alt3.to_s + datum.given_name_alt3.to_s

            values << datum.affiliation_ja
            values << datum.affiliation_en

            person = datum.get_master_value

            if person != nil
                values << person.encrypted_id

                values << person.family_name_ja
                values << person.given_name_ja
                values << person.family_name_ja.to_s + person.given_name_ja.to_s

                values << person.family_name_en
                values << person.given_name_en
                values << person.family_name_en.to_s + person.given_name_en.to_s

                values << person.family_name_tra
                values << person.given_name_tra
                values << person.family_name_tra.to_s + person.given_name_tra.to_s

                values << person.family_name_alt1
                values << person.given_name_alt1
                values << person.family_name_alt1.to_s + person.given_name_alt1.to_s

                values << person.family_name_alt2
                values << person.given_name_alt2
                values << person.family_name_alt2.to_s + person.given_name_alt2.to_s

                values << person.family_name_alt3
                values << person.given_name_alt3
                values << person.family_name_alt3.to_s + person.given_name_alt3.to_s

                values << person.affiliation_ja
                values << person.affiliation_en
            end

            values.reject{|a|a.blank?}.uniq
        end

        alias_method :to_filter_value, :to_csv_field

        def to_public_field(datum)
            pub_field = Pub.data_class.where(id: datum.id).first
            if pub_field.blank?
                pub_field = Pub.data_class.new
            end

            pub_field.person_id = datum.person_id
            pub_field.family_name_ja = datum.family_name_ja
            pub_field.given_name_ja = datum.given_name_ja
            pub_field.family_name_en = datum.family_name_en
            pub_field.given_name_en = datum.given_name_en
            pub_field.family_name_tra = datum.family_name_tra
            pub_field.given_name_tra = datum.given_name_tra

            pub_field.family_name_alt1 = datum.family_name_alt1
            pub_field.given_name_alt1 = datum.given_name_alt1
            pub_field.family_name_alt2 = datum.family_name_alt2
            pub_field.given_name_alt2 = datum.given_name_alt2
            pub_field.family_name_alt3 = datum.family_name_alt3
            pub_field.given_name_alt3 = datum.given_name_alt3

            pub_field.affiliation_ja = datum.affiliation_ja
            pub_field.affiliation_en = datum.affiliation_en

            pub_field.optional_key1 = datum.optional_key1
            pub_field.optional_key2 = datum.optional_key2

            pub_field
        end

        def get_search_document_ids(datum, limit)
            return nil if blank_value?(datum)

            relation = data_class.where(field_def_id: datum.field_def_id)

            relation = relation.where(person_id: datum.person_id) if datum.person_id.present?
            relation = relation.where(family_name_ja: datum.family_name_ja) if datum.family_name_ja.present?
            relation = relation.where(given_name_ja: datum.given_name_ja) if datum.given_name_ja.present?
            relation = relation.where(family_name_en: datum.family_name_en) if datum.family_name_en.present?
            relation = relation.where(given_name_en: datum.given_name_en) if datum.given_name_en.present?
            relation = relation.where(family_name_tra: datum.family_name_tra) if datum.family_name_tra.present?
            relation = relation.where(given_name_tra: datum.given_name_tra) if datum.given_name_tra.present?


            relation = relation.where(family_name_alt1: datum.family_name_alt1) if datum.family_name_alt1.present?
            relation = relation.where(given_name_alt1: datum.given_name_alt1) if datum.given_name_alt1.present?
            relation = relation.where(family_name_alt2: datum.family_name_alt2) if datum.family_name_alt2.present?
            relation = relation.where(given_name_alt2: datum.given_name_alt2) if datum.given_name_alt2.present?
            relation = relation.where(family_name_alt3: datum.family_name_alt3) if datum.family_name_alt3.present?
            relation = relation.where(given_name_alt3: datum.given_name_alt3) if datum.given_name_alt3.present?

            relation = relation.where(affiliation_ja: datum.affiliation_ja) if datum.affiliation_ja.present?
            relation = relation.where(affiliation_en: datum.affiliation_en) if datum.affiliation_en.present?

            relation.limit(limit).pluck(:document_id)
        end

        def get_search_document_relation(relation, datum)
            relation = relation.joins(:doc_field8)

            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.person_id = ?)", datum.field_def_id, datum.person_id) if datum.person_id.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.family_name_ja = ?)", datum.field_def_id, datum.family_name_ja) if datum.family_name_ja.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.given_name_ja = ?)",  datum.field_def_id, datum.given_name_ja)  if datum.given_name_ja.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.family_name_en = ?)", datum.field_def_id, datum.family_name_en) if datum.family_name_en.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.given_name_en = ?)",  datum.field_def_id, datum.given_name_en)  if datum.given_name_en.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.family_name_tra = ?)", datum.field_def_id, datum.family_name_tra) if datum.family_name_tra.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.given_name_tra = ?)",  datum.field_def_id, datum.given_name_tra)  if datum.given_name_tra.present?

            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.family_name_alt1 = ?)", datum.field_def_id, datum.family_name_alt1) if datum.family_name_alt1.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.given_name_alt1 = ?)",  datum.field_def_id, datum.given_name_alt1)  if datum.given_name_alt1.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.family_name_alt2 = ?)", datum.field_def_id, datum.family_name_alt2) if datum.family_name_alt2.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.given_name_alt2 = ?)",  datum.field_def_id, datum.given_name_alt2)  if datum.given_name_alt2.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.family_name_alt3 = ?)", datum.field_def_id, datum.family_name_alt3) if datum.family_name_alt3.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.given_name_alt3 = ?)",  datum.field_def_id, datum.given_name_alt3)  if datum.given_name_alt3.present?

            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.affiliation_ja = ?)", datum.field_def_id, datum.affiliation_ja) if datum.affiliation_ja.present?
            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.affiliation_en = ?)", datum.field_def_id, datum.affiliation_en) if datum.affiliation_en.present?

            relation
        end

        def get_search_field_relation(relation, datum)
            relation = relation.where("(field_def_id = ? AND person_id = ?)", datum.field_def_id, datum.person_id) if datum.person_id.present?
            relation = relation.where("(field_def_id = ? AND family_name_ja = ?)", datum.field_def_id, datum.family_name_ja) if datum.family_name_ja.present?
            relation = relation.where("(field_def_id = ? AND given_name_ja = ?)",  datum.field_def_id, datum.given_name_ja)  if datum.given_name_ja.present?
            relation = relation.where("(field_def_id = ? AND family_name_en = ?)", datum.field_def_id, datum.family_name_en) if datum.family_name_en.present?
            relation = relation.where("(field_def_id = ? AND given_name_en = ?)",  datum.field_def_id, datum.given_name_en)  if datum.given_name_en.present?
            relation = relation.where("(field_def_id = ? AND family_name_tra = ?)", datum.field_def_id, datum.family_name_tra) if datum.family_name_tra.present?
            relation = relation.where("(field_def_id = ? AND given_name_tra = ?)",  datum.field_def_id, datum.given_name_tra)  if datum.given_name_tra.present?

            relation = relation.where("(field_def_id = ? AND family_name_alt1 = ?)", datum.field_def_id, datum.family_name_alt1) if datum.family_name_alt1.present?
            relation = relation.where("(field_def_id = ? AND given_name_alt1 = ?)",  datum.field_def_id, datum.given_name_alt1)  if datum.given_name_alt1.present?
            relation = relation.where("(field_def_id = ? AND family_name_alt2 = ?)", datum.field_def_id, datum.family_name_alt2) if datum.family_name_alt2.present?
            relation = relation.where("(field_def_id = ? AND given_name_alt2 = ?)",  datum.field_def_id, datum.given_name_alt2)  if datum.given_name_alt2.present?
            relation = relation.where("(field_def_id = ? AND family_name_alt3 = ?)", datum.field_def_id, datum.family_name_alt3) if datum.family_name_alt3.present?
            relation = relation.where("(field_def_id = ? AND given_name_alt3 = ?)",  datum.field_def_id, datum.given_name_alt3)  if datum.given_name_alt3.present?

            relation = relation.where("(field_def_id = ? AND affiliation_ja = ?)", datum.field_def_id, datum.affiliation_ja) if datum.affiliation_ja.present?
            relation = relation.where("(field_def_id = ? AND affiliation_en = ?)", datum.field_def_id, datum.affiliation_en) if datum.affiliation_en.present?

            relation
        end

        def get_exist_values(datum, relation)
            return [] if blank_value?(datum)

            data_class.where(document_id: relation, field_def_id: datum.field_def_id).group(:person_id,
                :family_name_ja, :given_name_ja, :family_name_en, :given_name_en, :family_name_tra, :given_name_tra,
                :family_name_alt1, :given_name_alt1, :family_name_alt2, :given_name_alt2, :family_name_alt3, :given_name_alt3,
                :affiliation_ja, :affiliation_en).count
        end

        def get_all_exist_values(datum)
            return [] if blank_value?(datum)

            relation = data_class.joins(:document).where('pri_documents.deleted = false')

            get_search_field_relation(relation, datum).group(:person_id,
                :family_name_ja, :given_name_ja, :family_name_en, :given_name_en, :family_name_tra, :given_name_tra,
                :family_name_alt1, :given_name_alt1, :family_name_alt2, :given_name_alt2, :family_name_alt3, :given_name_alt3,
                :affiliation_ja, :affiliation_en).count
        end

        # get_exist_values と 対応
        def new_count_field(value)
            field = data_class.new
            field.person_id = value[0]
            field.family_name_ja = value[1]
            field.given_name_ja = value[2]
            field.family_name_en = value[3]
            field.given_name_en = value[4]
            field.family_name_tra = value[5]
            field.given_name_tra = value[6]

            field.family_name_alt1 = value[7]
            field.given_name_alt1 = value[8]
            field.family_name_alt2 = value[9]
            field.given_name_alt2 = value[10]
            field.family_name_alt3 = value[11]
            field.given_name_alt3 = value[12]

            field.affiliation_ja = value[13]
            field.affiliation_en = value[14]

            field
        end

        def update_values(datum, relation, search_field)
            update_value = {}

            update_value[:person_id] = datum.person_id if datum.person_id.present?
            update_value[:family_name_ja] = datum.family_name_ja if datum.family_name_ja.present?
            update_value[:given_name_ja] = datum.given_name_ja if datum.given_name_ja.present?
            update_value[:family_name_en] = datum.family_name_en if datum.family_name_en.present?
            update_value[:given_name_en] = datum.given_name_en if datum.given_name_en.present?
            update_value[:family_name_tra] = datum.family_name_tra if datum.family_name_tra.present?
            update_value[:given_name_tra] = datum.given_name_tra if datum.given_name_tra.present?

            update_value[:family_name_alt1] = datum.family_name_alt1 if datum.family_name_alt1.present?
            update_value[:given_name_alt1] = datum.given_name_alt1 if datum.given_name_alt1.present?
            update_value[:family_name_alt2] = datum.family_name_alt2 if datum.family_name_alt2.present?
            update_value[:given_name_alt2] = datum.given_name_alt2 if datum.given_name_alt2.present?
            update_value[:family_name_alt3] = datum.family_name_alt3 if datum.family_name_alt3.present?
            update_value[:given_name_alt3] = datum.given_name_alt3 if datum.given_name_alt3.present?

            update_value[:affiliation_ja] = datum.affiliation_ja if datum.affiliation_ja.present?
            update_value[:affiliation_en] = datum.affiliation_en if datum.affiliation_en.present?


            data_class.where(document_id: relation, field_def_id: datum.field_def_id)
                .update_all(update_value)
        end

        def update_field_values(datum, relation, new_field)
            update_value = {}

            relation = get_search_field_relation(data_class.includes(:document), datum)

            update_value[:person_id] = new_field.person_id
            update_value[:family_name_ja] = new_field.family_name_ja
            update_value[:given_name_ja] = new_field.given_name_ja
            update_value[:family_name_en] = new_field.family_name_en
            update_value[:given_name_en] = new_field.given_name_en
            update_value[:family_name_tra] = new_field.family_name_tra
            update_value[:given_name_tra] = new_field.given_name_tra

            update_value[:family_name_alt1] = new_field.family_name_alt1
            update_value[:given_name_alt1] = new_field.given_name_alt1
            update_value[:family_name_alt2] = new_field.family_name_alt2
            update_value[:given_name_alt2] = new_field.given_name_alt2
            update_value[:family_name_alt3] = new_field.family_name_alt3
            update_value[:given_name_alt3] = new_field.given_name_alt3

            update_value[:affiliation_ja] = new_field.affiliation_ja
            update_value[:affiliation_en] = new_field.affiliation_en

            update_field_values_and_update_document(relation, update_value)
        end

        def data_class
            'PriFieldRepositoryPersonDatum'.constantize
        end

        def get_view_dir
            'pri_field_repository_person'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end


    module Pub
        include FieldTypes::FieldTypeBase::Pub
        include Common

        def to_csv_field(datum)
            delim = datum.get_field_def.in_field_delim
            [datum.family_name_ja, datum.given_name_ja, datum.family_name_en, datum.given_name_en, datum.family_name_tra, datum.given_name_tra,
                datum.family_name_alt1, datum.given_name_alt1, datum.family_name_alt2, datum.given_name_alt2, datum.family_name_alt3, datum.given_name_alt3,
                datum.affiliation_ja, datum.affiliation_en].join(delim)
        end

        def get_search_values(datum)
            values = []
            values << datum.family_name_ja
            values << datum.given_name_ja
            values << datum.family_name_ja.to_s + datum.given_name_ja.to_s

            values << datum.family_name_en
            values << datum.given_name_en
            values << datum.family_name_en.to_s + datum.given_name_en.to_s

            values << datum.family_name_tra
            values << datum.given_name_tra
            values << datum.family_name_tra.to_s + datum.given_name_tra.to_s

            values << datum.family_name_alt1
            values << datum.given_name_alt1
            values << datum.family_name_alt1.to_s + datum.given_name_alt1.to_s

            values << datum.family_name_alt2
            values << datum.given_name_alt2
            values << datum.family_name_alt2.to_s + datum.given_name_alt2.to_s

            values << datum.family_name_alt3
            values << datum.given_name_alt3
            values << datum.family_name_alt3.to_s + datum.given_name_alt3.to_s

            values << datum.affiliation_ja
            values << datum.affiliation_en

            person = datum.get_master_value

            if person != nil
                values << person.encrypted_id

                values << person.family_name_ja
                values << person.given_name_ja
                values << person.family_name_ja.to_s + person.given_name_ja.to_s

                values << person.family_name_en
                values << person.given_name_en
                values << person.family_name_en.to_s + person.given_name_en.to_s

                values << person.family_name_tra
                values << person.given_name_tra
                values << person.family_name_tra.to_s + person.given_name_tra.to_s

                values << person.family_name_alt1
                values << person.given_name_alt1
                values << person.family_name_alt1.to_s + person.given_name_alt1.to_s

                values << person.family_name_alt2
                values << person.given_name_alt2
                values << person.family_name_alt2.to_s + person.given_name_alt2.to_s

                values << person.family_name_alt3
                values << person.given_name_alt3
                values << person.family_name_alt3.to_s + person.given_name_alt3.to_s

                values << person.affiliation_ja
                values << person.affiliation_en
            end
            values.reject{|a|a.blank?}.uniq
        end

        alias_method :to_filter_value, :to_csv_field

        def data_class
            'PubFieldRepositoryPersonDatum'.constantize
        end

        def get_view_dir
            'pub_field_repository_person'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

end
