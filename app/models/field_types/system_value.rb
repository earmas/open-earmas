#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FieldTypes::SystemValue
    include FieldTypes::SimpleText

    TYPE_ID = 16

    FIELD_SYSTEM_ID    = 1
    FIELD_CREATED_DATE = 2
    FIELD_CREATE_USER  = 3
    FIELD_SELF_DOI     = 4

    def caption_ja
        'システム値'
    end

    def caption_en
        'System Value'
    end

    def check_field_param(field_def)
        if field_def.field_param_id1.blank?
            field_def.errors.add(:field_param_id1, "対象は必須です")
        end

        if field_def.in_field_delim.present? \
        || field_def.default_value.present? \
        || field_def.field_param_id2.present? \
        || field_def.field_param_id3.present? \
        || field_def.field_param_text2.present? \
        || field_def.field_param_text3.present? \
        || field_def.check_format.present? \
        || field_def.sort_conv_pattern.present? \
        || field_def.sort_conv_format.present?
            field_def.errors.add(:base, "不要なパラメタがあります。")
        end
    end

    def lock_field_update?(field_def)
        false
    end

    def no_batch?
        true
    end

    def no_value?
        true
    end

    def must_show_field?(field_def, document)
        case field_def.field_param_id1
        when FIELD_SYSTEM_ID
            return true
        when FIELD_CREATED_DATE
            return true
        when FIELD_CREATE_USER
            return true
        when FIELD_SELF_DOI
            return document.self_doi_enable
        end
    end

    def get_select_id(field_def)
        nil
    end

    def get_url(document, field_def, datum)
        gen_data(field_def, document)
    end

    def gen_url_caption(field_def, document)
        case field_def.field_param_id1
        when FIELD_SYSTEM_ID
            return gen_data(field_def, document)
        when FIELD_CREATED_DATE
            return gen_data(field_def, document)
        when FIELD_CREATE_USER
            return gen_data(field_def, document)
        when FIELD_SELF_DOI
            return gen_self_doi_value(field_def, document)
        end
        ""
    end

    def gen_data(field_def, document)
        case field_def.field_param_id1
        when FIELD_SYSTEM_ID
            return gen_system_id(field_def, document)
        when FIELD_CREATED_DATE
            return gen_created_date(field_def, document)
        when FIELD_CREATE_USER
            return gen_creator_user_id(field_def, document)
        when FIELD_SELF_DOI
            return gen_self_doi(field_def, document)
        end
        ""
    end

    def gen_system_id(field_def, document)
        begin
            if field_def.field_param_text1.present?
                field_def.set_unit_by_locale(sprintf(field_def.field_param_text1, document.id.to_s))
            else
                field_def.set_unit_by_locale(document.id.to_s)
            end
        rescue Exception => ex
            Rails.logger.error ex.message
            return document.id
        end
    end
    def gen_created_date(field_def, document)
        begin
            if field_def.field_param_text1.present?
                field_def.set_unit_by_locale(document.created_at.strftime(field_def.field_param_text1))
            else
                field_def.set_unit_by_locale(document.created_at.strftime('%FT%TZ'))
            end
        rescue Exception => ex
            Rails.logger.error ex.message
            return document.created_at
        end
    end
    def gen_creator_user_id(field_def, document)
        return '' if document.pri? == false
        begin
            if field_def.field_param_text1.present?
                field_def.set_unit_by_locale(sprintf(field_def.field_param_text1, Staff.find(document.creator_staff_id).email))
            else
                field_def.set_unit_by_locale(Staff.find(document.creator_staff_id).email)
            end
        rescue Exception => ex
            Rails.logger.error ex.message
            return Staff.find(document.creator_staff_id).email
        end
    end
    def gen_self_doi(field_def, document)
        return '' if document.self_doi_enable == false
        begin
            if field_def.field_param_text1.present?
                SelfDoiDocument::SELF_DOI_BASE_URL + field_def.set_unit_by_locale(sprintf(field_def.field_param_text1, document.self_doi.to_s))
            else
                SelfDoiDocument::SELF_DOI_BASE_URL + field_def.set_unit_by_locale(document.self_doi)
            end
        rescue Exception => ex
            Rails.logger.error ex.message
            return document.self_doi
        end
    end
    def gen_self_doi_value(field_def, document)
        return '' if document.self_doi_enable == false
        begin
            if field_def.field_param_text1.present?
                field_def.set_unit_by_locale(sprintf(field_def.field_param_text1, document.self_doi.to_s))
            else
                field_def.set_unit_by_locale(document.self_doi)
            end
        rescue Exception => ex
            Rails.logger.error ex.message
            return document.self_doi
        end
    end
    def gen_self_doi_value_with_no_check(field_def, document)
        return '' if document.self_doi_enable == false
        begin
            if field_def.field_param_text1.present?
                field_def.set_unit_by_locale(sprintf(field_def.field_param_text1, document.self_doi.to_s))
            else
                field_def.set_unit_by_locale(document.self_doi)
            end
        rescue Exception => ex
            Rails.logger.error ex.message
            return document.self_doi
        end
    end

    def get_no_value_search_values(field_def, document)
        case field_def.field_param_id1
        when FIELD_SYSTEM_ID
            [gen_system_id(field_def, document)]
        when FIELD_CREATED_DATE
            [gen_created_date(field_def, document)]
        when FIELD_CREATE_USER
            [gen_creator_user_id(field_def, document)]
        when FIELD_SELF_DOI
            [gen_self_doi_value_with_no_check(field_def, document)]
        else
            []
        end
    end

    extend(self)
    def self.included(base)
        base.extend(self)
    end

    module Common

        def get_list_caption_ja(datum)
            raise "dont reach"
        end

        alias_method :get_list_caption_en, :get_list_caption_ja

        def get_list_param(datum)
            raise "dont reach"
        end

        def get_search_values(datum)
            raise "dont reach"
        end

        def blank_value?(datum)
            raise "dont reach"
        end

        def nil_value?(datum)
            raise "dont reach"
        end

        def to_csv_field(datum)
            raise "dont reach"
        end

        alias_method :to_filter_value, :to_csv_field

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

    module Pri
        include FieldTypes::FieldTypeBase::Pri
        include Common

        def get_oaipmh_set_value(datum)
            raise "dont reach"
        end

        def fields_validation_before(datum, document)
            raise "dont reach"
        end

        def fields_validation_after(datum, document)
            raise "dont reach"
        end

        def default_field(field_def)
            raise "dont reach"
        end

        def new_field(param)
            raise "dont reach"
        end

        def update_field(datum, param)
            raise "dont reach"
        end

        def from_csv_field(datum, csv_string)
            raise "dont reach"
        end

        def to_public_field(datum)
            raise "dont reach"
        end

        def update_values(datum, relation, search_field)
            raise "dont reach"
        end

        def data_class
            raise "dont reach"
        end

        def get_view_dir
            'pri_field_system_value'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end


    module Pub
        include FieldTypes::FieldTypeBase::Pub
        include Common

        def data_class
            raise "dont reach"
        end

        def get_view_dir
            'pub_field_system_value'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

end
