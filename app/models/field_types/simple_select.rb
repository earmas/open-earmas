#--
# Open Earmas
#
# Copyright (C) 2014 ENU Technologies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FieldTypes::SimpleSelect
    include FieldTypes::FieldTypeBase

    TYPE_ID = 3

    def caption_ja
        'シンプル選択'
    end

    def caption_en
        'simple select'
    end

    def check_field_param(field_def)
        if field_def.field_param_id1.blank?
            field_def.errors.add(:field_param_id1, "選択肢は必須です")
        end
        if field_def.in_field_delim.present? \
        || field_def.field_param_id2.present? \
        || field_def.field_param_id3.present? \
        || field_def.field_param_text1.present? \
        || field_def.field_param_text2.present? \
        || field_def.field_param_text3.present? \
        || field_def.check_format.present? \
        || field_def.sort_conv_pattern.present? \
        || field_def.sort_conv_format.present?
            field_def.errors.add(:base, "不要なパラメタがあります。")
        end
    end

    def lock_field_update?(field_def)
        false
    end

    def file_holder?
        false
    end

    def no_batch?
        false
    end

    def get_select_id(field_def)
        field_def.field_param_id1
    end

    # get_select_id が trueになる場合のみ
    def get_select_value_exists(field_def, custom_select_value)
        get_select_value_relation(field_def, custom_select_value).count != 0
    end

    def get_select_value_relation(field_def, custom_select_value)
        Pri::data_class.where(field_def_id: field_def.id, some_id: custom_select_value.id)
    end

    NO_VALUE = "値なし"

    extend(self)
    def self.included(base)
        base.extend(self)
    end

    module Common

        def get_key_value(datum)
            datum.send(get_key_name)
        end

        def get_sort_key_value(datum)
            custom_select_value = datum.get_master_value
            return Earmas::SORT_MAX if custom_select_value == nil

            Earmas.int32_to_s(custom_select_value.sort_index)
        end

        def nil_value?(datum)
            get_key_value(datum).nil?
        end

        def to_filter_key_value(datum)
            custom_select_value = datum.get_master_value

            custom_select_value.present? && custom_select_value.value || ''
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

    module Pri
        include FieldTypes::FieldTypeBase::Pri
        include Common

        def get_key_name
            'some_id'
        end

        def get_master_value(datum)
            return nil if datum.some_id.blank?
            CustomSelectValue.get_value(datum.some_id)
        end

        def get_list_caption_ja(datum)
            custom_select_value = datum.get_master_value

            if custom_select_value == nil
                "#{NO_VALUE}(#{datum.some_id})"
            else
                caption = custom_select_value.caption_ja
                caption = custom_select_value.caption_en if caption.blank?
                caption
            end
        end
        def get_list_caption_en(datum)
            custom_select_value = datum.get_master_value

            if custom_select_value == nil
                "#{NO_VALUE}(#{datum.some_id})"
            else
                caption = custom_select_value.caption_en
                caption = custom_select_value.caption_ja if caption.blank?
                caption
            end
        end

        def get_list_param(datum)
            custom_select_value = datum.get_master_value

            return nil if custom_select_value == nil # listに出さない

            custom_select_value.value
        end

        def get_list_param_prefix_ja(datum)
            get_list_caption_ja(datum)[0] || ''
        end

        def get_list_param_prefix_en(datum)
            get_list_caption_en(datum)[0] || ''
        end

        def get_search_values(datum)
            custom_select_value = datum.get_master_value
            return [] if custom_select_value.blank?

            [custom_select_value.caption_ja, custom_select_value.caption_en, custom_select_value.value].reject{|a|a.blank?}.uniq
        end

        def get_oaipmh_set_value(datum)
            custom_select_value = datum.get_master_value
            custom_select_value && custom_select_value.value || nil
        end

        def blank_value?(datum)
            (datum.some_id.blank? || datum.some_id == 0)
        end

        def fields_validation_before(datum, document)
            if datum.must?(document) && blank_value?(datum)
                document.errors.add(:base, "必須項目が未入力です")
                datum.errors.add(:some_id, "選択してください")
            end

            datum.errors.blank?
        end

        def fields_validation_after(datum, document)
        end

        def new_field(param)
            data_class.new(param.permit(:some_id, :deleted))
        end

        def update_field(datum, param)
            datum.assign_attributes(param.permit(:some_id, :deleted))
        end

        def to_csv_field(datum)
            custom_select_value = datum.get_master_value
            custom_select_value && custom_select_value.value || "#{NO_VALUE}(#{datum.some_id})"
        end

        def from_csv_field(datum, csv_string)
            custom_select_id = datum.get_field_def.field_param_id1

            custom_select_value = CustomSelectValue.get_value_by_custom_select_id_value(custom_select_id, csv_string)
            if custom_select_value.present?
                datum.some_id = custom_select_value.id
            end
        end

        alias_method :to_filter_value, :to_csv_field

        def to_public_field(datum)
            pub_field = Pub.data_class.where(id: datum.id).first
            if pub_field.blank?
                pub_field = Pub.data_class.new
            end

            custom_select_value = datum.get_master_value

            pub_field.value             = custom_select_value && custom_select_value.value || "#{NO_VALUE}"
            pub_field.caption_ja        = custom_select_value && custom_select_value.caption_ja || "#{NO_VALUE}"
            pub_field.caption_en        = custom_select_value && custom_select_value.caption_en || "#{NO_VALUE}"
            pub_field
        end

        def get_search_document_ids(datum, limit)
            return nil if blank_value?(datum)

            return data_class.where(field_def_id: datum.field_def_id)
                .where(some_id: datum.some_id)
                .limit(limit).pluck(:document_id)
        end

        def get_search_document_relation(relation, datum)
            relation = relation.joins(:doc_field3)

            relation = relation.where("(#{data_class.name.tableize}.field_def_id = ? AND #{data_class.name.tableize}.some_id = ?)", datum.field_def_id, datum.some_id) if datum.some_id.present?

            relation
        end

        def get_search_field_relation(relation, datum)
            relation = relation.where("(field_def_id = ? AND some_id = ?)", datum.field_def_id, datum.some_id) if datum.some_id.present?
            relation
        end

        def get_exist_values(datum, relation)
            return [] if blank_value?(datum)

            data_class.where(document_id: relation, field_def_id: datum.field_def_id).group(:some_id).count
        end

        def get_all_exist_values(datum)
            return [] if blank_value?(datum)

            relation = data_class.joins(:document).where('pri_documents.deleted = false')

            get_search_field_relation(relation, datum).group(:some_id).count
        end

        # get_exist_values と 対応
        def new_count_field(value)
            field = data_class.new
            field.some_id = value.to_i
            field
        end

        def update_values(datum, relation, search_field)
            if datum.some_id.present?
                data_class.where(document_id: relation, field_def_id: datum.field_def_id)
                    .update_all(some_id: datum.some_id)
            end
        end

        def update_field_values(datum, relation, new_field)
            update_value = {}

            relation = get_search_field_relation(data_class.includes(:document), datum)

            if new_field.some_id.present?
                update_value[:some_id] = new_field.some_id
                update_field_values_and_update_document(relation, update_value)
            else
                destory_field_values_and_update_document(relation, update_value)
            end
        end

        def data_class
            'PriFieldNumberDatum'.constantize
        end

        def get_view_dir
            'pri_field_simple_select'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end


    module Pub
        include FieldTypes::FieldTypeBase::Pub
        include Common

        def get_key_name
            'value'
        end

        def get_master_value(datum)
            return nil if datum.value.blank?
            CustomSelectValue.get_value_by_custom_select_id_value(datum.get_field_def.field_param_id1, datum.value)
        end

        def get_list_caption_ja(datum)
            custom_select_value = datum.get_master_value

            if custom_select_value == nil
                caption = datum.caption_ja
                caption = datum.caption_en if caption.blank?
                caption
            else
                caption = custom_select_value.caption_ja
                caption = custom_select_value.caption_en if caption.blank?
                caption
            end
        end
        def get_list_caption_en(datum)
            custom_select_value = datum.get_master_value

            if custom_select_value == nil
                caption = datum.caption_en
                caption = datum.caption_ja if caption.blank?
                caption
            else
                caption = custom_select_value.caption_en
                caption = custom_select_value.caption_ja if caption.blank?
                caption
            end
        end

        def get_list_param(datum)
            datum.value
        end

        def get_list_param_prefix_ja(datum)
            get_list_param(datum)[0] || ''
        end

        alias_method :get_list_param_prefix_en, :get_list_param_prefix_ja

        def get_search_values(datum)
            values = [datum.value, datum.caption_ja, datum.caption_en]

            custom_select_value = datum.get_master_value

            if custom_select_value != nil
                values << custom_select_value.value
                values << custom_select_value.caption_ja
                values << custom_select_value.caption_en
            end
            values.reject{|a|a.blank?}.uniq
        end

        def blank_value?(datum)
            datum.value.blank?
        end

        def to_csv_field(datum)
            datum.value
        end

        alias_method :to_filter_value, :to_csv_field

        def data_class
            'PubFieldValueCaptionDatum'.constantize
        end

        def get_view_dir
            'pub_field_simple_select'
        end

        extend(self)
        def self.included(base)
            base.extend(self)
        end
    end

end
