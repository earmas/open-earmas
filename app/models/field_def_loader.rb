#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module FieldDefLoader

    class Config

        attr_accessor :field_defs, :result_field_defs, :detail_field_defs
        attr_accessor :field_def_map

        attr_accessor :field_defs_by_field_type_id_map
        attr_accessor :field_defs_by_code_map

        attr_accessor :title_field_def, :csv_fields

        attr_accessor :input_groups, :input_group_map, :input_group_map_by_name
        attr_accessor :input_group_fields_by_input_group_id_map
        attr_accessor :input_group_field_by_field_def_id_by_input_group_id_map

        attr_accessor :field_defs_by_field_type_id_by_input_group_id_map
        attr_accessor :field_defs_by_code_by_input_group_id_map

        attr_accessor :field_defs_by_input_group_id_map, :result_field_defs_by_input_group_id_map, :detail_field_defs_by_input_group_id_map
        attr_accessor :title_field_def_by_input_group_id_map

        def set_field_defs
            return if @field_defs != nil
#            Delayed::Worker.logger.debug("Create Field Def Loader field def" + self.to_s)

            @field_defs = FieldDef.all.to_a
            @result_field_defs = []
            @detail_field_defs = []

            @field_def_map = {}

            @field_defs_by_field_type_id_map = {}
            @field_defs_by_code_map = {}

            @field_defs.each do |field_def|
                @field_def_map[field_def.id] = field_def

                @field_defs_by_field_type_id_map[field_def.field_type_id] ||= []
                @field_defs_by_field_type_id_map[field_def.field_type_id] << field_def
                @field_defs_by_code_map[field_def.code] ||= []
                @field_defs_by_code_map[field_def.code] << field_def

                if field_def.result
                    @result_field_defs << field_def
                end
                if field_def.detail
                    @detail_field_defs << field_def
                end
            end
            @title_field_def = @field_defs.first

            @input_groups = InputGroup.all.includes(:fields).to_a

            @input_group_map = {}
            @input_group_map_by_name = {}
            @input_group_fields_by_input_group_id_map = {}
            @input_group_field_by_field_def_id_by_input_group_id_map = {}

            @field_defs_by_field_type_id_by_input_group_id_map = {}
            @field_defs_by_code_by_input_group_id_map = {}

            @field_defs_by_input_group_id_map = {}
            @result_field_defs_by_input_group_id_map = {}
            @detail_field_defs_by_input_group_id_map = {}

            @title_field_def_by_input_group_id_map = {}



            @input_groups.each do |input_group|
                @input_group_map[input_group.id] = input_group
                @input_group_map_by_name[input_group.name] = input_group
                @input_group_fields_by_input_group_id_map[input_group.id] = []
                @input_group_field_by_field_def_id_by_input_group_id_map[input_group.id] = {}

                @field_defs_by_field_type_id_by_input_group_id_map[input_group.id] = {}
                @field_defs_by_code_by_input_group_id_map[input_group.id] = {}

                @field_defs_by_input_group_id_map[input_group.id] = []
                @result_field_defs_by_input_group_id_map[input_group.id] = []
                @detail_field_defs_by_input_group_id_map[input_group.id] = []

                input_group.fields.each do |field|
                    field_def = @field_def_map[field.field_def_id]
                    next if field_def == nil

                    @input_group_fields_by_input_group_id_map[input_group.id] << field
                    @input_group_field_by_field_def_id_by_input_group_id_map[input_group.id][field.field_def_id] = field

                    @field_defs_by_field_type_id_by_input_group_id_map[input_group.id][field_def.field_type_id] ||= []
                    @field_defs_by_field_type_id_by_input_group_id_map[input_group.id][field_def.field_type_id] << field_def
                    @field_defs_by_code_by_input_group_id_map[input_group.id][field_def.code] ||= []
                    @field_defs_by_code_by_input_group_id_map[input_group.id][field_def.code] << field_def

                    @field_defs_by_input_group_id_map[input_group.id] << @field_def_map[field.field_def_id]

                    if field.result
                        @result_field_defs_by_input_group_id_map[input_group.id] << @field_def_map[field.field_def_id]
                    end
                    if field.detail
                        @detail_field_defs_by_input_group_id_map[input_group.id] << @field_def_map[field.field_def_id]
                    end
                end

                @title_field_def_by_input_group_id_map[input_group.id] = @field_defs_by_input_group_id_map[input_group.id].first
            end
        end
    end

    class << self
        def config
            Thread.current[:earmas_field_def_loader_config] ||= Config.new
        end

        def clear
            Thread.current[:earmas_field_def_loader_config] = nil
            Thread.current[:earmas_field_def_loader_for_input_config] = nil
            Thread.current[:earmas_custom_select_value] = nil
            Thread.current[:earmas_view_state] = nil
            Thread.current[:earmas_sortable_field] = nil
            Thread.current[:earmas_left_menu] = nil
            Thread.current[:earmas_person] = nil
        end

        def get_title_field
            config.set_field_defs
            config.title_field_def
        end

        def set_csv_fields
            return if config.csv_fields != nil
            config.csv_fields = field_defs.delete_if {|a| a.csv_column == 0 }.sort{|x,y| x.csv_column <=> y.csv_column }
        end

        def get_field_def(field_def_id)
            field_def_map[field_def_id.to_i]
        end

        def get_field_def_by_code(field_def_code)
            config.set_field_defs
            config.field_defs_by_code_map[field_def_code]
        end

        def field_def_map
            config.set_field_defs
            config.field_def_map
        end

        def field_defs
            config.set_field_defs
            config.field_defs
        end

        def title_field_def_by_input_group_field(input_group_id)
            config.set_field_defs
            config.title_field_def_by_input_group_id_map[input_group_id.to_i] || config.title_field_def
        end

        def result_field_defs
            config.set_field_defs
            config.result_field_defs
        end

        def detail_field_defs
            config.set_field_defs
            config.detail_field_defs
        end

        def csv_fields
            set_csv_fields
            config.csv_fields
        end

        def get_input_group(input_group_id)
            input_group_map[input_group_id.to_i]
        end

        def get_input_group_map_by_name(input_group_name)
            input_group_map_by_name[input_group_name.to_s]
        end

        def input_group_fields(input_group_id)
            config.set_field_defs
            config.input_group_fields_by_input_group_id_map[input_group_id.to_i]
        end

        def input_group_field_by_field_def_id(input_group_id, field_def_id)
            config.set_field_defs
            input_group_field_by_field_def_id_map = config.input_group_field_by_field_def_id_by_input_group_id_map[input_group_id.to_i]
            input_group_field_by_field_def_id_map && input_group_field_by_field_def_id_map[field_def_id.to_i]
        end

        def field_defs_by_input_group_id(input_group_id)
            config.set_field_defs
            config.field_defs_by_input_group_id_map[input_group_id.to_i] || field_defs
        end

        def detail_field_defs_by_input_group_id(input_group_id)
            config.set_field_defs
            config.detail_field_defs_by_input_group_id_map[input_group_id.to_i] || detail_field_defs
        end

        def result_field_defs_by_input_group_id(input_group_id)
            config.set_field_defs
            config.result_field_defs_by_input_group_id_map[input_group_id.to_i] || result_field_defs
        end

        def field_defs_by_field_type_id_by_input_group_id(input_group_id, field_type_id)
            config.set_field_defs
            result = config.field_defs_by_field_type_id_by_input_group_id_map[input_group_id]
            if result.blank?
                config.field_defs_by_field_type_id_map[field_type_id.to_i]
            else
                result[field_type_id.to_i]
            end
        end

        def field_defs_by_code_by_input_group_id(input_group_id, code)
            config.set_field_defs
            result = config.field_defs_by_code_by_input_group_id_map[input_group_id]
            if result.blank?
                config.field_defs_by_code_map[code.to_s]
            else
                result[code.to_s]
            end
        end

        def input_group_map
            config.set_field_defs
            config.input_group_map
        end

        def input_group_map_by_name
            config.set_field_defs
            config.input_group_map_by_name
        end

        def input_groups
            config.set_field_defs
            config.input_groups
        end

    end

end
