#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class DocumentTemplateField < ActiveRecord::Base

    belongs_to :field_def
    belongs_to :document_template

    def get_document_field
        return @document_field if @document_field != nil

        @document_field = get_field_def.default_pri_field
        @document_field.from_csv_field(default_value)
        @document_field
    end

    def update_by_document_field
        return if @document_field == nil

        self.default_value = @document_field.to_csv_field
    end

    def get_field_def
        FieldDefLoaderForInput.get_field_def(field_def_id)
    end

end
