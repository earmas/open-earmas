#--
# Open Earmas
# 
# Copyright (C) 2014 ENU Technologies
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

class CustomSelectValue < ActiveRecord::Base

    class ThreadValue
        attr_accessor :value_map
        attr_accessor :value_by_value_map

        def set_value_map
            return if @value_map != nil
            @value_map = {}
            @value_by_value_map = {}
            CustomSelectValue.all.each do |custom_select_value|
                @value_map[custom_select_value.id] = custom_select_value
                @value_by_value_map[custom_select_value.custom_select_id.to_i] ||= {}
                @value_by_value_map[custom_select_value.custom_select_id.to_i][custom_select_value.value.to_s] = custom_select_value
            end
        end
    end


    class << self
        def thread_cache
            Thread.current[:earmas_custom_select_value] ||= CustomSelectValue::ThreadValue.new
        end

        def get_value(id)
            thread_cache.set_value_map
            thread_cache.value_map[id.to_i]
        end
        def get_value_by_custom_select_id_value(custom_select_id, value)
            thread_cache.set_value_map
            thread_cache.value_by_value_map[custom_select_id.to_i] && thread_cache.value_by_value_map[custom_select_id.to_i][value.to_s]
        end
    end

    default_scope { order(:custom_select_id, :sort_index) }

    validates :value, presence: true
    validates :caption_ja, presence: true, :unless => :caption_en?
    validates :caption_en, presence: true, :unless => :caption_ja?

    validate  :duplicate_value

private

    def duplicate_value
        if CustomSelectValue.where.not(id: id).where(custom_select_id: custom_select_id, value: value).size != 0
            errors.add(:value, "の値が重複しています")
        end
    end

end
