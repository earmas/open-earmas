# coding: utf-8
include Warden::Test::Helpers

module DataInitMacros

    def create_admin
        staff = Staff.new
        staff.email = 'admin@enut.jp'
        staff.password = 'abcdefgh'
        staff.has_admin_role = true
        staff.save!

        staff
    end

    def create_staff
        staff = Staff.new
        staff.email = 'staff@enut.jp'
        staff.password = 'abcdefgh'
        staff.has_admin_role = false
        staff.save!

        staff
    end

    def create_staff_data
        @staff1 = create_admin
        @staff2 = create_staff
    end


    def create_field_data

        @custom_selects = []
        @custom_select_values = []

        custom_select_1 = CustomSelect.new
        custom_select_1.name = 'NDC'
        custom_select_1.save!
        @custom_selects << custom_select_1

        custom_select_value = CustomSelectValue.new
        custom_select_value.custom_select_id = custom_select_1.id
        custom_select_value.value = '000'
        custom_select_value.caption_ja = '総記'
        custom_select_value.caption_en = '総記'
        custom_select_value.sort_index = 1
        custom_select_value.save!
        @custom_select_values << custom_select_value

        custom_select_value = CustomSelectValue.new
        custom_select_value.custom_select_id = custom_select_1.id
        custom_select_value.value = '007'
        custom_select_value.caption_ja = '情報科学'
        custom_select_value.caption_en = '情報科学'
        custom_select_value.sort_index = 2
        custom_select_value.save!
        @custom_select_values << custom_select_value


        custom_select_2 = CustomSelect.new
        custom_select_2.name = '言語'
        custom_select_2.save!
        @custom_selects << custom_select_2

        custom_select_value = CustomSelectValue.new
        custom_select_value.custom_select_id = custom_select_2.id
        custom_select_value.value = 'jpn'
        custom_select_value.caption_ja = '日本語'
        custom_select_value.caption_en = 'japanese'
        custom_select_value.sort_index = 1
        custom_select_value.save!
        @custom_select_values << custom_select_value

        custom_select_value = CustomSelectValue.new
        custom_select_value.custom_select_id = custom_select_2.id
        custom_select_value.value = 'eng'
        custom_select_value.caption_ja = '英語'
        custom_select_value.caption_en = 'english'
        custom_select_value.sort_index = 2
        custom_select_value.save!
        @custom_select_values << custom_select_value


        custom_select_3 = CustomSelect.new
        custom_select_3.name = 'NII Type'
        custom_select_3.save!
        @custom_selects << custom_select_3

        custom_select_value = CustomSelectValue.new
        custom_select_value.custom_select_id = custom_select_3.id
        custom_select_value.value = 'Journal Articles'
        custom_select_value.caption_ja = '学術雑誌論文'
        custom_select_value.caption_en = 'Journal Article'
        custom_select_value.sort_index = 1
        custom_select_value.save!
        @custom_select_values << custom_select_value

        custom_select_value = CustomSelectValue.new
        custom_select_value.custom_select_id = custom_select_3.id
        custom_select_value.value = 'Depertmental Bulletin Papers'
        custom_select_value.caption_ja = '学内刊行物(紀要等)'
        custom_select_value.caption_en = 'Depertmental Bulletin Paper'
        custom_select_value.sort_index = 2
        custom_select_value.save!
        @custom_select_values << custom_select_value

        custom_select_value = CustomSelectValue.new
        custom_select_value.custom_select_id = custom_select_3.id
        custom_select_value.value = 'dummy'
        custom_select_value.caption_ja = 'ダミー'
        custom_select_value.caption_en = 'dummy'
        custom_select_value.sort_index = 3
        custom_select_value.save!
        @custom_select_values << custom_select_value





        field_data = [
            [ 4, 'file',                        '本文ファイル',             'file',                        '本文ファイル',              false, true,  false, 1, 1, ';'  ],
            [ 1, 'title',                       'タイトル',                 'title',                       'タイトル',                 true,  false, true,  2, 2  ],
            [ 1, 'title_alternative',           '別タイトル',               'title alternative',           '別タイトル',                false, true,  false, 3, 3  ],
            [ 1, 'title_transcription',         'タイトルヨミ',             'title transcription',         'タイトルヨミ',              false, false, false, 4, 4  ],
            [ 7, 'creator',                     '著者',                     'creator',                     '著者',                     true,  true,  true,  5, 5, ';'  ],
            [ 1, 'subject',                     'キーワード',               'subject',                     'キーワード',                false, true,  false, 6, 6  ],
            [ 3, 'subject_ndc',                 'NDC',                      'NDC',                         'NDC',                      false, true,  false, 7, 7, nil, custom_select_1.id ],
            [ 5, 'description_abstract',        '抄録',                     'abstract',                    '抄録',                      false, true,  false, 8, 8  ],
            [ 5, 'description_tableofcontents', '目次',                     'tableofcontents',             '目次',                      false, true,  false, 9, 9  ],
            [ 5, 'description',                 '内容記述',                 'description',                 '内容記述',                  false, true,  false, 10, 10  ],
            [ 1, 'description_peerreviewed',    '査読の有無',               'peerreviewed',                '査読の有無',                false, true,  false, 11, 11  ],
            [ 1, 'identifier',                  '識別子',                   'identifier',                  '識別子',                    false, true,  false, 12, 12  ],
            [ 1, 'identifier_jtitle',           '掲載雑誌名',               'journal title',               '掲載雑誌名',                false, true,  true,  13, 13  ],
            [ 1, 'identifier_volume',           '巻',                       'volume',                      '巻',                        false, true,  true,  14, 14  ],
            [ 1, 'identifier_issue',            '号',                       'issue',                       '号',                        false, true,  true,  15, 15  ],
            [ 1, 'identifier_spage',            '開始ページ',               'start page',                  '開始ページ',                false, true,  true,  16, 16  ],
            [ 1, 'identifier_epage',            '終了ページ',               'end page',                    '終了ページ',                false, true,  true,  17, 17  ],
            [ 1, 'data_issued',                 '出版年月日',               'data of issued',              '出版年月日',                false, true,  true,  18, 18  ],
            [ 1, 'publisher',                   '出版者',                   'publisher',                   '出版者',                    false, true,  true,  19, 19  ],
            [ 1, 'publisher_transcription',     '出版者ヨミ',               'publisher transcription',     '出版者ヨミ',                false, true,  false, 20, 20  ],
            [ 1, 'contributor',                 '寄与者',                   'contributor',                 '寄与者',                    false, true,  false, 21, 21  ],
            [ 1, 'contributor_transcription',   '寄与者ヨミ',               'contributor transcription',   '寄与者ヨミ',                false, true,  false, 22, 22  ],
            [ 1, 'data_created',                '作成年月日',               'date of created',             '作成年月日',                false, true,  false, 23, 23  ],
            [ 1, 'date_modified',               '更新年月日',               'date of modified',            '更新年月日',                false, true,  false, 24, 24  ],
            [ 1, 'identifier_issn',             'ISSN',                     'issn',                        'ISSN',                      false, true,  false, 25, 25  ],
            [ 1, 'identifier_isbn',             'ISBN',                     'isbn',                        'ISBN',                      false, true,  false, 26, 26  ],
            [ 1, 'identifier_ncid',             'NCID',                     'ncid',                        'NCID',                      false, true,  false, 27, 27  ],
            [ 1, 'identifier_doi',              'DOI',                      'doi',                         'DOI',                       false, true,  false, 28, 28  ],
            [ 1, 'identifier_self_doi',         'SelfDOI',                  'self doi',                    'SelfDOI',                   false, true,  false, 29, 29  ],
            [ 1, 'identifier_naid',             'NAID',                     'naid',                        'NAID',                      false, true,  false, 30, 30  ],
            [ 1, 'identifier_pmid',             'PubMedID',                 'pubmed id',                   'PubMedID',                  false, true,  false, 31, 31  ],
            [ 1, 'identifier_ichushi',          '医中誌ID',                 'ichushi id',                  '医中誌ID',                  false, true,  false, 32, 32  ],
            [ 3, 'language',                    '言語',                     'language',                    '言語',                      false, true,  false, 33, 33, nil, custom_select_2.id ],
            [ 3, 'niitype',                     'NII資源タイプ',            'nii type',                    'NII資源タイプ',             false, true,  false, 34, 34, nil, custom_select_3.id ],
            [ 1, 'DCMItype',                    'DMCIタイプ',               'DCMI type',                   'DMCIタイプ',                false, true,  false, 35, 35  ],
            [ 1, 'format',                      'フォーマット',             'format',                      'フォーマット',              false, true,  false, 36, 36  ],
            [ 1, 'textversion',                 '著者版フラグ',             'text version',                '著者版フラグ',              false, true,  false, 37, 37  ],
            [ 1, 'rights',                      '権利情報',                 'rights',                      '権利情報',                  false, true,  false, 38, 38  ],
            [ 1, 'relation',                    '関連情報',                 'relation',                    '関連情報',                  false, true,  false, 39, 39  ],
            [ 1, 'relation_url',                '関連情報URL',              'relation url',                '関連情報URL',               false, true,  false, 40, 40  ],
            [ 1, 'relation_isversionof',        '関連情報(IsVersionOf)',    'relation is version of',      '関連情報(IsVersionOf)',     false, true,  false, 41, 41  ],
            [ 1, 'relation_isversionofURL',     '関連情報URL(IsVersionOf)', 'relation is version of URL',  '関連情報URL(IsVersionOf)',  false, true,  false, 42, 42  ],
            [ 1, 'relation_hasversion',         '関連情報(HasVersion)',     'relation is version of URL',  '関連情報(HasVersion)',      false, true,  false, 43, 43  ],
            [ 1, 'relation_hasversionURL',      '関連情報URL(HasVersion)',  'relation has version URL',    '関連情報URL(HasVersion)',   false, true,  false, 44, 44  ],
            [ 1, 'relation_haspart',            '関連情報(hasPart)',        'relation has part',           '関連情報(hasPart)',         false, true,  false, 45, 45  ],
            [ 1, 'relation_haspartURL',         '関連情報URL(hasPart)',     'relation has part URL',       '関連情報URL(hasPart)',      false, true,  false, 46, 46  ],
            [ 1, 'relation_references',         '関連情報(references)',     'relation references',         '関連情報(references)',      false, true,  false, 47, 47  ],
            [ 1, 'relation_referencesURL',      '関連情報URL(references)',  'relation references URL',     '関連情報URL(references)',   false, true,  false, 48, 48  ],
            [ 1, 'relation_ispartof',           '関連情報(isPartOf)',       'relation is part of',         '関連情報(isPartOf)',        false, true,  false, 49, 49  ],
            [ 1, 'relation_ispartofURL',        '関連情報URL(isPartOf)',    'relation is part of URL',     '関連情報URL(isPartOf)',     false, true,  false, 50, 50  ],
            [ 1, 'identifier_grantid',          '学位論文ID',               'grantid',                     '学位論文ID',                false, true,  false, 51, 51  ],
            [ 1, 'description_degreeGrantor',   '授与大学',                 'degreeGrantor',               '授与大学',                  false, true,  false, 52, 52  ],
            [ 1, 'description_degreenameJ',     '学位名',                   'degreename',                  '学位名',                    false, true,  false, 53, 53  ],
            [ 1, 'description_degreelevel',     '学位の種類の英名',          'degreelevel',                 '学位の種類の英名',          false, true,  false, 54, 54  ],
            [ 1, 'data_granted',                '学位授与年月日',            'data of granted',             '学位授与年月日',            false, true,  false, 55, 55  ],
            [ 1, 'note',                        'ローカル備考',              'note',                        'ローカル備考',              false, true,  false, 56, 56, nil, nil, false ],
        ]





        @field_map = {}
        fields = []
        @fields = fields
        field_data.each do |field_datum|

            field_def = FieldDef.new
            field_def.field_type_id     = field_datum[0]
            field_def.code              = field_datum[1]
            field_def.caption_ja        = field_datum[2]
            field_def.caption_en        = field_datum[3]
            field_def.description       = field_datum[4]

            field_def.must              = field_datum[5]
            field_def.multi             = field_datum[6]
            field_def.result            = field_datum[7]
            field_def.multi_field_delim = ' '

            field_def.csv_column        = field_datum[8]
            field_def.sort_index        = field_datum[9]

            field_def.in_field_delim    = field_datum[10] if field_datum[10].present?
            field_def.field_param_id1   = field_datum[11] if field_datum[11].present?

            field_def.to_pub            = field_datum[12] if field_datum[12] != nil
        
            begin
                field_def.save!
            rescue Exception => e
                   p field_def.code
                field_def.errors.full_messages.each do |msg|
                   p msg
                end
                raise e
            end

            @field_map[field_def.code] = field_def
            fields << field_def
        end





        list_def = ListDef.new
        list_def.code = "journal"
        list_def.caption_ja = "雑誌一覧"
        list_def.caption_en = "journal list"
        list_def.result_view_type_id = 1
        list_def.result_sub_header_view_type_id = 1
        list_def.result_sub_footer_view_type_id = 1
        list_def.detail_view_type_id = 1
        list_def.detail_sub_header_view_type_id = 1
        list_def.detail_sub_footer_view_type_id = 1
        list_def.save!


        list_field_def = ListFieldDef.new
        list_field_def.field_def_id = fields[13 - 1].id
        list_field_def.list_def_id = list_def.id
        list_field_def.list_param_view_type_id = 1
        list_field_def.list_param_sub_header_view_type_id = 1
        list_field_def.list_param_sub_footer_view_type_id = 1
        list_field_def.sort_index = 1
        list_field_def.save!

        list_field_def = ListFieldDef.new
        list_field_def.field_def_id = fields[14 - 1].id
        list_field_def.list_def_id = list_def.id
        list_field_def.list_param_view_type_id = 1
        list_field_def.list_param_sub_header_view_type_id = 1
        list_field_def.list_param_sub_footer_view_type_id = 1
        list_field_def.sort_index = 2
        list_field_def.save!

        list_field_def = ListFieldDef.new
        list_field_def.field_def_id = fields[15 - 1].id
        list_field_def.list_def_id = list_def.id
        list_field_def.list_param_view_type_id = 1
        list_field_def.list_param_sub_header_view_type_id = 1
        list_field_def.list_param_sub_footer_view_type_id = 1
        list_field_def.sort_index = 3
        list_field_def.save!


        list_def = ListDef.new
        list_def.code = "creator"
        list_def.caption_ja = "著者一覧"
        list_def.caption_en = "auther list"
        list_def.result_view_type_id = 1
        list_def.result_sub_header_view_type_id = 1
        list_def.result_sub_footer_view_type_id = 1
        list_def.detail_view_type_id = 1
        list_def.detail_sub_header_view_type_id = 1
        list_def.detail_sub_footer_view_type_id = 1
        list_def.save!


        list_field_def = ListFieldDef.new
        list_field_def.field_def_id = fields[5 - 1].id
        list_field_def.list_def_id = list_def.id
        list_field_def.list_param_view_type_id = 1
        list_field_def.list_param_sub_header_view_type_id = 1
        list_field_def.list_param_sub_footer_view_type_id = 1
        list_field_def.sort_index = 1
        list_field_def.save!


        list_def = ListDef.new
        list_def.code = "niitype"
        list_def.caption_ja = "資料タイプ一覧"
        list_def.caption_en = "nii type list"
        list_def.result_view_type_id = 1
        list_def.result_sub_header_view_type_id = 1
        list_def.result_sub_footer_view_type_id = 1
        list_def.detail_view_type_id = 1
        list_def.detail_sub_header_view_type_id = 1
        list_def.detail_sub_footer_view_type_id = 1
        list_def.save!


        list_field_def = ListFieldDef.new
        list_field_def.field_def_id = fields[34 - 1].id
        list_field_def.list_def_id = list_def.id
        list_field_def.list_param_view_type_id = 1
        list_field_def.list_param_sub_header_view_type_id = 1
        list_field_def.list_param_sub_footer_view_type_id = 1
        list_field_def.sort_index = 1
        list_field_def.save!




        search_def = SearchDef.new
        search_def.code = "title"
        search_def.caption_ja = "タイトル"
        search_def.caption_en = "title"
        search_def.description = "タイトル"
        search_def.sort_index = 1
        search_def.save!

        search_field_data = [fields[2 - 1].id, fields[3 - 1].id, fields[4 - 1].id]

        search_field_data.each do |search_field_datum|
            search_field_def = SearchFieldDef.new
            search_field_def.field_def_id = search_field_datum
            search_field_def.search_def_id = search_def.id
            search_field_def.save!
        end


        search_def = SearchDef.new
        search_def.code = "auther"
        search_def.caption_ja = "著者"
        search_def.caption_en = "auther"
        search_def.description = "著者"
        search_def.sort_index = 2
        search_def.save!

        search_field_data = [fields[5 - 1].id, fields[21 - 1].id]

        search_field_data.each do |search_field_datum|
            search_field_def = SearchFieldDef.new
            search_field_def.field_def_id = search_field_datum
            search_field_def.search_def_id = search_def.id
            search_field_def.save!
        end


        search_def = SearchDef.new
        search_def.code = "keyword"
        search_def.caption_ja = "キーワード"
        search_def.caption_en = "keyword"
        search_def.description = "キーワード"
        search_def.sort_index = 3
        search_def.save!

        search_field_data = [fields[6 - 1].id, fields[8 - 1].id, fields[9 - 1].id, fields[10 - 1].id]

        search_field_data.each do |search_field_datum|
            search_field_def = SearchFieldDef.new
            search_field_def.field_def_id = search_field_datum
            search_field_def.search_def_id = search_def.id
            search_field_def.save!
        end


        search_def = SearchDef.new
        search_def.code = "NDC"
        search_def.caption_ja = "NDC分類"
        search_def.caption_en = "NDC"
        search_def.description = "NDC分類"
        search_def.sort_index = 4
        search_def.save!

        search_field_data = [fields[7 - 1].id]

        search_field_data.each do |search_field_datum|
            search_field_def = SearchFieldDef.new
            search_field_def.field_def_id = search_field_datum
            search_field_def.search_def_id = search_def.id
            search_field_def.save!
        end


        search_def = SearchDef.new
        search_def.code = "Journal"
        search_def.caption_ja = "雑誌名"
        search_def.caption_en = "Journal Title"
        search_def.description = "雑誌名"
        search_def.sort_index = 5
        search_def.save!

        search_field_data = [fields[13 - 1].id]

        search_field_data.each do |search_field_datum|
            search_field_def = SearchFieldDef.new
            search_field_def.field_def_id = search_field_datum
            search_field_def.search_def_id = search_def.id
            search_field_def.save!
        end


        search_def = SearchDef.new
        search_def.code = "data"
        search_def.caption_ja = "出版年月日"
        search_def.caption_en = "data of issued"
        search_def.description = "出版年月日"
        search_def.sort_index = 6
        search_def.save!

        search_field_data = [fields[18 - 1].id, fields[23 - 1].id]

        search_field_data.each do |search_field_datum|
            search_field_def = SearchFieldDef.new
            search_field_def.field_def_id = search_field_datum
            search_field_def.search_def_id = search_def.id
            search_field_def.save!
        end


        search_def = SearchDef.new
        search_def.code = "publisher"
        search_def.caption_ja = "出版社"
        search_def.caption_en = "publisher"
        search_def.description = "出版社"
        search_def.sort_index = 7
        search_def.save!

        search_field_data = [fields[19 - 1].id]

        search_field_data.each do |search_field_datum|
            search_field_def = SearchFieldDef.new
            search_field_def.field_def_id = search_field_datum
            search_field_def.search_def_id = search_def.id
            search_field_def.save!
        end


        search_def = SearchDef.new
        search_def.code = "ISSN"
        search_def.caption_ja = "ISSN/ISBN"
        search_def.caption_en = "ISSN/ISBN"
        search_def.description = "ISSN/ISBN"
        search_def.sort_index = 8
        search_def.save!

        search_field_data = [fields[25 - 1].id,fields[26 - 1].id]

        search_field_data.each do |search_field_datum|
            search_field_def = SearchFieldDef.new
            search_field_def.field_def_id = search_field_datum
            search_field_def.search_def_id = search_def.id
            search_field_def.save!
        end


        search_def = SearchDef.new
        search_def.code = "ID"
        search_def.caption_ja = "ID類"
        search_def.caption_en = "kind of ID"
        search_def.description = "ID類"
        search_def.sort_index = 9
        search_def.save!

        search_field_data = [fields[27 - 1].id,fields[28 - 1].id,fields[29 - 1].id,fields[30 - 1].id,fields[31 - 1].id,fields[32 - 1].id]

        search_field_data.each do |search_field_datum|
            search_field_def = SearchFieldDef.new
            search_field_def.field_def_id = search_field_datum
            search_field_def.search_def_id = search_def.id
            search_field_def.save!
        end


        search_def = SearchDef.new
        search_def.code = "language"
        search_def.caption_ja = "言語"
        search_def.caption_en = "language"
        search_def.description = "言語"
        search_def.sort_index = 10
        search_def.save!

        search_field_data = [fields[33 - 1].id]

        search_field_data.each do |search_field_datum|
            search_field_def = SearchFieldDef.new
            search_field_def.field_def_id = search_field_datum
            search_field_def.search_def_id = search_def.id
            search_field_def.save!
        end


        search_def = SearchDef.new
        search_def.code = "GrantId"
        search_def.caption_ja = "学位授与番号"
        search_def.caption_en = "Grant ID"
        search_def.description = "学位授与番号"
        search_def.sort_index = 12
        search_def.save!

        search_field_data = [fields[51 - 1].id]

        search_field_data.each do |search_field_datum|
            search_field_def = SearchFieldDef.new
            search_field_def.field_def_id = search_field_datum
            search_field_def.search_def_id = search_def.id
            search_field_def.save!
        end


        search_def = SearchDef.new
        search_def.code = "DegreeName"
        search_def.caption_ja = "学位名"
        search_def.caption_en = "Degree Name"
        search_def.description = "学位名"
        search_def.sort_index = 13
        search_def.save!

        search_field_data = [fields[53 - 1].id, fields[54 - 1].id]

        search_field_data.each do |search_field_datum|
            search_field_def = SearchFieldDef.new
            search_field_def.field_def_id = search_field_datum
            search_field_def.search_def_id = search_def.id
            search_field_def.save!
        end



        search_def = SearchDef.new
        search_def.code = "data_granted"
        search_def.caption_ja = "学位授与年月日"
        search_def.caption_en = "data of granted"
        search_def.description = "資料タイプ"
        search_def.sort_index = 14
        search_def.save!

        search_field_data = [fields[55 - 1].id]

        search_field_data.each do |search_field_datum|
            search_field_def = SearchFieldDef.new
            search_field_def.field_def_id = search_field_datum
            search_field_def.search_def_id = search_def.id
            search_field_def.save!
        end

        @test_file = fixture_file_upload('spec/fixtures/sample1.pdf', 'application/pdf', true) # not work

        @field_data = {
            fields[1 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "title"=>"", "embargo"=>"" }, "0"=>{"title"=>"", "embargo"=>""}},
            fields[2 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
            fields[3 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"b"}, "0"=>{"value1"=>""}},
            fields[4 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"c"}, "0"=>{"value1"=>""}},
            fields[5 - 1].id.to_s =>{
                "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"a", "given_name_ja"=>"b", "family_name_en"=>"c", "given_name_en"=>"d", "family_name_tra"=>"", "given_name_tra"=>"",
                    family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                    family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
            },
            fields[6 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"d"}, "0"=>{"value1"=>""}},
            fields[7 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=>""}, "0"=>{"some_id"=>""}},
            fields[8 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"e"}, "0"=>{"value1"=>""}},
            fields[9 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"f"}, "0"=>{"value1"=>""}},
            fields[10 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"g"}, "0"=>{"value1"=>""}},
            fields[11 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[12 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[13 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
            fields[14 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[15 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[16 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[17 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[18 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[19 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[20 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[21 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[22 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[23 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[24 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[25 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[26 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[27 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[28 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[29 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[30 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[31 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[32 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[33 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[2]}, "0"=>{"some_id"=>""}},
            fields[34 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=>''}, "0"=>{"some_id"=>""}},
            fields[35 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[36 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[37 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[38 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[39 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[40 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[41 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[42 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[43 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[44 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[45 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[46 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[47 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[48 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[49 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[50 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[51 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[52 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[53 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[54 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[55 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[56 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}}
        }


        @field_data1 = {
            fields[1 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "title"=>"", "embargo"=>"" }, "0"=>{"title"=>"", "embargo"=>""}},
            fields[2 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
            fields[3 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"b"}, "0"=>{"value1"=>""}},
            fields[4 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"c"}, "0"=>{"value1"=>""}},
            fields[5 - 1].id.to_s =>{
                "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                    family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                    family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
            },
            fields[6 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"d"}, "0"=>{"value1"=>""}},
            fields[7 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=>""}, "0"=>{"some_id"=>""}},
            fields[8 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"e"}, "0"=>{"value1"=>""}},
            fields[9 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"f"}, "0"=>{"value1"=>""}},
            fields[10 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"g"}, "0"=>{"value1"=>""}},
            fields[11 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[12 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[13 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
            fields[14 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[15 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[16 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[17 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[18 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[19 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[20 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[21 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[22 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[23 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[24 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[25 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[26 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[27 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[28 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[29 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[30 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[31 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[32 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[33 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[2]}, "0"=>{"some_id"=>""}},
            fields[34 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=>''}, "0"=>{"some_id"=>""}},
            fields[35 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[36 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[37 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[38 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[39 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[40 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[41 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[42 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[43 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[44 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[45 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[46 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[47 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[48 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[49 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[50 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[51 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[52 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[53 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[54 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[55 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[56 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}}
        }


        @field_data_fail = {
            fields[1 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "title"=>"", "embargo"=>""}, "0"=>{"title"=>"", "embargo"=>""}},
            fields[2 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
            fields[3 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"b"}, "0"=>{"value1"=>""}},
            fields[4 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"c"}, "0"=>{"value1"=>""}},
            fields[5 - 1].id.to_s =>{
                "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                    family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                    family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
            },
            fields[6 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"d"}, "0"=>{"value1"=>""}},
            fields[7 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=>""}, "0"=>{"some_id"=>""}},
            fields[8 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"e"}, "0"=>{"value1"=>""}},
            fields[9 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"f"}, "0"=>{"value1"=>""}},
            fields[10 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"g"}, "0"=>{"value1"=>""}},
            fields[11 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[12 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[13 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
            fields[14 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[15 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[16 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[17 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[18 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[19 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[20 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[21 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[22 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[23 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[24 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[25 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[26 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[27 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[28 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[29 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[30 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[31 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[32 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[33 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[2]}, "0"=>{"some_id"=>""}},
            fields[34 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[4]}, "0"=>{"some_id"=>""}},
            fields[35 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[36 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[37 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[38 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[39 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[40 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[41 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[42 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[43 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[44 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[45 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[46 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[47 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[48 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[49 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[50 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[51 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[52 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[53 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[54 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[55 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            fields[56 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}}
        }
    end
end