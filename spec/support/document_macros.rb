# coding: utf-8
include Warden::Test::Helpers

module DocumentMacros

    def pri_document_found(document_id)
        clear_template_buffer
        get pri_item_path(document_id)
        response_success
        assert_template "layouts/application"
        assert_template "view_state/items/show"
    end

    def pri_document_not_found(document_id)
        clear_template_buffer
        expect{get pri_item_path(document_id)}.to raise_error ActiveRecord::RecordNotFound
    end

    def lim_document_found(document_id)
        clear_template_buffer
        get lim_item_path(document_id)
        response_success
        assert_template "layouts/application"
        assert_template "view_state/items/show"
    end

    def lim_document_not_found(document_id)
        clear_template_buffer
        expect{get lim_item_path(document_id)}.to raise_error ActiveRecord::RecordNotFound
    end

    def pub_document_found(document_id)
        clear_template_buffer
        get pub_item_path(document_id)
        response_success
        assert_template "layouts/application"
        assert_template "view_state/items/show"
    end

    def pub_document_not_found(document_id)
        clear_template_buffer
        expect{get pub_item_path(document_id)}.to raise_error ActiveRecord::RecordNotFound
    end

end