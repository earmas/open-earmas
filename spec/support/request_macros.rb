# coding: utf-8
include Warden::Test::Helpers

module RequestMacros

    def match_response(string, count = 1)
        if string == nil
            @response.should == nil
        else
            @response.body.scan(string).size.should == count
        end
    end

    def match_json_response(name, string, count = 1)
        if string == nil
            @body[name].should == nil 
        else
            @body[name].scan(string).size.should == count
        end
    end

    def response_success
        assert_response :success
        @response = response
    end

    def json_response_result_success
        assert_response :success
        @body = JSON.parse(response.body)
        @body['result'].should == 'success'
    end

    def json_response_result_fail
        assert_response :success
        @body = JSON.parse(response.body)
        @body['result'].should == 'fail'
    end


    def check_invalid_response
        assert_response :success
        match_response('不正な操作です。')
    end

    def check_unauthorized_response
        assert_response :unauthorized
    end

    def check_not_found_response
        assert_response :not_found
    end

    def check_locked_response
        assert_response :service_unavailable
        match_response('メンテナンス中です。')
    end

    def check_xml_locked_response
        assert_response :service_unavailable
        match_response('Service Unavailable')
    end



    def login_staff(staff)
        clear_template_buffer
        get root_path(locale: :ja)
        assert_response :success
        assert_template "view_state/root/index"

        clear_template_buffer
        post new_staff_session_path, staff: { email: staff.email, password: "abcdefgh" }
        assert_response :redirect
        assert_redirected_to staff_path

        clear_template_buffer
        get staff_path(locale: :ja)
        assert_response :success
        assert_template "pri_documents/index"
    end

    def logout_staff
        clear_template_buffer
        delete destroy_staff_session_path
        assert_response :redirect
        assert_redirected_to root_path

        clear_template_buffer
        get root_path(locale: :ja)
        assert_response :success
        assert_template "view_state/root/index"
    end

    def check_not_login
        assert_response :redirect
        assert_redirected_to new_account_session_path

        clear_template_buffer
        get new_account_session_path
        assert_response :success
        assert_template "layouts/devise"
        assert_template "devise/sessions/new"
    end

    def clear_template_buffer
        setup_subscriptions
    end

    def maintenance_mode_on
        clear_template_buffer
        put maintenance_mode_on_staff_system_settings_path
        assert_response :redirect
        assert_redirected_to staff_system_settings_path

        get staff_system_settings_path
        assert_template "layouts/staff"
        assert_template "system_settings/index"
    end

    def pri_reindex
        clear_template_buffer
        put pri_reindex_staff_system_settings_path
        assert_response :redirect
        assert_redirected_to staff_system_settings_path

        clear_template_buffer
        get staff_system_settings_path
        assert_template "layouts/staff"
        assert_template "system_settings/index"
    end

    def maintenance_mode_off
        clear_template_buffer
        put maintenance_mode_off_staff_system_settings_path
        assert_response :redirect
        assert_redirected_to staff_system_settings_path

        clear_template_buffer
        get staff_system_settings_path
        assert_template "layouts/staff"
        assert_template "system_settings/index"
    end

    def create_document(status, field_data)
        pri_document_data = { input_group_id: 0, publish_to: status }

        prev_doc_cont = PriDocument.all.size

        clear_template_buffer
        post staff_pri_documents_path(pri_document: pri_document_data, field: field_data)
        new_document = PriDocument.all.order(id: :desc).first

        assert_response :redirect
        assert_redirected_to staff_pri_document_path(id: new_document.id)

        clear_template_buffer
        get staff_pri_document_path(id: new_document.id)
        response_success
        assert_template "layouts/staff"
        assert_template "pri_documents/show"

        expect(PriDocument.all.size).to eq (prev_doc_cont + 1)

        new_document
    end

    def doc_first_field_id(document, id)
        document.get_fields(id).first.id
    end
    def update_document(document, status)
        pri_document_data = { input_group_id: 0, publish_to: status }

        updated_at = document.updated_at
        prev_doc_cont = PriDocument.all.size
        prev_doc_field_cont = document.get_all_fields.size

        first_id = nil
        field_3_value = nil
        field_33_value = nil
        document.get_all_fields.each do |field|
            field_3_value = field.value1 if field.field_def_id == @fields[3 - 1].id
            field_33_value = field.some_id if field.field_def_id == @fields[33 - 1].id
        end

        field_update_data = {
            @fields[1 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "title"=>"", "embargo"=>""}, "0"=>{"title"=>"", "embargo"=>""}},
            @fields[2 - 1].id.to_s =>{(doc_first_field_id(document, @fields[2 - 1].id)).to_s =>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
            @fields[3 - 1].id.to_s =>{(doc_first_field_id(document, @fields[3 - 1].id)).to_s =>{"deleted"=>"", "value1"=>"b"}, "0"=>{"value1"=>""}},
            @fields[4 - 1].id.to_s =>{(doc_first_field_id(document, @fields[4 - 1].id)).to_s =>{"deleted"=>"", "value1"=>"c"}, "0"=>{"value1"=>""}},
            @fields[5 - 1].id.to_s =>{
                (doc_first_field_id(document, @fields[5 - 1].id)).to_s =>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"a", "given_name_ja"=>"b", "family_name_en"=>"c", "given_name_en"=>"d", "family_name_tra"=>"", "given_name_tra"=>"",
                    family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                    family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
            },
            @fields[6 - 1].id.to_s =>{(doc_first_field_id(document, @fields[6 - 1].id)).to_s =>{"deleted"=>"", "value1"=>"d"}, "0"=>{"value1"=>""}},
            @fields[7 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=>""}, "0"=>{"some_id"=>""}},
            @fields[8 - 1].id.to_s =>{(doc_first_field_id(document, @fields[8 - 1].id)).to_s =>{"deleted"=>"", "value1"=>"e"}, "0"=>{"value1"=>""}},
            @fields[9 - 1].id.to_s =>{(doc_first_field_id(document, @fields[9 - 1].id)).to_s =>{"deleted"=>"", "value1"=>"f"}, "0"=>{"value1"=>""}},
            @fields[10 - 1].id.to_s =>{(doc_first_field_id(document, @fields[10 - 1].id)).to_s=>{"deleted"=>"", "value1"=>"g"}, "0"=>{"value1"=>""}},
            @fields[11 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[12 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[13 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[14 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[15 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[16 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[17 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[18 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[19 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[20 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[21 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[22 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[23 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[24 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[25 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[26 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[27 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[28 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[29 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[30 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[31 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[32 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[33 - 1].id.to_s =>{(doc_first_field_id(document, @fields[33 - 1].id)).to_s=>{"deleted"=>"", "some_id"=>@custom_select_values[3]}, "0"=>{"some_id"=>""}},
            @fields[34 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=>@custom_select_values[5]}, "0"=>{"some_id"=>""}},
            @fields[35 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[36 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[37 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[38 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[39 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[40 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[41 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[42 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[43 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[44 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[45 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[46 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[47 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[48 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[49 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[50 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[51 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[52 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[53 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[54 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>""}, "0"=>{"value1"=>""}},
            @fields[55 - 1].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
            @fields[56 - 1].id.to_s =>{(doc_first_field_id(document, @fields[56 - 1].id)).to_s=>{"deleted"=>"true", "value1"=>""}, "0"=>{"value1"=>""}}
        }

        put staff_pri_document_path(document.id, pri_document: pri_document_data, field: field_update_data)

        new_document = PriDocument.find(document.id)

        assert_response :redirect
        assert_redirected_to staff_pri_document_path(id: new_document.id)

        clear_template_buffer
        get staff_pri_document_path(id: new_document.id)
        response_success
        assert_template "layouts/staff"
        assert_template "pri_documents/show"

        expect(PriDocument.all.size).to eq (prev_doc_cont)
        expect(new_document.get_all_fields.size).to eq (prev_doc_field_cont + 1)

        new_field_3_value = nil
        new_field_33_value = nil
        new_document.get_all_fields.each do |field|
            new_field_3_value = field.value1 if field.field_def_id == @fields[3 - 1].id
            new_field_33_value = field.some_id if field.field_def_id == @fields[33 - 1].id
        end

        expect(new_document.updated_at).not_to eq (updated_at)
        expect(field_3_value).to eq (new_field_3_value)
        expect(field_33_value).not_to eq (new_field_33_value)

        new_document
    end

    def publish_document
        sleep 1
        put staff_publish_publish_path
    end

    def delete_document(document)
        prev_doc_cont = PriDocument.all.size

        delete staff_pri_document_path(document.id)

        assert_response :redirect
        assert_redirected_to staff_path

        clear_template_buffer
        get staff_path
        response_success
        assert_template "layouts/staff"
        assert_template "pri_documents/index"

        expect(PriDocument.all.size).to eq (prev_doc_cont - 1)
    end

    def change_oai_pmh_enable
        create_staff_data
        create_field_data
        login_staff(@staff1)

        maintenance_mode_on

        @system_setting = SystemSetting.all.first
        clear_template_buffer
        get edit_staff_system_setting_path(@system_setting.id)
        assert_template "layouts/staff"
        assert_template "system_settings/edit"

        clear_template_buffer
        put staff_system_setting_path(@system_setting.id ,system_setting: {enable_oaipmh: true} )
        assert_response :redirect
        assert_redirected_to staff_system_settings_path

        get staff_system_settings_path
        response_success
        assert_template "layouts/staff"
        assert_template "system_settings/index"


        @oaipmh_setting = OaipmhSetting.all.first

        clear_template_buffer
        get edit_staff_oaipmh_setting_path(@oaipmh_setting.id)
        response_success
        assert_template "layouts/staff"
        assert_template "oaipmh_settings/edit"

        clear_template_buffer
        put staff_oaipmh_setting_path(@oaipmh_setting.id, oaipmh_setting: {
            identifier_prefix: 'test/',
            repository_name: 'test pository',
            base_url: 'example.jp/oai/reauest',
            admin_email: 'test@enut.jp',
            set_spec_custom_select_id: @custom_selects[2].id,
            set_spec_default_value: 'abc'
        })
        assert_response :redirect
        assert_redirected_to staff_oaipmh_settings_path

        @oaipmh_setting = OaipmhSetting.all.first

        expect(@oaipmh_setting.identifier_prefix).to eq 'test/'
        expect(@oaipmh_setting.repository_name).to eq 'test pository'
        expect(@oaipmh_setting.base_url).to eq 'example.jp/oai/reauest'
        expect(@oaipmh_setting.admin_email).to eq 'test@enut.jp'
        expect(@oaipmh_setting.set_spec_default_value).to eq 'abc'

        pri_reindex
        maintenance_mode_off
    end

    def set_oai_pmh_filter(some_id, filter_value)
        clear_template_buffer
        post staff_oaipmh_filter_defs_path oaipmh_filter_def:{
            include_filter: true,
            filter_type: 1,
            some_id: some_id,
            filter_value: filter_value
        }
        assert_response :redirect
        assert_redirected_to staff_oaipmh_settings_path

        get staff_oaipmh_settings_path
        response_success
        assert_template "layouts/staff"
        assert_template "oaipmh_settings/index"
    end

end