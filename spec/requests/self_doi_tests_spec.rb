require 'spec_helper'

describe "Self DOI" do

    def change_oai_pmh_setting
        create_staff_data
        create_field_data
        login_staff(@staff1)

        maintenance_mode_on

        @system_setting = SystemSetting.all.first
        clear_template_buffer
        get edit_staff_system_setting_path(@system_setting.id)
        assert_template "layouts/staff"
        assert_template "system_settings/edit"

        clear_template_buffer
        put staff_system_setting_path(@system_setting.id ,system_setting: {enable_oaipmh: true} )
        assert_response :redirect
        assert_redirected_to staff_system_settings_path

        get staff_system_settings_path
        response_success
        assert_template "layouts/staff"
        assert_template "system_settings/index"


        @oaipmh_setting = OaipmhSetting.all.first

        clear_template_buffer
        get edit_staff_oaipmh_setting_path(@oaipmh_setting.id)
        response_success
        assert_template "layouts/staff"
        assert_template "oaipmh_settings/edit"

        clear_template_buffer
        put staff_oaipmh_setting_path(@oaipmh_setting.id, oaipmh_setting: {
            identifier_prefix: 'test/',
            repository_name: 'test pository',
            base_url: 'example.jp/oai/reauest',
            admin_email: 'test@enut.jp',
            set_spec_custom_select_id: @custom_selects[2].id,
            set_spec_default_value: 'abc'
        })
        assert_response :redirect
        assert_redirected_to staff_oaipmh_settings_path

        @oaipmh_setting = OaipmhSetting.all.first

        expect(@oaipmh_setting.identifier_prefix).to eq 'test/'
        expect(@oaipmh_setting.repository_name).to eq 'test pository'
        expect(@oaipmh_setting.base_url).to eq 'example.jp/oai/reauest'
        expect(@oaipmh_setting.admin_email).to eq 'test@enut.jp'
        expect(@oaipmh_setting.set_spec_default_value).to eq 'abc'

        pri_reindex







        oaipmh_setting = {
            enable_self_jalc_doi: true,
            self_doi_jalc_prefix: '123/',
            enable_self_crossref_doi: false,
            self_doi_crossref_prefix: '',
            title_field_def_id: @field_map['title'].id,
            creator_field_def_id:  @field_map['creator'].id,
            publisher_field_def_id: nil,
            issn_field_def_id: nil,
            isbn_field_def_id: nil,
            jtitle_field_def_id: nil,
            start_page_field_def_id: @field_map['identifier_spage'].id,
            nii_type_field_def_id:  @field_map['niitype'].id
        }


        clear_template_buffer
        patch self_doi_staff_oaipmh_setting_path(@oaipmh_setting.id, oaipmh_setting: oaipmh_setting )
        assert_response :redirect
        assert_redirected_to staff_oaipmh_settings_url

        @oaipmh_setting.reload

        maintenance_mode_off
    end

    def check_self_doi_document
        sleep 1
        post staff_self_doi_check_path
    end

    def create_doi_document(status, field_data, self_doi_type, self_doi_jalc_manu_input, self_doi_jalc_auto_input, self_doi_crossref_auto_input)
        pri_document_data = { 
            input_group_id: 0, publish_to: status, self_doi_type: self_doi_type,
            self_doi_jalc_manu_input: self_doi_jalc_manu_input,
            self_doi_jalc_auto_input: self_doi_jalc_auto_input,
            self_doi_crossref_auto_input: self_doi_crossref_auto_input,
        }
        pri_document_data.reject! {|k,v| v == nil }

        prev_doc_cont = PriDocument.all.size

        clear_template_buffer
        post staff_pri_documents_path(pri_document: pri_document_data, field: field_data)
        new_document = PriDocument.all.order(id: :desc).first

        assert_response :redirect
        assert_redirected_to staff_pri_document_path(id: new_document.id)

        clear_template_buffer
        get staff_pri_document_path(id: new_document.id)
        response_success
        assert_template "layouts/staff"
        assert_template "pri_documents/show"

        expect(PriDocument.all.size).to eq (prev_doc_cont + 1)

        new_document
    end

    def update_doi_document(document, status, field_data, self_doi_type, self_doi_jalc_manu_input, self_doi_jalc_auto_input, self_doi_crossref_auto_input)
        pri_document_data = {
            input_group_id: 0, publish_to: status, self_doi_type: self_doi_type,
            self_doi_jalc_manu_input: self_doi_jalc_manu_input,
            self_doi_jalc_auto_input: self_doi_jalc_auto_input,
            self_doi_crossref_auto_input: self_doi_crossref_auto_input,
        }
        pri_document_data.reject! {|k,v| v == nil }

        prev_doc_cont = PriDocument.all.size

        clear_template_buffer
        patch staff_pri_document_path(document.id, pri_document: pri_document_data, field: field_data)
        new_document = PriDocument.all.order(id: :desc).first

        assert_response :redirect
        assert_redirected_to staff_pri_document_path(id: new_document.id)

        clear_template_buffer
        get staff_pri_document_path(id: new_document.id)
        response_success
        assert_template "layouts/staff"
        assert_template "pri_documents/show"

        expect(PriDocument.all.size).to eq (prev_doc_cont)

        new_document
    end

    describe "check on document save" do
        it "success not set" do
            change_oai_pmh_setting

            field_data1 = {
                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[4]}, "0"=>{"some_id"=>""}},
            }

            self_doi_count = PriDocument.where(self_doi_checked: false).count
            publish_count = PriDocument.publish_query.count

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_UNUSED, nil, nil, nil)

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count
            PriDocument.publish_query.count.should == publish_count + 1

            check_self_doi_document
            publish_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count

            new_document.get_pub_document.self_doi.should be_nil
        end

        it "success set jalc auto" do
            change_oai_pmh_setting

            field_data1 = {
                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[4]}, "0"=>{"some_id"=>""}},
            }

            self_doi_count = PriDocument.where(self_doi_checked: false).count
            publish_count = PriDocument.publish_query.count

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_AUTO, nil, nil, nil)

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count +  1
            PriDocument.publish_query.count.should == publish_count

            check_self_doi_document
            publish_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count
            PriDocument.publish_query.count.should == publish_count

            new_document.get_pub_document.self_doi_type.should == SelfDoiDocument::SELF_DOI_TYPE_JALC_AUTO
            new_document.get_pub_document.self_doi.should == @oaipmh_setting.self_doi_jalc_prefix + new_document.id.to_s
        end

        it "success set jalc menu" do
            change_oai_pmh_setting

            field_data1 = {
                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[4]}, "0"=>{"some_id"=>""}},
            }

            self_doi_count = PriDocument.where(self_doi_checked: false).count
            publish_count = PriDocument.publish_query.count

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count + 1
            PriDocument.publish_query.count.should == publish_count

            check_self_doi_document
            publish_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count
            PriDocument.publish_query.count.should == publish_count

            new_document.get_pub_document.self_doi_type.should == SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU
            new_document.get_pub_document.self_doi.should == SelfDoiDocument::NDL_DOI_PREFIX + 'abc'
        end


        it "fail. nii type empty" do
            change_oai_pmh_setting

            field_data1 = {
                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
#                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[4]}, "0"=>{"some_id"=>""}},
            }

            self_doi_count = PriDocument.where(self_doi_checked: false).count
            publish_count = PriDocument.publish_query.count

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)

            check_self_doi_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count + 1
            PriDocument.publish_query.count.should == publish_count
        end

        it "fail. at jalc nii type was not match" do
            change_oai_pmh_setting

            field_data1 = {
                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[6]}, "0"=>{"some_id"=>""}},
            }

            self_doi_count = PriDocument.where(self_doi_checked: false).count
            publish_count = PriDocument.publish_query.count

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)

            check_self_doi_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count + 1
            PriDocument.publish_query.count.should == publish_count
        end

        it "fail. at jalc title field was empty" do
            change_oai_pmh_setting

            field_data1 = {
#                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[6]}, "0"=>{"some_id"=>""}},
            }

            self_doi_count = PriDocument.where(self_doi_checked: false).count
            publish_count = PriDocument.publish_query.count

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)

            check_self_doi_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count + 1
            PriDocument.publish_query.count.should == publish_count
        end

        it "fail. at jalc start page field was empty" do
            change_oai_pmh_setting

            field_data1 = {
                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
#                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[6]}, "0"=>{"some_id"=>""}},
            }

            self_doi_count = PriDocument.where(self_doi_checked: false).count
            publish_count = PriDocument.publish_query.count

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)

            check_self_doi_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count + 1
            PriDocument.publish_query.count.should == publish_count
        end

        it "fail. create duplicate value" do
            change_oai_pmh_setting

            field_data1 = {
                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[4]}, "0"=>{"some_id"=>""}},
            }

            self_doi_count = PriDocument.where(self_doi_checked: false).count
            publish_count = PriDocument.publish_query.count

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count + 1
            PriDocument.publish_query.count.should == publish_count

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)

            check_self_doi_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count + 2
            PriDocument.publish_query.count.should == publish_count
        end

        it "success. enable and disable" do
            change_oai_pmh_setting

            field_data1 = {
                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[4]}, "0"=>{"some_id"=>""}},
            }

            self_doi_count = PriDocument.where(self_doi_checked: false).count
            publish_count = PriDocument.publish_query.count

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)

            check_self_doi_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count
            PriDocument.publish_query.count.should == publish_count + 1

            updated_document = update_doi_document(new_document, PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_UNUSED, nil, nil, nil)

            check_self_doi_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count
            PriDocument.publish_query.count.should == publish_count + 1
        end

        it "fail. enable and publish and update duplicate value" do
            change_oai_pmh_setting

            field_data1 = {
                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[4]}, "0"=>{"some_id"=>""}},
            }

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)

            check_self_doi_document
            publish_document

            self_doi_count = PriDocument.where(self_doi_checked: false).count
            publish_count = PriDocument.publish_query.count

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)

            check_self_doi_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count + 1
            PriDocument.publish_query.count.should == publish_count
        end

        it "success. enable and publish and disable" do
            change_oai_pmh_setting

            field_data1 = {
                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[4]}, "0"=>{"some_id"=>""}},
            }

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)

            check_self_doi_document
            publish_document

            self_doi_count = PriDocument.where(self_doi_checked: false).count
            publish_count = PriDocument.publish_query.count

            updated_document = update_doi_document(new_document, PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_DELETED, nil, nil, nil)

            check_self_doi_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count
            PriDocument.publish_query.count.should == publish_count + 1
        end

        it "success. enable and publish and disable" do
            change_oai_pmh_setting

            field_data1 = {
                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[4]}, "0"=>{"some_id"=>""}},
            }

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)
            check_self_doi_document
            publish_document

            self_doi_count = PriDocument.where(self_doi_checked: false).count
            publish_count = PriDocument.publish_query.count

            updated_document = update_doi_document(new_document, PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_DELETED, nil, nil, nil)

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count + 1
            PriDocument.publish_query.count.should == publish_count 

            check_self_doi_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count
            PriDocument.publish_query.count.should == publish_count + 1
        end

        it "success. enable and publish and wrong disable" do
            change_oai_pmh_setting

            field_data1 = {
                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[4]}, "0"=>{"some_id"=>""}},
            }

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)
            check_self_doi_document
            publish_document

            self_doi_count = PriDocument.where(self_doi_checked: false).count
            publish_count = PriDocument.publish_query.count

            updated_document = update_doi_document(new_document, PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_CROS_DELETED, nil, nil, nil)

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count + 1
            PriDocument.publish_query.count.should == publish_count 

            check_self_doi_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count + 1
            PriDocument.publish_query.count.should == publish_count
        end

        it "success. enable and publish and disable and enable" do
            change_oai_pmh_setting

            field_data1 = {
                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[4]}, "0"=>{"some_id"=>""}},
            }

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)
            check_self_doi_document
            publish_document

            updated_document = update_doi_document(new_document, PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_DELETED, nil, nil, nil)

            new_document.reload
            new_document.self_doi_type.should == SelfDoiDocument::SELF_DOI_TYPE_JALC_DELETED
            new_document.self_doi_jalc_manu_input.should == SelfDoiDocument::NDL_DOI_PREFIX + 'abc'
            new_document.self_doi_jalc_auto_input.should == ''

            updated_document = update_doi_document(updated_document, PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, nil, nil, nil)

            new_document.reload
            new_document.self_doi_type.should == SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU
            new_document.self_doi_jalc_manu_input.should == SelfDoiDocument::NDL_DOI_PREFIX + 'abc'
            new_document.self_doi_jalc_auto_input.should == ''
        end

        it "fail. enable and publish and disable and publish and enable" do
            change_oai_pmh_setting

            field_data1 = {
                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[4]}, "0"=>{"some_id"=>""}},
            }

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)
            check_self_doi_document
            publish_document

            updated_document = update_doi_document(new_document, PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_DELETED, nil, nil, nil)
            new_document.reload
            new_document.self_doi_type.should == SelfDoiDocument::SELF_DOI_TYPE_JALC_DELETED
            new_document.self_doi_jalc_manu_input.should == SelfDoiDocument::NDL_DOI_PREFIX + 'abc'
            new_document.self_doi_jalc_auto_input.should == ''

            check_self_doi_document
            publish_document

            self_doi_count = PriDocument.where(self_doi_checked: false).count
            publish_count = PriDocument.publish_query.count

            updated_document = update_doi_document(updated_document, PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)

            check_self_doi_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count + 1
            PriDocument.publish_query.count.should == publish_count


            updated_document.reload
            updated_document.self_doi_type.should == SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU
            updated_document.self_doi_jalc_manu_input.should == SelfDoiDocument::NDL_DOI_PREFIX + 'abc'
            updated_document.self_doi_jalc_auto_input.should == ''
        end


        it "fail set jalc menu and update jalc auto" do
            change_oai_pmh_setting

            field_data1 = {
                @field_map['title'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"a"}, "0"=>{"value1"=>""}},
                @field_map['creator'].id.to_s =>{
                    "new_1"=>{"deleted"=>"", "person_id"=>"", "family_name_ja"=>"aa", "given_name_ja"=>"bb", "family_name_en"=>"cc", "given_name_en"=>"dd", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                    "0"=>{"person_id"=>"", "family_name_ja"=>"", "given_name_ja"=>"", "family_name_en"=>"", "given_name_en"=>"", "family_name_tra"=>"", "given_name_tra"=>"",
                        family_name_alt1: "", given_name_alt1: "", family_name_alt2: "", given_name_alt2: "", family_name_alt3: "", given_name_alt3: "", affiliation_ja: "", affiliation_en: ""},
                },
                @field_map['identifier_jtitle'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"title1"}, "0"=>{"value1"=>""}},
                @field_map['identifier_spage'].id.to_s =>{"new_1"=>{"deleted"=>"", "value1"=>"1"}, "0"=>{"value1"=>""}},
                @field_map['niitype'].id.to_s =>{"new_1"=>{"deleted"=>"", "some_id"=> @custom_select_values[4]}, "0"=>{"some_id"=>""}},
            }

            new_document = create_doi_document(PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_MANU, SelfDoiDocument::NDL_DOI_PREFIX + 'abc', nil, nil)
            check_self_doi_document
            publish_document

            self_doi_count = PriDocument.where(self_doi_checked: false).count
            publish_count = PriDocument.publish_query.count

            updated_document = update_doi_document(new_document, PriDocument::PUBLIC_TYPE, field_data1, SelfDoiDocument::SELF_DOI_TYPE_JALC_AUTO, nil, nil, nil)

            check_self_doi_document

            PriDocument.where(self_doi_checked: false).count.should == self_doi_count + 1
            PriDocument.publish_query.count.should == publish_count


            new_document.reload
            new_document.self_doi_type.should == SelfDoiDocument::SELF_DOI_TYPE_JALC_AUTO
            new_document.self_doi_jalc_manu_input.should == SelfDoiDocument::NDL_DOI_PREFIX + 'abc'
            new_document.self_doi_jalc_auto_input.should == ''
        end


    end
end
