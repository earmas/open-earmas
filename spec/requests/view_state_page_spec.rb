require 'spec_helper'

describe "View State Page" do

    before(:all) do
        change_oai_pmh_enable

        @new_document1 = create_document(2, @field_data)
        pri_field_file = PriFieldFileDatum.new
        pri_field_file.document_id = @new_document1.id
        pri_field_file.field_def_id = @fields[0].id
        pri_field_file.upload_file = @test_file
        pri_field_file.save!
        @pri_field_file = pri_field_file 

        put change_thumnail_staff_pri_document_path(@new_document1.id, field_def_id: @pri_field_file.field_def_id, field_id: @pri_field_file.id)

        local_file_path = File.join(@new_document1.document_attach_file_dir, 'sample1.pdf')
        FileUtils.cp 'spec/fixtures/sample1.pdf', local_file_path


        @new_document2 = create_document(2, @field_data)
        @new_document3 = create_document(2, @field_data1)
        @new_document4 = create_document(2, @field_data1)

        publish_document

        @pub_field_file = PubFieldFileDatum.find(@pri_field_file.id)
    end

    describe "pub" do
        describe "contact us" do
            it "index" do
                get pub_contact_us_path

                response_success
            end
            it "send_mail" do
                post pub_send_contact_us_path

                response_success
            end
            it "result" do
                get pub_result_contact_us_path

                response_success
            end
        end
        describe "files" do
            it "show_first" do
                get pub_first_file_path(@new_document1.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.file_path))
            end
            it "thumnail_first" do
                get pub_first_file_thumnail_path(@new_document1.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.thumnail_path))
            end
            it "show" do
                get pub_file_path(document_id: @pub_field_file.document_id, field_def_id: @pub_field_file.field_def_id, id: @pub_field_file.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.file_path))
            end
            it "thumnail" do
                get pub_file_thumnail_path(document_id: @pub_field_file.document_id, field_def_id: @pub_field_file.field_def_id, id: @pub_field_file.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.thumnail_path))
            end
        end
        describe "lists" do
            it "index" do
                get pub_list_path(list_def_code: 'creator')
                response_success
                assert_template "view_state/lists/index"
            end
            it "item" do
                get pub_list_path(list_def_code: 'creator', id: @new_document1.id)
                response_success
                assert_template "view_state/lists/index"
            end
            it "download_id" do
                get pub_list_path(list_def_code: 'creator', download_id: 'csv')
                response_success
                assert_template "view_state/lists/index"
            end

            it "result" do
                get pub_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b')
                response_success
                assert_template "view_state/lists/result"
            end

            it "download_id" do
                get pub_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b', download_id: 'csv')
                response_success
            end

            it "detail" do
                get pub_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b', id: @new_document1.id)
                response_success
                assert_template "view_state/lists/show"
            end
        end
        describe "root" do
            it "index" do
                get pub_path

                response_success
                assert_template "view_state/root/index"
            end
            it "item" do
                get pub_item_path(@new_document1.id)
                response_success
                assert_template "view_state/items/show"

                get pub_metadata_path(@new_document1.id)
                response_success
                assert_template "view_state/items/show"
            end
            it "thumnail" do
                get pub_thumnail_path(@new_document1.id)

                response_success
            end
            it "attach" do
                get pub_attach_path(1, name: "sample1.pdf")

                response_success
            end
        end
        describe "search" do
            it "index" do
                get pub_search_path

                response_success
                assert_template "view_state/searches/index"

                clear_template_buffer
                post pub_search_path(all: 'aa')
                assert_redirected_to pub_search_path(all: 'aa')

                get pub_search_path(all: 'aa')
                response_success
                assert_template "view_state/searches/index"
            end
            it "item" do
                get pub_search_path(id: @new_document1.id)

                response_success
                assert_template "view_state/searches/show"
            end
            it "download_id" do
                get pub_search_path(download_id: 'csv')

                response_success
            end
        end

        describe "oai" do
            it "index" do
                get api_oai_request_path(verb: 'Identify')
                response_success
                assert_template "view_state/oai/identify"
            end
        end

        describe "static page" do
            it "index" do
                get pub_static_page_path

                response_success
            end
        end
    end

    describe "lim" do
        describe "contact us" do
            it "index" do
                get lim_contact_us_path

                response_success
            end
            it "send_mail" do
                post lim_send_contact_us_path

                response_success
            end
            it "result" do
                get lim_result_contact_us_path

                response_success
            end
        end
        describe "files" do
            it "show_first" do
                get lim_first_file_path(@new_document1.id)
                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.file_path))
            end
            it "thumnail_first" do
                get lim_first_file_thumnail_path(@new_document1.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.thumnail_path))
            end
            it "show" do
                get lim_file_path(document_id: @pub_field_file.document_id, field_def_id: @pub_field_file.field_def_id, id: @pub_field_file.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.file_path))
            end
            it "thumnail" do
                get lim_file_thumnail_path(document_id: @pub_field_file.document_id, field_def_id: @pub_field_file.field_def_id, id: @pub_field_file.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.thumnail_path))
            end
        end
        describe "lists" do
            it "index" do
                get lim_list_path(list_def_code: 'creator')
                response_success
                assert_template "view_state/lists/index"
            end
            it "item" do
                get lim_list_path(list_def_code: 'creator', id: 1)
                response_success
                assert_template "view_state/lists/index"
            end
            it "download_id" do
                get lim_list_path(list_def_code: 'creator', download_id: 'csv')
                response_success
                assert_template "view_state/lists/index"
            end

            it "result" do
                get lim_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b')
                response_success
                assert_template "view_state/lists/result"
            end

            it "download_id" do
                get lim_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b', download_id: 'csv')
                response_success
            end

            it "detail" do
                get lim_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b', id: 1)
                response_success
                assert_template "view_state/lists/show"
            end
        end
        describe "root" do
            it "index" do
                get lim_path

                response_success
                assert_template "view_state/root/index"
            end
            it "item" do
                get lim_item_path(@new_document1.id)
                response_success
                assert_template "view_state/items/show"

                get lim_metadata_path(@new_document1.id)
                response_success
                assert_template "view_state/items/show"
            end
            it "thumnail" do
                get lim_thumnail_path(@new_document1.id)

                response_success
            end
            it "attach" do
                get lim_attach_path(1, name: "sample1.pdf")

                response_success
            end
        end
        describe "search" do
            it "index" do
                get lim_search_path

                response_success
                assert_template "view_state/searches/index"

                clear_template_buffer
                post lim_search_path(all: 'aa')
                assert_redirected_to lim_search_path(all: 'aa')

                get lim_search_path(all: 'aa')
                response_success
                assert_template "view_state/searches/index"
            end
            it "item" do
                get lim_search_path(id: @new_document1.id)

                response_success
                assert_template "view_state/searches/show"
            end
            it "download_id" do
                get lim_search_path(download_id: 'csv')

                response_success
            end
        end
        describe "static page" do
            it "index" do
                get lim_static_page_path

                response_success
            end
        end
    end

    describe "pri" do
        describe "contact us" do
            it "index" do
                get pri_contact_us_path

                response_success
            end
            it "send_mail" do
                post pri_send_contact_us_path

                response_success
            end
            it "result" do
                get pri_result_contact_us_path

                response_success
            end
        end
        describe "files" do
            it "show_first" do
                get pri_first_file_path(@new_document1.id)

                response_success
            end
            it "thumnail_first" do
                get pri_first_file_thumnail_path(@new_document1.id)

                response_success
            end
            it "show" do
                get pri_file_path(document_id: @pri_field_file.document_id, field_def_id: @pri_field_file.field_def_id, id: @pri_field_file.id)

                response_success
            end
            it "thumnail" do
                get pri_file_thumnail_path(document_id: @pri_field_file.document_id, field_def_id: @pri_field_file.field_def_id, id: @pri_field_file.id)

                response_success
            end
        end
        describe "lists" do
            it "index" do
                get pri_list_path(list_def_code: 'creator')
                response_success
                assert_template "view_state/lists/index"
            end
            it "item" do
                get pri_list_path(list_def_code: 'creator', id: 1)
                response_success
                assert_template "view_state/lists/index"
            end
            it "download_id" do
                get pri_list_path(list_def_code: 'creator', download_id: 'csv')
                response_success
                assert_template "view_state/lists/index"
            end

            it "result" do
                get pri_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b')
                response_success
                assert_template "view_state/lists/result"
            end

            it "download_id" do
                get pri_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b', download_id: 'csv')
                response_success
            end

            it "detail" do
                get pri_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b', id: 1)
                response_success
                assert_template "view_state/lists/show"
            end
        end
        describe "root" do
            it "index" do
                get pri_path

                response_success
                assert_template "view_state/root/index"
            end
            it "item" do
                get pri_item_path(@new_document1.id)
                response_success
                assert_template "view_state/items/show"

                get pri_metadata_path(@new_document1.id)
                response_success
                assert_template "view_state/items/show"
            end
            it "thumnail" do
                get pri_thumnail_path(@new_document1.id)

                response_success
            end
            it "attach" do
                get pri_attach_path(1, name: "sample1.pdf")

                response_success
            end
        end
        describe "search" do
            it "index" do
                get pri_search_path

                response_success
                assert_template "view_state/searches/index"

                clear_template_buffer
                post pri_search_path(all: 'aa')
                assert_redirected_to pri_search_path(all: 'aa')

                get pri_search_path(all: 'aa')
                response_success
                assert_template "view_state/searches/index"
            end
            it "item" do
                get pri_search_path(id: @new_document1.id)

                response_success
                assert_template "view_state/searches/show"
            end
            it "download_id" do
                get pri_search_path(download_id: 'csv')

                response_success
            end
        end
        describe "static page" do
            it "index" do
                get pri_static_page_path

                response_success
            end
        end
    end
end