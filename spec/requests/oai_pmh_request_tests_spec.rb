require 'spec_helper'

describe "OAI PMH" do
    include OaiHelper

    describe "GetRecord" do
        it "success" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'GetRecord', identifier: ('test/' + new_document.id.to_s), metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/get_record"

            match_response('<record>')
        end

        it "badArgument" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'GetRecord', identifier: ('test/' + new_document.id.to_s), metadataPrefix: 'junii2', from: '2001-11-11')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end

        it "badArgument" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'GetRecord', identifier: ('test/' + new_document.id.to_s), metadataPrefix: 'junii2', 'until' => '2001-11-11')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end
        it "badArgument" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'GetRecord', identifier: ('test/' + new_document.id.to_s), metadataPrefix: 'junii2', set: 'all')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end

        it "badArgument" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'GetRecord', identifier: ('test/' + new_document.id.to_s), metadataPrefix: 'junii2', resumptionToken: '123')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end

        it "badArgument" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'GetRecord', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end
        it "badArgument" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'GetRecord', identifier: ('test/' + new_document.id.to_s))
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end
        it "cannotDisseminateFormat" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'GetRecord', identifier: ('test/' + new_document.id.to_s), metadataPrefix: 'junii3')
            response_success
            assert_template "view_state/oai/error"
            match_response('cannotDisseminateFormat')
        end
        it "cannotDisseminateFormat" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'GetRecord', identifier: ('test/100'), metadataPrefix: 'junii3')
            response_success
            assert_template "view_state/oai/error"
            match_response('cannotDisseminateFormat')
        end
        it "idDoesNotExist" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'GetRecord', identifier: ('test/100'), metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/error"
            match_response('idDoesNotExist')
        end
    end
    describe "Identify" do
        it "success" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'Identify')
            response_success
            assert_template "view_state/oai/identify"

            match_response('<Identify>')
            match_response('<repositoryName>test pository</repositoryName>')
            match_response('<baseURL>example.jp/oai/reauest</baseURL>')
            match_response('<adminEmail>test@enut.jp</adminEmail>')
        end

        it "badArgument" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'Identify', identifier: '123')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end
        it "badArgument" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'Identify', metadataPrefix: '123')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end
        it "badArgument" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'Identify', from: '123')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end
        it "badArgument" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'Identify', 'until' => '123')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end
        it "badArgument" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'Identify', set: '123')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end
        it "badArgument" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'Identify', resumptionToken: '123')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end
    end
    describe "ListIdentifiers" do
        it "success" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_identifiers"

            match_response('<identifier>', 2)
            match_response('resumptionToken', 0)
        end

        it "success" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document1.id)))
            response_success
            assert_template "view_state/oai/list_identifiers"

            match_response('<identifier>', 2)

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/list_identifiers"

            match_response('<identifier>', 1)
        end

        it "success" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', 'until' => show_oai_publish_date(PubDocument.find(new_document1.id)))
            response_success
            assert_template "view_state/oai/list_identifiers"

            match_response('<identifier>', 1)

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/list_identifiers"

            match_response('<identifier>', 2)
        end

        it "success" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document1.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document1.id)))
            response_success
            assert_template "view_state/oai/list_identifiers"

            match_response('<identifier>', 1)

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document1.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/list_identifiers"

            match_response('<identifier>', 2)

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document2.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/list_identifiers"

            match_response('<identifier>', 1)
        end

        it "success" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            PubDocument.where(id: new_document2.id).update_all(min_publish_at: Time.zone.now - 10.days, max_publish_at: Time.zone.now + 10.days)

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document1.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document1.id)))
            response_success
            assert_template "view_state/oai/list_identifiers"

            match_response('<identifier>', 2)

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document1.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/list_identifiers"

            match_response('<identifier>', 2)

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document2.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/list_identifiers"

            match_response('<identifier>', 1)
        end

        it "success" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = update_document(new_document2, PriDocument::PUBLIC_TYPE)
            publish_document

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', set: 'abc', from: show_oai_publish_date(PubDocument.find(new_document1.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document1.id)))
            response_success
            assert_template "view_state/oai/list_identifiers"

            match_response('<identifier>', 1)

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', set: 'DepertmentalBulletinPapers', from: show_oai_publish_date(PubDocument.find(new_document1.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/list_identifiers"

            match_response('<identifier>', 1)
        end

        it "success" do
            change_oai_pmh_enable

            first_document = nil
            document_20 = nil
            document_40 = nil
            last_document = nil
            50.times do |index|
                last_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
                first_document = last_document if first_document == nil
                document_20 = last_document if index == 19
                document_40 = last_document if index == 39
            end

            publish_document

            from_param = show_oai_publish_date(PubDocument.find(first_document.id))
            until_param = show_oai_publish_date(PubDocument.find(last_document.id))


            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', from: from_param, 'until' => until_param)
            response_success
            assert_template "view_state/oai/list_identifiers"
            next_resumption_token = from_param + "/" + until_param + "//junii2/" + document_20.id.to_s
            match_response('<resumptionToken completeListSize="50" cursor="0">' + next_resumption_token + '</resumptionToken>')


            get api_oai_request_path(verb: 'ListIdentifiers', resumptionToken: next_resumption_token)
            response_success
            assert_template "view_state/oai/list_identifiers"
            next_resumption_token = from_param + "/" + until_param + "//junii2/" + document_40.id.to_s
            match_response('<resumptionToken completeListSize="50" cursor="20">' + next_resumption_token + '</resumptionToken>')


            get api_oai_request_path(verb: 'ListIdentifiers', resumptionToken: next_resumption_token)
            response_success
            assert_template "view_state/oai/list_identifiers"
            match_response('<resumptionToken completeListSize="50" cursor="40"/>')
        end

        it "badArgument" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', 'until' => "abc")
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', 'from' => "abc")
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')

            get api_oai_request_path(verb: 'ListIdentifiers')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', resumptionToken: 'abc')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', identifier: 'test/100')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end

        it "badResumptionToken" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            from_param = show_oai_publish_date(PubDocument.find(new_document1.id))
            until_param = show_oai_publish_date(PubDocument.find(new_document2.id))
            set_param = 'abc'

            next_resumption_token = from_param + "/" + until_param + "/" + set_param + "/junii2/" + new_document1.id.to_s

            get api_oai_request_path(verb: 'ListIdentifiers', resumptionToken: next_resumption_token)
            response_success
            assert_template "view_state/oai/list_identifiers"

            next_resumption_token = from_param + "/" + until_param + "/abcd/junii2/" + new_document1.id.to_s

            get api_oai_request_path(verb: 'ListIdentifiers', resumptionToken: next_resumption_token)
            response_success
            assert_template "view_state/oai/error"
            match_response('badResumptionToken')


            next_resumption_token = from_param + "/abc/" + set_param + "/junii2/" + new_document1.id.to_s

            get api_oai_request_path(verb: 'ListIdentifiers', resumptionToken: next_resumption_token)
            response_success
            assert_template "view_state/oai/error"
            match_response('badResumptionToken')

            next_resumption_token = from_param + "/abc/" + set_param + "/junii2/200"

            get api_oai_request_path(verb: 'ListIdentifiers', resumptionToken: next_resumption_token)
            response_success
            assert_template "view_state/oai/error"
            match_response('badResumptionToken')
        end
        it "cannotDisseminateFormat" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = update_document(new_document2, PriDocument::PUBLIC_TYPE)
            publish_document

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii3', from: show_oai_publish_date(PubDocument.find(new_document2.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/error"

            match_response('cannotDisseminateFormat')
        end
        it "noRecordsMatch" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = update_document(new_document2, PriDocument::PUBLIC_TYPE)
            publish_document

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', set: 'abc', from: show_oai_publish_date(PubDocument.find(new_document2.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/error"

            match_response('noRecordsMatch')
        end
        it "noSetHierarchy" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = update_document(new_document2, PriDocument::PUBLIC_TYPE)
            publish_document

            get api_oai_request_path(verb: 'ListIdentifiers', metadataPrefix: 'junii2', set: 'all', from: show_oai_publish_date(PubDocument.find(new_document2.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/error"
            match_response('noSetHierarchy')
        end
    end
    describe "ListMetadataFormats" do
        it "success" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = update_document(new_document2, PriDocument::PUBLIC_TYPE)
            publish_document

            get api_oai_request_path(verb: 'ListMetadataFormats')
            response_success
            assert_template "view_state/oai/list_metadata_formats"

            match_response('<metadataPrefix>junii2</metadataPrefix>', 1)
            match_response('<metadataPrefix>oai_dc</metadataPrefix>', 1)
        end

        it "badArgument" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = update_document(new_document2, PriDocument::PUBLIC_TYPE)
            publish_document

            get api_oai_request_path(verb: 'ListMetadataFormats', from: 'aaa')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')

            get api_oai_request_path(verb: 'ListMetadataFormats', 'until' => 'aaa')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')

            get api_oai_request_path(verb: 'ListMetadataFormats', set: 'aaa')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')

            get api_oai_request_path(verb: 'ListMetadataFormats', metadataPrefix: 'aaa')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
            
            get api_oai_request_path(verb: 'ListMetadataFormats', resumptionToken: 'aaa')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end
        it "idDoesNotExist" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = update_document(new_document2, PriDocument::PUBLIC_TYPE)
            publish_document

            get api_oai_request_path(verb: 'ListMetadataFormats', identifier: 100)
            response_success
            assert_template "view_state/oai/error"
            match_response('idDoesNotExist')
        end
        it "noMetadataFormats" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = update_document(new_document2, PriDocument::PUBLIC_TYPE)
            publish_document

            OaipmhFormat.all.delete_all

            get api_oai_request_path(verb: 'ListMetadataFormats')
            response_success
            assert_template "view_state/oai/error"
            match_response('noMetadataFormats')
        end
    end


    describe "ListRecords" do
        it "success" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"

            match_response('<identifier>', 2)
            match_response('resumptionToken', 0)
            match_response('<URI>' + pub_item_url(@system_setting.document_id_to_public_id(new_document1.id), locale: nil, logined: nil) + '</URI>', 1)
            match_response('<URI>' + pub_item_url(@system_setting.document_id_to_public_id(new_document2.id), locale: nil, logined: nil) + '</URI>', 1)
        end

        it "success" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document1.id)))
            response_success
            assert_template "view_state/oai/list_records"

            match_response('<identifier>', 2)

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/list_records"

            match_response('<identifier>', 1)
        end

        it "success" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', 'until' => show_oai_publish_date(PubDocument.find(new_document1.id)))
            response_success
            assert_template "view_state/oai/list_records"

            match_response('<identifier>', 1)

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/list_records"

            match_response('<identifier>', 2)
        end

        it "success" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document1.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document1.id)))
            response_success
            assert_template "view_state/oai/list_records"

            match_response('<identifier>', 1)

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document1.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/list_records"

            match_response('<identifier>', 2)

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document2.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/list_records"

            match_response('<identifier>', 1)
        end

        it "success" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            PubDocument.where(id: new_document2.id).update_all(min_publish_at: Time.zone.now - 10.days, max_publish_at: Time.zone.now + 10.days)

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document1.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document1.id)))
            response_success
            assert_template "view_state/oai/list_records"

            match_response('<identifier>', 2)

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document1.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/list_records"

            match_response('<identifier>', 2)

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', from: show_oai_publish_date(PubDocument.find(new_document2.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/list_records"

            match_response('<identifier>', 1)
        end

        it "success" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = update_document(new_document2, PriDocument::PUBLIC_TYPE)
            publish_document

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', set: 'abc', from: show_oai_publish_date(PubDocument.find(new_document1.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document1.id)))
            response_success
            assert_template "view_state/oai/list_records"

            match_response('<identifier>', 1)

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', set: 'DepertmentalBulletinPapers', from: show_oai_publish_date(PubDocument.find(new_document1.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/list_records"

            match_response('<identifier>', 1)
        end

        it "success" do
            change_oai_pmh_enable

            first_document = nil
            document_20 = nil
            document_40 = nil
            last_document = nil
            50.times do |index|
                last_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
                first_document = last_document if first_document == nil
                document_20 = last_document if index == 19
                document_40 = last_document if index == 39
            end

            publish_document

            from_param = show_oai_publish_date(PubDocument.find(first_document.id))
            until_param = show_oai_publish_date(PubDocument.find(last_document.id))


            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', from: from_param, 'until' => until_param)
            response_success
            assert_template "view_state/oai/list_records"
            next_resumption_token = from_param + "/" + until_param + "//junii2/" + document_20.id.to_s
            match_response('<resumptionToken completeListSize="50" cursor="0">' + next_resumption_token + '</resumptionToken>')


            get api_oai_request_path(verb: 'ListRecords', resumptionToken: next_resumption_token)
            response_success
            assert_template "view_state/oai/list_records"
            next_resumption_token = from_param + "/" + until_param + "//junii2/" + document_40.id.to_s
            match_response('<resumptionToken completeListSize="50" cursor="20">' + next_resumption_token + '</resumptionToken>')


            get api_oai_request_path(verb: 'ListRecords', resumptionToken: next_resumption_token)
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<resumptionToken completeListSize="50" cursor="40"/>')
        end

        it "badArgument" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', 'until' => "abc")
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', 'from' => "abc")
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')

            get api_oai_request_path(verb: 'ListRecords')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', resumptionToken: 'abc')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', identifier: 'test/100')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end

        it "badResumptionToken" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            from_param = show_oai_publish_date(PubDocument.find(new_document1.id))
            until_param = show_oai_publish_date(PubDocument.find(new_document2.id))
            set_param = 'abc'

            next_resumption_token = from_param + "/" + until_param + "/" + set_param + "/junii2/" + new_document1.id.to_s

            get api_oai_request_path(verb: 'ListRecords', resumptionToken: next_resumption_token)
            response_success
            assert_template "view_state/oai/list_records"

            next_resumption_token = from_param + "/" + until_param + "/abcd/junii2/" + new_document1.id.to_s

            get api_oai_request_path(verb: 'ListRecords', resumptionToken: next_resumption_token)
            response_success
            assert_template "view_state/oai/error"
            match_response('badResumptionToken')


            next_resumption_token = from_param + "/abc/" + set_param + "/junii2/" + new_document1.id.to_s

            get api_oai_request_path(verb: 'ListRecords', resumptionToken: next_resumption_token)
            response_success
            assert_template "view_state/oai/error"
            match_response('badResumptionToken')

            next_resumption_token = from_param + "/abc/" + set_param + "/junii2/200"

            get api_oai_request_path(verb: 'ListRecords', resumptionToken: next_resumption_token)
            response_success
            assert_template "view_state/oai/error"
            match_response('badResumptionToken')
        end
        it "cannotDisseminateFormat" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = update_document(new_document2, PriDocument::PUBLIC_TYPE)
            publish_document

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii3', from: show_oai_publish_date(PubDocument.find(new_document2.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/error"

            match_response('cannotDisseminateFormat')
        end
        it "noRecordsMatch" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = update_document(new_document2, PriDocument::PUBLIC_TYPE)
            publish_document

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', set: 'abc', from: show_oai_publish_date(PubDocument.find(new_document2.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/error"

            match_response('noRecordsMatch')
        end
        it "noSetHierarchy" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = update_document(new_document2, PriDocument::PUBLIC_TYPE)
            publish_document

            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2', set: 'all', from: show_oai_publish_date(PubDocument.find(new_document2.id)), 'until' => show_oai_publish_date(PubDocument.find(new_document2.id)))
            response_success
            assert_template "view_state/oai/error"
            match_response('noSetHierarchy')
        end
    end


    describe "ListSets" do
        it "success" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = update_document(new_document2, PriDocument::PUBLIC_TYPE)
            publish_document

            get api_oai_request_path(verb: 'ListSets')
            response_success
            assert_template "view_state/oai/list_sets"

            match_response('<setSpec>abc</setSpec>', 1)
            match_response('<setSpec>DepertmentalBulletinPapers</setSpec>', 1)
        end
        it "badArgument" do
            change_oai_pmh_enable

            new_document1 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document
            new_document2 = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            new_document2 = update_document(new_document2, PriDocument::PUBLIC_TYPE)
            publish_document

            get api_oai_request_path(verb: 'ListSets', identifier: 'aaa')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')

            get api_oai_request_path(verb: 'ListSets', from: 'aaa')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')

            get api_oai_request_path(verb: 'ListSets', 'until' => 'aaa')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')

            get api_oai_request_path(verb: 'ListSets', set: 'aaa')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')

            get api_oai_request_path(verb: 'ListSets', metadataPrefix: 'aaa')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
            
            get api_oai_request_path(verb: 'ListSets', resumptionToken: 'aaa')
            response_success
            assert_template "view_state/oai/error"
            match_response('badArgument')
        end
        it "badResumptionToken" do
            # no need
        end
        it "noSetHierarchy" do
            # no need
        end
    end
end
