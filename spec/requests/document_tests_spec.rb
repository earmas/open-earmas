require 'spec_helper'

describe "Document" do

    describe "create document" do
        it "show page new document" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            clear_template_buffer
            get new_staff_pri_document_path
            response_success
            assert_template "layouts/staff"
            assert_template "pri_documents/new"
        end

        it "create document" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            clear_template_buffer

            new_document = create_document(2, @field_data)

            pri_document_found(new_document.id)
            lim_document_not_found(new_document.id)
            pub_document_not_found(new_document.id)
        end

        it "create document fail" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            prev_doc_cont = PriDocument.all.size
            pri_document_data = { input_group_id: 0, publish_to: 2 }

            post staff_pri_documents_path(pri_document: pri_document_data, field: @field_data_fail)
            new_document = PriDocument.all.order(id: :desc).first

            response_success
            assert_template "layouts/staff"
            assert_template "pri_documents/new"

            expect(PriDocument.all.size).to eq (prev_doc_cont)
        end

        it "private field not found" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            clear_template_buffer

            new_document = create_document(2, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)


            expect(FieldDefLoader.get_field_def(@fields[56 - 1].id).to_pub).to eq false
            expect(PubDocument.find(new_document.id).get_all_fields.select{|a|a.field_def_id == @fields[56 - 1].id }.size).to eq 0
            expect(PubDocument.find(new_document.id).get_all_fields.select{|a|a.field_def_id == @fields[9 - 1].id }.size).to eq 1
        end
    end

    describe "update document" do
        it "show page edit document" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            new_document = create_document(2, @field_data)

            clear_template_buffer
            get edit_staff_pri_document_path(new_document.id)
            response_success
            assert_template "layouts/staff"
            assert_template "pri_documents/edit"
        end
        it "update" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            new_document = create_document(2, @field_data)
            new_document = update_document(new_document, 2)
        end

        it "field value not post" do
        end
        it "delete field" do
        end
        it "append field" do
        end
        it "invalid field id" do
        end
    end

    describe "delete document" do
        it "delete" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            new_document = create_document(2, @field_data)
            delete_document(new_document)
        end
        it "field value not post" do
        end
        it "delete field" do
        end
        it "append field" do
        end
        it "invalid field id" do
        end
    end

    describe "publish" do
        it "public data" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            new_document = create_document(2, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)
        end

        it "limited data" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            new_document = create_document(1, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_not_found(new_document.id)
        end

        it "priavte data" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            clear_template_buffer

            new_document = create_document(0, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_not_found(new_document.id)
            pub_document_not_found(new_document.id)
        end

        it "update private document after publish public document" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            clear_template_buffer

            new_document = create_document(2, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            new_document = update_document(new_document, 0)

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            publish_document

            pri_document_found(new_document.id)
            lim_document_not_found(new_document.id)
            pub_document_not_found(new_document.id)

        end

        it "update limited document after publish public document" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            clear_template_buffer

            new_document = create_document(2, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            new_document = update_document(new_document, 1)

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_not_found(new_document.id)

        end

        it "update publish document after publish limited document" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            clear_template_buffer

            new_document = create_document(1, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_not_found(new_document.id)

            new_document = update_document(new_document, 2)

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_not_found(new_document.id)

            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

        end

        it "update public document after publish public document" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            clear_template_buffer

            new_document = create_document(2, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            new_document = update_document(new_document, 2)

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)
        end

        it "delete public document after publish" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            clear_template_buffer

            new_document = create_document(2, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            delete_document(new_document)

            pri_document_not_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            publish_document

            pri_document_not_found(new_document.id)
            lim_document_not_found(new_document.id)
            pub_document_not_found(new_document.id)
        end

        it "delete limited document after publish" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            clear_template_buffer

            new_document = create_document(2, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            delete_document(new_document)

            pri_document_not_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            publish_document

            pri_document_not_found(new_document.id)
            lim_document_not_found(new_document.id)
            pub_document_not_found(new_document.id)
        end

        it "delete private document after publish" do
            create_staff_data
            create_field_data
            login_staff(@staff1)

            clear_template_buffer

            new_document = create_document(2, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            delete_document(new_document)

            pri_document_not_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            publish_document

            pri_document_not_found(new_document.id)
            lim_document_not_found(new_document.id)
            pub_document_not_found(new_document.id)
        end
    end

end
