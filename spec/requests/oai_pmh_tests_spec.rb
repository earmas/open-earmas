require 'spec_helper'

describe "OAI PMH" do
    include OaiHelper

    describe "change oai pmh setting" do
        it "success" do
            change_oai_pmh_enable
        end
    end

    describe "oai pmh page" do
        it "success" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"

            match_response('<record>')

        end

        it "fail limit" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::LIMITED_TYPE, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_not_found(new_document.id)


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/error"
            match_response('noRecordsMatch')

        end

        it "fail private" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PRIVATE_TYPE, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_not_found(new_document.id)
            pub_document_not_found(new_document.id)

            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/error"
            match_response('noRecordsMatch')
        end

    end

    describe "oai pmh page" do
        it "public to private" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 0)


            new_document = update_document(new_document, PriDocument::PRIVATE_TYPE)
            publish_document


            pri_document_found(new_document.id)
            lim_document_not_found(new_document.id)
            pub_document_not_found(new_document.id)


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"')

        end

        it "limited" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::LIMITED_TYPE, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_not_found(new_document.id)

            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/error"
            match_response('noRecordsMatch')


            new_document = update_document(new_document, PriDocument::PUBLIC_TYPE)
            publish_document


            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 0)

        end

        it "change oai pmh filter" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 0)


            maintenance_mode_on


            clear_template_buffer
            get staff_oaipmh_settings_path
            response_success
            assert_template "layouts/staff"
            assert_template "oaipmh_settings/index"

            clear_template_buffer
            get new_staff_oaipmh_filter_def_path
            response_success
            assert_template "layouts/staff"
            assert_template "oaipmh_filter_defs/new"

            set_oai_pmh_filter(@fields[55-1].id, '.+')

            get staff_oaipmh_settings_path
            response_success
            assert_template "layouts/staff"
            assert_template "oaipmh_settings/index"


            pri_reindex
            maintenance_mode_off


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 0)


            publish_document


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 1)

            new_document = update_document(new_document, PriDocument::PUBLIC_TYPE)
            publish_document


            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 0)
        end

        it "change set value" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document


            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success

            pub_document = PubDocument.find(new_document.id)
            datestamp1 = pub_document.publish_at_history.max

            assert_template "view_state/oai/list_records"
            match_response('<setSpec>abc</setSpec>')
            match_response('status="deleted"', 0)


            maintenance_mode_on


            put staff_oaipmh_setting_path(@oaipmh_setting.id, oaipmh_setting: {
                identifier_prefix: 'test/',
                repository_name: 'test pository',
                base_url: 'example.jp/oai/reauest',
                admin_email: 'test@enut.jp',
                set_spec_custom_select_id: '',
                set_spec_default_value: 'abcd'
            })
            assert_response :redirect
            assert_redirected_to staff_oaipmh_settings_path


            pri_reindex
            maintenance_mode_off


            pub_document = PubDocument.find(new_document.id)
            datestamp2 = pub_document.publish_at_history.max
            expect(datestamp1).to eq datestamp2


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            match_response('<setSpec>abc</setSpec>')
            match_response('status="deleted"', 0)


            publish_document


            pub_document = PubDocument.find(new_document.id)
            datestamp2 = pub_document.publish_at_history.max
            expect(datestamp1).not_to eq datestamp2

            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            match_response('<setSpec>abcd</setSpec>')
            match_response('status="deleted"', 0)


            new_document = update_document(new_document, PriDocument::PUBLIC_TYPE)
            publish_document


            pub_document = PubDocument.find(new_document.id)
            datestamp3 = pub_document.publish_at_history.max
            expect(datestamp2).not_to eq datestamp3

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            match_response('<setSpec>abcd</setSpec>')
            match_response('status="deleted"', 0)
        end

        it "change set setting" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)

            pub_document = PubDocument.find(new_document.id)
            datestamp1 = pub_document.publish_at_history.max

            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<setSpec>abc</setSpec>')
            match_response('status="deleted"', 0)


            maintenance_mode_on


            put staff_oaipmh_setting_path(@oaipmh_setting.id, oaipmh_setting: {
                identifier_prefix: 'test/',
                repository_name: 'test pository',
                base_url: 'example.jp/oai/reauest',
                admin_email: 'test@enut.jp',
                set_spec_custom_select_id: @custom_selects[1].id,
                set_spec_default_value: 'abcd'
            })

            assert_response :redirect
            assert_redirected_to staff_oaipmh_settings_path


            pri_reindex
            maintenance_mode_off


            pub_document = PubDocument.find(new_document.id)
            datestamp2 = pub_document.publish_at_history.max
            expect(datestamp1).to eq datestamp2

            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<setSpec>abc</setSpec>')
            match_response('status="deleted"', 0)


            publish_document


            pub_document = PubDocument.find(new_document.id)
            datestamp2 = pub_document.publish_at_history.max
            expect(datestamp1).not_to eq datestamp2


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<setSpec>jpn</setSpec>')
            match_response('status="deleted"', 0)


            new_document = update_document(new_document, PriDocument::PUBLIC_TYPE)
            publish_document


            pub_document = PubDocument.find(new_document.id)
            datestamp3 = pub_document.publish_at_history.max
            expect(datestamp2).not_to eq datestamp3

            pri_document_found(new_document.id)
            lim_document_found(new_document.id)
            pub_document_found(new_document.id)


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<setSpec>eng</setSpec>')
        end
    end

    describe "change document to pmh target" do
        it "delete" do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('status="deleted"', 0)



            delete_document(new_document)


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('status="deleted"', 0)


            publish_document


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('status="deleted"', 1)
        end

        it "oai pmh filter change, not match to match" do
            change_oai_pmh_enable

            maintenance_mode_on

            set_oai_pmh_filter(@fields[55-1].id, '.+')

            pri_reindex
            maintenance_mode_off


            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/error"
            match_response('noRecordsMatch')


            new_document = update_document(new_document, PriDocument::PUBLIC_TYPE)


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/error"
            match_response('noRecordsMatch')


            publish_document


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 0)
        end

        it "oai pmh filter change, match to not match" do
            change_oai_pmh_enable

            maintenance_mode_on

            set_oai_pmh_filter(@fields[56-1].id, '.+')

            pri_reindex
            maintenance_mode_off


            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 0)


            new_document = update_document(new_document, PriDocument::PUBLIC_TYPE)


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 0)


            publish_document


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 1)
        end

        it "oai pmh filtered. custom select value was change, not match to match" do
            change_oai_pmh_enable

            maintenance_mode_on

            set_oai_pmh_filter(@fields[33-1].id, 'eng')

            pri_reindex
            maintenance_mode_off


            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/error"
            match_response('noRecordsMatch')


            new_document = update_document(new_document, PriDocument::PUBLIC_TYPE)


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/error"
            match_response('noRecordsMatch')


            publish_document


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 0)
        end

        it "oai pmh filtered. custom select value was change, match to not match" do
            change_oai_pmh_enable

            maintenance_mode_on

            set_oai_pmh_filter(@fields[33-1].id, 'jpn')

            pri_reindex
            maintenance_mode_off


            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 0)


            new_document = update_document(new_document, PriDocument::PUBLIC_TYPE)


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 0)


            publish_document


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 1)
        end

        it "change view type (pub to pri) " do
            change_oai_pmh_enable

            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 0)


            new_document = update_document(new_document, PriDocument::PRIVATE_TYPE)


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 0)


            publish_document


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 1)
        end
        it "change view type (pub to lim) " do
            change_oai_pmh_enable


            new_document = create_document(PriDocument::PUBLIC_TYPE, @field_data)
            publish_document


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 0)


            new_document = update_document(new_document, PriDocument::LIMITED_TYPE)


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 0)


            publish_document


            clear_template_buffer
            get api_oai_request_path(verb: 'ListRecords', metadataPrefix: 'junii2')
            response_success
            assert_template "view_state/oai/list_records"
            match_response('<record>')
            match_response('status="deleted"', 1)
        end
    end

end
