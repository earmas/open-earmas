require 'spec_helper'

describe "Lock" do
    include SystemSettingHelper

    before(:all) do
        change_oai_pmh_enable

        new_document = create_document(2, @field_data)
        @new_document1 = update_document(new_document, 2)
        pri_field_file = PriFieldFileDatum.new
        pri_field_file.document_id = new_document.id
        pri_field_file.field_def_id = @fields[0].id
        pri_field_file.upload_file = @test_file
        pri_field_file.save!
        @pri_field_file = pri_field_file 

        put change_thumnail_staff_pri_document_path(new_document.id, field_def_id: @pri_field_file.field_def_id, field_id: @pri_field_file.id)

        local_file_path = File.join(new_document.document_attach_file_dir, 'sample1.pdf')
        FileUtils.cp 'spec/fixtures/sample1.pdf', local_file_path


        @new_document2 = create_document(2, @field_data)
        @new_document3 = create_document(2, @field_data1)
        @new_document4 = create_document(2, @field_data1)

        publish_document

        @pub_field_file = PubFieldFileDatum.find(@pri_field_file.id)

        maintenance_mode_on
    end

    describe "field lock" do
        before(:all) do
            lock_sort
        end
        after(:all) do
            unlock_sort
        end

        describe "files" do
            it "show_first" do
                get pub_first_file_path(@new_document1.id)
                check_locked_response
            end
            it "thumnail_first" do
                get pub_first_file_thumnail_path(@new_document1.id)
                check_locked_response
            end
            it "show" do
                get pub_file_path(document_id: @pub_field_file.document_id, field_def_id: @pub_field_file.field_def_id, id: @pub_field_file.id)
                check_locked_response
            end
            it "thumnail" do
                get pub_file_thumnail_path(document_id: @pub_field_file.document_id, field_def_id: @pub_field_file.field_def_id, id: @pub_field_file.id)
                check_locked_response
            end
        end
        describe "lists" do
            it "index" do
                get pub_list_path(list_def_code: 'creator')
                check_locked_response
            end
            it "item" do
                get pub_list_path(list_def_code: 'creator', id: @new_document1.id)
                check_locked_response
            end
            it "download_id" do
                get pub_list_path(list_def_code: 'creator', download_id: 'csv')
                check_locked_response
            end

            it "result" do
                get pub_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b')
                check_locked_response
            end

            it "download_id" do
                get pub_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b', download_id: 'csv')
                check_locked_response
            end

            it "detail" do
                get pub_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b', id: @new_document1.id)
                check_locked_response
            end
        end
        describe "root" do
            it "index" do
                get pub_path

                response_success
                assert_template "view_state/root/index"
            end
            it "item" do
                get pub_item_path(@new_document1.id)
                check_locked_response

                get pub_metadata_path(@new_document1.id)
                check_locked_response
            end
            it "thumnail" do
                get pub_thumnail_path(@new_document1.id)
                check_locked_response
            end
            it "attach" do
                get pub_attach_path(@new_document1.id, name: "sample1.pdf")
                check_locked_response
            end
        end
        describe "search" do
            it "index" do
                get pub_search_path
                check_locked_response

                post pub_search_path(all: 'aa')
                check_locked_response

                get pub_search_path(all: 'aa')
                check_locked_response
            end
            it "item" do
                get pub_search_path(id: @new_document1.id)
                check_locked_response
            end
            it "download_id" do
                get pub_search_path(download_id: 'csv')
                check_locked_response
            end
        end

        describe "oai" do
            it "index" do
                get api_oai_request_path(verb: 'Identify')
                check_xml_locked_response
            end
        end
    end

    describe "list lock" do
        before(:all) do
            lock_list(1)
            lock_list(3)
        end
        after(:all) do
            unlock_list
        end

        describe "files" do
            it "show_first" do
                get pub_first_file_path(@new_document1.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.file_path))
            end
            it "thumnail_first" do
                get pub_first_file_thumnail_path(@new_document1.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.thumnail_path))
            end
            it "show" do
                get pub_file_path(document_id: @pub_field_file.document_id, field_def_id: @pub_field_file.field_def_id, id: @pub_field_file.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.file_path))
            end
            it "thumnail" do
                get pub_file_thumnail_path(document_id: @pub_field_file.document_id, field_def_id: @pub_field_file.field_def_id, id: @pub_field_file.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.thumnail_path))
            end
        end
        describe "lists" do
            it "index" do
                get pub_list_path(list_def_code: 'journal')
                check_locked_response
                get pub_list_path(list_def_code: 'creator')
                response_success
                assert_template "view_state/lists/index"
                get pub_list_path(list_def_code: 'niitype')
                check_locked_response
            end
            it "item" do
                get pub_list_path(list_def_code: 'journal', id: @new_document1.id)
                check_locked_response
                get pub_list_path(list_def_code: 'creator', id: @new_document1.id)
                response_success
                assert_template "view_state/lists/index"
                get pub_list_path(list_def_code: 'niitype', id: @new_document1.id)
                check_locked_response
            end
            it "download_id" do
                get pub_list_path(list_def_code: 'journal', download_id: 'csv')
                check_locked_response
                get pub_list_path(list_def_code: 'creator', download_id: 'csv')
                response_success
                assert_template "view_state/lists/index"
                get pub_list_path(list_def_code: 'niitype', download_id: 'csv')
                check_locked_response
            end

            it "result" do
                get pub_list_level1_path(list_def_code: 'journal', level1: 'title1')
                check_locked_response
                get pub_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b')
                response_success
                assert_template "view_state/lists/result"
                get pub_list_level1_path(list_def_code: 'niitype', level1: 'Depertmental Bulletin Papers')
                check_locked_response
            end

            it "download_id" do
                get pub_list_level1_path(list_def_code: 'journal', level1: 'title1', download_id: 'csv')
                check_locked_response
                get pub_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b', download_id: 'csv')
                response_success
                get pub_list_level1_path(list_def_code: 'niitype', level1: 'Depertmental Bulletin Papers', download_id: 'csv')
                check_locked_response
            end

            it "detail" do
                get pub_list_level1_path(list_def_code: 'journal', level1: 'title1', id: @new_document1.id)
                check_locked_response
                get pub_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b', id: @new_document1.id)
                response_success
                assert_template "view_state/lists/show"
                get pub_list_level1_path(list_def_code: 'niitype', level1: 'Depertmental Bulletin Papers', id: @new_document1.id)
                check_locked_response
            end
        end
        describe "root" do
            it "index" do
                get pub_path

                response_success
                assert_template "view_state/root/index"
            end
            it "item" do
                get pub_item_path(@new_document1.id)
                response_success
                assert_template "view_state/items/show"

                get pub_metadata_path(@new_document1.id)
                response_success
                assert_template "view_state/items/show"
            end
            it "thumnail" do
                get pub_thumnail_path(@new_document1.id)

                response_success
            end
            it "attach" do
                get pub_attach_path(@new_document1.id, name: "sample1.pdf")

                response_success
            end
        end
        describe "search" do
            it "index" do
                get pub_search_path

                response_success
                assert_template "view_state/searches/index"

                clear_template_buffer
                post pub_search_path(all: 'aa')
                assert_redirected_to pub_search_path(all: 'aa')

                get pub_search_path(all: 'aa')
                response_success
                assert_template "view_state/searches/index"
            end
            it "item" do
                get pub_search_path(id: @new_document1.id)

                response_success
                assert_template "view_state/searches/show"
            end
            it "download_id" do
                get pub_search_path(download_id: 'csv')

                response_success
            end
        end

        describe "oai" do
            it "index" do
                get api_oai_request_path(verb: 'Identify')
                response_success
                assert_template "view_state/oai/identify"
            end
        end
    end

    describe "saerch lock" do
        before(:all) do
            lock_search
        end
        after(:all) do
            unlock_search
        end

        describe "files" do
            it "show_first" do
                get pub_first_file_path(@new_document1.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.file_path))
            end
            it "thumnail_first" do
                get pub_first_file_thumnail_path(@new_document1.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.thumnail_path))
            end
            it "show" do
                get pub_file_path(document_id: @pub_field_file.document_id, field_def_id: @pub_field_file.field_def_id, id: @pub_field_file.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.file_path))
            end
            it "thumnail" do
                get pub_file_thumnail_path(document_id: @pub_field_file.document_id, field_def_id: @pub_field_file.field_def_id, id: @pub_field_file.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.thumnail_path))
            end
        end
        describe "lists" do
            it "index" do
                get pub_list_path(list_def_code: 'creator')
                response_success
                assert_template "view_state/lists/index"
            end
            it "item" do
                get pub_list_path(list_def_code: 'creator', id: @new_document1.id)
                response_success
                assert_template "view_state/lists/index"
            end
            it "download_id" do
                get pub_list_path(list_def_code: 'creator', download_id: 'csv')
                response_success
                assert_template "view_state/lists/index"
            end

            it "result" do
                get pub_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b')
                response_success
                assert_template "view_state/lists/result"
            end

            it "download_id" do
                get pub_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b', download_id: 'csv')
                response_success
            end

            it "detail" do
                get pub_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b', id: @new_document1.id)
                response_success
                assert_template "view_state/lists/show"
            end
        end
        describe "root" do
            it "index" do
                get pub_path

                response_success
                assert_template "view_state/root/index"
            end
            it "item" do
                get pub_item_path(@new_document1.id)
                response_success
                assert_template "view_state/items/show"

                get pub_metadata_path(@new_document1.id)
                response_success
                assert_template "view_state/items/show"
            end
            it "thumnail" do
                get pub_thumnail_path(@new_document1.id)

                response_success
            end
            it "attach" do
                get pub_attach_path(@new_document1.id, name: "sample1.pdf")

                response_success
            end
        end
        describe "search" do
            it "index" do
                get pub_search_path
                check_locked_response

                clear_template_buffer
                post pub_search_path(all: 'aa')
                check_locked_response

                get pub_search_path(all: 'aa')
                check_locked_response
            end
            it "item" do
                get pub_search_path(id: @new_document1.id)
                check_locked_response
            end
            it "download_id" do
                get pub_search_path(download_id: 'csv')
                check_locked_response
            end
        end

        describe "oai" do
            it "index" do
                get api_oai_request_path(verb: 'Identify')
                response_success
                assert_template "view_state/oai/identify"
            end
        end
    end

    describe "aoi lock" do
        before(:all) do
            lock_oaipmh
        end
        after(:all) do
            unlock_oaipmh
        end

        describe "files" do
            it "show_first" do
                get pub_first_file_path(@new_document1.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.file_path))
            end
            it "thumnail_first" do
                get pub_first_file_thumnail_path(@new_document1.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.thumnail_path))
            end
            it "show" do
                get pub_file_path(document_id: @pub_field_file.document_id, field_def_id: @pub_field_file.field_def_id, id: @pub_field_file.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.file_path))
            end
            it "thumnail" do
                get pub_file_thumnail_path(document_id: @pub_field_file.document_id, field_def_id: @pub_field_file.field_def_id, id: @pub_field_file.id)

                assert_response :redirect
                assert_redirected_to (root_url.chop + public_dir_file_to_url_path(@pub_field_file.thumnail_path))
            end
        end
        describe "lists" do
            it "index" do
                get pub_list_path(list_def_code: 'creator')
                response_success
                assert_template "view_state/lists/index"
            end
            it "item" do
                get pub_list_path(list_def_code: 'creator', id: @new_document1.id)
                response_success
                assert_template "view_state/lists/index"
            end
            it "download_id" do
                get pub_list_path(list_def_code: 'creator', download_id: 'csv')
                response_success
                assert_template "view_state/lists/index"
            end

            it "result" do
                get pub_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b')
                response_success
                assert_template "view_state/lists/result"
            end

            it "download_id" do
                get pub_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b', download_id: 'csv')
                response_success
            end

            it "detail" do
                get pub_list_level1_path(list_def_code: 'creator', level1: 'd,c,a,b', id: @new_document1.id)
                response_success
                assert_template "view_state/lists/show"
            end
        end
        describe "root" do
            it "index" do
                get pub_path

                response_success
                assert_template "view_state/root/index"
            end
            it "item" do
                get pub_item_path(@new_document1.id)
                response_success
                assert_template "view_state/items/show"

                get pub_metadata_path(@new_document1.id)
                response_success
                assert_template "view_state/items/show"
            end
            it "thumnail" do
                get pub_thumnail_path(@new_document1.id)

                response_success
            end
            it "attach" do
                get pub_attach_path(@new_document1.id, name: "sample1.pdf")

                response_success
            end
        end
        describe "search" do
            it "index" do
                get pub_search_path

                response_success
                assert_template "view_state/searches/index"

                clear_template_buffer
                post pub_search_path(all: 'aa')
                assert_redirected_to pub_search_path(all: 'aa')

                get pub_search_path(all: 'aa')
                response_success
                assert_template "view_state/searches/index"
            end
            it "item" do
                get pub_search_path(id: @new_document1.id)

                response_success
                assert_template "view_state/searches/show"
            end
            it "download_id" do
                get pub_search_path(download_id: 'csv')

                response_success
            end
        end

        describe "oai" do
            it "index" do
                get api_oai_request_path(verb: 'Identify')
                check_xml_locked_response
            end
        end
    end
end
