require 'spec_helper'

describe "Login Test" do

    def init_test_data
        @default_account_input = { email: "admin@enut.jp", password: "abcdefgh" }

        @test_datas = []
        @test_datas << {
            data: @default_account_input.merge({ password: "abcdedfhik" }),
            message: 'メールアドレスまたはパスワードが違います。',
        }
        @test_datas << {
            data: @default_account_input.merge({ email: "test@enut" }),
            message: 'メールアドレスまたはパスワードが違います。',
        }
        @test_datas << {
            data: @default_account_input.merge({ name: "" }),
            message: 'メールアドレスまたはパスワードが違います。',
        }
        @test_datas << {
            data: @default_account_input.merge({ password: "" }),
            message: 'メールアドレスまたはパスワードが違います。',
        }
    end


    describe "login test fails" do

        it "invalid input" do
            create_staff_data
            init_test_data

            @test_datas.each do |test_data|
                clear_template_buffer
                get root_path(locale: :ja)
                response_success
                assert_template "layouts/application"
                assert_template "view_state/root/index"

                clear_template_buffer
                post new_staff_session_path, staff: test_data
                response_success
                assert_template "layouts/application"
                assert_template "devise/sessions/new"
                expect(response.body).to include(test_data[:message])
            end
        end
    end

    describe "login test success" do
        it "success" do
            create_staff_data
            login_staff(@staff1)

            expect(Staff.all.size).to be 2
        end
    end

    describe "login and logout test success" do
        it "success" do
            create_staff_data
            login_staff(@staff2)
            logout_staff

            expect(Staff.all.size).to be 2
        end

        it "double logout" do
            create_staff_data
            login_staff(@staff1)
            logout_staff
            logout_staff

            expect(Staff.all.size).to be 2
        end
    end
end
