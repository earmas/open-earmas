require 'spec_helper'

describe "url checker" do

    it "with lang" do
        bot_names = [
            "spider",
            "bot",
            "crawler",
            "slurp",
            "Fetcher",
            "froute.jp",
            "BaiduMobaider",
            "Ask Jeeves",
            "ichiro",
            "ndl-japan-warp",
            "Preview",
            "Yandex",
            "reader",
            "Manager",
            "telnet",
        ].join("\n")

        bot_address = '127.0.0.1/32 123.123.0.0/16'

        bot_names_array = []
        bot_names.split("\n").each do |bot_name|
            next if bot_name.strip.blank?
            bot_names_array << bot_name.strip
        end

        bot_addrs_array = []
        bot_addr_ranges_array = []
        bot_address.split(" ").each do |bot_addr|
            next if bot_addr.strip.blank?

            striped_bot_addr = bot_addr.strip
            if striped_bot_addr.include?('/')
                bot_addr_ranges_array << IPAddr.new(striped_bot_addr)
            else
                bot_addrs_array << striped_bot_addr
            end
        end

        bot_checker = Logs::BotChecker.new(bot_names_array, bot_addrs_array, bot_addr_ranges_array)

        bot_checker.bot_addr?('123.124.123.123').should == false
        bot_checker.bot_addr?('123.123.123.123').should == true
        bot_checker.bot_addr?('123.124.123.123, 123.124.123.124').should == false
        bot_checker.bot_addr?('123.124.123.123, 123.123.123.123').should == false
        bot_checker.bot_addr?('123.123.123.123, 123.123.123.124').should == true
    end

end