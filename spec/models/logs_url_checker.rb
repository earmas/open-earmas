require 'spec_helper'

describe "url checker" do

    describe "access count" do
        it "std access" do
            url_checker = Logs::UrlChecker.new
            url = '/10000'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == "10000"
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "with lang" do
            url_checker = Logs::UrlChecker.new
            url = '/ja/10000'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == "10000"
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "with page" do
            url_checker = Logs::UrlChecker.new
            url = '/p/1/10000'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == "10000"
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "with page and locale" do
            url_checker = Logs::UrlChecker.new
            url = '/ja/p/1/10000'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == "10000"
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "pattern metadata" do
            url_checker = Logs::UrlChecker.new
            url = '/metadata/10000'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == "10000"
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "pattern metadata with lang" do
            url_checker = Logs::UrlChecker.new
            url = '/ja/metadata/10000'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == "10000"
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "search" do
            url_checker = Logs::UrlChecker.new
            url = '/ja/search/item/10000?all=abc'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == "10000"
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "search metadata with locale" do
            url_checker = Logs::UrlChecker.new
            url = '/ja/search/item/10000?all=abc'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == "10000"
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "search metadata with page and locale" do
            url_checker = Logs::UrlChecker.new
            url = '/ja/search/p/1/item/10000?all=abc'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == "10000"
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "list" do
            url_checker = Logs::UrlChecker.new
            url = '/list/list_name/list_code/item/10000'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == "10000"
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "list with locale" do
            url_checker = Logs::UrlChecker.new
            url = '/ja/list/list_name/list_value/item/10000'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == "10000"
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "list with locale and page" do
            url_checker = Logs::UrlChecker.new
            url = '/ja/list/list_name/list_value/p/1/item/10000'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == "10000"
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "list with locale and page" do
            url_checker = Logs::UrlChecker.new
            url = '/ja/list/list_name/list_value/list_value/p/1/item/10000'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == "10000"
            url_checker.get_doc_download_identifier(url).should == nil
        end
    end

    describe "download count" do
        it "download public pattern" do
            url_checker = Logs::UrlChecker.new
            url = '/files/public/10000/20141016203830495176/tes-t_1.pdf'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == nil
            url_checker.get_doc_download_identifier(url).should == "10000"
        end

        it "download public pattern" do
            url_checker = Logs::UrlChecker.new
            url = '/files/public/4/46110/20160528075411583214/43_2217.pdf'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == nil
            url_checker.get_doc_download_identifier(url).should == "46110"
        end

        it "download first file direct pattern" do
            url_checker = Logs::UrlChecker.new
            url = '/10000/file'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == nil
            url_checker.get_doc_download_identifier(url).should == "10000"
        end

        it "download first file direct pattern with locale" do
            url_checker = Logs::UrlChecker.new
            url = '/ja/10000/file'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == nil
            url_checker.get_doc_download_identifier(url).should == "10000"
        end

        it "download direct pattern" do
            url_checker = Logs::UrlChecker.new
            url = '/file/10000/5/100001'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == nil
            url_checker.get_doc_download_identifier(url).should == "10000"
        end

        it "download direct pattern with locale" do
            url_checker = Logs::UrlChecker.new
            url = '/ja/file/10000/5/100001'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == nil
            url_checker.get_doc_download_identifier(url).should == "10000"
        end
    end

    describe "download count thumnail" do
        it "download public pattern" do
            url_checker = Logs::UrlChecker.new
            url = 'files/public/10000/20141016203830495176/thumnail.png'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == nil
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "download public pattern" do
            url_checker = Logs::UrlChecker.new
            url = 'files/public/0/10000/20141016203830495176/thumnail.png'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == nil
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "download first file direct pattern" do
            url_checker = Logs::UrlChecker.new
            url = 'file/10000/file/thumnail'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == nil
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "download direct pattern" do
            url_checker = Logs::UrlChecker.new
            url = 'file/10000/5/100001/thumnail'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == nil
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "download public pattern" do
            url_checker = Logs::UrlChecker.new
            url = 'files/public/0/10000/20141016203830495176/thumnail.png'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == nil
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "download first file direct pattern" do
            url_checker = Logs::UrlChecker.new
            url = 'file/0/10000/file/thumnail'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == nil
            url_checker.get_doc_download_identifier(url).should == nil
        end

        it "download direct pattern" do
            url_checker = Logs::UrlChecker.new
            url = 'file/0/10000/5/100001/thumnail'

            url_checker.check_not_public(url).should == false
            url_checker.get_doc_access_identifier(url).should == nil
            url_checker.get_doc_download_identifier(url).should == nil
        end
    end
end
