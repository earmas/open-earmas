
def get_ext(file_name)
    file_name_parts = file_name.split('.')
    return nil if file_name_parts.size < 2
    return file_name_parts.last
end

def old_file_path(stored_dir_path, org_file_name)
    ext = get_ext(org_file_name)
    if ext.blank?
        File.join(stored_dir_path, 'file')
    else
        File.join(stored_dir_path, 'file' + '.' + ext)
    end
 end


PriFieldFileDatum.find_each(batch_size: 100) do |file_field|
    if File.exist?(old_file_path(file_field.stored_dir_path, file_field.org_file_name))
        p "hit : " + old_file_path(file_field.stored_dir_path, file_field.org_file_name)
        p "move : " + old_file_path(file_field.stored_dir_path, file_field.org_file_name) + ' to ' + file_field.file_path
    end

end


PubFieldFileDatum.find_each(batch_size: 100) do |file_field|
    if File.exist?(old_file_path(file_field.stored_dir_path, file_field.org_file_name))
        p "hit : " + old_file_path(file_field.stored_dir_path, file_field.org_file_name)
        p "move : " + old_file_path(file_field.stored_dir_path, file_field.org_file_name) + ' to ' + file_field.file_path
    end

end

p "---------------------------------"
p "move start 5 seconds later"
p "---------------------------------"

sleep 1
p '5'
sleep 1
p '4'
sleep 1
p '3'
sleep 1
p '2'
sleep 1
p '1'

p "---------------------------------"
p "file move start"
p "---------------------------------"



PriFieldFileDatum.find_each(batch_size: 100) do |file_field|
    if File.exist?(old_file_path(file_field.stored_dir_path, file_field.org_file_name))
        p "move : " + old_file_path(file_field.stored_dir_path, file_field.org_file_name) + ' to ' + file_field.file_path
        FileUtils.mv(old_file_path(file_field.stored_dir_path, file_field.org_file_name), file_field.file_path)
    end

end


PubFieldFileDatum.find_each(batch_size: 100) do |file_field|
    if File.exist?(old_file_path(file_field.stored_dir_path, file_field.org_file_name))
        p "move : " + old_file_path(file_field.stored_dir_path, file_field.org_file_name) + ' to ' + file_field.file_path
        FileUtils.mv(old_file_path(file_field.stored_dir_path, file_field.org_file_name), file_field.file_path)
    end

end
