class AddShowDetailToInputGroupField < ActiveRecord::Migration
    def change

        add_column :input_group_fields, :detail, :boolean, null: false, default: true

    end
end
