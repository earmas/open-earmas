class ChangeNavSomeIdType < ActiveRecord::Migration
    def change
        rename_column   :pri_field_nav_data,    :some_id, :some_id_old
        add_column      :pri_field_nav_data,    :some_id, :integer

        reversible do |dir|
            dir.up do
                PriFieldNavDatum.update_all('some_id = CAST (some_id_old AS INTEGER)')
                change_column :pri_field_nav_data,  :some_id, :integer, null: false
                remove_column :pri_field_nav_data,  :some_id_old
                add_index :pri_field_nav_data, :some_id
            end
        end
    end
end
