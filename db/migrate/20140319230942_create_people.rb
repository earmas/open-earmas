class CreatePeople < ActiveRecord::Migration
    def change
        create_table :people, id: false do |t|
            t.string :id

            t.string :family_name_ja
            t.string :family_name_en
            t.string :family_name_tra

            t.string :given_name_ja
            t.string :given_name_en
            t.string :given_name_tra

            t.text   :url
            t.string :key1
            t.string :key2
            t.string :key3
            t.string :key4
            t.string :key5

            t.timestamps
        end
        reversible do |dir|
            dir.up do
                execute "ALTER TABLE people ADD PRIMARY KEY (id);"
            end
        end
        create_table :people_settings do |t|
            t.string    :key1_name
            t.text      :key1_param
            t.string    :key2_name
            t.text      :key2_param
            t.string    :key3_name
            t.text      :key3_param
            t.string    :key4_name
            t.text      :key4_param
            t.string    :key5_name
            t.text      :key5_param

            t.timestamps
        end
    end
end
