class AddPersonFieldIndex < ActiveRecord::Migration
    def change
        add_index :pri_field_repository_person_data, :person_id
        add_index :pub_field_repository_person_data, :person_id

        add_column :access_count_settings, :api_token, :text, null: false, default: ""
    end
end
