class AddDefaultDetailViewSetting < ActiveRecord::Migration
    def change

        add_column :system_settings, :detail_view_type_id, :integer
        add_column :system_settings, :detail_sub_header_view_type_id, :integer
        add_column :system_settings, :detail_sub_footer_view_type_id, :integer
        add_column :system_settings, :detail_html_set_def_id, :text


        add_column :list_defs, :result_sub_footer_view_type_id, :integer
        add_column :list_defs, :detail_sub_footer_view_type_id, :integer
        add_column :list_field_defs, :list_param_sub_footer_view_type_id, :integer

        add_column :search_settings, :detail_sub_footer_view_type_id, :integer


        reversible do |dir|
            dir.up do
                SystemSetting.update_all(detail_view_type_id: 1)
                SystemSetting.update_all(detail_sub_header_view_type_id: 1)
                SystemSetting.update_all(detail_sub_footer_view_type_id: 1)

                ListDef.update_all(result_sub_footer_view_type_id: 1)
                ListDef.update_all(detail_sub_footer_view_type_id: 1)
                ListFieldDef.update_all(list_param_sub_footer_view_type_id: 1)

                SearchSetting.where(detail_sub_header_view_type_id: nil).update_all(detail_sub_header_view_type_id: 1)
                SearchSetting.update_all(detail_sub_footer_view_type_id: 1)


                change_column :system_settings,  :detail_view_type_id, :integer, null: false
                change_column :system_settings,  :detail_sub_header_view_type_id, :integer, null: false
                change_column :system_settings,  :detail_sub_footer_view_type_id, :integer, null: false


                change_column :list_defs, :result_sub_footer_view_type_id, :integer, null: false
                change_column :list_defs, :detail_sub_footer_view_type_id, :integer, null: false
                change_column :list_field_defs, :list_param_sub_footer_view_type_id, :integer, null: false

                change_column :search_settings, :detail_sub_header_view_type_id, :integer, null: false
                change_column :search_settings, :detail_sub_footer_view_type_id, :integer, null: false
            end
        end

    end
end
