class AddJournalToc < ActiveRecord::Migration
    def change
        add_column :ej_journal_defs, :all_journal_index, :text

        add_column :ej_journal_defs, :journal_all_index_view_type_id,             :integer,   null: false, default: 1
        add_column :ej_journal_defs, :journal_all_index_sub_header_view_type_id,  :integer,   null: false, default: 1
        add_column :ej_journal_defs, :journal_all_index_sub_footer_view_type_id,  :integer,   null: false, default: 1
        add_column :ej_journal_defs, :journal_all_index_html_set_def_id,          :integer
    end
end
