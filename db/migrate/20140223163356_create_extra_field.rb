class CreateExtraField < ActiveRecord::Migration
    def change
        create_table :pri_field_nav_data do |t|
            t.integer   :document_id,   null: false
            t.integer   :field_def_id,  null: false
            t.integer   :sort_index,    null: false, default: 0

            t.text      :some_id,       null: false
            t.integer   :value1,        null: false, default: 0
            t.integer   :value2,        null: false, default: 0
            t.integer   :value3,        null: false, default: 0
            t.integer   :value4,        null: false, default: 0
            t.integer   :value5,        null: false, default: 0

            t.timestamps
        end
        add_index :pri_field_nav_data, :document_id
        add_index :pri_field_nav_data, :field_def_id
        add_index :pri_field_nav_data, :sort_index
        add_index :pri_field_nav_data, :some_id
        add_index :pri_field_nav_data, :value1
        add_index :pri_field_nav_data, :value2
        add_index :pri_field_nav_data, :value3
        add_index :pri_field_nav_data, :value4
        add_index :pri_field_nav_data, :value5



        create_table :pub_field_nav_data, id: false do |t|
            t.integer   :id,            null: false
            t.integer   :document_id,   null: false
            t.integer   :field_def_id,  null: false
            t.integer   :sort_index,    null: false, default: 0

            t.string    :value,         null: false
            t.text      :caption_ja,    null: false
            t.text      :caption_en,    null: false
            t.integer   :value1,        null: false, default: 0
            t.integer   :value2,        null: false, default: 0
            t.integer   :value3,        null: false, default: 0
            t.integer   :value4,        null: false, default: 0
            t.integer   :value5,        null: false, default: 0

            t.timestamps
        end
        add_index :pub_field_nav_data, :document_id
        add_index :pub_field_nav_data, :field_def_id
        add_index :pub_field_nav_data, :sort_index
        add_index :pub_field_nav_data, :value
        add_index :pub_field_nav_data, :value1
        add_index :pub_field_nav_data, :value2
        add_index :pub_field_nav_data, :value3
        add_index :pub_field_nav_data, :value4
        add_index :pub_field_nav_data, :value5
        reversible do |dir|
            dir.up do
                execute "ALTER TABLE pub_field_nav_data ADD PRIMARY KEY (id);"
            end
        end


        create_table :pri_field_text9_data do |t|
            t.integer   :document_id,   null: false
            t.integer   :field_def_id,  null: false
            t.integer   :sort_index,    null: false, default: 0

            t.text      :some_id,       null: false
            t.text      :value1,        null: false
            t.text      :value2,        null: false
            t.text      :value3,        null: false
            t.text      :value4,        null: false
            t.text      :value5,        null: false
            t.text      :value6,        null: false
            t.text      :value7,        null: false
            t.text      :value8,        null: false
            t.text      :value9,        null: false

            t.timestamps
        end
        add_index :pri_field_text9_data, :document_id
        add_index :pri_field_text9_data, :field_def_id
        add_index :pri_field_text9_data, :sort_index


        create_table :pub_field_text9_data, id: false do |t|
            t.integer   :id,            null: false
            t.integer   :document_id,   null: false
            t.integer   :field_def_id,  null: false
            t.integer   :sort_index,    null: false, default: 0

            t.text      :some_id,       null: false
            t.text      :value1,        null: false
            t.text      :value2,        null: false
            t.text      :value3,        null: false
            t.text      :value4,        null: false
            t.text      :value5,        null: false
            t.text      :value6,        null: false
            t.text      :value7,        null: false
            t.text      :value8,        null: false
            t.text      :value9,        null: false


            t.timestamps
        end
        add_index :pub_field_text9_data, :document_id
        add_index :pub_field_text9_data, :field_def_id
        add_index :pub_field_text9_data, :sort_index
        reversible do |dir|
            dir.up do
                execute "ALTER TABLE pub_field_text9_data ADD PRIMARY KEY (id);"
            end
        end
    end
end
