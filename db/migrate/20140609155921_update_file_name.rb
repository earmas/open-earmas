class UpdateFileName < ActiveRecord::Migration
  	def change
        reversible do |dir|
            dir.up do
            	PriFieldFileDatum.all.each do |pri_file_datum|
            		dir = pri_file_datum.stored_dir_path
            		content_file_path = File.join(dir, 'content')
            		thumnail_file_path = File.join(dir, 'content.png')
            		if File.exists?(content_file_path)
						FileUtils.mv content_file_path, pri_file_datum.file_path
						p "move " + content_file_path + ' to ' + pri_file_datum.file_path
            		end

            		if File.exists?(thumnail_file_path)
						FileUtils.mv thumnail_file_path, pri_file_datum.thumnail_path
						p "move " + thumnail_file_path + ' to ' + pri_file_datum.thumnail_path
            		end
            	end
            	PubFieldFileDatum.all.each do |pri_file_datum|
            		dir = pri_file_datum.stored_dir_path
            		content_file_path = File.join(dir, 'content')
            		thumnail_file_path = File.join(dir, 'content.png')
            		if File.exists?(content_file_path)
						FileUtils.mv content_file_path, pri_file_datum.file_path
						p "move " + content_file_path + ' to ' + pri_file_datum.file_path
            		end

            		if File.exists?(thumnail_file_path)
						FileUtils.mv thumnail_file_path, pri_file_datum.thumnail_path
						p "move " + thumnail_file_path + ' to ' + pri_file_datum.thumnail_path
            		end
            	end
            end
        end
  	end
end
