class AddListFieldType < ActiveRecord::Migration
    def change
        add_column :list_field_defs, :list_field_type,       :integer, null: false, default: 1

        remove_index :list_field_defs, column: [:list_def_id, :field_def_id], unique: true
        add_index :list_field_defs, [:list_def_id, :field_def_id, :list_field_type], unique: true, name: :list_field_def_uniq_key
    end
end
