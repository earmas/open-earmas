class AddParmlinkSetting < ActiveRecord::Migration
    def change
        add_column :system_settings, :permalink_caption_ja, :text
        add_column :system_settings, :permalink_caption_en, :text

        create_table :permalink_fields do |t|
            t.integer   :field_def_id,      null: false

            t.text      :caption_ja,        null: false
            t.text      :caption_en,        null: false

            t.integer   :sort_index,        null: false, default: 0

            t.timestamps
        end
        add_index :permalink_fields, [:sort_index], unique: true
        add_index :permalink_fields, [:field_def_id], unique: true

    end
end
