class CreateSearches < ActiveRecord::Migration
    def change
        create_table :search_settings do |t|
            t.integer   :result_view_type_id,       null: false
            t.text      :result_html_set_def_id,    null: true

            t.integer   :detail_view_type_id,       null: false
            t.text      :detail_html_set_def_id,    null: true

            t.timestamps
        end


        create_table :search_defs do |t|
            t.text      :caption_ja,           null: false
            t.text      :caption_en,           null: false
            t.text      :description,          null: false
            t.integer   :sort_index,           null: false, default: 0

            t.timestamps
        end
        add_index :search_defs, :sort_index, unique: true



        create_table :search_field_defs do |t|
            t.integer   :search_def_id,     null: false
            t.integer   :field_def_id,      null: false

            t.timestamps
        end
        add_index :search_field_defs, :search_def_id
        add_index :search_field_defs, [:search_def_id, :field_def_id], unique: true

    end
end
