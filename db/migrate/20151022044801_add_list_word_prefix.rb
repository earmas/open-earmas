class AddListWordPrefix < ActiveRecord::Migration
    def change
        add_column :pri_list_level5_words, :value5_prefix,       :text, null: false, default: ''
        add_index  :pri_list_level5_words, :value5_prefix

        add_column :pri_list_level4_words, :value4_prefix,       :text, null: false, default: ''
        add_index  :pri_list_level4_words, :value4_prefix

        add_column :pri_list_level3_words, :value3_prefix,       :text, null: false, default: ''
        add_index  :pri_list_level3_words, :value3_prefix

        add_column :pri_list_level2_words, :value2_prefix,       :text, null: false, default: ''
        add_index  :pri_list_level2_words, :value2_prefix

        add_column :pub_list_level5_words, :value5_prefix,       :text, null: false, default: ''
        add_index  :pub_list_level5_words, :value5_prefix

        add_column :pub_list_level4_words, :value4_prefix,       :text, null: false, default: ''
        add_index  :pub_list_level4_words, :value4_prefix

        add_column :pub_list_level3_words, :value3_prefix,       :text, null: false, default: ''
        add_index  :pub_list_level3_words, :value3_prefix

        add_column :pub_list_level2_words, :value2_prefix,       :text, null: false, default: ''
        add_index  :pub_list_level2_words, :value2_prefix


        add_column :list_defs, :show_others_link_type,       :integer, null: false, default: 1
        add_index  :list_defs, :show_others_link_type
    end
end
