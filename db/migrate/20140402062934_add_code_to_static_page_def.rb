class AddCodeToStaticPageDef < ActiveRecord::Migration
    def change
        add_column :static_page_defs, :code, :string

        reversible do |dir|
            dir.up do
                StaticPageDef.update_all('code = id')

                change_column :static_page_defs, :code,  :string, null: false
                add_index :static_page_defs, :code, unique: true
            end
        end
    end
end
