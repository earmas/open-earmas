class AddEjEnField < ActiveRecord::Migration
  def change
        rename_column   :ej_detail_fields,    :field_def_id, :ja_field_def_id
        add_column :ej_detail_fields, :en_field_def_id,  :integer
        add_column :ej_detail_fields, :en_instead_ja,  :boolean, null: false, default: false
        add_column :ej_detail_fields, :ja_instead_en,  :boolean, null: false, default: false



        rename_column   :ej_result_fields,    :field_def_id, :ja_field_def_id
        add_column :ej_result_fields, :en_field_def_id,  :integer
        add_column :ej_result_fields, :en_instead_ja,  :boolean, null: false, default: false
        add_column :ej_result_fields, :ja_instead_en,  :boolean, null: false, default: false



        reversible do |dir|
            dir.up do
                EjDetailField.update_all(en_field_def_id: 0)
                EjResultField.update_all(en_field_def_id: 0)

                change_column :ej_detail_fields, :en_field_def_id,  :integer, null: false
                change_column :ej_result_fields, :en_field_def_id,  :integer, null: false
            end
        end

  end
end
