class AddDataIdFormat < ActiveRecord::Migration
    def change
        add_column :system_settings, :id_conv_format, :text
        add_column :system_settings, :id_conv_pattern, :text
    end
end
