class AddInputGroupFieldCaption < ActiveRecord::Migration
    def change
        add_column :input_group_fields, :caption_ja, :text
        add_column :input_group_fields, :caption_en, :text
    end
end
