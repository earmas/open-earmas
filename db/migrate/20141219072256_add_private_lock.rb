class AddPrivateLock < ActiveRecord::Migration
    def change
        add_column :system_settings, :pri_field_locked_at,  :datetime
        add_column :system_settings, :pri_oaipmh_locked_at,  :datetime

        add_column :system_settings, :pri_list_locked_at,   :datetime
        add_column :system_settings, :locked_pri_list_ids,  :integer, array: true

        add_column :system_settings, :pri_ej_locked_at,     :datetime
        add_column :system_settings, :locked_pri_ej_ids,    :integer, array: true

        add_column :system_settings, :pri_search_locked_at, :datetime
    end
end
