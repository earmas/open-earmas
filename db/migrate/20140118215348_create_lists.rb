class CreateLists < ActiveRecord::Migration
	def change
		create_table :list_defs do |t|
            t.string    :code,                  null: false
            t.text      :caption_ja,            null: false
            t.text      :caption_en,            null: false

            t.integer   :result_view_type_id,       null: false
            t.text      :result_html_set_def_id,    null: true

            t.integer   :detail_view_type_id,       null: false
            t.text      :detail_html_set_def_id,    null: true

            t.integer   :sortable_field_id,     null: true
            t.boolean   :sort_reverse,          null: true

            t.boolean   :updated,               null: false, default: false

			t.timestamps
		end
        add_index :list_defs, :code, unique: true



		create_table :list_field_defs do |t|
            t.integer   :list_def_id,       null: false
            t.integer   :field_def_id,      null: false

            t.integer   :sort_index,        null: false, default: 0

            t.integer   :list_param_view_type_id,       null: false
            t.text      :list_param_html_set_def_id,    null: true

			t.timestamps
		end
        add_index :list_field_defs, :list_def_id
        add_index :list_field_defs, [:list_def_id, :field_def_id], unique: true
        add_index :list_field_defs, [:list_def_id, :sort_index], unique: true



        create_table :pri_list_words do |t|
            t.integer   :list_field_def_id,     null: false

            t.text      :param_value,       null: false
            t.text      :sort_value_ja,     null: false
            t.text      :sort_value_en,     null: false
            t.text      :caption_ja,        null: false
            t.text      :caption_en,        null: false

            t.timestamps
        end
        add_index :pri_list_words, [:list_field_def_id, :param_value], unique: true
        add_index :pri_list_words, :list_field_def_id
        add_index :pri_list_words, :param_value
        add_index :pri_list_words, :updated_at



        create_table :pub_list_words do |t|
            t.integer   :list_field_def_id,     null: false

            t.text      :param_value,       null: false
            t.text      :sort_value_ja,     null: false
            t.text      :sort_value_en,     null: false
            t.text      :caption_ja,        null: false
            t.text      :caption_en,        null: false
            
            t.timestamps
        end
        add_index :pub_list_words, [:list_field_def_id, :param_value], unique: true
        add_index :pub_list_words, :list_field_def_id
        add_index :pub_list_words, :param_value
        add_index :pub_list_words, :updated_at
        




        create_table :pri_list_data do |t|
            t.integer   :list_def_id,   null: false
            t.integer   :document_id,   null: false
            t.integer   :publish_to,    null: false

            t.text      :level1,     null: false, array: true
            t.text      :level2,     null: false, array: true
            t.text      :level3,     null: false, array: true
            t.text      :level4,     null: false, array: true
            t.text      :level5,     null: false, array: true

            t.text      :sort_value_ja_1,   null: true
            t.text      :sort_value_en_1,   null: true
            t.text      :sort_value_ja_2,   null: true
            t.text      :sort_value_en_2,   null: true
            t.text      :sort_value_ja_3,   null: true
            t.text      :sort_value_en_3,   null: true

            t.timestamps
        end
        add_index :pri_list_data, [:list_def_id, :document_id], unique: true
        add_index :pri_list_data, :list_def_id
        add_index :pri_list_data, :document_id
        add_index :pri_list_data, :publish_to

        add_index :pri_list_data, :level1,   using: :gin
        add_index :pri_list_data, :level2,   using: :gin
        add_index :pri_list_data, :level3,   using: :gin
        add_index :pri_list_data, :level4,   using: :gin
        add_index :pri_list_data, :level5,   using: :gin

        add_index :pri_list_data, :sort_value_ja_1
        add_index :pri_list_data, :sort_value_en_1
        add_index :pri_list_data, :sort_value_ja_2
        add_index :pri_list_data, :sort_value_en_2
        add_index :pri_list_data, :sort_value_ja_3
        add_index :pri_list_data, :sort_value_en_3



        create_table :pub_list_data do |t|
            t.integer   :list_def_id,   null: false
            t.integer   :document_id,   null: false
            t.boolean   :limited,       null: false

            t.text      :level1,     null: false, array: true
            t.text      :level2,     null: false, array: true
            t.text      :level3,     null: false, array: true
            t.text      :level4,     null: false, array: true
            t.text      :level5,     null: false, array: true
            
            t.text      :sort_value_ja_1,   null: true
            t.text      :sort_value_en_1,   null: true
            t.text      :sort_value_ja_2,   null: true
            t.text      :sort_value_en_2,   null: true
            t.text      :sort_value_ja_3,   null: true
            t.text      :sort_value_en_3,   null: true

            t.timestamps
        end
        add_index :pub_list_data, [:list_def_id, :document_id], unique: true
        add_index :pub_list_data, :list_def_id
        add_index :pub_list_data, :document_id
        add_index :pub_list_data, :limited

        add_index :pub_list_data, :level1,   using: :gin
        add_index :pub_list_data, :level2,   using: :gin
        add_index :pub_list_data, :level3,   using: :gin
        add_index :pub_list_data, :level4,   using: :gin
        add_index :pub_list_data, :level5,   using: :gin

        add_index :pub_list_data, :sort_value_ja_1
        add_index :pub_list_data, :sort_value_en_1
        add_index :pub_list_data, :sort_value_ja_2
        add_index :pub_list_data, :sort_value_en_2
        add_index :pub_list_data, :sort_value_ja_3
        add_index :pub_list_data, :sort_value_en_3
	end
end
