class AddListFieldOrder < ActiveRecord::Migration
    def change
        add_column :list_field_defs, :sort_reverse,    :boolean,    null: true, default: false
    end
end
