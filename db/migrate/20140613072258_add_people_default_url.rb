class AddPeopleDefaultUrl < ActiveRecord::Migration
    def change
        add_column :people_settings, :url, :text

        rename_column :system_settings, :sytem_name, :sytem_name_ja
        add_column :system_settings, :sytem_name_en, :text

        add_index :document_templates, :name

        reversible do |dir|
            dir.up do
                SystemSetting.update_all(sytem_name_en: '')

                change_column :system_settings,  :sytem_name_en, :text, null: false
            end
        end

    end
end
