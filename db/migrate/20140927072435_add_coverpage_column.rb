class AddCoverpageColumn < ActiveRecord::Migration
    def change
        add_column :system_settings, :enable_coverpage, :boolean, null: false, default: false
        add_column :pri_field_file_data, :has_coverpage, :boolean, null: false, default: false
    end
end
