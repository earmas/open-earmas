class AddTemplateDescription < ActiveRecord::Migration
    def change
        add_column :input_group_fields, :description, :text

        reversible do |dir|
            dir.up do
                InputGroupField.update_all("description = ''")

                change_column :input_group_fields, :description, :text, null: false
            end
        end

    end
end
