class ChangeCustomSelectValueIndex < ActiveRecord::Migration
    def change
        remove_index :custom_select_values, :sort_index
        add_index :custom_select_values, [:custom_select_id, :sort_index], unique: true
    end
end
