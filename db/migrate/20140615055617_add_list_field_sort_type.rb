class AddListFieldSortType < ActiveRecord::Migration
    def change

        add_column :list_field_defs, :sort_type, :integer, default: 1

        add_column :system_settings, :locked_list_ids, :integer, array: true

        reversible do |dir|
            dir.up do
                SystemSetting.all.each do |system_setting|
                    system_setting.locked_list_ids = [1]
                    system_setting.save!
                    system_setting.locked_list_ids = []
                    system_setting.save!
                end

                change_column :system_settings, :locked_list_ids, :integer, array: true, null: false

                SystemSetting.where(data_lock: false, mentenance_mode: false).update_all(mentenance_mode: true)
                SystemSetting.where(mentenance_mode: true).update_all(field_locked_at: Time.zone.now)
            end
        end
    end
end

