class AddCheckFormatToFieldDef < ActiveRecord::Migration
  	def change
        add_column :field_defs, :check_format, :text, null: false, default: ""

        add_column :system_settings, :upload_xml_string, :text
  	end
end
