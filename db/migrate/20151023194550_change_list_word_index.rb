class ChangeListWordIndex < ActiveRecord::Migration
    def change
        remove_index :pri_list_level5_words, column: [:list_def_id, :value1, :value2, :value3, :value4, :value5], unique: true, name: 'pri_list_level5_words_uniq'
        remove_index :pri_list_level4_words, column: [:list_def_id, :value1, :value2, :value3, :value4], unique: true, name: 'pri_list_level4_words_uniq'
        remove_index :pri_list_level3_words, column: [:list_def_id, :value1, :value2, :value3], unique: true, name: 'pri_list_level3_words_uniq'
        remove_index :pri_list_level2_words, column: [:list_def_id, :value1, :value2], unique: true, name: 'pri_list_level2_words_uniq'

        remove_index :pub_list_level5_words, column: [:list_def_id, :value1, :value2, :value3, :value4, :value5, :limited], unique: true, name: 'pub_list_level5_words_uniq'
        remove_index :pub_list_level4_words, column: [:list_def_id, :value1, :value2, :value3, :value4, :limited], unique: true, name: 'pub_list_level4_words_uniq'
        remove_index :pub_list_level3_words, column: [:list_def_id, :value1, :value2, :value3, :limited], unique: true, name: 'pub_list_level3_words_uniq'
        remove_index :pub_list_level2_words, column: [:list_def_id, :value1, :value2, :limited], unique: true, name: 'pub_list_level2_words_uniq'


        add_index :pri_list_level5_words, [:list_def_id, :value1, :value2, :value3, :value4, :value5, :value5_prefix], unique: true, name: 'pri_list_level5_words_uniq'
        add_index :pri_list_level4_words, [:list_def_id, :value1, :value2, :value3, :value4, :value4_prefix], unique: true, name: 'pri_list_level4_words_uniq'
        add_index :pri_list_level3_words, [:list_def_id, :value1, :value2, :value3, :value3_prefix], unique: true, name: 'pri_list_level3_words_uniq'
        add_index :pri_list_level2_words, [:list_def_id, :value1, :value2, :value2_prefix], unique: true, name: 'pri_list_level2_words_uniq'

        add_index :pub_list_level5_words, [:list_def_id, :value1, :value2, :value3, :value4, :value5, :limited, :value5_prefix], unique: true, name: 'pub_list_level5_words_uniq'
        add_index :pub_list_level4_words, [:list_def_id, :value1, :value2, :value3, :value4, :limited, :value4_prefix], unique: true, name: 'pub_list_level4_words_uniq'
        add_index :pub_list_level3_words, [:list_def_id, :value1, :value2, :value3, :limited, :value3_prefix], unique: true, name: 'pub_list_level3_words_uniq'
        add_index :pub_list_level2_words, [:list_def_id, :value1, :value2, :limited, :value2_prefix], unique: true, name: 'pub_list_level2_words_uniq'
    end
end
