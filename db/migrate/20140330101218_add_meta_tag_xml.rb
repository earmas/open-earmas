class AddMetaTagXml < ActiveRecord::Migration
    def change
        add_column :system_settings, :meta_info_xml_string, :text
        ListDef.update_all(rss_xml_string: nil)
    end
end
