class AddJournalPartsViews < ActiveRecord::Migration
    def change
        add_column :ej_journal_defs, :voliss_list_html_set_def_id,  :integer
        add_column :ej_journal_defs, :article_list_html_set_def_id, :integer
        add_column :ej_journal_defs, :static_list_html_set_def_id,  :integer
        add_column :ej_journal_defs, :journal_top_html_set_def_id,  :integer

        remove_column :ej_journal_defs, :journals_html_ja,  :integer
        remove_column :ej_journal_defs, :journals_html_en,  :integer
    end
end
