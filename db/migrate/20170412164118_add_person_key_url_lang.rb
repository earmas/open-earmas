class AddPersonKeyUrlLang < ActiveRecord::Migration
    def change
        add_column :people_settings, :key1_param_en,       :string
        add_column :people_settings, :key2_param_en,       :string
        add_column :people_settings, :key3_param_en,       :string
        add_column :people_settings, :key4_param_en,       :string
        add_column :people_settings, :key5_param_en,       :string
        add_column :people_settings, :key6_param_en,       :string
        add_column :people_settings, :key7_param_en,       :string
        add_column :people_settings, :key8_param_en,       :string
        add_column :people_settings, :key9_param_en,       :string
        add_column :people_settings, :key10_param_en,      :string

        rename_column :people_settings, :key1_param, :key1_param_ja
        rename_column :people_settings, :key2_param, :key2_param_ja
        rename_column :people_settings, :key3_param, :key3_param_ja
        rename_column :people_settings, :key4_param, :key4_param_ja
        rename_column :people_settings, :key5_param, :key5_param_ja
        rename_column :people_settings, :key6_param, :key6_param_ja
        rename_column :people_settings, :key7_param, :key7_param_ja
        rename_column :people_settings, :key8_param, :key8_param_ja
        rename_column :people_settings, :key9_param, :key9_param_ja
        rename_column :people_settings, :key10_param,:key10_param_ja


        reversible do |dir|
            dir.up do
                PeopleSetting.all.each do |people_setting|
                    people_setting.key1_param_en = people_setting.key1_param_ja
                    people_setting.key2_param_en = people_setting.key2_param_ja
                    people_setting.key3_param_en = people_setting.key3_param_ja
                    people_setting.key4_param_en = people_setting.key4_param_ja
                    people_setting.key5_param_en = people_setting.key5_param_ja
                    people_setting.key6_param_en = people_setting.key6_param_ja
                    people_setting.key7_param_en = people_setting.key7_param_ja
                    people_setting.key8_param_en = people_setting.key8_param_ja
                    people_setting.key9_param_en = people_setting.key9_param_ja
                    people_setting.key10_param_en = people_setting.key10_param_ja
                    people_setting.save!
                end
            end
        end

    end
end
