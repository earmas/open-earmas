class CreateLeftMenuDefs < ActiveRecord::Migration
    def change
        create_table :left_menu_defs do |t|
            t.integer   :parent_menu,       null: true
            t.integer   :menu_type,         null: false

            t.integer   :menu_id,           null: true
            t.string    :caption_ja,        null: true
            t.string    :caption_en,        null: true

            t.integer   :sort_index,        null: false, default: 0

            t.timestamps
        end
        add_index :left_menu_defs, [:parent_menu, :sort_index], unique: true
    end
end
