class AddEjUserField < ActiveRecord::Migration
    def change
        add_column :staffs, :ej_journal_def_id,       :integer, nil: true
        add_column :staffs, :role,                    :integer, nil: false, default: 1

        reversible do |dir|
            dir.up do
                Staff.all.each do |a|
                    a.role = a.has_admin_role ? 9 : 1
                    a.save!
                end
            end
        end

    end
end
