class AddDocument < ActiveRecord::Migration

    def change
        create_table :field_defs do |t|
            t.string    :code,              null: false
            t.text      :caption_ja,        null: false
            t.text      :caption_en,        null: false
            t.text      :description,       null: false

            t.boolean   :must,              null: false, default: false
            t.boolean   :multi,             null: false, default: true

            t.boolean   :result,            null: false, default: true
            t.boolean   :detail,            null: false, default: true
            t.integer   :csv_column,        null: false, default: 0
            t.boolean   :sortable,          null: false, default: false
            t.boolean   :to_pub,            null: false, default: true

            t.string    :unit_prefix_ja,    null: true
            t.string    :unit_suffix_ja,    null: true
            t.string    :unit_prefix_en,    null: true
            t.string    :unit_suffix_en,    null: true

            t.integer   :sort_index,        null: false, default: 0

            t.integer   :field_type_id,     null: false

            t.text      :default_value,     null: true
            t.string    :in_field_delim,    null: true

            t.integer   :field_param_id1,   null: true
            t.integer   :field_param_id2,   null: true
            t.integer   :field_param_id3,   null: true
            t.text      :field_param_text1, null: true
            t.text      :field_param_text2, null: true
            t.text      :field_param_text3, null: true

            t.text      :sort_conv_pattern, null: true
            t.text      :sort_conv_format,  null: true

            t.timestamps
        end
        add_index :field_defs, :sort_index, unique: true
        add_index :field_defs, :code, unique: true



        create_table :sortable_fields do |t|
            t.integer   :field_def_id,      null: false
            t.integer   :sort_index,        null: false, default: 0

            t.timestamps
        end
        add_index :sortable_fields, :sort_index, unique: true
        add_index :sortable_fields, :field_def_id, unique: true



        create_table :pri_documents do |t|
            t.integer   :input_group_id,            null: false, default: 0

            t.string    :thumnail_mime,             null: true
            t.integer   :thumnail_size,             null: true

            t.integer   :publish_to,                null: false, default: 2
            t.datetime  :publish_at,                null: true
            t.integer   :publisher_staff_id,        null: true

            t.boolean   :oaipmh,                    null: false, default: false
            t.text      :set_spec,                  null: false, array: true
            t.boolean   :deleted,                   null: false, default: false

            t.integer   :updater_staff_id,          null: false
            t.integer   :creator_staff_id,          null: false

            t.timestamps
        end
        add_index :pri_documents, :creator_staff_id
        add_index :pri_documents, :updater_staff_id
        add_index :pri_documents, :created_at
        add_index :pri_documents, :updated_at
        add_index :pri_documents, :publish_at

        create_table :pub_documents, id: false do |t|
            t.integer   :id,                        null: false
            t.integer   :input_group_id,            null: false, default: 0

            t.string    :thumnail_mime,             null: true
            t.integer   :thumnail_size,             null: true

            t.boolean   :limited,                   null: false
            t.string    :publish_at_history,        null: true, array: true
            t.datetime  :max_publish_at,            null: true
            t.datetime  :min_publish_at,            null: true

            t.boolean   :oaipmh,                    null: false, default: false
            t.boolean   :oaipmh_deleted,            null: false, default: false
            t.text      :set_spec,                  null: false, array: true
            t.boolean   :deleted,                   null: false, default: false

            t.timestamps
        end
        add_index :pub_documents, :limited
        add_index :pub_documents, :created_at
        add_index :pub_documents, :updated_at
        add_index :pub_documents, :set_spec,  using: :gin
        add_index :pub_documents, :oaipmh
        add_index :pub_documents, :oaipmh_deleted
        reversible do |dir|
            dir.up do
                execute "ALTER TABLE pub_documents ADD PRIMARY KEY (id);"
            end
        end


        create_table :document_filter_defs do |t|
            t.integer   :parent_type,           null: false
            t.integer   :parent_id,             null: false
            t.boolean   :include_filter,        null: false, default: true
            t.integer   :filter_type,           null: false
            t.integer   :some_id,               null: false
            t.text      :filter_value,          null: true

            t.timestamps
        end
        add_index :document_filter_defs, :parent_type
        add_index :document_filter_defs, :parent_id




    end
end
