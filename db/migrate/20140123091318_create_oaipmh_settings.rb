class CreateOaipmhSettings < ActiveRecord::Migration
    def change
        create_table :oaipmh_settings do |t|
            t.text      :repository_name,           null: false
            t.text      :base_url,                  null: false
            t.text      :admin_email,               null: false
            t.text      :identifier_prefix,         null: false

            t.integer   :set_spec_field_def_id,     null: true
            t.text      :set_spec_default_value,    null: true

            t.timestamps
        end

        create_table :oaipmh_formats do |t|
            t.string    :metadata_prefix,       null: false
            t.text      :schema,                null: false
            t.text      :metadata_namespace,    null: false
            t.text      :xml_string,            null: false

            t.timestamps
        end
    end
end

