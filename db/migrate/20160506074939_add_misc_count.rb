class AddMiscCount < ActiveRecord::Migration
    def change

        create_table :misc_counts do |t|
            t.integer   :document_id,       null: false

            t.integer   :count_1,           null: false, default: 0
            t.integer   :count_2,           null: false, default: 0
            t.integer   :count_3,           null: false, default: 0
            t.integer   :count_4,           null: false, default: 0

            t.integer   :count_5,           null: false, default: 0
            t.integer   :count_6,           null: false, default: 0
            t.integer   :count_7,           null: false, default: 0
            t.integer   :count_8,           null: false, default: 0
            t.integer   :count_9,           null: false, default: 0

            t.timestamps
        end
        add_index :misc_counts, :document_id, unique: true
    end
end
