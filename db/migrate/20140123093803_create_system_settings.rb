class CreateSystemSettings < ActiveRecord::Migration
    def change
        create_table :system_settings do |t|
            t.boolean   :enable_oaipmh,     null: false
            t.text      :sytem_name,        null: false

            t.integer   :thema_id,          null: true
            t.text      :css_string,        null: true

            t.integer   :top_page_type,     null: true
            t.integer   :top_page_id,       null: true


            t.boolean   :data_lock,         null: false, default: false
            t.boolean   :mentenance_mode,   null: false, default: false

            t.datetime  :field_locked_at,   null: true
            t.datetime  :list_locked_at,    null: true
            t.datetime  :search_locked_at,  null: true
            t.datetime  :oaipmh_locked_at,  null: true

            t.timestamps
        end

        create_table :download_formats do |t|
            t.text      :caption_ja,    null: false
            t.text      :caption_en,    null: false
            t.text      :xml_string,    null: false

            t.integer   :sort_index,    null: false, default: 0

            t.timestamps
        end

    end
end
