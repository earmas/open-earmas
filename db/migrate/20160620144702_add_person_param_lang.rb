class AddPersonParamLang < ActiveRecord::Migration
    def change
        add_column :people_settings, :key1_name_en,       :string
        add_column :people_settings, :key2_name_en,       :string
        add_column :people_settings, :key3_name_en,       :string
        add_column :people_settings, :key4_name_en,       :string
        add_column :people_settings, :key5_name_en,       :string
        add_column :people_settings, :key6_name_en,       :string
        add_column :people_settings, :key7_name_en,       :string
        add_column :people_settings, :key8_name_en,       :string
        add_column :people_settings, :key9_name_en,       :string
        add_column :people_settings, :key10_name_en,      :string

        rename_column :people_settings, :key1_name, :key1_name_ja
        rename_column :people_settings, :key2_name, :key2_name_ja
        rename_column :people_settings, :key3_name, :key3_name_ja
        rename_column :people_settings, :key4_name, :key4_name_ja
        rename_column :people_settings, :key5_name, :key5_name_ja
        rename_column :people_settings, :key6_name, :key6_name_ja
        rename_column :people_settings, :key7_name, :key7_name_ja
        rename_column :people_settings, :key8_name, :key8_name_ja
        rename_column :people_settings, :key9_name, :key9_name_ja
        rename_column :people_settings, :key10_name,:key10_name_ja


        reversible do |dir|
            dir.up do
                PeopleSetting.all.each do |people_setting|
                    people_setting.key1_name_en = people_setting.key1_name_ja
                    people_setting.key2_name_en = people_setting.key2_name_ja
                    people_setting.key3_name_en = people_setting.key3_name_ja
                    people_setting.key4_name_en = people_setting.key4_name_ja
                    people_setting.key5_name_en = people_setting.key5_name_ja
                    people_setting.key6_name_en = people_setting.key6_name_ja
                    people_setting.key7_name_en = people_setting.key7_name_ja
                    people_setting.key8_name_en = people_setting.key8_name_ja
                    people_setting.key9_name_en = people_setting.key9_name_ja
                    people_setting.key10_name_en = people_setting.key10_name_ja
                    people_setting.save!
                end
            end
        end

    end
end
