class AddEjSetting < ActiveRecord::Migration
    def change
        add_column :system_settings, :ej_locked_at, :datetime, null: true
        add_column :system_settings, :locked_ej_ids, :integer, array: true

        reversible do |dir|
            dir.up do
                SystemSetting.all.each do |system_setting|
                    system_setting.locked_ej_ids = [1]
                    system_setting.save!
                    system_setting.locked_ej_ids = []
                    system_setting.save!
                end
                change_column :system_settings, :locked_ej_ids, :integer, array: true, null: false
            end
        end

        create_table :ej_defs do |t|
            t.integer   :journal_field_def_id,       null: false
            t.integer   :volume_field_def_id,        null: false
            t.integer   :issue_field_def_id,         null: false
            t.integer   :section_field_def_id,       null: false
            t.integer   :publish_date_field_def_id,  null: false
            t.integer   :sort_field_def_id,          null: true

            t.text      :css_string
            t.text      :rss_xml_string

            t.timestamps
        end



        create_table :ej_journal_defs do |t|
            t.integer   :ej_def_id,             null: false
            t.string    :code,                  null: false
            t.text      :value,                 null: false
            t.integer   :visible,               null: false, default: 2

            t.text      :css_string
            t.text      :journals_html_ja
            t.text      :journals_html_en

            t.string    :thumnail_mime
            t.integer   :thumnail_size


            t.integer   :journal_view_type_id,              null: false
            t.integer   :journal_sub_header_view_type_id,   null: false
            t.integer   :journal_sub_footer_view_type_id,   null: false
            t.integer   :journal_html_set_def_id

            t.integer   :article_view_type_id,              null: false
            t.integer   :article_sub_header_view_type_id,   null: false
            t.integer   :article_sub_footer_view_type_id,   null: false
            t.integer   :article_html_set_def_id

            t.timestamps
        end
        add_index :ej_journal_defs, :code, unique: true
        add_index :ej_journal_defs, :ej_def_id


        create_table :ej_static_page_defs do |t|
            t.integer  :ej_journal_def_id,      null: false
            t.string   :code,                   null: false
            t.text     :caption_ja,             null: false
            t.text     :caption_en,             null: false
            t.integer  :sort_index,             null: false, default: 0

            t.integer  :journal_static_view_type_id,               null: false
            t.integer  :journal_static_sub_header_view_type_id,    null: false
            t.integer  :journal_static_sub_footer_view_type_id,    null: false
            t.text     :journal_static_html_set_def_id

            t.timestamps
        end
        add_index :ej_static_page_defs, :ej_journal_def_id
        add_index :ej_static_page_defs, [:ej_journal_def_id, :code], unique: true
        add_index :ej_static_page_defs, [:ej_journal_def_id, :sort_index], unique: true


        create_table :ej_result_fields do |t|
            t.integer   :ej_def_id,             null: false
            t.integer   :field_def_id,          null: false

            t.integer   :sort_index,            null: false, default: 0

            t.timestamps
        end
        add_index :ej_result_fields, :ej_def_id
        add_index :ej_result_fields, [:ej_def_id, :sort_index], unique: true
        add_index :ej_result_fields, [:ej_def_id, :field_def_id], unique: true


        create_table :ej_detail_fields do |t|
            t.integer   :ej_def_id,             null: false
            t.integer   :field_def_id,          null: false

            t.integer   :sort_index,            null: false, default: 0

            t.timestamps
        end
        add_index :ej_detail_fields, :ej_def_id
        add_index :ej_detail_fields, [:ej_def_id, :sort_index], unique: true
        add_index :ej_detail_fields, [:ej_def_id, :field_def_id], unique: true


        create_table :pri_ej_data do |t|
            t.integer  :ej_def_id,              null: false
            t.integer  :document_id,            null: false
            t.integer  :publish_to,             null: false

            t.text     :journal,                null: false
            t.text     :volume,                 null: false
            t.text     :issue,                  null: false
            t.text     :publish_date,           null: false

            t.text     :sort_value_ja
            t.text     :sort_value_en

            t.integer  :ej_word_ids,            array: true

            t.datetime :document_created_at,    null: false
            t.datetime :document_updated_at,    null: false

            t.timestamps
        end
        add_index :pri_ej_data, [:ej_def_id, :document_id], unique: true
        add_index :pri_ej_data, :ej_def_id
        add_index :pri_ej_data, :document_id
        add_index :pri_ej_data, :publish_to
        add_index :pri_ej_data, :journal
        add_index :pri_ej_data, :volume
        add_index :pri_ej_data, :issue
        add_index :pri_ej_data, :publish_date

        add_index :pri_ej_data, :sort_value_ja
        add_index :pri_ej_data, :sort_value_en



        create_table :pri_ej_words do |t|
            t.integer  :ej_def_id,                  null: false
            t.integer  :document_count,             null: false, default: 0
 
            t.text     :journal,                    null: false, default: ""
            t.text     :journal_caption_ja
            t.text     :journal_caption_en

            t.text     :volume,                     null: false, default: ""
            t.text     :volume_caption_ja
            t.text     :volume_caption_en

            t.text     :issue,                      null: false, default: ""
            t.text     :issue_caption_ja
            t.text     :issue_caption_en

            t.text     :publish_date,               null: false, default: ""
            t.text     :publish_date_sort_value_ja
            t.text     :publish_date_sort_value_en
            t.text     :publish_date_caption_ja
            t.text     :publish_date_caption_en

            t.timestamps
        end
        add_index :pri_ej_words, [:ej_def_id, :journal, :volume, :issue, :publish_date], unique: true, name: 'pri_ej_words_uniq'

        add_index :pri_ej_words, :ej_def_id
        add_index :pri_ej_words, :journal
        add_index :pri_ej_words, :volume
        add_index :pri_ej_words, :issue
        add_index :pri_ej_words, :publish_date

        add_index :pri_ej_words, :publish_date_sort_value_ja
        add_index :pri_ej_words, :publish_date_sort_value_en



        create_table :pub_ej_data do |t|
            t.integer  :ej_def_id,              null: false
            t.integer  :document_id,            null: false
            t.boolean  :limited,                null: false

            t.text     :journal,                null: false
            t.text     :volume,                 null: false
            t.text     :issue,                  null: false
            t.text     :publish_date,           null: false

            t.text     :sort_value_ja
            t.text     :sort_value_en

            t.integer  :ej_word_ids,            array: true

            t.datetime :document_created_at,    null: false
            t.datetime :document_updated_at,    null: false

            t.timestamps
        end
        add_index :pub_ej_data, [:ej_def_id, :document_id], unique: true
        add_index :pub_ej_data, :ej_def_id
        add_index :pub_ej_data, :document_id
        add_index :pub_ej_data, :limited
        add_index :pub_ej_data, :journal
        add_index :pub_ej_data, :volume
        add_index :pub_ej_data, :issue
        add_index :pub_ej_data, :publish_date

        add_index :pub_ej_data, :sort_value_ja
        add_index :pub_ej_data, :sort_value_en



        create_table :pub_ej_words do |t|
            t.integer  :ej_def_id,                  null: false
            t.integer  :document_count,             null: false, default: 0
            t.boolean  :limited,                    null: false

            t.text     :journal,                    null: false, default: ""
            t.text     :journal_caption_ja
            t.text     :journal_caption_en

            t.text     :volume,                     null: false, default: ""
            t.text     :volume_caption_ja
            t.text     :volume_caption_en

            t.text     :issue,                      null: false, default: ""
            t.text     :issue_caption_ja
            t.text     :issue_caption_en

            t.text     :publish_date,               null: false, default: ""
            t.text     :publish_date_sort_value_ja
            t.text     :publish_date_sort_value_en
            t.text     :publish_date_caption_ja
            t.text     :publish_date_caption_en

            t.timestamps
        end
        add_index :pub_ej_words, [:ej_def_id, :journal, :volume, :issue, :publish_date, :limited], unique: true, name: 'pub_ej_words_uniq'

        add_index :pub_ej_words, :ej_def_id
        add_index :pub_ej_words, :limited
        add_index :pub_ej_words, :journal
        add_index :pub_ej_words, :volume
        add_index :pub_ej_words, :issue
        add_index :pub_ej_words, :publish_date

        add_index :pub_ej_words, :publish_date_sort_value_ja
        add_index :pub_ej_words, :publish_date_sort_value_en
    end
end

