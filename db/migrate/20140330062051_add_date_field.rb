class AddDateField < ActiveRecord::Migration
    def change
        create_table :pri_field_date_data do |t|
            t.integer   :document_id,   null: false
            t.integer   :field_def_id,  null: false
            t.integer   :sort_index,    null: false, default: 0

            t.timestamp :value1,        null: false

            t.timestamps
        end
        add_index :pri_field_date_data, :document_id
        add_index :pri_field_date_data, :field_def_id
        add_index :pri_field_date_data, :sort_index


        create_table :pub_field_date_data, id: false do |t|
            t.integer   :id,            null: false
            t.integer   :document_id,   null: false
            t.integer   :field_def_id,  null: false
            t.integer   :sort_index,    null: false, default: 0

            t.timestamp :value1,        null: false

            t.timestamps
        end
        add_index :pub_field_date_data, :document_id
        add_index :pub_field_date_data, :field_def_id
        add_index :pub_field_date_data, :sort_index
        reversible do |dir|
            dir.up do
                execute "ALTER TABLE pub_field_date_data ADD PRIMARY KEY (id);"
            end
        end
    end
end
