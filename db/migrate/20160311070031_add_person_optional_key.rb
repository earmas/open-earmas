class AddPersonOptionalKey < ActiveRecord::Migration
    def change

        add_column :pri_field_repository_person_data, :optional_key1,       :string, null: false, default: ''
        add_column :pri_field_repository_person_data, :optional_key2,       :string, null: false, default: ''

        add_column :pub_field_repository_person_data, :optional_key1,       :string, null: false, default: ''
        add_column :pub_field_repository_person_data, :optional_key2,       :string, null: false, default: ''
    end
end
