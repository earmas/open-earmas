class ChangePeopleNameLenght < ActiveRecord::Migration
    def change
        change_column :people, :url, :text

        change_column :people, :affiliation_ja, :text
        change_column :people, :affiliation_en, :text
    end
end
