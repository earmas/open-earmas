class AddSubHeaderType < ActiveRecord::Migration
    def change
        add_column :list_defs, :result_sub_header_view_type_id, :integer
        add_column :list_defs, :detail_sub_header_view_type_id, :integer

        add_column :list_field_defs, :list_param_sub_header_view_type_id, :integer

        add_column :search_settings, :detail_sub_header_view_type_id, :integer



        reversible do |dir|
            dir.up do
                ListDef.update_all(result_sub_header_view_type_id: 1)
                ListDef.update_all(detail_sub_header_view_type_id: 1)

                ListFieldDef.update_all(list_param_sub_header_view_type_id: 1)

                SearchSetting.update_all(detail_sub_header_view_type_id: 1)

                change_column :list_defs, :result_sub_header_view_type_id,  :integer, null: false
                change_column :list_defs, :detail_sub_header_view_type_id,  :integer, null: false

                change_column :list_field_defs, :list_param_sub_header_view_type_id,    :integer, null: false

                change_column :search_settings, :detail_sub_header_view_type_id,    :integer, null: true
            end
        end
    end
end
