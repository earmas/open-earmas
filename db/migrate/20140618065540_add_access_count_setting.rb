class AddAccessCountSetting < ActiveRecord::Migration
    def change

        create_table :access_count_settings do |t|
            t.text      :my_url,            null: false, default: 'localhost'
            t.boolean   :filter_bot,        null: false, default: true
            t.text      :bot_names
            t.text      :bot_address

            t.timestamps
        end
    end
end
