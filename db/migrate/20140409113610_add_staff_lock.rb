class AddStaffLock < ActiveRecord::Migration
    def change
        add_column :staffs, :failed_attempts, :integer,  default: 0, null: false
        add_column :staffs, :unlock_token, :string
        add_column :staffs, :locked_at, :datetime
    end
end
