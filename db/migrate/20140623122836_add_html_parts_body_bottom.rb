class AddHtmlPartsBodyBottom < ActiveRecord::Migration
    def change
        add_column :html_set_defs, :body_bottom_html_string_ja, :text
        add_column :html_set_defs, :body_bottom_html_string_en, :text

        rename_column   :html_set_defs,    :body_html_string_ja, :body_top_html_string_ja
        rename_column   :html_set_defs,    :body_html_string_en, :body_top_html_string_en
    end
end
