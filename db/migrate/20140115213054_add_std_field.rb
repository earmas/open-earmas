class AddStdField < ActiveRecord::Migration

    def change
        create_table :pri_field_text1_data do |t|
            t.integer   :document_id,   null: false
            t.integer   :field_def_id,  null: false
            t.integer   :sort_index,    null: false, default: 0

            t.text      :value1,        null: false

            t.timestamps
        end
        add_index :pri_field_text1_data, :document_id
        add_index :pri_field_text1_data, :field_def_id
        add_index :pri_field_text1_data, :sort_index


        create_table :pub_field_text1_data, id: false do |t|
            t.integer   :id,            null: false
            t.integer   :document_id,   null: false
            t.integer   :field_def_id,  null: false
            t.integer   :sort_index,    null: false, default: 0

            t.text      :value1,        null: false

            t.timestamps
        end
        add_index :pub_field_text1_data, :document_id
        add_index :pub_field_text1_data, :field_def_id
        add_index :pub_field_text1_data, :sort_index
        reversible do |dir|
            dir.up do
                execute "ALTER TABLE pub_field_text1_data ADD PRIMARY KEY (id);"
            end
        end


        create_table :pri_field_text2_data do |t|
            t.integer   :document_id,   null: false
            t.integer   :field_def_id,  null: false
            t.integer   :sort_index,    null: false, default: 0

            t.text      :value1,        null: false
            t.text      :value2,        null: false

            t.timestamps
        end
        add_index :pri_field_text2_data, :document_id
        add_index :pri_field_text2_data, :field_def_id
        add_index :pri_field_text2_data, :sort_index


        create_table :pub_field_text2_data, id: false do |t|
            t.integer   :id,            null: false
            t.integer   :document_id,   null: false
            t.integer   :field_def_id,  null: false
            t.integer   :sort_index,    null: false, default: 0

            t.text      :value1,        null: false
            t.text      :value2,        null: false

            t.timestamps
        end
        add_index :pub_field_text2_data, :document_id
        add_index :pub_field_text2_data, :field_def_id
        add_index :pub_field_text2_data, :sort_index
        reversible do |dir|
            dir.up do
                execute "ALTER TABLE pub_field_text2_data ADD PRIMARY KEY (id);"
            end
        end


        create_table :pri_field_number_data do |t|
            t.integer   :document_id,   null: false
            t.integer   :field_def_id,  null: false
            t.integer   :sort_index,    null: false, default: 0

            t.integer   :some_id,       null: false, default: 0

            t.timestamps
        end
        add_index :pri_field_number_data, :document_id
        add_index :pri_field_number_data, :field_def_id
        add_index :pri_field_number_data, :sort_index
        add_index :pri_field_number_data, :some_id



        create_table :pub_field_value_caption_data, id: false do |t|
            t.integer   :id,            null: false
            t.integer   :document_id,   null: false
            t.integer   :field_def_id,  null: false
            t.integer   :sort_index,    null: false, default: 0

            t.string    :value,         null: false
            t.text      :caption_ja,    null: false
            t.text      :caption_en,    null: false

            t.timestamps
        end
        add_index :pub_field_value_caption_data, :document_id
        add_index :pub_field_value_caption_data, :field_def_id
        add_index :pub_field_value_caption_data, :sort_index
        add_index :pub_field_value_caption_data, :value
        reversible do |dir|
            dir.up do
                execute "ALTER TABLE pub_field_value_caption_data ADD PRIMARY KEY (id);"
            end
        end



        create_table :pri_field_file_data do |t|
            t.integer   :document_id,       null: false
            t.integer   :field_def_id,      null: false
            t.integer   :sort_index,        null: false, default: 0

            t.text      :org_file_name,     null: false
            t.string    :mime,              null: false
            t.integer   :size,              null: false, default: 0
            t.text      :stored_dir_name,   null: false

            t.timestamps
        end
        add_index :pri_field_file_data, :document_id
        add_index :pri_field_file_data, :field_def_id
        add_index :pri_field_file_data, :sort_index



        create_table :pub_field_file_data, id: false do |t|
            t.integer   :id,                null: false
            t.integer   :document_id,       null: false
            t.integer   :field_def_id,      null: false
            t.integer   :sort_index,        null: false, default: 0

            t.text      :org_file_name,     null: false
            t.string    :mime,              null: false
            t.integer   :size,              null: false, default: 0
            t.text      :stored_dir_path,   null: false

            t.timestamps
        end
        add_index :pub_field_file_data, :document_id
        add_index :pub_field_file_data, :field_def_id
        add_index :pub_field_file_data, :sort_index
        reversible do |dir|
            dir.up do
                execute "ALTER TABLE pub_field_file_data ADD PRIMARY KEY (id);"
            end
        end

    end
end
