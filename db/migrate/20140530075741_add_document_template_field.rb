class AddDocumentTemplateField < ActiveRecord::Migration
    def change

        create_table :document_templates do |t|
            t.integer   :staff_id,          null: false
            t.integer   :input_group_id,    null: false, default: 0
            t.integer   :publish_to,        null: false, default: 2

            t.string    :name,              null: false
            t.text      :description,       null: false

            t.timestamps
        end
        add_index :document_templates, :staff_id


        create_table :document_template_fields do |t|
            t.integer   :staff_id,              null: false
            t.integer   :document_template_id,  null: false
            t.integer   :field_def_id,          null: false

            t.text      :default_value,         null: true
            t.timestamps
        end
        add_index :document_template_fields, [:staff_id, :document_template_id, :field_def_id], unique: true, name: 'document_template_fields_uniq'
        add_index :document_template_fields, :staff_id
        add_index :document_template_fields, :field_def_id
        add_index :document_template_fields, :document_template_id
    end
end
