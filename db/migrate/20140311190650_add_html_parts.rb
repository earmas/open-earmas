class AddHtmlParts < ActiveRecord::Migration
    def change

        create_table :html_parts_defs do |t|
            t.string    :name,              null: false
            t.text      :html_string_ja,    null: true
            t.text      :html_string_en,    null: true
            t.timestamps
        end

        create_table :html_set_defs do |t|
            t.text      :body_html_string_ja,   null: true
            t.text      :body_html_string_en,   null: true
            t.integer   :header_parts_id,       null: true
            t.integer   :footer_parts_id,       null: true
            t.integer   :right_side_parts_id,   null: true
            t.timestamps
        end
    end
end
