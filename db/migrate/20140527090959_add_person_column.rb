class AddPersonColumn < ActiveRecord::Migration

    def change
        add_column :people, :family_name_alt1, :string
        add_column :people, :given_name_alt1,  :string
        add_column :people, :family_name_alt2, :string
        add_column :people, :given_name_alt2,  :string
        add_column :people, :family_name_alt3, :string
        add_column :people, :given_name_alt3,  :string

        add_column :people, :affiliation_ja, :string
        add_column :people, :affiliation_en, :string


        create_table :pri_field_repository_person_data do |t|
            t.integer   :document_id,   null: false
            t.integer   :field_def_id,  null: false
            t.integer   :sort_index,    null: false, default: 0

            t.text      :person_id,             null: true

            t.text      :family_name_ja,        null: false
            t.text      :given_name_ja,         null: false
            t.text      :family_name_en,        null: false
            t.text      :given_name_en,         null: false
            t.text      :family_name_tra,       null: false
            t.text      :given_name_tra,        null: false

            t.text      :family_name_alt1,      null: false
            t.text      :given_name_alt1,       null: false
            t.text      :family_name_alt2,      null: false
            t.text      :given_name_alt2,       null: false
            t.text      :family_name_alt3,      null: false
            t.text      :given_name_alt3,       null: false

            t.text      :affiliation_ja,        null: false
            t.text      :affiliation_en,        null: false

            t.timestamps
        end
        add_index :pri_field_repository_person_data, :document_id
        add_index :pri_field_repository_person_data, :field_def_id
        add_index :pri_field_repository_person_data, :sort_index


        create_table :pub_field_repository_person_data, id: false do |t|
            t.integer   :id,            null: false
            t.integer   :document_id,   null: false
            t.integer   :field_def_id,  null: false
            t.integer   :sort_index,    null: false, default: 0

            t.text      :person_id,             null: true

            t.text      :family_name_ja,        null: false
            t.text      :given_name_ja,         null: false
            t.text      :family_name_en,        null: false
            t.text      :given_name_en,         null: false
            t.text      :family_name_tra,       null: false
            t.text      :given_name_tra,        null: false

            t.text      :family_name_alt1,      null: false
            t.text      :given_name_alt1,       null: false
            t.text      :family_name_alt2,      null: false
            t.text      :given_name_alt2,       null: false
            t.text      :family_name_alt3,      null: false
            t.text      :given_name_alt3,       null: false

            t.text      :affiliation_ja,        null: false
            t.text      :affiliation_en,        null: false

            t.timestamps
        end
        add_index :pub_field_repository_person_data, :document_id
        add_index :pub_field_repository_person_data, :field_def_id
        add_index :pub_field_repository_person_data, :sort_index
        reversible do |dir|
            dir.up do
                execute "ALTER TABLE pub_field_repository_person_data ADD PRIMARY KEY (id);"
            end
        end


        reversible do |dir|
            dir.up do
                FieldDef.where(field_type_id: FieldTypes::RepositoryPerson::TYPE_ID).each do |field_def|
                    PriFieldText9Datum.where(field_def_id: field_def.id).each do |field|
                        new_field = PriFieldRepositoryPersonDatum.new

                        new_field.id = field.id
                        new_field.document_id = field.document_id
                        new_field.field_def_id = field.field_def_id
                        new_field.sort_index = field.sort_index

                        new_field.person_id = field.some_id

                        new_field.family_name_ja = field.value1
                        new_field.given_name_ja = field.value2
                        new_field.family_name_en = field.value3
                        new_field.given_name_en = field.value4
                        new_field.family_name_tra = field.value5
                        new_field.given_name_tra = field.value6

                        new_field.family_name_alt1 = ''
                        new_field.given_name_alt1 = ''
                        new_field.family_name_alt2 = ''
                        new_field.given_name_alt2 = ''
                        new_field.family_name_alt3 = ''
                        new_field.given_name_alt3 = ''

                        new_field.affiliation_ja = ''
                        new_field.affiliation_en = ''

                        new_field.save!
                    end
                    PubFieldText9Datum.where(field_def_id: field_def.id).each do |field|
                        new_field = PubFieldRepositoryPersonDatum.new

                        new_field.id = field.id
                        new_field.document_id = field.document_id
                        new_field.field_def_id = field.field_def_id
                        new_field.sort_index = field.sort_index

                        new_field.person_id = field.some_id

                        new_field.family_name_ja = field.value1
                        new_field.given_name_ja = field.value2
                        new_field.family_name_en = field.value3
                        new_field.given_name_en = field.value4
                        new_field.family_name_tra = field.value5
                        new_field.given_name_tra = field.value6

                        new_field.family_name_alt1 = ''
                        new_field.given_name_alt1 = ''
                        new_field.family_name_alt2 = ''
                        new_field.given_name_alt2 = ''
                        new_field.family_name_alt3 = ''
                        new_field.given_name_alt3 = ''

                        new_field.affiliation_ja = ''
                        new_field.affiliation_en = ''

                        new_field.save!
                    end
                    PriFieldText9Datum.where(field_def_id: field_def.id).delete_all
                    PubFieldText9Datum.where(field_def_id: field_def.id).delete_all
                end

                ActiveRecord::Base.connection.execute("SELECT setval('pri_field_repository_person_data_id_seq', coalesce((SELECT MAX(id)+1 FROM pri_field_repository_person_data), 1), false)")
            end
        end
    end
end
