class AddFileFieldPublishType < ActiveRecord::Migration
    def change
        add_column :pri_field_file_data, :publish_type, :integer, null: false, default: 1
        add_column :pub_field_file_data, :publish_type, :integer, null: false, default: 1
    end
end
