class AddShowResultToInputGroupField < ActiveRecord::Migration
    def change

        add_column :input_group_fields, :result, :boolean, null: false, default: true

    end
end
