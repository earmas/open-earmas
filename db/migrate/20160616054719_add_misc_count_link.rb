class AddMiscCountLink < ActiveRecord::Migration
    def change
        add_column :misc_counts, :count_1_url,       :text, null: false, default: ''
        add_column :misc_counts, :count_2_url,       :text, null: false, default: ''
        add_column :misc_counts, :count_3_url,       :text, null: false, default: ''
        add_column :misc_counts, :count_4_url,       :text, null: false, default: ''
        add_column :misc_counts, :count_5_url,       :text, null: false, default: ''
        add_column :misc_counts, :count_6_url,       :text, null: false, default: ''
        add_column :misc_counts, :count_7_url,       :text, null: false, default: ''
        add_column :misc_counts, :count_8_url,       :text, null: false, default: ''
        add_column :misc_counts, :count_9_url,       :text, null: false, default: ''
    end
end
