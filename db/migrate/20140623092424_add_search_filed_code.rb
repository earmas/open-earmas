class AddSearchFiledCode < ActiveRecord::Migration
    def change
        add_column :search_defs, :code, :string
        add_column :system_settings, :search_api_xml_string, :text

        reversible do |dir|
            dir.up do
                SearchDef.update_all('code = id')

                change_column :search_defs, :code, :string, null: false
                add_index :search_defs, :code, unique: true
            end
        end
    end
end
