class ChangeThemaToTheme < ActiveRecord::Migration
    def change
        rename_column :system_settings, :thema_id, :theme_id
    end
end
