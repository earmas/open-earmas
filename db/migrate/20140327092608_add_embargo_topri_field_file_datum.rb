class AddEmbargoTopriFieldFileDatum < ActiveRecord::Migration
    def change
        add_column :pri_field_file_data, :title, :string
        add_column :pri_field_file_data, :embargo, :datetime

        add_column :pub_field_file_data, :title, :string
        add_column :pub_field_file_data, :embargo, :datetime

        add_column :pri_documents, :embargo_until, :datetime
        add_index :pri_documents, :embargo_until
    end
end
