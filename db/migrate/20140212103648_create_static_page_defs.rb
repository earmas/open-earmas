class CreateStaticPageDefs < ActiveRecord::Migration
    def change
        create_table :static_page_defs do |t|
            t.text     :caption_ja,         null: false
            t.text     :caption_en,         null: false
            t.text     :html_set_def_id,    null: true

            t.timestamps
        end
    end
end
