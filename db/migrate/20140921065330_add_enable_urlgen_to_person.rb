class AddEnableUrlgenToPerson < ActiveRecord::Migration
    def change
        add_column :people, :enable_gen_url, :boolean, null: false, default: true
    end
end

