class AddLeftMenuDefsHtml < ActiveRecord::Migration
    def change
        add_column :left_menu_defs, :html_class_ja, :text, null: false, default: ''
        add_column :left_menu_defs, :html_class_en, :text, null: false, default: ''
    end
end
