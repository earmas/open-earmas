class AddMetaDocument < ActiveRecord::Migration
    def change
        add_column :pri_documents, :meta_tag_xml,  :text
        add_column :pub_documents, :meta_tag_xml,  :text
    end
end
