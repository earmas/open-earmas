class ChangeListWordWithLevel < ActiveRecord::Migration
    def change

        reversible do |dir|
            dir.up do
                PriListDatum.delete_all
                PubListDatum.delete_all
            end
        end

        drop_table :pri_list_words
        drop_table :pub_list_words

        remove_column :pri_list_data, :list_word_ids
        add_column :pri_list_data, :list_word_level1_ids, :integer, array: true
        add_column :pri_list_data, :list_word_level2_ids, :integer, array: true
        add_column :pri_list_data, :list_word_level3_ids, :integer, array: true
        add_column :pri_list_data, :list_word_level4_ids, :integer, array: true
        add_column :pri_list_data, :list_word_level5_ids, :integer, array: true

        add_column :pri_list_data, :document_created_at, :datetime, null: false
        add_column :pri_list_data, :document_updated_at, :datetime, null: false


        remove_column :pub_list_data, :list_word_ids
        add_column :pub_list_data, :list_word_level1_ids, :integer, array: true
        add_column :pub_list_data, :list_word_level2_ids, :integer, array: true
        add_column :pub_list_data, :list_word_level3_ids, :integer, array: true
        add_column :pub_list_data, :list_word_level4_ids, :integer, array: true
        add_column :pub_list_data, :list_word_level5_ids, :integer, array: true

        add_column :pub_list_data, :document_created_at, :datetime, null: false
        add_column :pub_list_data, :document_updated_at, :datetime, null: false



        create_table :pri_list_level5_words do |t|
            t.integer   :list_def_id,       null: false
            t.integer   :document_count,    null: false, default: 0

            t.text      :value1,            null: false, default: ''
            t.text      :sort_value_ja1,    null: true
            t.text      :sort_value_en1,    null: true
            t.text      :caption_ja1,       null: true
            t.text      :caption_en1,       null: true

            t.text      :value2,            null: false, default: ''
            t.text      :sort_value_ja2,    null: true
            t.text      :sort_value_en2,    null: true
            t.text      :caption_ja2,       null: true
            t.text      :caption_en2,       null: true

            t.text      :value3,            null: false, default: ''
            t.text      :sort_value_ja3,    null: true
            t.text      :sort_value_en3,    null: true
            t.text      :caption_ja3,       null: true
            t.text      :caption_en3,       null: true

            t.text      :value4,            null: false, default: ''
            t.text      :sort_value_ja4,    null: true
            t.text      :sort_value_en4,    null: true
            t.text      :caption_ja4,       null: true
            t.text      :caption_en4,       null: true

            t.text      :value5,            null: false, default: ''
            t.text      :sort_value_ja5,    null: true
            t.text      :sort_value_en5,    null: true
            t.text      :caption_ja5,       null: true
            t.text      :caption_en5,       null: true

            t.timestamps
        end
        add_index :pri_list_level5_words, :list_def_id
        add_index :pri_list_level5_words, [:list_def_id, :value1, :value2, :value3, :value4, :value5], unique: true, name: 'pri_list_level5_words_uniq'

        add_index :pri_list_level5_words, :value1
        add_index :pri_list_level5_words, :value2
        add_index :pri_list_level5_words, :value3
        add_index :pri_list_level5_words, :value4

        add_index :pri_list_level5_words, :sort_value_ja5
        add_index :pri_list_level5_words, :sort_value_en5



        create_table :pri_list_level4_words do |t|
            t.integer   :list_def_id,       null: false
            t.integer   :document_count,    null: false, default: 0

            t.text      :value1,            null: false, default: ''
            t.text      :sort_value_ja1,    null: true
            t.text      :sort_value_en1,    null: true
            t.text      :caption_ja1,       null: true
            t.text      :caption_en1,       null: true

            t.text      :value2,            null: false, default: ''
            t.text      :sort_value_ja2,    null: true
            t.text      :sort_value_en2,    null: true
            t.text      :caption_ja2,       null: true
            t.text      :caption_en2,       null: true

            t.text      :value3,            null: false, default: ''
            t.text      :sort_value_ja3,    null: true
            t.text      :sort_value_en3,    null: true
            t.text      :caption_ja3,       null: true
            t.text      :caption_en3,       null: true

            t.text      :value4,            null: false, default: ''
            t.text      :sort_value_ja4,    null: true
            t.text      :sort_value_en4,    null: true
            t.text      :caption_ja4,       null: true
            t.text      :caption_en4,       null: true

            t.timestamps
        end
        add_index :pri_list_level4_words, :list_def_id
        add_index :pri_list_level4_words, [:list_def_id, :value1, :value2, :value3, :value4], unique: true, name: 'pri_list_level4_words_uniq'

        add_index :pri_list_level4_words, :value1
        add_index :pri_list_level4_words, :value2
        add_index :pri_list_level4_words, :value3

        add_index :pri_list_level4_words, :sort_value_ja4
        add_index :pri_list_level4_words, :sort_value_en4




        create_table :pri_list_level3_words do |t|
            t.integer   :list_def_id,       null: false
            t.integer   :document_count,    null: false, default: 0

            t.text      :value1,            null: false, default: ''
            t.text      :sort_value_ja1,    null: true
            t.text      :sort_value_en1,    null: true
            t.text      :caption_ja1,       null: true
            t.text      :caption_en1,       null: true

            t.text      :value2,            null: false, default: ''
            t.text      :sort_value_ja2,    null: true
            t.text      :sort_value_en2,    null: true
            t.text      :caption_ja2,       null: true
            t.text      :caption_en2,       null: true

            t.text      :value3,            null: false, default: ''
            t.text      :sort_value_ja3,    null: true
            t.text      :sort_value_en3,    null: true
            t.text      :caption_ja3,       null: true
            t.text      :caption_en3,       null: true

            t.timestamps
        end
        add_index :pri_list_level3_words, :list_def_id
        add_index :pri_list_level3_words, [:list_def_id, :value1, :value2, :value3], unique: true, name: 'pri_list_level3_words_uniq'

        add_index :pri_list_level3_words, :value1
        add_index :pri_list_level3_words, :value2

        add_index :pri_list_level3_words, :sort_value_ja3
        add_index :pri_list_level3_words, :sort_value_en3




        create_table :pri_list_level2_words do |t|
            t.integer   :list_def_id,       null: false
            t.integer   :document_count,    null: false, default: 0

            t.text      :value1,            null: false, default: ''
            t.text      :sort_value_ja1,    null: true
            t.text      :sort_value_en1,    null: true
            t.text      :caption_ja1,       null: true
            t.text      :caption_en1,       null: true

            t.text      :value2,            null: false, default: ''
            t.text      :sort_value_ja2,    null: true
            t.text      :sort_value_en2,    null: true
            t.text      :caption_ja2,       null: true
            t.text      :caption_en2,       null: true

            t.timestamps
        end
        add_index :pri_list_level2_words, :list_def_id
        add_index :pri_list_level2_words, [:list_def_id, :value1, :value2], unique: true, name: 'pri_list_level2_words_uniq'

        add_index :pri_list_level2_words, :value1

        add_index :pri_list_level2_words, :sort_value_ja2
        add_index :pri_list_level2_words, :sort_value_en2




        create_table :pri_list_level1_words do |t|
            t.integer   :list_def_id,       null: false
            t.integer   :document_count,    null: false, default: 0

            t.text      :value1,            null: false, default: ''
            t.text      :sort_value_ja1,    null: true
            t.text      :sort_value_en1,    null: true
            t.text      :caption_ja1,       null: true
            t.text      :caption_en1,       null: true

            t.timestamps
        end
        add_index :pri_list_level1_words, :list_def_id
        add_index :pri_list_level1_words, [:list_def_id, :value1], unique: true, name: 'pri_list_level1_words_uniq'

        add_index :pri_list_level1_words, :sort_value_ja1
        add_index :pri_list_level1_words, :sort_value_en1




        create_table :pub_list_level5_words do |t|
            t.integer   :list_def_id,       null: false
            t.integer   :document_count,    null: false, default: 0
            t.boolean   :limited,           null: false

            t.text      :value1,            null: false, default: ''
            t.text      :sort_value_ja1,    null: true
            t.text      :sort_value_en1,    null: true
            t.text      :caption_ja1,       null: true
            t.text      :caption_en1,       null: true

            t.text      :value2,            null: false, default: ''
            t.text      :sort_value_ja2,    null: true
            t.text      :sort_value_en2,    null: true
            t.text      :caption_ja2,       null: true
            t.text      :caption_en2,       null: true

            t.text      :value3,            null: false, default: ''
            t.text      :sort_value_ja3,    null: true
            t.text      :sort_value_en3,    null: true
            t.text      :caption_ja3,       null: true
            t.text      :caption_en3,       null: true

            t.text      :value4,            null: false, default: ''
            t.text      :sort_value_ja4,    null: true
            t.text      :sort_value_en4,    null: true
            t.text      :caption_ja4,       null: true
            t.text      :caption_en4,       null: true

            t.text      :value5,            null: false, default: ''
            t.text      :sort_value_ja5,    null: true
            t.text      :sort_value_en5,    null: true
            t.text      :caption_ja5,       null: true
            t.text      :caption_en5,       null: true
            
            t.timestamps
        end
        add_index :pub_list_level5_words, :list_def_id
        add_index :pub_list_level5_words, :limited
        add_index :pub_list_level5_words, [:list_def_id, :value1, :value2, :value3, :value4, :value5, :limited], unique: true, name: 'pub_list_level5_words_uniq'

        add_index :pub_list_level5_words, :value1
        add_index :pub_list_level5_words, :value2
        add_index :pub_list_level5_words, :value3
        add_index :pub_list_level5_words, :value4

        add_index :pub_list_level5_words, :sort_value_ja5
        add_index :pub_list_level5_words, :sort_value_en5




        create_table :pub_list_level4_words do |t|
            t.integer   :list_def_id,       null: false
            t.integer   :document_count,    null: false, default: 0
            t.boolean   :limited,           null: false

            t.text      :value1,            null: false, default: ''
            t.text      :sort_value_ja1,    null: true
            t.text      :sort_value_en1,    null: true
            t.text      :caption_ja1,       null: true
            t.text      :caption_en1,       null: true

            t.text      :value2,            null: false, default: ''
            t.text      :sort_value_ja2,    null: true
            t.text      :sort_value_en2,    null: true
            t.text      :caption_ja2,       null: true
            t.text      :caption_en2,       null: true

            t.text      :value3,            null: false, default: ''
            t.text      :sort_value_ja3,    null: true
            t.text      :sort_value_en3,    null: true
            t.text      :caption_ja3,       null: true
            t.text      :caption_en3,       null: true

            t.text      :value4,            null: false, default: ''
            t.text      :sort_value_ja4,    null: true
            t.text      :sort_value_en4,    null: true
            t.text      :caption_ja4,       null: true
            t.text      :caption_en4,       null: true
            
            t.timestamps
        end
        add_index :pub_list_level4_words, :list_def_id
        add_index :pub_list_level4_words, :limited
        add_index :pub_list_level4_words, [:list_def_id, :value1, :value2, :value3, :value4, :limited], unique: true, name: 'pub_list_level4_words_uniq'

        add_index :pub_list_level4_words, :value1
        add_index :pub_list_level4_words, :value2
        add_index :pub_list_level4_words, :value3

        add_index :pub_list_level4_words, :sort_value_ja4
        add_index :pub_list_level4_words, :sort_value_en4




        create_table :pub_list_level3_words do |t|
            t.integer   :list_def_id,       null: false
            t.integer   :document_count,    null: false, default: 0
            t.boolean   :limited,           null: false

            t.text      :value1,            null: false, default: ''
            t.text      :sort_value_ja1,    null: true
            t.text      :sort_value_en1,    null: true
            t.text      :caption_ja1,       null: true
            t.text      :caption_en1,       null: true

            t.text      :value2,            null: false, default: ''
            t.text      :sort_value_ja2,    null: true
            t.text      :sort_value_en2,    null: true
            t.text      :caption_ja2,       null: true
            t.text      :caption_en2,       null: true

            t.text      :value3,            null: false, default: ''
            t.text      :sort_value_ja3,    null: true
            t.text      :sort_value_en3,    null: true
            t.text      :caption_ja3,       null: true
            t.text      :caption_en3,       null: true

            t.timestamps
        end
        add_index :pub_list_level3_words, :list_def_id
        add_index :pub_list_level3_words, :limited
        add_index :pub_list_level3_words, [:list_def_id, :value1, :value2, :value3, :limited], unique: true, name: 'pub_list_level3_words_uniq'

        add_index :pub_list_level3_words, :value1
        add_index :pub_list_level3_words, :value2

        add_index :pub_list_level3_words, :sort_value_ja3
        add_index :pub_list_level3_words, :sort_value_en3




        create_table :pub_list_level2_words do |t|
            t.integer   :list_def_id,       null: false
            t.integer   :document_count,    null: false, default: 0
            t.boolean   :limited,           null: false

            t.text      :value1,            null: false, default: ''
            t.text      :sort_value_ja1,    null: true
            t.text      :sort_value_en1,    null: true
            t.text      :caption_ja1,       null: true
            t.text      :caption_en1,       null: true

            t.text      :value2,            null: false, default: ''
            t.text      :sort_value_ja2,    null: true
            t.text      :sort_value_en2,    null: true
            t.text      :caption_ja2,       null: true
            t.text      :caption_en2,       null: true

            t.timestamps
        end
        add_index :pub_list_level2_words, :list_def_id
        add_index :pub_list_level2_words, :limited
        add_index :pub_list_level2_words, [:list_def_id, :value1, :value2, :limited], unique: true, name: 'pub_list_level2_words_uniq'

        add_index :pub_list_level2_words, :value1

        add_index :pub_list_level2_words, :sort_value_ja2
        add_index :pub_list_level2_words, :sort_value_en2




        create_table :pub_list_level1_words do |t|
            t.integer   :list_def_id,       null: false
            t.integer   :document_count,    null: false, default: 0
            t.boolean   :limited,           null: false

            t.text      :value1,            null: false, default: ''
            t.text      :sort_value_ja1,    null: true
            t.text      :sort_value_en1,    null: true
            t.text      :caption_ja1,       null: true
            t.text      :caption_en1,       null: true
            
            t.timestamps
        end
        add_index :pub_list_level1_words, :list_def_id
        add_index :pub_list_level1_words, :limited
        add_index :pub_list_level1_words, [:list_def_id, :value1, :limited], unique: true, name: 'pub_list_level1_words_uniq'

        add_index :pub_list_level1_words, :sort_value_ja1
        add_index :pub_list_level1_words, :sort_value_en1





        reversible do |dir|
            dir.up do
                SystemSetting.where(data_lock: false, mentenance_mode: false).update_all(mentenance_mode: true)
                SystemSetting.where(mentenance_mode: true).update_all(list_locked_at: Time.zone.now)
            end
        end

    end
end
