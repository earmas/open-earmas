class CreateCustomSelectValues < ActiveRecord::Migration
    def change
        create_table :custom_selects do |t|
            t.string :name,     null: false

            t.timestamps
        end

        create_table :custom_select_values do |t|
            t.integer  :custom_select_id,   null: false

            t.string   :value,              null: false
            t.text     :caption_ja,         null: false
            t.text     :caption_en,         null: false

            t.integer  :sort_index,         null: false, default: 0

            t.timestamps
        end
        add_index :custom_select_values, :custom_select_id
        add_index :custom_select_values, :value
        add_index :custom_select_values, :sort_index, unique: true
        add_index :custom_select_values, [:custom_select_id, :value], unique: true
    end
end
