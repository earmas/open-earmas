class AddLeftMenuLink < ActiveRecord::Migration
    def change
        add_column :left_menu_defs, :value,       :text, null: false, default: ''
    end
end
