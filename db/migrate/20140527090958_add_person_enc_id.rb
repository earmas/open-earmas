class AddPersonEncId < ActiveRecord::Migration

    def change
        add_column :people, :encrypted_id, :string
        add_index :people, [:encrypted_id], unique: true

        reversible do |dir|
            dir.up do
                people_setting = PeopleSetting.all.first
                if people_setting != nil 
                    Person.all.each do |person|
                        person.encrypted_id = people_setting.encrypt_data(person.id)
                    end
                end
            end
        end
    end
end
