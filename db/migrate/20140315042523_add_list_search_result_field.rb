class AddListSearchResultField < ActiveRecord::Migration
    def change
        create_table :list_result_fields do |t|
            t.integer   :list_def_id,   null: false
            t.integer   :field_def_id,  null: false

            t.integer   :sort_index,    null: false, default: 0

            t.timestamps
        end
        add_index :list_result_fields, :list_def_id
        add_index :list_result_fields, [:list_def_id, :sort_index], unique: true
        add_index :list_result_fields, [:list_def_id, :field_def_id], unique: true


        create_table :search_result_fields do |t|
            t.integer   :field_def_id,  null: false

            t.integer   :sort_index,    null: false, default: 0

            t.timestamps
        end
        add_index :search_result_fields, [:sort_index], unique: true
        add_index :search_result_fields, [:field_def_id], unique: true
    end
end
