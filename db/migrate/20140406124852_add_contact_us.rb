class AddContactUs < ActiveRecord::Migration
    def change
        create_table :contact_us_settings do |t|
            t.text     :caption_ja,         null: false
            t.text     :caption_en,         null: false

            t.text     :from_address,       null: false
            t.text     :to_address,         null: false
            t.text     :subject,            null: false

            t.text     :html_set_def_id,    null: true

            t.timestamps
        end
    end
end
