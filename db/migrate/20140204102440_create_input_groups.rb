class CreateInputGroups < ActiveRecord::Migration
    def change
        create_table :input_groups do |t|
            t.string  :name,            null: false
            t.integer :sort_index,      null: false, default: 0

            t.timestamps
        end
        add_index :input_groups, :sort_index, unique: true

        create_table :input_group_fields do |t|
            t.integer   :input_group_id,    null: false
            t.integer   :field_def_id,      null: false

            t.boolean   :must,              null: false, default: false
            t.integer   :sort_index,        null: false, default: 0

            t.timestamps
        end
        add_index :input_group_fields, :input_group_id
        add_index :input_group_fields, [:input_group_id, :sort_index], unique: true
        add_index :input_group_fields, [:input_group_id, :field_def_id], unique: true, name: :input_group_fields_unique_first
    end
end
