class ChangeViewHtmlSet < ActiveRecord::Migration
    def change
        add_column :input_groups, :detail_view_type_id,            :integer
        add_column :input_groups, :detail_sub_header_view_type_id, :integer
        add_column :input_groups, :detail_sub_footer_view_type_id, :integer
        add_column :input_groups, :detail_html_set_def_id,         :integer

        rename_column   :search_settings,    :detail_html_set_def_id, :detail_html_set_def_id_old
        add_column      :search_settings,    :detail_html_set_def_id, :integer

        rename_column   :search_settings,    :result_html_set_def_id, :result_html_set_def_id_old
        add_column      :search_settings,    :result_html_set_def_id, :integer

        rename_column   :list_defs,    :detail_html_set_def_id, :detail_html_set_def_id_old
        add_column      :list_defs,    :detail_html_set_def_id, :integer

        rename_column   :list_defs,    :result_html_set_def_id, :result_html_set_def_id_old
        add_column      :list_defs,    :result_html_set_def_id, :integer

        rename_column   :system_settings,    :detail_html_set_def_id, :detail_html_set_def_id_old
        add_column      :system_settings,    :detail_html_set_def_id, :integer

        reversible do |dir|
            dir.up do
                InputGroup.update_all(detail_view_type_id: 1)
                InputGroup.update_all(detail_sub_header_view_type_id: 1)
                InputGroup.update_all(detail_sub_footer_view_type_id: 1)

                change_column :input_groups,  :detail_view_type_id, :integer, null: false
                change_column :input_groups,  :detail_sub_header_view_type_id, :integer, null: false
                change_column :input_groups,  :detail_sub_footer_view_type_id, :integer, null: false


                SearchSetting.update_all('detail_html_set_def_id = CAST (detail_html_set_def_id_old AS INTEGER)')
                SearchSetting.update_all('result_html_set_def_id = CAST (result_html_set_def_id_old AS INTEGER)')
                remove_column :search_settings,  :detail_html_set_def_id_old
                remove_column :search_settings,  :result_html_set_def_id_old

                ListDef.update_all('detail_html_set_def_id = CAST (detail_html_set_def_id_old AS INTEGER)')
                ListDef.update_all('result_html_set_def_id = CAST (result_html_set_def_id_old AS INTEGER)')
                remove_column :list_defs,  :detail_html_set_def_id_old
                remove_column :list_defs,  :result_html_set_def_id_old

                SystemSetting.update_all('detail_html_set_def_id = CAST (detail_html_set_def_id_old AS INTEGER)')
                remove_column :system_settings,  :detail_html_set_def_id_old
            end
        end

    end
end


