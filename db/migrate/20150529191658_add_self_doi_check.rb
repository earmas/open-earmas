class AddSelfDoiCheck < ActiveRecord::Migration
    def change

        add_column :pri_documents, :self_doi_type,          :integer, null: false, default: 0
        add_column :pri_documents, :self_doi_checked,       :boolean, null: false, default: true
        add_column :pri_documents, :self_doi,               :string
        add_column :pri_documents, :self_doi_jalc_manu_input,      :string, default: ''
        add_column :pri_documents, :self_doi_jalc_auto_input,      :string, default: ''
        add_column :pri_documents, :self_doi_crossref_auto_input,  :string, default: ''

        add_index  :pri_documents, :self_doi_type



        add_column :pub_documents, :self_doi_type,      :integer, null: false, default: 0
        add_column :pub_documents, :self_doi,           :string

        add_index  :pub_documents, :self_doi_type


        add_column :oaipmh_settings, :enable_self_jalc_doi,     :boolean, null: false, default: false
        add_column :oaipmh_settings, :enable_self_crossref_doi, :boolean, null: false, default: false

        add_column :oaipmh_settings, :title_field_def_id,      :integer
        add_column :oaipmh_settings, :creator_field_def_id,    :integer
        add_column :oaipmh_settings, :publisher_field_def_id,  :integer
        add_column :oaipmh_settings, :issn_field_def_id,       :integer
        add_column :oaipmh_settings, :isbn_field_def_id,       :integer
        add_column :oaipmh_settings, :jtitle_field_def_id,     :integer
        add_column :oaipmh_settings, :start_page_field_def_id, :integer

        add_column :oaipmh_settings, :nii_type_field_def_id,   :integer

        add_column :oaipmh_settings, :self_doi_jalc_prefix,        :string
        add_column :oaipmh_settings, :self_doi_crossref_prefix,    :string
    end
end
