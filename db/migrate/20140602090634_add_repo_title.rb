class AddRepoTitle < ActiveRecord::Migration
    def change
        create_table :pri_field_repository_title_data do |t|
            t.integer   :document_id,       null: false
            t.integer   :field_def_id,      null: false
            t.integer   :sort_index,        null: false, default: 0

            t.string    :value,             null: true
            t.text      :caption_ja,        null: false
            t.text      :caption_en,        null: false

            t.timestamps
        end
        add_index :pri_field_repository_title_data, :document_id
        add_index :pri_field_repository_title_data, :field_def_id
        add_index :pri_field_repository_title_data, :sort_index


        create_table :pub_field_repository_title_data, id: false do |t|
            t.integer   :id,                null: false
            t.integer   :document_id,       null: false
            t.integer   :field_def_id,      null: false
            t.integer   :sort_index,        null: false, default: 0

            t.string    :value,             null: true
            t.text      :caption_ja,        null: false
            t.text      :caption_en,        null: false

            t.timestamps
        end
        add_index :pub_field_repository_title_data, :document_id
        add_index :pub_field_repository_title_data, :field_def_id
        add_index :pub_field_repository_title_data, :sort_index
        reversible do |dir|
            dir.up do
                execute "ALTER TABLE pub_field_repository_title_data ADD PRIMARY KEY (id);"
            end
        end

        reversible do |dir|
            dir.up do
                change_column :pri_field_repository_person_data, :person_id, :string, null: false
                change_column :pub_field_repository_person_data, :person_id, :string, null: false
            end
        end

    end
end
