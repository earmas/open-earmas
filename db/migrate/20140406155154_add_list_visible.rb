class AddListVisible < ActiveRecord::Migration
    def change
        add_column :list_defs, :visible, :integer
        add_column :static_page_defs, :visible, :integer

        reversible do |dir|
            dir.up do
                ListDef.update_all('visible = 2')
                StaticPageDef.update_all('visible = 2')

                change_column :list_defs,        :visible,  :integer, null: false, default: 2
                change_column :static_page_defs, :visible,  :integer, null: false, default: 2
            end
        end
    end
end
