class AddJournalPage < ActiveRecord::Migration
    def change
        add_column :ej_defs, :start_page_field_def_id, :integer
        add_column :ej_defs, :end_page_field_def_id, :integer

        add_column :ej_static_page_defs, :body_main_html_string_ja, :text
        add_column :ej_static_page_defs, :body_main_html_string_en, :text

        add_column :static_page_defs, :body_main_html_string_ja, :text
        add_column :static_page_defs, :body_main_html_string_en, :text
    end
end
