class ChangeSetSpecFieldDefIdName < ActiveRecord::Migration
    def change
        rename_column   :oaipmh_settings,    :set_spec_field_def_id, :set_spec_custom_select_id
    end
end
