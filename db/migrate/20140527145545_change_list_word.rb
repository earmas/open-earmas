class ChangeListWord < ActiveRecord::Migration
    def change
        drop_table :pri_list_words
        drop_table :pub_list_words

        add_column :pri_list_data, :list_word_ids, :integer, array: true
        add_column :pub_list_data, :list_word_ids, :integer, array: true


        create_table :pri_list_words do |t|
            t.integer   :list_def_id,        null: false
            t.integer   :document_count,     null: false, default: 0

            t.text      :param_value1,       null: false, default: ''
            t.text      :sort_value_ja1,     null: true
            t.text      :sort_value_en1,     null: true
            t.text      :caption_ja1,        null: true
            t.text      :caption_en1,        null: true

            t.text      :param_value2,       null: false, default: ''
            t.text      :sort_value_ja2,     null: true
            t.text      :sort_value_en2,     null: true
            t.text      :caption_ja2,        null: true
            t.text      :caption_en2,        null: true

            t.text      :param_value3,       null: false, default: ''
            t.text      :sort_value_ja3,     null: true
            t.text      :sort_value_en3,     null: true
            t.text      :caption_ja3,        null: true
            t.text      :caption_en3,        null: true

            t.text      :param_value4,       null: false, default: ''
            t.text      :sort_value_ja4,     null: true
            t.text      :sort_value_en4,     null: true
            t.text      :caption_ja4,        null: true
            t.text      :caption_en4,        null: true

            t.text      :param_value5,       null: false, default: ''
            t.text      :sort_value_ja5,     null: true
            t.text      :sort_value_en5,     null: true
            t.text      :caption_ja5,        null: true
            t.text      :caption_en5,        null: true

            t.timestamps
        end
        add_index :pri_list_words, :list_def_id
        add_index :pri_list_words, [:list_def_id, :param_value1, :param_value2, :param_value3, :param_value4, :param_value5], unique: true, name: 'pri_list_words_params'

        add_index :pri_list_words, [:sort_value_ja1, :param_value1, :sort_value_en1, :caption_ja1, :caption_en1], name: 'pri_list_words_level1_ja'
        add_index :pri_list_words, [:sort_value_en1, :param_value1, :sort_value_ja1, :caption_ja1, :caption_en1], name: 'pri_list_words_level1_en'

        add_index :pri_list_words, [:sort_value_ja2, :param_value2, :sort_value_en2, :caption_ja2, :caption_en2], name: 'pri_list_words_level2_ja'
        add_index :pri_list_words, [:sort_value_en2, :param_value2, :sort_value_ja2, :caption_ja2, :caption_en2], name: 'pri_list_words_level2_en'

        add_index :pri_list_words, [:sort_value_ja3, :param_value3, :sort_value_en3, :caption_ja3, :caption_en3], name: 'pri_list_words_level3_ja'
        add_index :pri_list_words, [:sort_value_en3, :param_value3, :sort_value_ja3, :caption_ja3, :caption_en3], name: 'pri_list_words_level3_en'

        add_index :pri_list_words, [:sort_value_ja4, :param_value4, :sort_value_en4, :caption_ja4, :caption_en4], name: 'pri_list_words_level4_ja'
        add_index :pri_list_words, [:sort_value_en4, :param_value4, :sort_value_ja4, :caption_ja4, :caption_en4], name: 'pri_list_words_level4_en'

        add_index :pri_list_words, [:sort_value_ja5, :param_value5, :sort_value_en5, :caption_ja5, :caption_en5], name: 'pri_list_words_level5_ja'
        add_index :pri_list_words, [:sort_value_en5, :param_value5, :sort_value_ja5, :caption_ja5, :caption_en5], name: 'pri_list_words_level5_en'

        add_index :pri_list_words, :param_value1
        add_index :pri_list_words, :param_value2
        add_index :pri_list_words, :param_value3
        add_index :pri_list_words, :param_value4
        add_index :pri_list_words, :param_value5




        create_table :pub_list_words do |t|
            t.integer   :list_def_id,        null: false
            t.integer   :document_count,     null: false, default: 0
            t.boolean   :limited,            null: false

            t.text      :param_value1,       null: false, default: ''
            t.text      :sort_value_ja1,     null: true
            t.text      :sort_value_en1,     null: true
            t.text      :caption_ja1,        null: true
            t.text      :caption_en1,        null: true

            t.text      :param_value2,       null: false, default: ''
            t.text      :sort_value_ja2,     null: true
            t.text      :sort_value_en2,     null: true
            t.text      :caption_ja2,        null: true
            t.text      :caption_en2,        null: true

            t.text      :param_value3,       null: false, default: ''
            t.text      :sort_value_ja3,     null: true
            t.text      :sort_value_en3,     null: true
            t.text      :caption_ja3,        null: true
            t.text      :caption_en3,        null: true

            t.text      :param_value4,       null: false, default: ''
            t.text      :sort_value_ja4,     null: true
            t.text      :sort_value_en4,     null: true
            t.text      :caption_ja4,        null: true
            t.text      :caption_en4,        null: true

            t.text      :param_value5,       null: false, default: ''
            t.text      :sort_value_ja5,     null: true
            t.text      :sort_value_en5,     null: true
            t.text      :caption_ja5,        null: true
            t.text      :caption_en5,        null: true
            
            t.timestamps
        end
        add_index :pub_list_words, :list_def_id
        add_index :pub_list_words, :limited
        add_index :pub_list_words, [:list_def_id, :param_value1, :param_value2, :param_value3, :param_value4, :param_value5, :limited], unique: true, name: 'pub_list_words_params'

        add_index :pub_list_words, [:sort_value_ja1, :param_value1, :sort_value_en1, :caption_ja1, :caption_en1], name: 'pub_list_words_level1_ja'
        add_index :pub_list_words, [:sort_value_en1, :param_value1, :sort_value_ja1, :caption_ja1, :caption_en1], name: 'pub_list_words_level1_en'

        add_index :pub_list_words, [:sort_value_ja2, :param_value2, :sort_value_en2, :caption_ja2, :caption_en2], name: 'pub_list_words_level2_ja'
        add_index :pub_list_words, [:sort_value_en2, :param_value2, :sort_value_ja2, :caption_ja2, :caption_en2], name: 'pub_list_words_level2_en'

        add_index :pub_list_words, [:sort_value_ja3, :param_value3, :sort_value_en3, :caption_ja3, :caption_en3], name: 'pub_list_words_level3_ja'
        add_index :pub_list_words, [:sort_value_en3, :param_value3, :sort_value_ja3, :caption_ja3, :caption_en3], name: 'pub_list_words_level3_en'

        add_index :pub_list_words, [:sort_value_ja4, :param_value4, :sort_value_en4, :caption_ja4, :caption_en4], name: 'pub_list_words_level4_ja'
        add_index :pub_list_words, [:sort_value_en4, :param_value4, :sort_value_ja4, :caption_ja4, :caption_en4], name: 'pub_list_words_level4_en'

        add_index :pub_list_words, [:sort_value_ja5, :param_value5, :sort_value_en5, :caption_ja5, :caption_en5], name: 'pub_list_words_level5_ja'
        add_index :pub_list_words, [:sort_value_en5, :param_value5, :sort_value_ja5, :caption_ja5, :caption_en5], name: 'pub_list_words_level5_en'

        add_index :pub_list_words, :param_value1
        add_index :pub_list_words, :param_value2
        add_index :pub_list_words, :param_value3
        add_index :pub_list_words, :param_value4
        add_index :pub_list_words, :param_value5

        reversible do |dir|
            dir.up do
                SystemSetting.where(data_lock: false, mentenance_mode: false).update_all(mentenance_mode: true)
                SystemSetting.where(mentenance_mode: true).update_all(list_locked_at: Time.zone.now)
            end
        end
    end
end
