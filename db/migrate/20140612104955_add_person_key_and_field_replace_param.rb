class AddPersonKeyAndFieldReplaceParam < ActiveRecord::Migration
    def change
        add_column :people, :key6, :string
        add_column :people, :key7, :string
        add_column :people, :key8, :string
        add_column :people, :key9, :string
        add_column :people, :key10, :string


        add_column :people_settings, :key6_name, :string
        add_column :people_settings, :key6_param, :text

        add_column :people_settings, :key7_name, :string
        add_column :people_settings, :key7_param, :text

        add_column :people_settings, :key8_name, :string
        add_column :people_settings, :key8_param, :text

        add_column :people_settings, :key9_name, :string
        add_column :people_settings, :key9_param, :text

        add_column :people_settings, :key10_name, :string
        add_column :people_settings, :key10_param, :text

    end
end
