class AddFieldDefMultiDelim < ActiveRecord::Migration
    def change
        add_column :field_defs, :multi_field_delim, :string


        reversible do |dir|
            dir.up do
                FieldDef.update_all("multi_field_delim = ' '")

                change_column :field_defs, :multi_field_delim, :string, null: false
            end
        end

    end

end


