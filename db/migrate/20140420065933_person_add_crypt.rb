class PersonAddCrypt < ActiveRecord::Migration
    def change
        add_column :people_settings, :cipher_iv, :string
        add_column :people_settings, :cipher_key, :string
        add_column :people_settings, :cipher_type, :string
    end
end
